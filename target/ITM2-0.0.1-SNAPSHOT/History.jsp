<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM2/index.jsp");
		}
	}
%>
<html>
<head>
<LINK REL=StyleSheet HREF="css/stickyhead.css" TYPE="text/css"></LINK>
<%
	String username=null;
	String strdb = request.getParameter("database");
	session = request.getSession(false);
	if(session != null){
		username = (String)session.getAttribute("username");
	}
	
	ResultSet rs_problem = null,rs_idea=null, rs_more=null,rs_time=null;
	Operatedb opdb_problem=null, opdb_idea=null,opdb_more=null,opdb=null;
	Statement stmt=null;
	sqls s=new sqls();
	ResultSet temprs=null;
	String strname = request.getParameter("projectname");
	String strfocus = request.getParameter("threadfocus");
	if(strdb != null){
		opdb_problem = new Operatedb(new sqls(),strdb);
		opdb_idea = new Operatedb(new sqls(),strdb);
		opdb_more = new Operatedb(new sqls(),strdb);
		opdb = new Operatedb(new sqls(),strdb);
		stmt= s.Connect(strdb);
		//get the project id
		temprs=opdb.GetRecordsFromDB("project","idproject","projectname='"+strname+"';");
	}
	
	String strconstrain;
	String strcontent=null;
	String strid=null;		
	int number=0;
	
	if(temprs != null &&temprs.next()){
		strid = temprs.getString(1);
	}
	
	if(temprs != null){
		try{
			temprs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	if(opdb != null){
		opdb.CloseCon();
	}
	
	//read the create time from the table
	String strsql="select createtime,author_problem from thread_problem where projectid="+strid+" and threadfocus_problem='"
	               +strfocus+"' order by createtime";
	rs_time = stmt.executeQuery(strsql);
%>
<script type="text/javascript">
	function ShowContent(a){
		document.content_form.Content.value = a;
	}
	function OnClose(){
		window.close();
	}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Summary History</title>
</head>
<body>
<div id="sticky">
	<div id="header">
	<table border="0">
		<tbody><tr>
			<td><img src="img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="60" width="850">
			Changes Made in the Summary of Idea thread: <%=request.getParameter("threadfocus")%>
			</td>
		</tr>
	</tbody></table>			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
<div style="float:left;width: 300px;">
<font  face="verdana">Please select a version:</font><br>
<form name="history_form" action="/ITM2/History.jsp?projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus") %>" method="post">
   <div style="position:relative;height:500px; overflow:scroll;">
    <% 
	while(rs_time.next()){
		number++;
		//read the problem history from the database
		strconstrain = "threadfocus_problem = '"+strfocus+"' AND projectid="+strid+" and createtime='"+rs_time.getString(1)+"';";
		rs_problem = opdb_problem.GetRecordsFromDB("thread_problem","problem",strconstrain);
		
		//read the idea history from database
		strconstrain = "threadfocus_idea = '" +strfocus+"' AND projectid="+strid+" and createtime='"+rs_time.getString(1)+"';";
		rs_idea = opdb_idea.GetRecordsFromDB("thread_idea","idea",strconstrain);
		
		//read the more history from database
		strconstrain = "threadfocus_more = '" +strfocus+"' AND projectid="+strid+" and createtime='"+rs_time.getString(1)+"';";
		rs_more = opdb_more.GetRecordsFromDB("thread_more","more",strconstrain);
		
		if(rs_problem.next()&rs_idea.next()&rs_more.next()){
			strcontent = "Our Problems:\n"+rs_problem.getString(1)+"\n\n"+"Big ideas we've learned \n"+rs_idea.getString(1)+"\n\n"+"We need to do more\n"+rs_more.getString(1);
			//replace the double quote
			strcontent = strcontent.replaceAll("\"","&#34;");
		}	
	%>
		
		<table>	
		<tr>
			
			<td style= "cursor:pointer">
			<div onclick="javascript:var a = document.history_form.<%="pre"+number%>.value;ShowContent(a);"">
			<font color="#1ABDE6"><U><%=rs_time.getString(1)%>by<%=rs_time.getString(2)%></U></font>
			<input type="hidden" name="<%="pre"+number%>" value="<%=strcontent%>"/> 
			</div>
			</td>
				
		</tr>		
		</table>
	
	<%}%>
	</div>
	</form>
</div>
<div style="float:left;width: 600px;">
<form name="content_form">
	<textarea name="Content" rows="30" cols="50" readonly='readonly'></textarea>
	</form>
</div>

<center><input type="button"  name="close" value="Close" onclick="OnClose()"/></center>

</div>

	
	
	</div>
	
</body>
</html>
<%	
	if(rs_time != null){
		try{
			rs_time.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	if(rs_problem != null){
		try{
			rs_problem.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	if(rs_idea != null){
		try{
			rs_idea.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	if(rs_more != null){
		try{
			rs_more.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	if(stmt != null){
		try{
			stmt.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	if(opdb_problem != null){
		opdb_problem.CloseCon();
	}
	if(opdb_more != null){
		opdb_more.CloseCon();
	}
	if(opdb_idea != null){
		opdb_idea.CloseCon();
	}
	if(s != null){
		s.Close();
	}
%>