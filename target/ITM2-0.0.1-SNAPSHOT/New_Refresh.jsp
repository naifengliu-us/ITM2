<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM2/index.jsp");
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Refresh Database</title>
<script type="text/javascript">
function OnCancel(){
	window.location= "/ITM2/New_HomePage.jsp?database=<%=request.getParameter("database")%>";
}
</script>
</head>

<%
/*********************read the database refresh time***********************************/
	String database = request.getParameter("database");
	String strrefresh = null;
	String strcon = "dbname='"+database+"'";
	ResultSet rs = null;
	Operatedb opdb_refresh = new Operatedb(new sqls(),"itm");
	try{
		rs = opdb_refresh.GetRecordsFromDB("db_table","refreshtime",strcon);
		if(rs.next()){
		strrefresh= rs.getString(1);		
		}
		opdb_refresh.CloseCon();
	}catch(SQLException e){
		e.printStackTrace();
	}
%>
<body>
	The database was updated at <%=strrefresh %> 
	<br/>
	Do you want to update the database data now? It may take few minutes....
	<br/>
	<form name="refresh_from" action="/ITM2/RefreshDB?database=<%=database%>&projectname=<%=request.getParameter("projectname")%>" method="post">
	<input type="submit" value="Update"/>
	<input type="button" value="Cancel" onclick="OnCancel()"/>
	</form>
</body>
</html>