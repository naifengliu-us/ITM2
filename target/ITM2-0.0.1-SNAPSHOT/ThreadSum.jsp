<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*,java.text.SimpleDateFormat;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
String user =" ";
String usertype =" ";
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM2/index.jsp");
		}
		else
		{
		    user = (String)session.getAttribute("username");
			usertype = (String)session.getAttribute("usertype");
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Journey of Thinking</title>
<style type="text/css">
.inButtonColor {
 background-image: url(images/button.jpg);
 width:197px;
 height:72px;}
.divinput{
margin: 0px 0px 0px 10px;
width: 300px;
float: left;
}

</style>

<script type="text/javascript" >
	function OnHistory(){
		window.open ('History.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus")%>'); 
	}
	
	function OnClose(){
		window.close();
	}
	
	function OnTag1(){
		document.summary_form.ProblemContent.value +="\n\n[ We want to understand:]";
	}
	
	function OnTag2(){
		document.summary_form.IdeaContent.value +="\n\n[ We used to think:]";
	}
	
	function OnTag3(){
		document.summary_form.IdeaContent.value +="\n\n[ We now understand:]";
	}
		
	function OnTag4(){
			document.summary_form.MoreContent.value +="\n\n[ We need to further understand ]";
	}
	
	function OnTag5(){
		document.summary_form.MoreContent.value +="\n\n[ We need better theories about ]";
	}
	
	function OnTag6(){
		document.summary_form.MoreContent.value +="\n\n[ We need to read more about ]";
	}
	function OnTag7(){
		document.summary_form.MoreContent.value +="\n\n[ We need evidence about ]";
	}
	function OnTag8(){
		document.summary_form.MoreContent.value +="\n\n[ We need to look at our different ideas about ]";
	}
	
	/*Control journey  updates or edit*/
  function UpdateThreadControl(caneditjourney)
  {
  	var selproblem = document.getElementById("problemcontent");
  	var selidea = document.getElementById("ideacontent");
  	var selmore = document.getElementById("morecontent");
  	var sel = document.getElementById("save");
  	if (caneditjourney == "false")
  	{
  		console.log("[DEBUG] User does not has edit permissions");
  		
  		// Disable edit functionality
  		selproblem.readOnly = true;
  		selidea.readOnly = true;
  		selmore.readOnly = true;
  		sel.removeAttribute("href");
  		sel.removeAttribute("onclick");
  		sel.setAttribute("id", "save");
  		sel.style.color  = "gray";
  	}
  }
  
  function checkIfReadOnly()
  {
    var selproblem = document.getElementById("problemcontent");
  	var selidea = document.getElementById("ideacontent");
  	var selmore = document.getElementById("morecontent");
  	if(selproblem.readOnly || selidea.readOnly || selmore.readOnly)
  	  {
  	    alert("You do not have the right to edit this text");
  	  }
  }
</script>
<LINK REL=StyleSheet HREF="css/stickyhead.css" TYPE="text/css"></LINK>
</head>

	
<% 
	String strname = request.getParameter("projectname");
	String strfocus = request.getParameter("threadfocus");
	String strdb = request.getParameter("database");
	
	Operatedb opdb_author = new Operatedb(new sqls(),strdb);
	AccessToProjects atp = new AccessToProjects();
	
	boolean caneditjourney = atp.canEdit(user,usertype,strname,strdb);
	System.out.println("CanEditJourney is "+ caneditjourney);
	
	String strProblem = null;
	String strIdea = null;
	String strMore = null;
	String strsql;
	String strproject;
	Operatedb opdb=null;
	sqls s=new sqls();
	Statement stmt= s.Connect(strdb);
	
	if(request.getParameter("save")!=null && caneditjourney)
	{
		String strcontent = request.getParameter("ProblemContent");
		String ideacon = request.getParameter("IdeaContent");
		String morecon = request.getParameter("MoreContent");
		strcontent = strcontent.replace("'","''").replace("\\","\\\\");
		ideacon = ideacon.replace("'","''").replace("\\","\\\\");
		morecon = morecon.replace("'","''").replace("\\","\\\\");
		
		//store the three text sections into database
		String strtime;
	    java.util.Date curdate = new java.util.Date();
		opdb = new Operatedb(s,strdb);
		SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		//get the projectid
		ResultSet temprs=null;
		temprs=opdb.GetRecordsFromDB("project","idproject","projectname='"+strname+"';");//get the project id
		temprs.next();
		strproject = temprs.getString(1);
		if(temprs !=null){
			try{
				temprs.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
		//get current time
		strtime = spdtformat.format(curdate);
		
		//get the author
		String username=null;
		session = request.getSession(false);
		if(session != null){
			username = (String)session.getAttribute("username");
		}
		
		//store the problem into database
		strsql = " (`threadfocus_problem`, `createtime`,`problem`,projectid,author_problem) VALUES ( '"+strfocus+"','" + strtime+"','" + strcontent+"',"+strproject+",'"+username+"');"; 
		opdb.WritebacktoDB("thread_problem","INSERT",strsql);
		System.out.println("write into thread_problem table is :"+strsql);
		
		//store the idea  into database
		strsql = " (`threadfocus_idea`, `createtime`,`idea`,projectid,author_idea) VALUES ( '"+strfocus+"','" + strtime+"','" + ideacon+"',"+strproject+",'"+username+"');";  
		opdb.WritebacktoDB("thread_idea","INSERT",strsql);
		
		//store the "more" content into database
		strsql = " (`threadfocus_more`, `createtime`,`more`,projectid,author_more) VALUES ( '"+strfocus+"','" + strtime+"','" + morecon+"',"+strproject+",'"+username+"');"; 
		opdb.WritebacktoDB("thread_more","INSERT",strsql);
	}	
%>

<%
	//Get the latest problem from  database
	ResultSet rs_problem = null;
	strsql = "select problem from thread_problem, project WHERE threadfocus_problem='"
	                +strfocus+"' and thread_problem.projectid = project.idProject and project.projectname='"
	                +strname+"' order by createtime desc limit 1";
	rs_problem = stmt.executeQuery(strsql);
	if(rs_problem.next()){
		strProblem = rs_problem.getString(1);
		//System.out.println("rs_problem.getString(1) is : "+rs_problem.getString(1));
	}
	if(rs_problem != null){
		try{
			rs_problem.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	//Get the latest idea from  database
	ResultSet rs_idea = null;
	strsql = "select idea from thread_idea, project WHERE threadfocus_idea='"
	                +strfocus+"' and thread_idea.projectid = project.idProject and project.projectname='"
	                +strname+"' order by createtime desc limit 1";
	rs_idea = stmt.executeQuery(strsql);
	if(rs_idea.next()){
		strIdea = rs_idea.getString(1);
	}
	if(rs_idea != null){
		try{
			rs_idea.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//Get the latest "more" from  database
	ResultSet rs_more = null;
	strsql = "select more from thread_more, project WHERE threadfocus_more='"
	                +strfocus+"' and thread_more.projectid = project.idProject and project.projectname='"
	                +strname+"' order by createtime desc limit 1";
	rs_more = stmt.executeQuery(strsql);
	if(rs_more.next()){
		strMore = rs_more.getString(1);
	}
	if(rs_more != null){
		try{
			rs_more.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
%>
<body onload="UpdateThreadControl('<%= caneditjourney%>');">
<div id="sticky">
	<div id="header">
	<table border="0">
		<tbody><tr>
			<td><img src="img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="60" width="850">
			&nbsp;&nbsp;Journey of Thinking in Idea Thread: <%=strfocus%>
			</td>
		</tr>
	</tbody></table>			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
<div>
<form name="summary_form" action="/ITM2/ThreadSum.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus")%>" method="post">
<div class="divinput">
<font face="verdana" color="green">Our Problems</font><br>
<img src="img/problem.gif" alt="We want to understand" title="We want to understand"  onmouseup="OnTag1()"height="42" width="42"><br>
	<% if(strProblem != null){%>
		<textarea onclick="checkIfReadOnly();" name="ProblemContent" id="problemcontent" rows="20" cols="30">
<%=strProblem %>
		</textarea>
	<%}else{%>
		<textarea onclick="checkIfReadOnly()" name="ProblemContent" id="problemcontent" rows="20" cols="30"></textarea>
		<%}%>
</div>
<div class="divinput">
<font face="verdana" color="green">Big ideas we have learned:</font><br>
<img src="img/think.gif" alt="We used to think" title="We used to think"onmouseup="OnTag2()"height="42" width="42">
<img src="img/insight.gif" alt="We now understand" title="We now understand" onmouseup="OnTag3()"height="42" width="42"><br>
<% if(strIdea != null){%>	
		<textarea onclick="checkIfReadOnly();" name="IdeaContent" id="ideacontent" rows="20" cols="30">
<%=strIdea %>
		</textarea>
		<%}else{%>	
		<textarea onclick="checkIfReadOnly();" name="IdeaContent" id="ideacontent" rows="20" cols="30"></textarea>
		<%}%>
</div>
<div class="divinput">
	<font face="verdana" color="green">We need to do more:</font><br>
<img src="img/understand.gif" alt="We need to further understand" title="We need to further understand" onmouseup="OnTag4()"height="42" width="42">
<img src="img/Better theory.gif" alt="We need better theories about" title="We need better theories about" onmouseup="OnTag5()"height="42" width="42">
<img src="img/read.gif" alt="We need to read more about" title="We need to read more about"onmouseup="OnTag6()"height="42" width="42">
<img src="img/evidence.gif" alt="We need evidence about" title="We need evidence about"onmouseup="OnTag7()"height="42" width="42">
<img src="img/disagree.gif" alt="We need to look at our different ideas about" title="We need to look at our different ideas about"onmouseup="OnTag8()"height="42" width="42">
	<br><% if(strMore != null){%>
	<textarea onclick="checkIfReadOnly();" name="MoreContent" id="morecontent" rows="20" cols="30">
<%=strMore%>	
		</textarea>	
		<%}else{%>
		<textarea onclick="checkIfReadOnly();" name="MoreContent" id="morecontent" rows="20" cols="30"></textarea>	
		<%}%>
	
</div>
<center>
<input type="submit" name="save" id="save" value="Save" onclick="OnSave()"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" name="" id="" value="History" onclick="OnHistory()"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" name="" id="" value="Close" onclick="OnClose()"/>	
</center>
	</form>
</div>
</div>
</div>
</body>
</html>
<%
	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(stmt != null){
		stmt.close();
	}
	
	if(s != null){
		s.Close();
	}
%>