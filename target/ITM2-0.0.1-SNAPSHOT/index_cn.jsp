<%@page import="database.DBUtil"%>
<%@page import="database.Operatedb"%>
<%@page import="code.sqls"%>
<%@ page import="java.sql.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="true"%>
<!DOCTYPE html>
<html>
<%
	String strsucceed = request.getParameter("ifsucceed");
	String strlogout = request.getParameter("logout");
	String dbase = request.getParameter("dbase");
	String hostname = request.getParameter("host");
        short isCN=1;
        session.setAttribute("lan", isCN);
        boolean isChinese=true;
                session.setAttribute("isCN", isChinese);
	if (strlogout != null) {
		System.out.println("Database"+dbase);
		System.out.println("host"+hostname);
		session.invalidate();
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>欢迎访问团队思维脉络图谱</title>

<script type="text/javascript">
	var info = '<%=strsucceed%>';
	var hostnm = '<%= hostname %>';
	var dbase = '<%= dbase %>';
	if (info == 'no') {
		alert("帐号或密码错误，请重新登录");
}
    //Generate a hashmap of url with respect to databases for dropdown
    //Initialize a global array
    var arrayUrlMap = [];
	function changeDatabaseList(urlName,kfdbName)
	{ 
		//alert(urlName);
		//alert(kfdbName);
		//alert(arrayUrlMap[urlName]);
		if(arrayUrlMap[urlName] === undefined)
		{
		  arrayUrlMap[urlName] = new Array();
		}
	    arrayUrlMap[urlName].push(kfdbName);
	}	

	function onSubmit(form)
	{
		form.url.value=location.href;
		return true;
	}
	

	function SelectionChange(value)
	{   
		document.getElementById('db').options.length = 0;
		var p = document.getElementById('db');   
		var index = 0;
			for(var i = 0;i< arrayUrlMap[value].length;i++)
			{ 
				if(hostnm == "null")
				{
			      if(arrayUrlMap[value][i] =='J-ICS 2012-2013')
				  {
			        index = i;
				  }
				}
				else
			    {
				  if(arrayUrlMap[value][i] == dbase)
				  {
			        index = i;
				  }
			   }
			   //Extract the values stored in the key to populate dropdown
			   p.options.add(new Option(arrayUrlMap[value][i]));
			}
		//select the default value of database.
		p.selectedIndex=index;
		hostnm = "null";
	}

</script>
<script src="js/jquery-1.6.min.js"></script>
        <script>
            $(document).ready(function(){
                $('#lan').click(function(e){
                    $('html').load('index.jsp');
                    return false;
                })
            })
        </script>
</head>
<LINK REL=StyleSheet HREF="css/default.css" TYPE="text/css"></LINK>
<body bgcolor="aliceblue">
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<center>
		<form style="border-width:1px;border-style:solid;border-color: #006600;width:50%"
			name="login_form" action="/itmcndev/Connect" method="post" onsubmit="onSubmit(this)">
                        <h4 id="lan"><a href="" id="lan">English</a></h4>
			<br />
			<h2>
				<font size="8" color="#71C2B1" face="Times New Roman"> 团队思维脉络图谱 </font>
			</h2>
			<br>
			<h3>连接到一个知识论坛数据库</h3>
			<br>
			<table>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>网址:</b></label></font></td>
					<td>
					    
						<!-- <input
						class="fields" type="text" name="host" id="host" type="text"
						size="20" value="builder.ikit.org" /> -->
						
						<% 
                        sqls sobj = new sqls();
                        Operatedb op = new Operatedb(sobj,"itm");
                        String[] columnNames = new String[2];
                        columnNames[0] = "distinct(URL)";
                        columnNames[1] = "kfdb";
                        ResultSet rs = op.GetMultipleRecordsFromDB("connection_table",columnNames,"");
                        
                        String urlName = null;
                        String kfdbName = null;
                        while(rs.next())
                        {
                           urlName  =  rs.getString("URL"); 
                           kfdbName =  rs.getString("kfdb");
                           
 						%> 
 						  <script type ="text/javascript">
 						   var urlName  = "<%= rs.getString("URL")%>";
 						   var kfdbName = "<%= rs.getString("kfdb")%>";
 						   changeDatabaseList(urlName,kfdbName);
 						  </script>
 						<%
						 }
                        DBUtil.closeResultSet(rs);
                        sobj.Close();
						%>
						<select name="host" id="host"  onChange = "SelectionChange(value)" style = "width:150px">     
						</select>
						<script type ="text/javascript">
							var e = document.getElementById('host');
							var j = 0;
							var indx = 0;
							
							if(hostnm == "null")
							{
							  for (var i in arrayUrlMap) 
							  {
							    if(i == 'builder.ikit.org')
							      {
							         indx = j;
							      }
								e.options.add(new Option(i));
								j++;
							  }
							  e.selectedIndex = indx;
							}
							else
							{
							  for (var i in arrayUrlMap) 
							  {
							    if(i == hostnm)
							      {
							         indx = j; 
							      }
								  e.options.add(new Option(i));
								  j++;
							  }
							  e.selectedIndex = indx;
							}
						</script>	
					</td>
				 </tr>
				<tr>
					<td>
						<font size="3" color="#71C2B1" face="Times New Roman">
							<label><b>数据库:</b></label>
						</font>
					</td>
					<td>	
 						<!--  <input class="fields" name="db" id="db" type="text"
						size="20" value="TaCCL-UAlbany" /> -->
						<!-- <input class="fields" name="db" id="db" type="text"
						size="20" value="J-ICS 2012-2013" /> -->
						<!-- <input class="fields"   name="db" id="db" type = "text" size = "20" value = "ICS 2012" /> --> 
                        
                        <select name="db" id="db" style = "width:150px">  
                        </select>
                        <script type="text/javascript">
                          SelectionChange(document.getElementById("host").options[indx].value);
                        </script>
					</td>
				 </tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>用户名:</b></label></font></td>
					<td>
						<%
							if (strsucceed != null) {
						%> 
							<input class="fields" name="username"
							id="username" type="text" style = "width:150px"
							value="<%=request.getParameter("username")%>" /> 
						<%
						 	} else {
						%> 
						<input
						class="fields" name="username" id="username" type="text" style = "width:150px"
						value="" /> <%
						 	}
						%>
						
						<input type="hidden" name="url"  id="url" />
					</td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>密码:</b>
						</label></font></td>
					<td><input class="fields" name="password" id="password"
						type="password" style = "width:150px" value="" /></td>
				</tr>
			</table>
			<font color="#4889EB" size="3" face="Times New Roman"><b><input
					style="width: 80px; height: 31" name="submit" id="" type="submit"
					value="登录"></b></font> <br/><br/>
                                        <a href="/itmcndev/cn/Registration.jsp">点击此处注册新数据库</a> <br/><br/>
					<h6>Idea Thread Mapper (ITM) version 1.13, updated on June 03, 2013</h6>
		</form>
	</center>
</body>

</html>