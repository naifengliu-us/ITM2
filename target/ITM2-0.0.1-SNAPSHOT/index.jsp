<%@page import="database.DBUtil"%>
<%@page import="database.Operatedb"%>
<%@page import="code.sqls"%>
<%@ page import="java.sql.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String strsucceed = request.getParameter("ifsucceed");
  String strsucceedtime = request.getParameter("ifsucceedtime");
	String strlogout = request.getParameter("logout");
	String dbase = request.getParameter("dbase");
	String hostname = request.getParameter("host");
	System.out.println("Database info"+dbase);
	System.out.println("Hostname info"+hostname);
        short isCN=0;
       // session.setAttribute("lan", isCN);
       // boolean isChinese=false;
                //session.setAttribute("isCN", isChinese);
	if (strlogout != null) {
		System.out.println("Database"+dbase);
		System.out.println("host"+hostname);
		session.invalidate();
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome - Idea Thread Mapper</title>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<script type="text/javascript">
// check if browser is IE6 (when IE) or not FF6 (when FF)
function checkBrowser()
{
	if(($.browser.msie) || ($.browser.mozilla)) {
	        $('#alertMessage').dialog();
	} 
}
</script>

<script type="text/javascript">
	var info = '<%=strsucceed%>';
	var hostnm = '<%= hostname %>';
	var dbase = '<%= dbase %>';
	var mesg ='<%= strsucceedtime%>';
	console.log(hostnm);
	console.log(dbase);
	if (info == 'no') {
		alert("Wrong login information,please try again");
	}
	if(mesg == 'no'){
	  alert("Connection timed out. Please try again later");	
}
    //Generate a hashmap of url with respect to databases for dropdown
    //Initialize a global array
    var arrayUrlMap = [];
	function changeDatabaseList(urlName,kfdbName)
	{ 
		//alert(urlName);
		//alert(kfdbName);
		//alert(arrayUrlMap[urlName]);
		if(arrayUrlMap[urlName] === undefined)
		{
		  arrayUrlMap[urlName] = new Array();
		}
	    arrayUrlMap[urlName].push(kfdbName);
	}	

	function onSubmit(form)
	{
		form.url.value=location.href;
		return true;
	}
	

	function SelectionChange(value)
	{   
		document.getElementById('db').options.length = 0;
		var p = document.getElementById('db');   
		var index = 0;
			for(var i = 0;i< arrayUrlMap[value].length;i++)
			{ 
				if(hostnm == "null")
				{
			      if(arrayUrlMap[value][i] =='J-ICS 2013-2014')
				  {
			        index = i;
				  }
				}
				else
			    {
				  if(arrayUrlMap[value][i] == dbase)
				  {
			        index = i;
				  }
			   }
			   //Extract the values stored in the key to populate dropdown
			   p.options.add(new Option(arrayUrlMap[value][i]));
			}
		//select the default value of database.
		p.selectedIndex=index;
		//hostnm = "null";
	}

</script>
</head>
<LINK REL=StyleSheet HREF="css/default.css" TYPE="text/css"></LINK>
<body bgcolor="aliceblue" onload="checkBrowser()">
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
<div id="alertMessage" style="display:none;"> 
<h5><center>Recommended browsers for ITM :  GOOGLE CHROME and SAFARI.</center><br/></h5>
<h5><center>Warning: Some features may not be supported by FIREFOX and INTERNET EXPLORER.</center></h5>
</div>  

<div id="section" style="display:none;">
</div>
			  
<center>    
		<div class="loading" style="display:none;"><img src="img/ajax-loader.gif"/>Login Processing .....</div>
		<form style="border-width:1px;border-style:solid;border-color: #006600;width:50%"
			name="login_form" action="/ITM2/kf5Connect" method="post">
                    <!--  <h4 id="lan"><a href="">简体中文</a></h4>-->
			<br />
			<h2>
				<font size="8" color="#71C2B1" face="Times New Roman"> Idea Thread Mapper </font>
			</h2>
			<br>
			<h3>Connect to a Knowledge Forum 5 Database</h3>
			<br>
			<table>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Username:</b></label></font></td>
					<td>
						<%
							if (strsucceed != null) {
						%> 
							<input class="fields" name="username"
							id="username" type="text" style = "width:150px"
							value="<%=request.getParameter("username")%>" /> 
						<%
						 	} else {
						%> 
						<input
						class="fields" name="username" id="username" type="text" style = "width:150px"
						value="" /> <%
						 	}
						%>
						
						<input type="hidden" name="url"  id="url" />
					</td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Password:</b>
						</label></font></td>
					<td><input class="fields" name="password" id="password"
						type="password" style = "width:150px" value="" /></td>
				</tr>
			</table>
		
			<font color="#4889EB" size="3" face="Times New Roman"><b><input id="form_submit"
					style="width: 80px; height: 31" name="submit" id="" type="button"
					value="Sign In"></b></font> <br/><br/>
					<a href="/ITM2/en/Registration.jsp">Click here to register a new Database</a> <br/><br/>
					<!--<center><h6>Idea Thread Mapper (ITM) version 3.0, updated on <%= new java.util.Date() %></h6></center>-->
						<center><h6>Idea Thread Mapper (ITM) version 8.1, updated on <%= "Thu May 15, 07:00:00 EDT 2014" %></h6></center>
					<a href="http://tccl.rit.albany.edu/wpsite?page_id=265" target="_blank" style ="float:right;"><b>About ITM&nbsp;</b></a><br/>
		</form>                                               
	</center>
</body>
<script type="text/javascript">
$(document).ready(function(){
    $(document).ajaxStart(function(){
        $(".loading").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $(".loading").css("display", "none");
    });
  });


function enterDatabase(database,user_name,password,type){
	 $.post("Connect", {'username': user_name,'password':password,'type':type,'db':database},function(result){
		 if(result=="Do not have database"){
			 alert("Sorry , The database "+database+" haven't been loaded into ITM, please contact admin for help.");
		 }else{
			 window.location.replace(result);
		 }
		
	 })
	
}
$("#form_submit").click(function(){
    var user_name=$("#username").val();
    var password=$("#password").val();
    $.post("kf5connect", {'user_name': user_name,'password':password}, function(result){
    	if(result!=""&&result!=null){
    		var obj = jQuery.parseJSON(result);
    		var htmlContent=" <ul>";
    		for (i = 0; i < obj.length; i++) { 
    			htmlContent+=" <li><input type='radio' name='radioSection' value='"+obj[i].sectionTitle+"&"+obj[i].roleInfo.name+"'>"+obj[i].sectionTitle+"</li>";
    		}
    		htmlContent+=" </ul>";
    		$("#section").html(htmlContent);
    		  $('#section').dialog({title:"Choose Database",
    			  buttons: [
    			            {
    			              text: "OK",
    			              click: function() {
    			            	  var dialCheckResult=$("input:radio[name='radioSection']:checked").val();
    			            	  if(dialCheckResult){
    			            		  enterDatabase(dialCheckResult.split("&")[0],user_name,password,dialCheckResult.split("&")[1]);
    			            		  }else{
    			            			  console.log("User do not choose any database")
    			            			  }
    			            	
    			                $( this ).dialog( "close" );
    			              }
    			            }
    			          ]});
    		console.log(obj)
    		
    	}else{
    		alert("Wrong login information,please try again");
    	}
       
    });
});
</script>


</html>