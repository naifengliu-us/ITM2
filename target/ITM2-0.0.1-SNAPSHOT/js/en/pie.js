
var pieChart = function(node, data) {
  var width = 200,
      height = 200,
      radius = Math.min(width, height) / 2;
  
  var color = d3.scale.category20();
  
  var arc = d3.svg.arc()
      .outerRadius(radius - 10)
      .innerRadius(0);
  
  var pie = d3.layout.pie()
      .sort(null)
      .value(function(d) { return d.value; });
  
  var svg = d3.select(node).append("svg")
      .attr("width", width)
      .attr("height", height)
    .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
  
  //Computes the angle of an arc, converting from radians to degrees.
  function angle(d) {
    var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
    return a > 90 ? a - 180 : a;
  }
  
  data.forEach(function(d) {
    d.value = +d.value;
  });

  var g = svg.selectAll(".arc")
      .data(pie(data))
    .enter().append("g")
      .attr("class", "arc");

  g.append("path")
      .attr("d", arc)
      .style("fill", function(d) { return color(d.data.label); });

  g.append("text")
      //.attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
      .attr("transform", function(d) { //set the label's origin to the center of the arc
        //we have to make sure to set these before calling arc.centroid
        //d.outerRadius = outerRadius; // Set Outer Coordinate
        //d.innerRadius = outerRadius/2; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")";
      })
      .attr("dy", ".35em")
      .style("text-anchor", "middle")
      .text(function(d) { return d.data.label; });
};
