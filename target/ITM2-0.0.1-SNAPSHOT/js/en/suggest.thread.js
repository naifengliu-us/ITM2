$(document).ready(function(){    
/**
 * selection event
 * Stan
 */
//                Initialize suggestThread.jsp’s list and content panels; 
                $('select option').click(function(){
                     $('#list_area li').remove();
                     $('#note_content').children().remove();
                     var items = [];
//                     Get selected views for parsing; 
                    $('#views option:selected').each(function(){items.push($(this).val());});
                    var result = 'view='+items.join('@ ');                    
                    var url='../SuggestThreadServlet';
//                    Pass views to SuggestThreadServlet, and retrieve server’s JSON data.
                    $.getJSON(url,result,function(data) {
                        var $this;
                        //change append li logic below when we have multiple data entry
                        if(data!=null){
                            $('#list_area ul').append('<li>'+data[0].threadName+'</li>');                            
                        }
                        $('#list_area li').click(function(){
                            $this=$(this);
                             $('.search_tr').remove();                             
                            $('#note_content h3').remove();
                            $('#note_content').append('<div class="search_tr"><label for="h5">Threads</label><h5></h5></div>'+
                                '<div class="search_tr"><label for="h6">Keywords</label><h6></h6></div>'+
                                '<div class="search_tr"><label for="note">Notes</label><ul id=note></ul></div>');
                            var tableHTML='';
                          $.each(data, function(key, val) {
                                $('#note_content h5').append(val.threadName+' ');
                                //Keywords                                
                                $('#note_content h6').append(val.keyWord+' ');
                                //Note
                              var content=val.noteContent;
                              if(content.length>0){
                                tableHTML='<li><p>Title: ';
                                tableHTML+=val.title;
                                tableHTML+='</p>';
//                                $('#note_content div:eq(2)').append(tableHTML);
                                $('#note').append(tableHTML);
                                tableHTML='<p>Author: ';
                                tableHTML+=val.author;
                                tableHTML+='</p>';
                                $('#note').append(tableHTML);                                
                                //add content
                                    tableHTML='<p>Content: ';
                                    tableHTML+=val.noteContent+'...</p></li>';
                                    $('#note').append(tableHTML);
                                 }
                            });//end each
                            //highlight selection
                            $this.css('background', 'yellow');
                        });
                      });//end json
                });//end click
                //                select all views as default
                $('#views option::first-child').click();
            });//end doc
            
            function submitThread(link){    
                 var $this=link.innerHTML;
//                //window.open('thread_note.jsp?database=${par.database}&projectname=${par.projectName}&threadfocus=${par.threadName}');
                    $('#input_threadfocus').val($this);
//                 $.post('../addThreadServlet', data);
//                 document.forms["createThreadForm"].submit();
//                 window.alert("Thread added.");                 
                    return false;                
                }  