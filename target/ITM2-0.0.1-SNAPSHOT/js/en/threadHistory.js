
var threadHistory = function() {
  this.showThreadRevision = function(threadHistoryId, ts) {
    // Get the data from the thread history table
    var myurl = "/ITM2/ThreadHistory",
        threadDisplayDiv = $("#thread_history_display");
    threadDisplayDiv.empty();
    
    $.ajax({
      type: "POST",
      url: myurl,
      async: false,
      data: { 
        database: _database,
        threadhistory_id: threadHistoryId   
      },
      success : function(response) {
        var threadRawData = jQuery.parseJSON(response),
            formattedData = {}, nodes = [], links = [], i, tg;
        for (i = 0; i < threadRawData.data_array.length; ++i) {
          nodes.push({"author":threadRawData.data_array[i][6], 
                      "id":threadRawData.data_array[i][4], 
                      "noteid": threadRawData.data_array[i][0], 
                      "content": threadRawData.data_array[i][3], 
                      "time": parseDT(threadRawData.data_array[i][2]),
                      "view":threadRawData.data_array[i][5],
                      "timestamp":threadRawData.data_array[i][2]});
        }
        formattedData["nodes"] = nodes;
        formattedData["links"] = links;
        console.log(formattedData);
        //Capture the timestamp and split to remove 0
        var blob_time = ts;
        var create_time = blob_time.split(".",1);
        var width = 930;
        var height = 200;
        var dataDiv = $(document.createElement('div'));
        dataDiv.attr('id', 'thread_history_graph');
        dataDiv.attr('width', width + 'px');
        dataDiv.attr('height', height + 'px');
        //dataDiv.append(document.createTextNode(formattedData));
        threadDisplayDiv.append(dataDiv);
        threadDisplayDiv.dialog({
          minWidth: width,
          minHeight: height,
          title:'Saved by  '+threadRawData.modified_by+' at  '+create_time+'    .Note(s):  '+threadRawData.notecount+' Author(s):  '+ threadRawData.authorcount
        });
        tg = new threadGraph(null, width, height);
        tg.readData(formattedData, "#thread_history_graph");
        
        //Toggle the title names and author names
        tg.toggleTitles(this);
        tg.toggleAuthors(this);
        
        // Now update highlight state
        for (i = 0; i < threadRawData.data_array.length; ++i) {
          var noteid, highlightState;
          noteid = threadRawData.data_array[i][0];
          highlightState = threadRawData.data_array[i][1];
          if (highlightState === "1") {
            tg.toggleHiglightNodeById(noteid);
          }
        }
      },
      failure : function(response) {
        console.log('[error] Failed to get thread history data ', response);
      }
    });
  };
};

var threadHistoryInstance = new threadHistory();


