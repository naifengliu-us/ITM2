// From d3 js: http://bl.ocks.org/MoritzStefaner/1377729
var networkGraph = function(node, data) { 
  if ($(node).children().length === 1) {
    return;
  }
    
  var w = $(node).width(), h = $(node).height();
  
  var vis = d3.select(node).append("svg:svg").attr("width", w).attr("height", h);
  
  var nodes = data.nodes;
  var links = data.links;
  var groups = [];
  
  var labelAnchors = [];
  var labelAnchorLinks = [];
 
  // Calculate domain from given nodes
  var min = 0;
  var max = 0;
  var nc = 0;
  
  data.nodes.forEach( function(d) {
    nc = parseInt(d.notecount, 10);
    if (nc < min) {
      min = nc;
    }
    if (nc > max) {
      max = nc;
    }
    
    if ($.inArray(d.id, groups) === -1) {
      groups.push(d.id);
    }
  });
  var radiusScale = d3.scale.linear().range([5,10]);
  radiusScale.domain([min,max]);
  
  var color = d3.scale.category20();
  color.domain(groups);
  
//  var colorScale = d3.scale.linear().range([0, 10]);
//  colorScale.domain([min, max]);
  
  
  //Calculate domain from common link values
  var countCommonNotesMin = 0;
  var countCommonNotesMax = 0;
  var nv = 0;
  data.links.forEach(function(d){
  nv = d.value;
  if(nv <countCommonNotesMin){
    countCommonNotesMin = nv;
   }
  if(nv >countCommonNotesMax){
    countCommonNotesMax = nv;
   }
  });
  
  var linkScale = d3.scale.linear().range([0,5]);
  linkScale.domain([countCommonNotesMin,countCommonNotesMax]);
  
  
  for(var i = 0; i < nodes.length; i++) {
    labelAnchors.push({
      node : nodes[i]
    });
    labelAnchors.push({
      node : nodes[i]
    });
  };
  
  for(var i = 0; i < nodes.length; i++) {
    labelAnchorLinks.push({
      source : i * 2,
      target : i * 2 + 1,
      weight : 1
    });
  };
  
  var force = d3.layout.force().size([w, h])
    .nodes(nodes)
    .links(links)
    .gravity(1)
    .linkDistance(w * 0.3)
    .charge(-3000)
    .linkStrength(function(x) {
      return x.value + 1;
    });
  
  force.start();
  
  var force2 = d3.layout
    .force().nodes(labelAnchors)
    .links(labelAnchorLinks).gravity(0)
    .linkDistance(0).linkStrength(8)
    .charge(-100).size([w, h]);
  force2.start();
  
  marker = vis.append("svg:defs").selectAll("marker")
    .data(["commonNoteCount", "crossBuildons"])
  .enter().append("svg:marker")
    .attr("id", String)
    .attr("viewBox", "0 -5 10 10")
    .attr("refX", 15)
    .attr("refY", -1.5)
    .attr("markerUnits", "userSpaceOnUse")
    .attr("markerWidth", 6)
    .attr("markerHeight", 6)
    .attr("orient", "auto")
    .style("stroke-width", 1)
  .append("svg:path")
    .attr("d", "M0,-5L10,0L0,5");  
  
  var link = vis.selectAll("line")
    .data(links).enter()
    .append("svg:g");
  
  var line = link.append("svg:line")
    .attr("class", function(d) {return d.type;})
    .attr("stroke-width", function(d) { return linkScale(d.value); })
    .attr("type", function(d) {return d.type;})
    .attr("marker-end", function(d) { if (d.type === "crossBuildons") {
        //return "url(#" + d.type + ")";
      return "url(#)"; 
      } else {
        return "url(#)"; }
      })
    .attr("opacity", function(d) {
      if (d.type === "commonNoteCount") {
        return 1.0;
      } else {
        return 0.0;
      }
    })
    .style("stroke", "#01B0F1");
  
  var linkLabel = link.append('svg:text')
    .attr("class", function(d) {return "linkLabel-" + d.type;})
    .attr('text-anchor', 'middle')
    .style('font-size', '14px')
    .attr("opacity", function(d) {
      if (d.type === "commonNoteCount") {
        return 1.0;
      } else {
        return 0.0;
      }
    })
    .text(function(d) { if (d.value > 0){ return d.value; } else { return ""; } });
  
  var node = vis.selectAll("g.node")
    .data(force.nodes())
    .enter().append("svg:g")
    .attr("class", "node");
  
  node.append("svg:circle")
  .attr("r", function(d){ return radiusScale(d.notecount); })
  .style("stroke", "#FFF").style("stroke-width", 3)
  .style("fill", function(d) {
    return color(d.id); });
  node.call(force.drag);
  
  var anchorLink = vis.selectAll("line.anchorLink").data(labelAnchorLinks); // enter().append("svg:line").attr("class", "anchorLink").style("stroke", "#999");
  
  var anchorNode = vis.selectAll("g.anchorNetworkNode")
    .data(force2.nodes())
    .enter()
    .append("svg:g")
    .attr("class", "anchorNetworkNode");
  
  anchorNode
    .append("svg:circle")
    .attr("r", 0)
    .style("fill", "#FFF");
  
  anchorNode
    .append("svg:text")
    .text(function(d, i) {
      return i % 2 == 0 ? "" : d.node.id + ' (' + d.node.notecount + ')';
    }).style("fill", "#555").style("font-family", "Arial").style("font-size", 12).style("stroke", "4");
  
  var updateLink = function() {
    this.attr("x1", function(d) {
      return d.source.x;
    }).attr("y1", function(d) {
      return d.source.y;
    }).attr("x2", function(d) {
      return d.target.x;
    }).attr("y2", function(d) {
      return d.target.y;
    });
  };
  
  var updateNode = function() {
    this.attr("transform", function(d) {
      return "translate(" + d.x + "," + d.y + ")";
    });
  };
  
  var updateLinkLabels = function() {
    this.attr("transform", function(d) {
      if (d.type === "commonNoteCount") {
        return "translate(" + (d.target.x + d.source.x)  / 2 + "," + (d.target.y + d.source.y) / 2 + ")";
      } else {
        return "translate(" + (d.target.x + (d.source.x - d.target.x) / 4) + "," 
          + (d.target.y + (d.source.y - d.target.y) / 4) + ")";
      }
    });
  };
  
  force.on("tick", function() {
    force2.start();
    node.call(updateNode);
    anchorNode.each(function(d, i) {
      if(i % 2 == 0) {
        d.x = d.node.x;
        d.y = d.node.y;
      } else {
        var b = this.childNodes[1].getBBox();
        //console.log(b);
        var diffX = d.x - d.node.x;
        var diffY = d.y - d.node.y;
  
        var dist = Math.sqrt(diffX * diffX + diffY * diffY);
  
        var shiftX = b.width * (diffX - dist) / (dist * 2);
        shiftX = Math.max(-b.width, Math.min(0, shiftX));
        var shiftY = 5;
        this.childNodes[1].setAttribute("transform", "translate(" + shiftX + "," + shiftY + ")");
      }
    });
  
    anchorNode.call(updateNode);
    line.call(updateLink);
    anchorLink.call(updateLink);
    linkLabel.call(updateLinkLabels);
  });
  
  return {
    setGraphTypeToCrossBuildons : function() {
      var commonNoteCountLinks = vis.selectAll("line.commonNoteCount");
      // TODO Check for length
      commonNoteCountLinks[0].forEach(function(d) {
          d.setAttribute('opacity', 0.0);
      });
      
      var crossBuildonsLinks = vis.selectAll("line.crossBuildons");
      // TODO Check for length
      crossBuildonsLinks[0].forEach(function(d) {
          d.setAttribute('opacity', 1.0);
      });
      
      var commonNotelinkTexts = vis.selectAll("text.linkLabel-commonNoteCount");
      commonNotelinkTexts[0].forEach(function(d) {
        d.setAttribute('opacity', 0.0);
      });
      
      var crossBuildonsLinkText = vis.selectAll("text.linkLabel-crossBuildons");
      crossBuildonsLinkText[0].forEach(function(d, i) {
        d.setAttribute('opacity', 1.0);
      });
    },
    setGraphTypeToCommonNoteCount : function() {
      var crossBuildonsLinks = vis.selectAll("line.crossBuildons");
      // TODO Check for length
      crossBuildonsLinks[0].forEach(function(d) {
          d.setAttribute('opacity', 0.0);
      });
      
      var commonNoteCountLinks = vis.selectAll("line.commonNoteCount");
      // TODO Check for length
      commonNoteCountLinks[0].forEach(function(d) {
          d.setAttribute('opacity', 1.0);
      });
      
      var commonNotelinkTexts = vis.selectAll("text.linkLabel-commonNoteCount");
      commonNotelinkTexts[0].forEach(function(d) {
        d.setAttribute('opacity', 1.0);
      });
      
      var crossBuildonsLinkText = vis.selectAll("text.linkLabel-crossBuildons");
      crossBuildonsLinkText[0].forEach(function(d) {
        d.setAttribute('opacity', 0.0);
      });
    }
  };
};