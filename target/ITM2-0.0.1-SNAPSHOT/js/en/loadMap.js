var _load_map = null;
var published;

function MapLoad (pub) {
  published = pub;
	var threadl = document.get_threads.threadcheck;
	var threadnames="@";
	if (threadl.length > 1){
		for (var i=0;i<threadl.length;i++){
			if (threadl[i].checked == true){
					threadnames = threadnames + threadl[i].value+"@";
			}
		}
	}
	else {
		if (threadl.checked == true){
			threadnames = threadnames+threadl.value+"@";
		}
			}
	if (threadnames == "@") return;
	var root_draw = document.getElementById("draw_area");
	var root_raw = document.getElementById("raw_area");
	var net_area = document.getElementById("network_area");
	var composite_area = document.getElementById("compositeGraph");
	var cnt = root_draw.children.length;
	//clean up
	for (var i = 0;i<cnt;i++){
		root_draw.removeChild(root_draw.children[0]);
	}
	cnt = root_raw.children.length;
	for (var i = 0;i<cnt;i++){
		root_raw.removeChild(root_raw.children[0]);
	}
	cnt = net_area.children.length;
	for(var i = 0;i<cnt;i++)
	{
	  net_area.removeChild(net_area.children[0]);
	}
	
	// @TODO Shweta: Need to look into this
//	cnt = composite_area.children.length;
	cnt = 0;
  for(var i = 0;i<cnt;i++)
  {
    composite_area.removeChild(composite_area.children[0]);
  }
	_load_map = initXMLHttpObject();
	if (_load_map == null){
		alert("your browser does not support Ajax");
		return;
	}
	
	var url = "loadMap.jsp";
	
	if (published !== undefined) {
	  url = "loadPublishedMap.jsp";
	}
	
	url = url + "?projectname=" + _projectname;
	url = url + "&database="+ _database;
	url = url + "&threadname=" + threadnames;
	 if (published !== undefined) {
	url = url + "&timestamp=" + _timestamp;
	 }
	
	 console.log(url);
	_load_map.onreadystatechange = getOutput;
	_load_map.open("GET",url,true);
	_load_map.send(null);
}

function initXMLHttpObject(){
	var xr = null;
	if(window.XMLHttpRequest){
		xr = new XMLHttpRequest();
	}
	else if(window.ActiveXObject){
		xr = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xr;
}

function getOutput(){
	var ready = _load_map.readyState;
	var root_raw = document.getElementById("raw_area");
	if (ready != 4){
		//TODO
	}
	else if(ready == 4){
		root_raw.innerHTML =  _load_map.responseText;
		
		drawMap(published);
		CurrThreadNotesPieChart();
		drawBigBangVisualization();
		drawCompleteMapVisualization();
	}
}

// Generate Data structure for current project threads vs no of notes
function CurrThreadNotesPieChart(){
  var root_raw = document.getElementById("raw_area");
  var threadObj =[];
  //var i=0;
  
  for (var h = 0;h< root_raw.children.length;++h){
    var localObj ={};
    localObj["label"] = (root_raw.children[h].getAttribute("focusname"));
    localObj["value"] =(root_raw.children[h].getAttribute("nt_num"));
    threadObj[h] = localObj;       
  }
  //console.log('CurrThreadNotesPieChart',threadObj);
 setOrCreatePieChart('pie_curr_thread_notes', threadObj);
}

function setOrCreatePieChart(id, data) {
  var parent = $(document.getElementById("pie_area"));
  var children = $('#' + id);
  if (children.length === 0) {
    children = $(document.createElement('div'));
    children.attr('id', id);
    parent.append(children);
  } else {
    children.empty();
  }
  var piechart = new pieChart(children.get(0), data); 
}

$(function() {
  console.log('new slider');
  $( "#slider" ).slider(
    {
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        comGraph.updateZoomExtents([ui.values[ 0 ], ui.values[ 1 ]]);
      }
  });
});
