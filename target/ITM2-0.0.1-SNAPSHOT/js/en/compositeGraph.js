// From d3 js: http://bl.ocks.org/MoritzStefaner/1377729

/**
 * @param node
 * @param data
 * @param timeRange
 * @returns {compositeGraph}
 */
var compositeGraph = function(node, data, timeRange) { 
  if (timeRange[0] === timeRange[1]) {
    throw "timeRange has invalid range. Start and end time are same";
  }
  
  $(node).html('');
  
  // Calculate domain from given nodes
  var groups = data.groups; 
  console.log('Groups',groups);
  var w = $(node).width(), h = $(node).height();
  var vis = d3.select(node)
    .append("svg:svg")
      .attr("width", w)
      .attr("height", h)
      .on('click', deselect);
  
  var squareDimHalf = 4;
  var nodes = data.nodes;
  var links = data.links; 
  var labelAnchors = [];
  var labelAnchorLinks = [];
  var timeRangeGroups = {};
  var linkedByIndex = {};
  var color = d3.scale.category20();
  var nodeConnection = null;
  var nodeConnections = null;
  var colorOpacity = 0.3;
  var anchorNode = null;
  var anchorLink = null;
  
  color.domain(groups);
  
  // Populate linked by index ds as it will be used to find out if
  // nodes are connected
  links.forEach(function(d) {
    linkedByIndex[d.source + "," + d.target] = 1;
  });
  
  nodes.forEach(function(d) {
    if (timeRangeGroups.hasOwnProperty(d.group)) {
      if (timeRangeGroups[d.group][0] > d.time) {
        timeRangeGroups[d.group][0] = d.time;
      }
      if (timeRangeGroups[d.group][1] < d.time) {
        timeRangeGroups[d.group][1] = d.time;
      }
    } else {
      timeRangeGroups[d.group] = [d.time || 0, d.time || 0];
    }
  });
  
  console.log('timeRangeGroups', timeRangeGroups);
  
  for(var i =0;i<groups.length;i++){
      if(groups[i] in timeRangeGroups){
        //Do nothing
      }
      else{
        timeRangeGroups[groups[i]] =[0,0];
      }
  }
    
  /// View parameters
  var totalWidth = $(node).width() ,
      totalHeight = $(node).height(),  
      margin = {top: totalHeight * 0.01, right: 0,
                bottom: totalHeight * 0.01, left: 0,
                offset: totalWidth * 0.01},            
      width = totalWidth - margin.left - margin.right;

  /// Some other necessary parameters
  var xOffset = totalWidth * 0.01;
 
  // Set x scale that will be used
  var xScale = d3.time.scale().range([0 + xOffset, width - xOffset]);
  
  // We will set the domain for y later
  xScale.domain(timeRange);
  xScale.nice();
  
  var tmin = xScale.domain()[0];
  var tmax = xScale.domain()[1];
  
  updateIntefaceExtents([tmin.getTime(),tmax.getTime()], true);
  
  function updateIntefaceExtents(extents, updateRange) {
    if (updateRange) {
      $("#slider").slider('option',{min: extents[0], max: extents[1]});
    }
    $("#slider").slider({ values: [ extents[0], extents[1] ] });
  }
  
  function computeGroupBoundry(index) {
    var length = groups.length,
        heightPerGroup = (400 / length),
        min = 0, 
        delta = 0;
    
    min =  index * heightPerGroup + heightPerGroup * 0.04;
    delta =  heightPerGroup - heightPerGroup * 0.08;
    
    return [min, delta];
  }
  
  /// Create y-scale to compute y position of the nodes
  function yScale(groupId) {
    if (!groupId) {
      console.log('invalid group id');
    } 
    
    var i = 0, 
        index = -1,
        length = groups.length,
        min = 0, 
        delta = 0,
        position = 0;
    
    for (i = 0; i < length; ++i) {
      if (groups[i] === groupId) {
        index = i;
        break;
      }
    }
    
    var boundry = computeGroupBoundry(index);
    min = boundry[0] + squareDimHalf;
    delta = boundry[1] - squareDimHalf * 2;
   
    position = (Math.random()*delta)+min;
    //console.log('position is ', min, delta, index, position);
    return position;
  }
  
  /// Given a node, find all matching nodes and then draw a connection
  /// between all matching nodes
  /**
   * @param node
   */
  function drawNodeConnection(node) {
    // First de-select
    deselect();
    
    d3.select(this).classed("selected", true);
    
    var noteid = node.noteid;
    console.log(node);
    
    /// This will select all of the rect entities in the vis
    var matchedNodes = vis.selectAll("rect").filter(function(d, i) { 
      if (typeof d === "undefined") {
        return false;
      }
      return d.noteid === noteid; 
    });
   
    var x1 = xScale(node.time);
    var y1 = node.yPos;
    var x2 = xScale(node.time);
    var y2 = node.yPos;
    
    matchedNodes.each(function() {
      var yPos =  parseInt(d3.select(this).attr('y'), 10);
      if (y2 < yPos) {
        y2 = yPos;
      }
      if (y1 > yPos) {
        y1 = yPos;
      }
      d3.select(this).classed("selected", true);
      d3.select(this).classed("svgNode", false);
    });
    
    if (nodeConnection) {
      nodeConnection.remove();
    }
    
    var connections = [];
    connections.push({"time": node.time, "id": node.noteid, "x1": x1, "y1": y1, "x2": x2, "y2": y2});
    
    nodeConnection = vis.selectAll("line.connections")
      .data(connections)
      .enter()
      .append("svg:line")
        .attr("class", "connections")
        .attr("data-time", function(d){return d.time;})
        .attr("data-id", function(d){return d.id;})
        .attr("x1", function(d){return d.x1;})
        .attr("y1", function(d){return d.y1;})
        .attr("x2", function(d){return d.x2;})
        .attr("y2", function(d){return d.y2;})
        .attr("stroke-width", 2)
        .attr("stroke", "green")
        .attr("stroke-dasharray", "5,5")
        .attr("d", "M5 20 l215 0");
      
    d3.event.stopPropagation();
  }
  
  function drawAllNodeConnections() {
    var connections = [];
    var connectionIds = {};
    
    // First de-select
    deselect();
    
    if (nodeConnections) {
      nodeConnections.remove();
    }
    
    var nodes = vis.selectAll("rect.svgNodeBase");
    nodes.each(function(d, i) {
      if (!(d.noteid in connectionIds)) {
        var nodes = [];
        
        var matchedNodes = vis.selectAll("rect").filter(function(e, i) { 
          if (typeof e === "undefined") {
            return false;
          }
          
          if (e === d) {
            nodes.push(this);
            return false;
          }
          
          return e.noteid === d.noteid; 
        });
       
        var x1 = xScale(d.time);
        var y1 = d.yPos;
        var x2 = xScale(d.time);
        var y2 = d.yPos;
        
        matchedNodes.each(function(e, i) {
          console.log(this);
          var yPos =  parseInt(d3.select(this).attr('y'), 10);
          if (y2 < yPos) {
            y2 = yPos;
          }
          if (y1 > yPos) {
            y1 = yPos;
          }
          nodes.push(this);
        });
        
        if (nodes.length > 1) {
          for (var j = 0; j < nodes.length; ++j) {
            d3.select(nodes[j]).classed("selected", true);
            d3.select(nodes[j]).classed("svgNode", false);
          }
          connections.push({"id": d.noteid, "time" : d.time, "x1" : x1, "y1" : y1, "x2" : x2, "y2" : y2 });
        }
        connectionIds[d.noteid] = 1;
      }
    });
    
    
    
    nodeConnections = vis.selectAll("line.connections")
    .data(connections)
    .enter()
      .append("svg:line")
        .attr("class", "connections")
        .attr("data-time", function(d){return d.time;})
        .attr("data-id", function(d){return d.id;})
        .attr("x1", function(f) {return f.x1;})
        .attr("y1", function(f) {return f.y1;})
        .attr("x2", function(f) {return f.x2;})
        .attr("y2", function(f) {return f.y2;})
        .attr("stroke-width", 2)
        .attr("stroke", "green")
        .attr("stroke-dasharray", "5,5")
        .attr("d", "M5 20 l215 0");
    //alert(connections.length);
    
    return (connections.length > 0);
  }
  
  /// Undo any selection performed earlier
  function deselect() {
    
    if (nodeConnection || nodeConnections) {
      $("body").trigger("composite-deselected");
    }
    
    if (nodeConnection) {
      nodeConnection.remove();
      nodeConnection = null;
    }
    
    if (nodeConnections) {
      nodeConnections.remove();
      nodeConnections = null;
    }
    
    vis.selectAll("rect.selected").classed('svgNode', true);
    vis.selectAll("rect.selected").classed("selected", false);
  }
  
  data.nodes.forEach( function(d) {
    d.yPos = yScale(d.group);
  });
  
  /**
   * Check if nodes are connected to each other via some link
   */
  function isConnected(a, b) {
   if (a.noteid === b.noteid) {
     return true;
   }
    
    return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index] || a.index == b.index;
  }
  
  /**
   * Check if a node is connected to its label
   */
  function areNodeAndLabel(a, b) {
   if (a.noteid === b.noteid && a.group === b.group) {
     return true;
   }
    
    return false;
  }

  /**
   * Fade nodes, and any other entities in the scene that are not linked
   * to the hovered node
   */
  function fade(opacity) {
    return function(d) {
      vis.selectAll("rect.svgNodeBase").style("stroke-opacity", function(o) {
            thisOpacity = isConnected(d, o) ? 1 : opacity;
            this.setAttribute('fill-opacity', thisOpacity);
            return thisOpacity;
      });
      
      vis.selectAll("rect.selected").style("stroke-opacity", function(o) {
        thisOpacity = isConnected(d, o) ? 1 : opacity;
        this.setAttribute('fill-opacity', thisOpacity);
        return thisOpacity;
      });
      
      vis.selectAll("line.connections")
        .style("stroke-opacity", function(o) {
        console.log(o.id);
        if (o.id === d.noteid) {
          this.setAttribute('fill-opacity', 1);
          return 1;
        }
        this.setAttribute('fill-opacity', opacity);
        return opacity;
      })
      .map(function() { return this.dataset; });

      link.style("opacity", opacity).style("opacity", function(o) {
          var defaultOpacity = 0.6 * opacity;
          
          // If the mode is to show only highlightd mode then 
          // default opacity should be 0
          if($("#toggle-highlights").html() === "Show All Notes" ) {
            if(o.source.highlight_state === "0" || o.target.highlight_state === "0") {
              defaultOpacity = 0.0;
            }
          }
          
          return o.source === d || o.target === d ? 1 : defaultOpacity;
      });

      anchorNode.style("opacity", function(o) {
        thisOpacity = isConnected(d, o.node) ? 1 : opacity;
        this.setAttribute('opacity', thisOpacity);
        return thisOpacity;
      });    
    };
  }
  
  for(var i = 0; i < nodes.length; i++) {
    labelAnchors.push({
      node : nodes[i],
      label: nodes[i].id
    });
    labelAnchors.push({
      node : nodes[i],
      label: nodes[i].id
    });
    labelAnchors.push({
      node : nodes[i],
      label: nodes[i].author
    });
  };
  
  for(var i = 0; i < nodes.length; i++) {
    labelAnchorLinks.push({
      source : (i) * 3,
      target : (i * 3 + 1),
      weight : 1
    });
    
    labelAnchorLinks.push({
      source : (i) * 3,
      target : (i * 3 + 2),
      weight : 1
    });

  };
  
  var force = d3.layout.force().size([w, h])
    .nodes(nodes)
    .links(links)
    .gravity(1)
    .linkDistance(w * 0.3)
    .charge(-3000)
    .linkStrength(function(x) {
      return x.value + 1;
    });
  
  force.start();
  
  var force2 = d3.layout
    .force().nodes(labelAnchors)
    .links(labelAnchorLinks).gravity(0)
    .linkDistance(0).linkStrength(8)
    .charge(-20).size([w, h]);
  force2.start();
  
  /// Create background
  for (i = 0; i < groups.length; ++i) {
    var boundry = computeGroupBoundry(i);
    var width = 
      xScale(timeRangeGroups[groups[i]][1]) - xScale(timeRangeGroups[groups[i]][0]); 
    vis.append("svg:g").append("svg:rect")
      .attr("class", "background")
      .attr("id", groups[i])
      .attr("x", xScale(timeRangeGroups[groups[i]][0]) - squareDimHalf)
      .attr("y", boundry[0] - 6 * 0.5)
      .attr("height", boundry[1] + 6 * 0.5)
      .attr("width", width + squareDimHalf * 2)
      .style("fill", color(groups[i]))
      .style("opacity", colorOpacity)
      .on("click", null);
    
  }
  
  /// Create links
  marker = vis.append("svg:defs").selectAll("marker")
    .data(["buildons", "references", "cross-buildons", "cross-references"])
  .enter().append("svg:marker")
    .attr("id", String)
    .attr("viewBox", "0 -5 10 10")
    .attr("refX", 15)
    .attr("refY", -1.5)
    .attr("markerWidth", 6)
    .attr("markerHeight", 6)
    .attr("orient", "auto")
  .append("svg:path")
    .attr("d", "M0,-5L10,0L0,5");  
    
  link = vis.selectAll("path.link")
    .data(data.links)
    .enter().append("svg:path")
      .attr("class", function(d) { return "links link-" + d.type; })
      .style("stroke-width", function(d) { return 1.2; })
      .attr("marker-end", function(d) { return "url(#" + d.type + ")"; });
  
  var node = vis.selectAll("g.node")
    .data(force.nodes())
    .enter().append("svg:g")
    .attr("class", "node");
  
  node.append("svg:rect")
   .attr("width", squareDimHalf * 2)
   .attr("height", squareDimHalf * 2)
   .attr("class", function(d) {
     if (d.highlight_state == 0) { 
       return "svgNodeBase svgNode";
     }
     return "svgNodeBase svgNodeHighlight";
   })
   .attr("x", function(d){ return (xScale(d.time) - squareDimHalf); })
   .attr("y", function(d){ return (d.yPos - squareDimHalf); })
   .attr("group", function(d) { return d.group;})
   .on('click', drawNodeConnection)
   .on("mouseover", fade(.1)).on("mouseout", fade(1));

  // Add tooltips
  node.selectAll("rect").each(function(d) {
    var format = d3.time.format("%Y-%m-%d %X");
    //console.log(this);
    //console.log(d);
    $(this).qtip({
      content: {
        text: "<b>Title    :  " + d.id + "(View : " + d.view + ")</b><br><br>"+ "<b>By :   "+d.author+"  "
        +" At : "+ format(new Date(d.time))+"</b><br><br>"  + "<b>Content: </b>  " + d.content 
      },
      hide: {
        fixed: true,
        delay: 300
      }
    });
  });
  
  anchorLink = vis.selectAll("line.anchorLink").data(labelAnchorLinks); // enter().append("svg:line").attr("class", "anchorLink").style("stroke", "#999");
  
  anchorNode = vis.selectAll("g.anchorNetworkNode")
    .data(force2.nodes())
    .enter()
    .append("svg:g")
    .attr("class", "anchorNetworkNode");
  
  anchorNode
    .append("svg:circle")
    .attr("r", 0)
    .style("fill", "#FFF");
  
  anchorNode
    .append("svg:text")
    .attr("class", function(d, i){ return (i % 3 === 1 ? "title" : "author"); })
    .attr("group", function(d){ return d.group; })
    .text(function(d, i) {
      return i % 3 == 0 ? "" : d.label;
    }).style("font-family", "Arial").style("font-size", 10);
  
  var updateLink = function() {
    this.attr("d", function(d) {
      var sourceX =  xScale(d.source.time);
      var sourceY = d.source.yPos; 
      var targetX = xScale(d.target.time);
      var targetY = d.target.yPos;
      var dx = xScale(d.target.time) - xScale(d.source.time),
        dy = d.target.yPos - d.source.yPos,
        dr = Math.sqrt(dx * dx + dy * dy)*1.6;
      return "M" + sourceX + "," + sourceY + "A" + dr + "," + dr + " 0 0,1 " + targetX + "," + targetY;
    });
  };
  
  var updateNode = function() {
    this.attr("transform", function(d) {
      return "translate(" + d.x + "," + d.y + ")";
    });
  };
  
  var updateLinkLabels = function() {
    this.attr("transform", function(d) {
      return "translate(" + (d.target.x + d.source.x)  / 2 + "," + (d.target.y + d.source.y) / 2 + ")";
    });
  };
  
  force.on("tick", function() {
    force2.start();
    //node.call(updateNode);
    anchorNode.each(function(d, i) {
      if(i % 3 == 0) {
        d.x = xScale(d.node.time);
        d.y = d.node.yPos;
      } else {
        //console.log(this);
        var b = this.childNodes[1].getBBox();
        //console.log(d);
        var diffX = d.x - xScale(d.node.time);
        var diffY = d.y - d.node.yPos;
        //console.log('diffX', diffX);
        var dist = Math.sqrt(diffX * diffX + diffY * diffY);
        var shiftX = b.width * (diffX - dist) / (dist * 2);
        shiftX = Math.max(-b.width, Math.min(0, shiftX));
        var shiftY = 8;
        this.childNodes[1].setAttribute("transform", "translate(" + shiftX + "," + shiftY + ")");
      }
    });
  
    anchorNode.call(updateNode);
    link.call(updateLink);
    anchorLink.call(updateLink);
    //linkLabel.call(updateLinkLabels);
  });
  
  function recomputePositions (timeRange) {
    xScale.domain(timeRange);
    //xScale.nice();
    var bks = d3.selectAll("rect.background")[0];
    for (var i = 0; i < bks.length; ++i) {
      var width = 
        xScale(timeRangeGroups[groups[i]][1]) - xScale(timeRangeGroups[groups[i]][0]); 
      d3.select(bks[i]).attr("x", xScale(timeRangeGroups[groups[i]][0]) - squareDimHalf);
      d3.select(bks[i]).attr("width", width + squareDimHalf * 2);
    }
    
    d3.selectAll("rect.svgNodeBase").attr("x", function(d){ return (xScale(d.time) - squareDimHalf); });
    d3.selectAll("rect.selected").attr("x", function(d){ return (xScale(d.time) - squareDimHalf); });
    d3.selectAll("line.connections").attr("x1", function(d){ return (xScale(d.time)); });
    d3.selectAll("line.connections").attr("x2", function(d){ return (xScale(d.time)); });
    force.start();
  }
  
  return {
    recomputePositions : function(timeRange) {
      recomputePositions(timeRange);
    }, 
    toggleTitles: function(displayTitles) {
      // Need to do more but for now just toggle
      var titleTexts = d3.selectAll(".title"); 

      if (displayTitles) {
        titleTexts.style("display", "");
      } 
      else {
        titleTexts.style("display", "none");
      }
    },
    toggleAuthors: function(displayAuthors) {
      // Need to do more but for now just toggle
      var authorTexts = d3.selectAll(".author");
      if (displayAuthors) {
        authorTexts.style("display", "");
      } 
      else {
        authorTexts.style("display", "none");
      }
    },
    getColor : function(groupName) {
      var rgba =  d3.rgb(color(groupName));
      rgba.a = colorOpacity;
      return rgba;
    },
    toggleLinks: function(elem, type, noLinks) {
      console.log('toggle links ', type);
      // Need to do more but for now just toggle
      var selector = null;
      if (type === null || type === undefined) {
        return;
      } else {
        selector = "path.link-" + type;
      }

      var links = vis.selectAll(selector);
      if (links === null || links.empty()) {
        if(elem)
        {
          if(noLinks)
          {
            $(noLinks).dialog();
          }
          return;
        }
        return;
      }

      var currentState = links.style("display");
      // console.log('current state value is ', currentState);
      // console.log('current state is ', currentState === 'none');
      if (currentState !== "none") {
        links.style("display", "none");
        if (elem === null || elem === undefined) {
          return;
        }
        
        if (type === 'buildons') {
          $(elem).html('Show Build-on');
        } else {
          $(elem).html('Show Reference');
        }
      }
      else {
        // console.log("Ok setting the display to not none");   
        links.style("display", "inline");
        if (elem === null || elem === undefined) {
          return;
        }
        if (type === 'buildons') {
          $(elem).html('Hide Build-on');
        } else {
          $(elem).html('Hide Reference');
        }
      }
      
      //draw();
    },
    updateZoomExtents: function(extents) {
      if (timelineRen) {
        timelineRen.updateZoomExtents(extents);
      }
      recomputePositions(extents);
    },
    drawAllNodeConnections : function() {
      return drawAllNodeConnections();
    },
    deselect : function() {
      deselect();
    },
    showHighlighted: function() {
      node.selectAll('rect.svgNode').classed('svgNodeHide', true);
      node.selectAll('rect.svgNode').classed('svgNode', false);
      
      
      d3.selectAll('rect.svgNodeHide').each(function(d) {
        d3.selectAll('.author').each(function(a){
          if (areNodeAndLabel(d, a.node)) {
            this.setAttribute('opacity', 0);
          }
        });
      });
      
      d3.selectAll('rect.svgNodeHighlight').each(function(d) {
        d3.selectAll('.author').each(function(a){
          if (areNodeAndLabel(d, a.node)) {
            this.setAttribute('opacity', 1);
          }
        });
      });
      
      d3.selectAll('rect.svgNodeHide').each(function(d) {
        d3.selectAll('.title').each(function(a){
          if (areNodeAndLabel(d, a.node) && d.group === a.node.group) {
            this.setAttribute('opacity', 0);
          }
        });
      });
      
      d3.selectAll('rect.svgNodeHighlight').each(function(d) {
        d3.selectAll('.title').each(function(a){
          if (areNodeAndLabel(d, a.node)) {
            this.setAttribute('opacity', 1);
          }
        });
      });
      
        
      link.each(function(d) {
       if(d.source.highlight_state === "0" || d.target.highlight_state === "0") {
         this.setAttribute('opacity', 0);
       }
      });
    },
    showAllNotes: function() {
      node.selectAll('rect.svgNodeHide').classed('svgNode', true);
      node.selectAll('rect.svgNodeHide').classed('svgNodeHide', false);
      
      d3.selectAll('.author').each(function(a){
        this.setAttribute('opacity', 1);
      });
      
      d3.selectAll('.title').each(function(a){
        this.setAttribute('opacity', 1);
      });
      
      link.each(function(d) {
        this.setAttribute('opacity', 1);
      });
    }
  };
};