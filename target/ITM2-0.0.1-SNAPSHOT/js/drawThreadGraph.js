/// View parameters
var margin = {top: 10, right: 0, bottom: 30, left: 0, offset:50},
    margin2 = {top: 0, right: 0, bottom: 20, left: 0},
    totalWidth = 930,
    totalHeight = 390, 
    topViewHeight = 330,
    width = totalWidth - margin.left - margin.right,
    topSceneHeight = totalHeight - margin.top - margin.bottom,
    topSceneBottom = topViewHeight - margin.bottom,
    bottomSceneBottom = totalHeight - topViewHeight - margin2.bottom,
    bottomSceneHeight = totalHeight - topViewHeight - margin2.bottom - margin2.top;

/// Some other necessary parameters
var parseDate = d3.time.format("%b %Y").parse;
var xOffset = 50;

/// X, Y scale
var xCurr = d3.time.scale().range([0 + xOffset, width - xOffset]),
    xBase = d3.time.scale().range([0 + xOffset, width - xOffset]),
    yCurr = d3.scale.linear().range([topSceneBottom - margin.offset, margin.top]),
    yBase = d3.scale.linear().range([topSceneBottom - margin.offset, margin.top]),
    yFocus = d3.scale.linear().range([topSceneBottom - margin.offset, margin.top]),
    yBrush = d3.scale.linear().range([bottomSceneBottom, margin2.top]);

var yBar = d3.scale.linear()
    .range([bottomSceneHeight, margin2.top]);

var xAxis = d3.svg.axis().scale(xCurr).orient("bottom"),
    xAxis2 = d3.svg.axis().scale(xBase).orient("bottom"),
    yAxis = d3.svg.axis().scale(yCurr).orient("left");

var zoom = d3.behavior.zoom()
  .on("zoom", draw);
  
var brusho = null;
var node1;
var node2;
var countByDate = {};
var dateByIndex = {};
var valueByIndex = {};  
var index = 0;
var forcedSim = 0;

/// Label acchor and its links
var nodes = [];

/// Labels for authors
var labelAnchors = [];
var labelAnchorLinks = [];

/// Labels for titles
var titleAnchors = [];
var titleAnchorLinks = [];

/// Force layout
var link;
var force = d3.layout.force()
              .size([width, topSceneHeight]);

/// Other variables
var force2, force3, anchorNode, anchorLin, titleNode, titleLink, svg = null, 
  focus = null, halfSqDim = 5;

function readData(data) {
  svg = d3.select("#right_area").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", topSceneHeight + margin.top + margin.bottom);

  var path = svg.append("defs").append("clipPath").attr("id", "clip");
  path.append("rect")
  .attr("width", width)
  .attr("height", topSceneHeight)
  .attr("fill", "blue");
  
  focus = svg.append("g")
    .attr("clip-path",  "url(#clip)")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
  var context = svg.append("g")
    .attr("transform", "translate(" + margin2.left + "," + topViewHeight + ")");
  
  focus.append("rect")
    .attr("width", "100%")
    .attr("height", "100%")
    .attr("class", "pane")
    .call(zoom);
  
  context.append("rect").attr("width", "100%").attr("height", "100%").attr("fill", "#F1FAC9");
  
  force
    .nodes(data.nodes)
    .links(data.links)
    .start();

  data.nodes.forEach(function(d) {
    console.log(d.id);
    d.date = d.time;
    d.value = 0;
    d.ran = Math.random() * 0.8;

    if (countByDate.hasOwnProperty(d.date)) {
      countByDate[d.date] += 1;
      d.value = countByDate[d.date];
    } else {
      countByDate[d.date] = 1;
      d.value = countByDate[d.date];
    }

    dateByIndex[index] = d.date;

    /// Fill in data for author lables
    labelAnchors.push({
      node : d,
      label : d.author,
      value : d.value,
      ran: d.ran,
      date: d.date
    });
    labelAnchors.push({
      node : d,
      label : d.author,
      value : d.value,
      ran: d.ran,
      date: d.date
    });
    labelAnchors.push({
      node : d,
      label : d.id,
      value : d.value,
      ran: d.ran,
      date: d.date
    });
    
    labelAnchorLinks.push({
      source : (index) * 3,
      target : (index * 3 + 1),
      weight : 1
    });
    
    /// Fill in data for title lables
//    titleAnchors.push({
//      node : d,
//      label : d.id,
//      value : d.value,
//      ran: d.ran,
//      date: d.date
//    });
    
    labelAnchorLinks.push({
      source : (index) * 3,
      target : (index * 3 + 2),
      weight : 1
    });

    console.log(labelAnchorLinks);
    index += 1;
  });
  
  /// Fill up value by index
  index = 0;
  data.nodes.forEach(function(d) {
    valueByIndex[index++] = d.value;
  });

  /// Define domains for scales
  var domain = d3.extent(data.nodes.map(function(d) { return d.date; }));
  
  if (domain[0] === domain[1]) {
    domain[1] = 2 * domain[0];
  }
  
  xCurr.domain(domain);
  yBase.domain([0, 0.9]);
  yCurr.domain(yBase.domain());
  yFocus.domain([0, 0.9]);
  yBrush.domain([0, 0.9]);

  xBase.domain(xCurr.domain());
  yBar.domain([0, d3.max(data.nodes, function(d) { return d.value; })]);

  node1 = focus.selectAll("rect.mnode")
          .data(data.nodes)
            .enter().append("svg:rect")
              .attr("noteid", function(d) { return d.noteid; })
              .attr("class","mnode")
              .attr("width", 2 * halfSqDim)
              .attr("height", 2 * halfSqDim)
              .attr("x", function(d){ return (xCurr(d.date) - halfSqDim); })
              .attr("y", function(d){ return (yBase(d.ran) - halfSqDim); });

  /// Create links
  link = focus.selectAll("line.link")
    .data(data.links)
    .enter().append("line")
      .attr("class", "link")
      .style("stroke-width", function(d) { return Math.sqrt(10); });

  link.attr("x1", function(d) { return xCurr(dateByIndex[d.source.index]); })
      .attr("y1", function(d) { return yBase(valueByIndex[d.source.index]); })
      .attr("x2", function(d) { return xCurr(dateByIndex[d.target.index]); })
      .attr("y2", function(d) { return yBase(valueByIndex[d.target.index]); });

  focus.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + topSceneBottom + ")")
    .call(xAxis);

  /// Create context nodes
  node2 = context.selectAll("circle.dot2")
          .data(data.nodes)
          .enter().append("rect")
           .attr("class","bar")
           .attr("x", function(d){ return xBase(d.date); })
           .attr("width", 4)
           .attr("y", function(d) { return yBar(d.value); })
           .attr("height", function(d) { return bottomSceneBottom - yBar(d.value); });

  context.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + bottomSceneBottom + ")")
    .call(xAxis2);

  context.append("g")
    .attr("class", "brush")
    .call(d3.svg.brush().x(xBase).y(yBrush)
    .on("brushstart", brushstart)
    .on("brush", brushmove)
    .on("brushend", brushend));

  force2 = d3.layout.force()
             .nodes(labelAnchors)
             .links(labelAnchorLinks)
             .gravity(0)
             .linkDistance(12)
             .linkStrength(8)
             .charge(-100)
             .size([width, topSceneHeight]);
  force2.start();
  
//  force3 = d3.layout.force()
//    .nodes(titleAnchors)
//    .links(titleAnchorLinks)
//    .gravity(0)
//    .linkDistance(15)
//    .linkStrength(8)
//    .charge(-150)
//    .size([width, topSceneHeight]);
//  force3.start();
  
  anchorLink = focus.selectAll("line.anchorLink")
    .data(labelAnchorLinks);

  /// Create author label nodes
  anchorNode = focus.selectAll("g.anchorNode")
    .data(force2.nodes())
    .enter()
    .append("svg:g")
    .attr("class", "anchorNode");

  anchorNode.append("svg:circle")
    .attr("r", 4);

  anchorNode.append("svg:text")
    .attr("class", function(d, i) {
      return i % 3 === 1 ? "anchorText" : "titleText";
      })
    .attr("text-anchor", "start")
    .text(function(d, i) {
      return i % 3 == 0 ? "" : d.label;
    });

  /// Create title label nodes
  titleLink = focus.selectAll("line.titleLink")
    .data(titleAnchorLinks);
  
  zoom.x(xCurr);
}

function brushstart() {
  svg.classed("selecting", true);
}

function brushend() {
  svg.classed("selecting", !d3.event.target.empty());
}

function brushmove() {
  var e = d3.event.target.extent();
  node1.classed("selected", function(d) {
    return (e[0][0] <= d.date && d.date <= e[1][0]);
  });
  node1.classed("mnode", function(d) {
      return (e[0][0] > d.date || d.date > e[1][0]);
  });

  brush();
}

function listProperties(obj) {
   var propList = "";
   for(var propName in obj) {
      if(typeof(obj[propName]) != "undefined") {
         propList += (propName + ", ");
      }
   }
}

function brush() {
  brusho = d3.event.target;

  // Set the domain of x and y (scale domain)
  xCurr.domain(brusho.empty() ? xBase.domain() : [brusho.extent()[0][0], brusho.extent()[1][0]]);
  yCurr.domain(brusho.empty() ? yBase.domain() : [brusho.extent()[0][1], brusho.extent()[1][1]]);
  yFocus.domain(brusho.empty() ? yBase.domain() : [brusho.extent()[0][1], brusho.extent()[1][1]]);
 
  recomputePositions();

  // Start the simulation again because it will trigger the tick event which
  // will re-evaluate the label positions
  forcedSim = 1;
  force.start();
}

var updateLink = function() {
  this.attr("x1", function(d) {
    return d.source.x;
  }).attr("y1", function(d) {
    return d.source.y;
  }).attr("x2", function(d) {
    return d.target.x;
  }).attr("y2", function(d) {
    return d.target.y;
  });
};

var updateNode = function() {
  this.attr("transform", function(d) {
    return "translate(" + d.x + "," + d.y + ")";
  });
};

/// On tick event start the simulation for anchor nodes
force.on("tick", function() {
  force2.start();
  
  anchorNode.each(function(d, i) {
    if (this.childNodes === undefined || this.childNodes === null ||
        this.childNodes === "") {
      console.log('child nodes are undefined');
      return;
    }
    
    /// forcedSim is ON when we brush context view 
    if(i % 3 === 0) {
      d.x = xCurr(d.node.date);
      if (forcedSim === 1) {
        d.y = yFocus(d.node.ran);
      } else {
        d.y = yBase(d.node.ran);
      }

    } else {
      var b = this.childNodes[1].getBBox();
      
      var delX, delY;
      
      /// forcedSim is ON when we brush context view
      if (forcedSim === 1) {
        delY = yFocus(d.node.ran);
      } else {
        delY = yBase(d.node.ran);       
      }
      
      delX= xCurr(d.node.date);
      
      var diffX = d.x - delX;
      var diffY = d.y - delY;

      var dist = Math.sqrt(diffX * diffX + diffY * diffY);

      var shiftX = b.width * (diffX - dist) / (dist * 2);
      shiftX = Math.max(-b.width, Math.min(0, shiftX));
      var shiftY = 5; 
      
      /// Update both the circle and the text
      //this.childNodes[0].setAttribute("transform", "translate(" + shiftX + "," + shiftY + ")");
      this.childNodes[1].setAttribute("transform", "translate(" + shiftX + "," + shiftY + ")");
    }
  });

  anchorNode.call(updateNode);
  anchorLink.call(updateLink);
});


function draw() {
  if (brusho != null) {
    d3.select(".brush").call(brusho.clear());
  }
  recomputePositions();
  forcedSim = 1;
  force.start();
}

function recomputePositions() {
  focus.selectAll("line.link").attr("x1", function(d) { return xCurr(dateByIndex[d.source.index]); });
  focus.selectAll("line.link").attr("y1", function(d) { return yFocus(valueByIndex[d.source.index]); });
  focus.selectAll("line.link").attr("x2", function(d) { return xCurr(dateByIndex[d.target.index]); });
  focus.selectAll("line.link").attr("y2", function(d) { return yFocus(valueByIndex[d.target.index]); });

  // Re-evaluate the center of nodes in this new domain
  focus.selectAll("rect.mnode").attr("x", function(d) { return (xCurr(d.date) - halfSqDim); });
  focus.selectAll("rect.mnode").attr("y", function(d) { return (yFocus(d.ran) - halfSqDim); });

  focus.selectAll("rect.selected").attr("x", function(d) { return (xCurr(d.date) - halfSqDim); });
  focus.selectAll("rect.selected").attr("y", function(d) { return (yFocus(d.ran) - halfSqDim); });

  // Kind of same of the tick labels
  focus.select(".x.axis").call(xAxis);
}

/** 
 * Toggle display of author labels
 * 
 * @param elem
 */
function toggleAuthors(elem) {
  // Need to do more but for now just toggle
  var anchorTexts = d3.selectAll(".anchorText");
  var currentState = anchorTexts.style("display");
  if (currentState !== "none") {
    anchorTexts.style("display", "none");
    if (elem === null || elem === undefined) {
      return;
    }
    
    elem.innerHTML = 'Show Author';
  }
  else {
    anchorTexts.style("display", "");
    if (elem === null || elem === undefined) {
      return;
    }
    elem.innerHTML = 'Hide Author';
  }
  
  draw();
}

/** 
 * Toggle display of titles
 * 
 * @param elem
 */
function toggleTitles(elem) {
  
  // Need to do more but for now just toggle
  var titleTexts = d3.selectAll(".titleText");
  var currentState = titleTexts.style("display");
  if (currentState !== "none") {
    titleTexts.style("display", "none");
    if (elem === null || elem === undefined) {
      return;
    }
    
    elem.innerHTML = 'Show Title';
  }
  else {
    titleTexts.style("display", "");
    if (elem === null || elem === undefined) {
      return;
    }
    elem.innerHTML = 'Hide Title';
  }
  
  draw();
}
/* *
 * Highlight notes
 */
function toggleHighlightNodes() {
  return;
}

/** 
 * 
 * @param event
 */
function toggleSelectNode(item) {
  if (item === null) {
    return;
  }
  
  var selectedNodes = d3.selectAll(".mnode").filter(function(d, i) { return (d.noteid === item.getAttribute("noteid")); });
  if ( selectedNodes !== null && selectedNodes.length === 1 && selectedNodes !== "") {
    if ((selectedNodes).attr("class") === "selected") {
      (selectedNodes).classed("selected", false); 
    }
    else {
      d3.selectAll(".selected").classed("selected", false);
      (selectedNodes).classed("selected", true);
    }
  }
}
