<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM2/index.jsp");
		}
	}
%>
<html>
<%
	//get the parameters
	String strdb = request.getParameter("database");
	String strproject = request.getParameter("projectname");
	String strthread = request.getParameter("threadfocus");
	
	sqls s = new sqls();
	Statement stmt = s.Connect(strdb);
	//ResultSet rs = null;
	String strsql_pro = null,strsql_note=null,strsql_more=null,strsql_idea=null,strsql_problem=null;
	String strid = null;
	Operatedb opdb = new Operatedb(s,strdb);
	
	//get the projectid
	ResultSet temprs=null;
	temprs=opdb.GetRecordsFromDB("project","idproject","projectname='"+strproject+"';");
	temprs.next();
	strid = temprs.getString(1);
	
	if(temprs != null){
		try{
			temprs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	
	strsql_pro = "DELETE FROM project_thread WHERE projectid="+strid+" AND threadfocus='"+strthread+"'";
	strsql_note = "DELETE FROM thread_note WHERE projectid="+strid+" AND threadfocus='"+strthread+"'";
	strsql_more = "DELETE FROM thread_more WHERE projectid="+strid+" AND threadfocus_more='"+strthread+"'";
	strsql_idea = "DELETE FROM thread_idea WHERE projectid="+strid+" AND threadfocus_idea='"+strthread+"'";
	strsql_problem = "DELETE FROM thread_problem WHERE projectid="+strid+" AND threadfocus_problem='"+strthread+"'";
	//System.out.println(strsql_pro);
	try{
		stmt.executeUpdate(strsql_pro);
		stmt.executeUpdate(strsql_note);
		stmt.executeUpdate(strsql_more);
		stmt.executeUpdate(strsql_idea);
		stmt.executeUpdate(strsql_problem);
	}catch(SQLException e){
		e.printStackTrace();
	}finally{
		if(opdb != null){
			opdb.CloseCon();
		}
		
		if(stmt != null){
			try{
				stmt.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		
		if(s != null){
			s.Close();
		}
	}
	

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

</body>
</html>