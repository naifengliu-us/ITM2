<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page
	import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*"%>

<%@ page import="itm.models.NoteModel"%>
<%@ page import="itm.controllers.NoteController"%>
<%@ page import="edu.cmu.side.recipe.ITMPredictor"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	//request.setCharacterEncoding("UTF-8"); 
	/************validate the user session*******************/
	String username = " ";
	String usertype = " ";
	String usertype2="";
	session = request.getSession(false);
	if (session != null) {
		if (session.getAttribute("username") == null) {
			response.sendRedirect("/ITM2/index.jsp");
		} else {
			username = (String) session.getAttribute("username");
			usertype = (String) session.getAttribute("usertype");
			usertype2 = (String) session.getAttribute("usertype2");
		}
	}
	String projectn = null;
	if (session.getAttribute("project") != null) {
		projectn = session.getAttribute("project").toString();
	} else if (request.getParameter("projectname") != null) {
		projectn = request.getParameter("projectname").toString();
	}
	String threadName;
	if (request.getParameter("threadfocus") == null) {
		threadName = request.getParameter("threadfocus");
	} else {
		threadName = request.getParameter("threadfocus");
	}
	String database = session.getAttribute("database").toString();
	if (session.getAttribute("par") == null) {
		ParameterObj parameterObj = new ParameterObj(database, projectn, threadName);
		session.setAttribute("par", parameterObj);
		int count = 1;
		session.setAttribute("count", count);
	} else {
		int watchdog = 1;
		if (session.getAttribute("count") == null)
			watchdog = 1;
		else
			watchdog = Integer.parseInt(session.getAttribute("count").toString());
		watchdog++;
		if (watchdog > 2) {
			ParameterObj parameterObj = (ParameterObj) session.getAttribute("par");

			parameterObj.setThreadName(threadName);
			session.setAttribute("par", parameterObj);
		}
		session.setAttribute("count", watchdog);
	}
%>
<html>
<head>
<title>Thread <%=threadName%> in Project <%=projectn%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="../css/stickyhead.css" rel="stylesheet" type="text/css">
<link href="../css/screenLock.css" rel="stylesheet" type="text/css">
<link href="../css/itm_week_beta.css" rel="stylesheet" type="text/css">
<link REL=StyleSheet HREF="../css/btn_link.css" TYPE="text/css">
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="../css/jquery.qtip.min.css">
<link href="../css/thread.css" rel="stylesheet" type="text/css">
<link href="../css/impress.css" rel="stylesheet" type="text/css">

<style type="text/css">
/* A class used by the jQuery UI CSS framework for their dialogs. */
.ui-front {
	z-index: 10000 !important;
	/* The default is 100. !important overrides the default. */
}
</style>

<script src="../js/en/jquery.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://d3js.org/d3.v3.min.js" type="text/javascript"></script>
<script src="../js/en/jquery.qtip.min.js" type="text/javascript"></script>
<script src="../js/en/drawThreadGraph.js" type="text/javascript"></script>
<script src="../js/en/threadHistory.js" type="text/javascript"></script>
<script src="../js/en/barChart.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$('br').remove();
	$(/&nbsp;/g).remove();
	var had=0;
	$('#myTable').hide();
	function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
                'href': csvData,
                'target': '_blank'
        });
    }

	
$('a[href="#showLightSide"]').click(function(){
	//alert('12')
	//alert($('#raw_area .rawnote').attr('noteid'));
	var node='';
	var title='';
	var content='';
	var finaltext='';
	var view='';
	var date='';
	var author='';
	$('#raw_area .rawnote').each(function(i)
	{
   		node=node+$(this).attr('noteid')+','; // This is your rel value
   		title=title+$(this).find('.rawtitle').text()+',';
   		content=content+$(this).find('.rawcontent').text().replace(/&nbsp;/g, '').replace(/[^a-zA-Z0-9 -?!.]/g, '')+',';
   		finaltext=finaltext+$(this).attr('noteid')+','+$(this).find('.rawtitle').text()+','+$(this).find('.rawcontent').text().replace(/&nbsp;/g, '').replace(/[^a-zA-Z0-9 -?!.]/g, '')+'\n';

	});
	
    //alert("1");
   // alert(title);
   // alert(content);
	if(had==0){
		
		

	$.ajax({
        url : '/ITM2/LightSideServlet', // Your Servlet mapping or JSP(not suggested)
        //data:{node,title,content},
        data:{finaltext: finaltext},
        type : 'POST',
        dataType : 'html', // Returns HTML as plain text; included script tags are evaluated when inserted in the DOM.
        success : function(response) {
           //alert("run LightSide Success");
           $.get("../LightSideServlet", function(responseText) {   // Execute Ajax GET request on URL of "someservlet" and execute the following function with Ajax response text...
               //$("#somediv").text(responseText);           // Locate HTML DOM element with ID "somediv" and set its text content with the response text.
           	var textget=responseText;
               //alert(responseText);
           	var rows = textget.split(";!;");
       		//alert(rows[0]);
       		//alert(rows[1]);
           	for (var i = 0; i < rows.length; i++)
           	    rows[i] = rows[i].split(",!,");

           	//alert("2");
           	$('#list_area li').each(function(i)
           		{
           			var j =0;
           			while(rows[j][4]!=$(this).attr('noteid'))
           				j++;
           			//alert(j);
           				if(rows[j][0]==1){
           					$(this).text('[Evidence] '+$(this).text());
           					
           					a=1;
           				}
           				if(rows[j][1]==1){
           					$(this).text('[Explaining] '+$(this).text());
           					
           					b=1;
           				}
           				if(rows[j][2]==1){
           					$(this).text('[Questioning] '+$(this).text());
           					c=1;
           				}
           				if(rows[j][3]==1){
           					$(this).text('[Referencing] '+$(this).text());
           					
           					d=1;
           				}
           				$(this).html($(this).html().replace("[Evidence]", "<span class='blue'>•</span>"));
           				$(this).html($(this).html().replace("[Explaining]", "<span class='red'>•</span>"));
       					$(this).html($(this).html().replace("[Questioning]", "<span class='purple'>•</span>"));
           				$(this).html($(this).html().replace("[Referencing]", "<span class='green'>•</span>"));
           				//$('#myTable').append('<tr><td>' +a+'</td><td>' +b+'</td><td>' +c+'</td><td>' +d+'</td><td>' +i+'</td><td>');
           		   		//if($(this).attr('noteid')%2==0){
           		   		//	$(this).text('[Question] '+$(this).text());
           		   		//}
           		});
           	//alert("3");
           	$('#raw_area .rawnote').each(function(i)
           			{
		           		var j =0;
		           		var a =0;
		           		var b =0;
		           		var c =0;
		           		var d =0;
		       			while(rows[j][4]!=$(this).attr('noteid'))
		       				j++;
		       			//alert(j);
		       				if(rows[j][0]==1){
		       					a=1;
		       				}
		       				if(rows[j][1]==1){
		       					b=1;
		       				}
		       				if(rows[j][2]==1){
		       					c=1;
		       				}
		       				if(rows[j][3]==1){
		       					d=1;
		       				}
           		   		node=$(this).attr('noteid'); // This is your rel value
           		   		title=$(this).find('.rawtitle').text();
           		   		content=$(this).find('.rawcontent').text().replace(/&nbsp;/g, '').replace(/[^a-zA-Z0-9 -?!.]/g, '');
           		   		author=$(this).attr('author').replace(/&nbsp;/g, '').replace(/[^a-zA-Z0-9 -?!.]/g, '');
           		   		view=$(this).attr('view');
           		   		date=$(this).attr('date');
           		   		$('#myTable').append('<tr><td>' +a+'</td><td>' +b+'</td><td>' +c+'</td><td>' +d+'</td><td>' +node+'</td><td>' +author+'</td><td>' +view+'</td><td>' +date+'</td><td>' +title+'</td><td>' +content+'</td></tr>');
           			});
           	//alert(rows[0][3]);
           	
           });
        },
        error : function(request, textStatus, errorThrown) {
        	alert(errorThrown);
        }
    });



	
	//$("#list_area li:contains('[Question]')").css('color','yellow');
	}
	had=1;
	});
$('a[href="#download"]').on('click', function (event) {
	if(had==1){
	exportTableToCSV.apply(this, [$('#myTable'), 'export.csv']);}
	else if(had==0){
		alert("please click 'Show Contribution Types' first");
	}
});

});	
</script>
<script language="javascript" type="text/javascript">
  <%String[] strid = null;
			//String database=request.getParameter("database");
			//String projectn=request.getParameter("projectname");
			Operatedb opdb_author = new Operatedb(new sqls(), database);
			AccessToProjects atp = new AccessToProjects();
			System.out.println("projectname");
			boolean canEditThread = atp.canEdit(username, usertype, projectn, database);
			String nidts_str = request.getParameter("nidts");
			if (nidts_str != null) {
				nidts_str = nidts_str.replaceAll(" ", "&#32;");
			}

			//count distinct authorid's
			sqls st = new sqls();
			Operatedb opdb_ct = new Operatedb(st, request.getParameter("database"));
			ResultSet project_id = opdb_ct.getC()
					.executeQuery("select idProject from project where projectname='" + projectn + "'");
			int pid = 0;
			if (project_id.next()) {
				pid = project_id.getInt(1);
			}

			if (project_id != null) {
				try {
					project_id.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			ResultSet tars = opdb_ct.getC().executeQuery(
					"select count(distinct authorid) from  thread_note inner join  author_note on  thread_note.noteid =  author_note.noteid and  thread_note.threadfocus='"
							+ threadName + "'and thread_note.projectid='" + pid + "';");
			String authorcnt = null;
			if (tars.next()) {
				authorcnt = tars.getString(1);
			}

			if (tars != null) {
				try {
					tars.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			String thread_owner = null;
			int deleted = 0;
			//Access thread_owner from the database
			sqls s0 = new sqls();
			Operatedb opdb0 = new Operatedb(s0, request.getParameter("database"));
			ResultSet rs0 = opdb0.getC().executeQuery("select author,deleted from project_thread where threadfocus='"
					+ threadName + "'and projectid=" + pid + ";");
			if (rs0.next()) {
				thread_owner = rs0.getString(1);
				deleted = rs0.getInt(2);
			}

			//Set the boolean value for thread
			boolean isThreadDeleted = false;

			if (deleted == 1) {
				isThreadDeleted = true;
			}

			if (rs0 != null) {
				try {
					rs0.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			if (opdb0 != null) {
				opdb0.CloseCon();
			}

			if (s0 != null) {
				s0.Close();
			}%>
  
  var _projectname = "<%=projectn%>";
  var _threadfocus = "<%=threadName%>";
  var _database = "<%=database%>";
  var _nitds = "<%=request.getParameter("nidts")%>";
  var thread_owner = "<%=thread_owner%>";
  var username = "<%=username%>";
  var usertype = "<%=usertype%>";
  var usertype2 = "<%=usertype2%>";
  var projectid = "<%=pid%>";
  var authorcnt = "<%=authorcnt%>";
  var max_range = null;
  var min_range = null;
  
  //Check the status of thread if its deleted 
  function UpdateDeleteRestoreStatus(IsThreadDeleted)
  {
    //alert("Update delete restore called");
    var elem = document.getElementById("delete_thread");
    //var isDeleted = IsProjectDeleted;
    if(IsThreadDeleted == "true")
    {
      elem.innerHTML = 'Restore Thread';
    }
    else
    {
      elem.innerHTML = 'Delete Thread';
    }
  }
  
  function UpdateDeleteRestoreThread()
  {
     var elem = document.getElementById("delete_thread");
     if(elem.innerHTML == "Delete Thread")
       {
          ShowDeleteThread();
       }
       else
       {
         ShowRestoreThread();
       }
  }
  
  /*Control thread updates or creation*/
  function UpdateThreadControl(canEditThread)
  {
  	var selt = document.getElementById("rename_thread");
  	var self = document.getElementById("find_more_notes");
  	//var selj = document.getElementById("journey_of_thinking");
  	var seld = document.getElementById("delete_thread");
  	var selr = document.getElementById("remove_note");
  	var selh = document.getElementById("hightlight_note");
  	if(!(usertype === "manager" || usertype === "editor")&&usertype2=="1")
  	{
  		console.log("[DEBUG] User does not has create new thread permissions");
  		
  		// Disable create functionality
  		selt.removeAttribute("href");
  		selt.removeAttribute("onclick");
  		selt.setAttribute("id", "rename_thread");
  		selt.style.color  = "gray";
  		
  		self.removeAttribute("href");
  		self.removeAttribute("onclick");
  		self.setAttribute("id", "find_more_notes");
  		self.style.color  = "gray";
  		if(canEditThread == "false" ){
  		//selj.removeAttribute("href");
  		seld.removeAttribute("href");
  		seld.removeAttribute("onclick");
  		seld.setAttribute("id", "delete_thread");
  		seld.style.color  = "gray";
  		}
  		if(selr != null)
  		{
  		   selr.removeAttribute("href");
  		   selr.removeAttribute("onclick");
  		   selr.setAttribute("id", "remove_note");
  		   selr.style.color  = "gray";
  		}
  		if(selh != null)
  		{
  		  selh.removeAttribute("href");
  		  selh.removeAttribute("onclick");
  		  selh.setAttribute("id", "highlight_note");
  		  selh.style.color  = "gray";
  		}	
  	}
  	
  }
  
  //Grey out show buildon/hide buildon when show highlighted buildon is selected.
  function greyOutSomeFeatures(elem) {
    //return; 
    var elem_clicked = elem;
    var h = document.getElementById("showallhighlightedbuildons");
    var sb = document.getElementById("showalllinkbuildon");
    var sr = document.getElementById("showalllinkreferences");
    var sh = document.getElementById("showallhighlighted");
    
    //If the Hide build on is turned on then first reset to show build on
    
    if(sb.innerHTML === "Hide Build-on") {
      tg.toggleLinks('#showalllinkbuildon', 'buildons', $('#alertDialog'));
    }
    
    if(sb.innerHTML === "Hide Reference") {
      tg.toggleLinks('#showalllinkreferences', 'references', $('#alertDialog'));
    }
    
    
    if(h.innerHTML === "Show Highlighted Buildons" && sh.innerHTML === "Show Highlighted Notes") {
       //do this;
       // Store the initial value
       
      sessionStorage.myvalue1_sb =  sb.getAttribute("href");
      sessionStorage.myvalue2_sb =  sb.getAttribute("onclick");
      
    /*  sessionStorage.myvalue1_bldons =  bldons.getAttribute("href");
      sessionStorage.myvalue2_bldons =  bldons.getAttribute("onclick");*/
         
      sessionStorage.myvalue1_sr =  sr.getAttribute("href");
      sessionStorage.myvalue2_sr =  sr.getAttribute("onclick");
      
      
      sb.removeAttribute("href");
      sb.removeAttribute("onclick");
      sb.style.color  = "gray";
      
      sr.removeAttribute("href");
      sr.removeAttribute("onclick");
      sr.style.color  = "gray";
    } 
    else if(h.innerHTML === "Hide Highlighted Buildons" && sh.innerHTML === "Show Highlighted Notes" && elem_clicked ==="HighlightedNotes") {
      //do nothing
    }
    else if(h.innerHTML ==="Show Highlighted Buildons" && sh.innerHTML ==="Show All Notes" && elem_clicked ==="HighlightedBuildons") {
      //do nothing
    }
    else if((h.innerHTML === "Hide Highlighted Buildons" && sh.innerHTML ==="Show All Notes") && (elem_clicked ==="HighlightedNotes" || elem_clicked ==="HighlightedBuildons")) {
      //do nothing
    }
    else if(h.innerHTML === "Hide Highlighted Buildons" && sh.innerHTML === "Show Highlighted Notes" && elem_clicked === "HighlightedBuildons") {
	      if (sessionStorage.myvalue1_sb) {
	        sb.setAttribute("href", sessionStorage.myvalue1_sb);
	        sb.setAttribute("onclick", sessionStorage.myvalue2_sb);
	        sb.setAttribute("style", " ");
	      }
	      if (sessionStorage.myvalue1_sr) {
	        sr.setAttribute("href", sessionStorage.myvalue1_sr);
	        sr.setAttribute("onclick", sessionStorage.myvalue2_sr);
	        sr.setAttribute("style", " ");
      }
    } 
    else if(h.innerHTML === "Show Highlighted Buildons"  && sh.innerHTML === "Show All Notes" && elem_clicked === "HighlightedNotes") {
      if (sessionStorage.myvalue1_sb) {
        sb.setAttribute("href", sessionStorage.myvalue1_sb);
        sb.setAttribute("onclick", sessionStorage.myvalue2_sb);
        sb.setAttribute("style", " ");
      }
      if (sessionStorage.myvalue1_sr) {
        sr.setAttribute("href", sessionStorage.myvalue1_sr);
        sr.setAttribute("onclick", sessionStorage.myvalue2_sr);
        sr.setAttribute("style", " ");
      }
    }
  }

</script>
<link href="../css/drawThreadGraph.css" rel="stylesheet" type="text/css">
<script src="../js/en/thread_update.js"></script>
<script src="../js/en/drawThreadGraph.js"></script>
<script src="../js/en/itm_week_beta.js"></script>
<script src="../js/en/screenLock.js"></script>

<script language="javascript" type="text/javascript">

if("yes"=="<%=request.getParameter("ifexist")%>"){
	alert("the thread already exists! please change the focus");
}

function SumWin() {	
  window.open('ThreadSum.jsp?database=<%=database%>&threadfocus=<%=request.getParameter("threadfocus")%>&projectname=<%=request.getParameter("projectname")%>','sumwin'); 
}

function onCreate() {
  if (document.getElementById("input_threadfocus").value=="") {
    window.alert('Please input the focus!' );
    return false;
  }
  document.forms["createThreadForm"].submit();
}

function onCreateThread() {
  if (document.getElementById("createThreadForm").value=="") {
	window.alert('Please input the focus!' );
	return false;
  }
  document.forms["createThreadForm"].submit();
}

function OnFind() {
  if (document.getElementById("input_threadfocus").value=="") {
    window.alert('Please input the focus!' );
	return false;
  }
  var thread_focus = document.getElementById("input_threadfocus").value;
  var dataBase = "<%=request.getParameter("database")%>";
  $.get('ViewHashtagServlet',{"database": dataBase, "input_threadfocus": thread_focus},
			function(responseText) { // on success
	  				alert(responseText);
				if(responseText.equals(""))
					{
						alert("This thread name already exists. Please enter a different name.");
						//return false;
						window.location="thread.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>";
					}
				
			})
			 .fail(function() { //on failure
				 
			});
  top.window.location="thread_action.jsp?database=<%=request.getParameter("database")%>&projectname=<%=request.getParameter("projectname")%>&threadfocus="+document.getElementById("input_threadfocus").value;
}

function OnFindMore() {  
  OnSave();        
  try {
    top.buttomframe2.location="SearchMore.jsp?database=${par.database}&projectname=${par.projectName}&threadfocus=${par.threadName}";
    top.document.getElementById("framesetter").rows = "60px,*" 
   } catch(e){
     top.buttomframe1.location="SearchMore.jsp?database=${par.database}&projectname=${par.projectName}&threadfocus=${par.threadName}";
     top.document.getElementById("framesetter").rows = "60px,*" 
   }
}

</script>
<script>
            $(document).ready(function(){
                $('#linkSuggest a').click(function(){
                    $('#td_name').css({
                        'overflow-y': 'auto',
                        'height': '620px', 
                        'width': '1024px'
                    });
                    var url=$(this).attr('href');                    
                    $('#td_name').load(url);                    
                    return false;
                });//end click
                
            });
        </script>
</head>

<%
	NoteModel noteobj = new NoteModel(database);
	NoteController nc = new NoteController(database);
	String isThreadChanged = request.getParameter("threadchanged");
%>
<body class="impress-not-supported"
	onload="render('<%=isThreadChanged%>');UpdateThreadControl('<%=canEditThread%>');UpdateDeleteRestoreStatus('<%=isThreadDeleted%>')">
	<!-- alert for no links available -->
	<div id="alertDialog" style="display: none;">
		<h5>
			<center>No Links Available</center>
			</center>
		</h5>
	</div>
	<div id="alertNoNoteDialog" style="display: none;">
		<h5>
			<center>None Note Selected</center>
			</center>
		</h5>
	</div>

	<div id="sticky">
		<div id="header">
			<table border="0">
				<tr>
					<td><img src="../img/itmlogo100x100.gif" alt="ITM2 LOGO"
						height="60" width="60" /></td>
					<td
						style="background-color: #01B0F1; color: yellow; font-family: Arial, Helvetica, sans-serif;"
						height="60" width="870">
						<%
							if (request.getParameter("recommend") != null) {
						%> &nbsp;&nbsp;&nbsp;<font size="5">Recommend Idea Thread</font><br>
						<font size="3">&nbsp;&nbsp;&nbsp;Project: <%=projectn%></font> <%
 	} else if (request.getParameter("ifexist") == null && request.getParameter("threadfocus") == null) {
 %> &nbsp;&nbsp;&nbsp;<font size="5">Add an Idea Thread for
							Project: <%=request.getParameter("projectname")%></font> <%
 	} else if (request.getParameter("ifexist") != null && request.getParameter("threadfocus") != null) {
 %> &nbsp;&nbsp;&nbsp;<font size="5">Add an Idea Thread for
							Project: <%=request.getParameter("projectname")%></font> <%
 	} else {
 %> &nbsp;&nbsp;&nbsp;<font size="5">Idea Thread: <%=threadName%></font><br>
						<font size="3">&nbsp;&nbsp;&nbsp;Project: <%=projectn%></font> <%
 	}
 %>

					</td>
				</tr>
			</table>
			<div id=td_info>
				<div id="td_name">
					<%
						if (request.getParameter("ifexist") != null && request.getParameter("threadfocus") != null) {
					%>
					Enter Thread Name (Focus):
					<form>
						<input class="fields" name="thread_focus" id="input_threadfocus"
							type="text" size="25"
							value="<%=request.getParameter("threadfocus")%>" /> <a href="#"
							onclick="OnFind()">OK</a>
					</form>
					<%
						} else if (request.getParameter("ifexist") == null && request.getParameter("threadfocus") == null) {
					%>
					You can add an idea thread in one of the following two ways:
						
					<form id="createThreadForm"
						action="../addThreadServlet?database=<%=database%>" method="post">
						
						<a href="New_HomePage.jsp?database=<%=database%>&projectname=<%=projectn%>"
							onclick="window.close();" class="close" style="float: right;">Cancel</a>
							<br><br>
						<Div style="display:block;">
						<br><br><br>
						1 .You may enter a thread topic (focus) and find related notes manually:<br>
						<input class="fields" id="input_threadfocus" name="thread_focus"
							type="text" size="25" value=""
							onkeydown="new_thread_searchKeyPress(event);" /> <a href="#"
							onclick="OnFind()" class="close">OK</a> 
							</Div>
							<br><br><br>
							<Div style="display:block;">2. Or you can ask ITM to recommend possible topics and related notes.<a
							href="thread_recommend.jsp?recommend=yes&database=<%=database%>&projectname=<%=projectn%>&threadfocus=Recommend"
							class="close">Recommend</a></Div>
					</form>
					<h4 id="linkSuggest">
						<!--  <a href="../GetViewServlet">Search Suggest Thread Names</a> -->
					</h4>
					<!--        Stan-->
					<%
						} else if (request.getParameter("ifexist") == null && request.getParameter("threadfocus") != null) {
					%>

				</div>
				<span id="ajax_state"></span>
				<div id="button">
					<ul id="thread_sddm">
						<%
							if (!request.getParameter("getvalue").equals("@")) {
						%>
						<li><a href="#" onclick="OnFindMore()" id="find_more_notes">Find
								More Notes</a></li>
						<%
							}
						%>
						<li><a href="#" onclick="SumWin()" id="journey_of_thinking">Journey
								of Thinking</a></li>
						<li><a href="#" onclick="ShowRenameThread()"
							id="rename_thread">Rename Thread</a></li>
						<li><a href="#" onclick="UpdateDeleteRestoreThread()"
							id="delete_thread">Delete Thread</a></li>
						<li><a href="#">Thread History</a>
							<div id="thread_history_options" class="dropdown"></div></li>
						<li><a href="#" onclick="OnClose()">Save and Close</a></li>
					</ul>
				</div>
				<%
					}
				%>
			</div>

		</div>
	</div>
	<div id="wrapperu">
		<div id="wrapper">

			<!--------------------------------display the individual thread map----------------------------------------->
			<%
				String getvalue = request.getParameter("getvalue");
				System.out.println(getvalue);
				//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   "+getvalue);
				if ((getvalue != null) && !(getvalue.equals("null")) && !(getvalue.equals("@"))) {
					//System.out.println(request.getParameter("getvalue"));
					String[] strvalue = request.getParameter("getvalue").split("&");
					strid = strvalue[0].split("@");
					String strcon = "( note_table.noteid = ";
					String threadNoteQueryStr = "thread_note.noteid =";

					int[] intid = new int[3000]; //this limits the number of id is 1000 at most!
					int ival;

					// Get the id of selected notes 
					for (int i = 1; i < strid.length; i++) { // Be careful, the index starts with "1" here!!
						intid[i] = Integer.valueOf(strid[i]).intValue(); //change the id value from string to int
						if (i < strid.length - 1) {
							strcon += intid[i] + " or note_table.noteid = ";
						} else {
							strcon += intid[i] + ")";
						}

						// System.out.println("thread.jsp : the selected note's id is "+intid[i]);
					}

					for (int i = 1; i < strid.length; i++) { // Be careful, the index starts with "1" here!!
						if (i < strid.length - 1) {
							//threadNoteQueryStr += intid[i]+ " and thread_note.noteid = ";	
							threadNoteQueryStr += intid[i] + " or thread_note.noteid = ";
						} else {
							threadNoteQueryStr += intid[i] + "";
						}

						// System.out.println("thread.jsp : the selected note's id is "+intid[i]);
					}

					//get the notes from database
					sqls s = new sqls();
					Operatedb opdb = new Operatedb(s, database);

					//using hashtable to store the note's relation
					ResultSet noters;
					String strtemp;
					String strtype = null;
					Hashtable<Integer, Vector<Integer>> butable = new Hashtable<Integer, Vector<Integer>>();
					Hashtable<Integer, Vector<Integer>> antable = new Hashtable<Integer, Vector<Integer>>();
					Hashtable<Integer, Vector<Integer>> retable = new Hashtable<Integer, Vector<Integer>>();

					for (int i = 1; i < strid.length; i++) {
						//query the database				
						//System.out.println("the length is "+strid.length);
						//System.out.println("the i value is "+i);
						//System.out.println("the intid is "+intid[i]);
						strtemp = "fromnoteid = " + intid[i];

						//System.out.println("the strcon is "+strcon);
						noters = opdb.GetRecordsFromDB("note_note", "linktype,tonoteid", strtemp);

						//add to hash table
						Vector<Integer> anvec = new Vector<Integer>();
						Vector<Integer> buvec = new Vector<Integer>();
						Vector<Integer> revec = new Vector<Integer>();

						while (noters.next()) {
							strtype = noters.getString(1);
							//System.out.println("note: "+intid[i]+"  "+ strtype+" note: "+noters.getInt(2));
							if (strtype.equals("annotates")) {
								anvec.add(noters.getInt(2));
							} else if (strtype.equals("buildson")) {
								buvec.add(noters.getInt(2));
							} else if (strtype.equals("references")) {
								revec.add(noters.getInt(2));
							}
						}

						if (noters != null) {
							try {
								noters.close();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
						antable.put(intid[i], anvec);
						butable.put(intid[i], buvec);
						retable.put(intid[i], revec);

						//System.out.println("note "+intid[i]+"'s anvec's size is "+ anvec.size());
						//System.out.println("note "+intid[i]+"'s buvec's size is "+ buvec.size());
						//System.out.println("note "+intid[i]+"'s revec's size is "+ revec.size());
					}
					Vector<Integer> vec;

					String[] namearray = new String[20];

					namearray[0] = "thread_note";
					String threadFocusName = request.getParameter("threadfocus");
					strtemp = "thread_note.threadfocus='" + threadFocusName + "'";

					// Variables used for query
					ResultSet disrs = null;
					boolean threadIsPopulated = false;
					int numberOfTables = 5;
					String colNames = "note_table.noteid,notetitle,notecontent,createtime,firstname,lastname,view_table.title";

					disrs = opdb.MulGetRecordsFromDB(namearray, "projectid,threadfocus,noteid,highlight", strtemp, 1);

					//System.out.println("[DEBUG] Query executed for check is: " + strtemp);  
					// Store state of highlight 
					Hashtable<Integer, Integer> idToHighlightMap = new Hashtable<Integer, Integer>();

					if (disrs != null && disrs.next()) {
						int rowcount = 0;

						do {
							idToHighlightMap.put(disrs.getInt(3), disrs.getInt(4));
							++rowcount;
						} while (disrs.next());

						// Assuming that ids are same, we could do a better job here where we go id by id
						if (rowcount == (strid.length - 1)) {
							System.out.println("thread_note is already populated");
							threadIsPopulated = true;
						}
					}

					// Check if incoming ids are contained in the hashtable, if they do, do nothing, if not
					// then insert them with highlight state being 0 since we don't know anything about them (they are new)
					for (int i = 0; i < intid.length; ++i) {
						if (idToHighlightMap.containsKey(intid[i])) {
							// Do nothing since we already know about the state of highlight for this note id
						} else {
							idToHighlightMap.put(intid[i], 0);
						}
					}

					namearray[0] = "note_table";
					namearray[1] = "author_table";
					namearray[2] = "author_note";
					namearray[3] = "view_table";
					namearray[4] = "view_note";

					//strtemp = strcon + " AND note_table.noteid=author_note.noteid AND author_note.authorid=author_table.authorid AND note_table.noteid=view_note.noteid AND view_note.viewid=view_table.idview";
					strtemp = strcon
							+ " AND note_table.noteid=author_note.noteid AND author_note.authorid=author_table.authorid AND note_table.noteid=view_note.noteid AND view_note.viewid=view_table.idview";
					if (threadIsPopulated) {
						namearray[5] = "thread_note";
						strtemp += " AND thread_note.noteid = note_table.noteid";
						colNames += ",thread_note.highlight";
						numberOfTables = 6;
					}
					strtemp += " GROUP BY note_table.noteid";
					//System.out.println("Query executed is: " + strtemp);

					// Check first if the thread_note alread contains entry entry for this note, if it does, then form a query 
					// that includes the thread_note or else, just use the query below.		
					disrs = opdb.MulGetRecordsFromDB(namearray, colNames, strtemp, numberOfTables);
			%>
			<div id="fm">
				<div id="threadinfo"></div>

				<ul id="flag">
					<li id="showalltitle">&nbsp;|&nbsp;<a
						href="javascript:void(0)" onclick="tg.toggleTitles(this)";>Show
							Title</a>&nbsp;|&nbsp;
					</li>
					<li id="showallauthor"><a href="javascript:void(0)"
						onclick="tg.toggleAuthors(this)">Show Author</a>&nbsp;|&nbsp;</li>
					<li id="showalllink"><a id="showalllinkbuildon"
						href="javascript:void(0)"
						onclick="tg.toggleLinks(this, 'buildons', $('#alertDialog'));">Show
							Build-on</a>&nbsp;|&nbsp; <a id="showalllinkreferences"
						href="javascript:void(0)"
						onclick="tg.toggleLinks(this, 'references', $('#alertDialog'))">Show
							Reference</a>&nbsp;|&nbsp;</li>
				</ul>

			</div>
			<div id="up_area">
				<div id="wrapper_draw">
					<div id="right_area">
						<!-- <div id="draw_area" ondblclick="redraw()" onclick="drawnoteclick(event)"></div>
		     <div id="reference_area"></div>
		     <div id="buildon_area"></div>
		     <div id="annotate_area"></div> -->
						<!--<div id="background_area"></div>
		     
		     <div id="bar_left" onclick="windowmoveleft()"></div>
		     <div id="bar_area">
		     <div id="bar"></div>
		     <div id="pointer" onmousedown="ptdown(event)" onmouseup="ptup(event)"></div>
		     </div>
		     <div id="bar_right" onclick="windowmoveright()"></div>
		     <div id="time_area"></div>-->
						<div id="thread_vis"></div>
						<div id="slider"
							style="width: 830px; position: absolute; top: 392px; left: 50px"></div>
					</div>
				</div>
				<div id="notes_slide">
					<div id="impress"></div>
				</div>
				<div id="draw_ctrl">
					<ul id="node_contral">
						<li>Note:&nbsp;&nbsp;</li>
						<li><a href="javascript:void(0)"
							onclick="remove_note($('#alertNoNoteDialog'))" id="remove_note">Remove
								Note</a>&nbsp;|&nbsp;</li>
						<li id="showhighlight"><a href="javascript:void(0)"
							onclick="showhighlight($('#alertNoNoteDialog'))"
							id="hightlight_note">Highlight Note</a>&nbsp;|&nbsp;</li>
						<li><a id="showallhighlighted" href="javascript:void(0)"
							onclick="greyOutSomeFeatures('HighlightedNotes');tg.toggleHighlightedNotes(this);">Show
								Highlighted Notes</a>&nbsp;|&nbsp;</li>
						<li><a id="showallhighlightedbuildons"
							href="javascript:void(0)"
							onclick="greyOutSomeFeatures('HighlightedBuildons');tg.toggleHighlightedBuildons(this);">Show
								Highlighted Buildons</a>&nbsp;|&nbsp;</li>
						<li><a id="viewNotesSlide" href="javascript:void(0)"
							onclick="viewNotesSlide();">Notes SlideShow</a>&nbsp;&nbsp;</li>
						<li><a id="showLightSideCode" href="#showLightSide">Show Contribution Types</a>&nbsp;&nbsp;</li>

						<!-- <li><a href="#download">DownloadTest</a></li>-->
					</ul>
					<ul id="zoom">
						<li>Time Range:&nbsp;&nbsp;</li>
						<li><a href="javascript:void(0)"
							onclick="tg.resetGraphView()" id="resetGraph">View All</a>&nbsp;&nbsp;&nbsp;</li>

						<!--  	<li>Zoom:&nbsp;&nbsp;</li>-->
						<!--  <li><a href="javascript:void(0)" onclick="bar(0)">1 Day</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(1)">1 Week</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(2)">2 Weeks</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(3)">1 Month</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="reloadMainThreadGraph();">All</a> </li>-->
					</ul>
				</div>
				<div><table id="myTable"><tr><td>Evidence</td><td>Explaining</td><td>Questioning</td><td>Referencing</td><td>noteid</td><td>author</td><td>view</td><td>date</td><td>title</td><td>text</td></tr></table></div>
				<div id="wrapper_ln">
					<div id="list_area" onclick="selectListItem(event)"></div>
					<div id="note_content">
						<h3>Select a note to show its content here...</h3>

					</div>
				</div>
				<li><a href="#download">Download notes and tags</a></li>
				<div><label><span class='blue'>•</span>Evidence; <span class='red'>•</span>Explaining; <span class='purple'>•</span>Questioning; <span class='green'>•</span>Referencing</label></div>
			</div>
			<div id="noteid_with_timestamp" value=<%=nidts_str%>></div>
			<div id="raw_area">
				<%
					//System.out.println("[INFO : thread.jsp]******************" +  request.getParameter("nidts"));

						while (disrs.next()) {
							//System.out.println("the note's id is " + disrs.getString(1));
							//System.out.println("the note's date is "+ disrs.getString(4));
							//System.out.println("the note's author is "+ disrs.getString(5)+"  "+disrs.getString(6));
							/*
							vec = retable.get(disrs.getInt(1));
							for(int i=0; i<vec.size();i++){
							System.out.println("reference1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
							}
							vec.clear();
							
							vec = butable.get(disrs.getInt(1));
							for(int i=0; i<vec.size();i++){
							System.out.println("buildson1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
							}
							vec.clear();
							
							vec = antable.get(disrs.getInt(1));
							for(int i=0; i<vec.size();i++){
							System.out.println("annotate1:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
							}*/

							// Default
							String highlighted = "0";
							highlighted = Integer.toString(idToHighlightMap.get(disrs.getInt(1)));
				%>
				<div class="rawnote" highlight="<%=highlighted%>"
					noteid="<%=disrs.getString(1)%>" view="<%=disrs.getString(7)%>"
					author="<%List authorList = opdb_author.getMultipleAuthors(disrs.getString(1));
					for (int i = 0; i < authorList.size(); i++) {
						String name = (String) authorList.get(i);%>
						  <%=name%>
						  <%if (i < authorList.size() - 1) {%>
						  ,&nbsp;
						  <%}%>	  
					  <%}%>"
					firstn="<%=disrs.getString(5)%>" date="<%=disrs.getString(4)%>">
					<!--display the note's relationship -->
					<div class="references">
						<%
							vec = retable.get(disrs.getInt(1));
									for (int i = 0; i < vec.size(); i++) {
										//System.out.println("reference:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
						%>
						<div class="reference" target="<%=vec.get(i)%>"></div>
						<%
							}
									vec.clear();
						%>
					</div>
					<div class="buildsons">
						<%
							vec = butable.get(disrs.getInt(1));
									for (int i = 0; i < vec.size(); i++) {
										//System.out.println("buildson:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
						%>
						<div class="buildson" target="<%=vec.get(i)%>"></div>
						<%
							}
									vec.clear();
						%>
					</div>
					<div class="annotates">
						<%
							vec = antable.get(disrs.getInt(1));
									for (int i = 0; i < vec.size(); i++) {
										//System.out.println("annotates:	from note "+disrs.getInt(1)+" to note " + vec.get(i));
						%>
						<div class="annotate" target="<%=vec.get(i)%>"></div>
						<%
							}
									vec.clear();
						%>
					</div>
					<div class="rawtitle"><%=disrs.getString(2)%></div>
					<%
						/* String content="";
										if(disrs.getString("support")==null)
											content = disrs.getString("notecontent");
										else
											content = nc.getContent(disrs.getString("noteid"));
										// 				content = nc.getOffsetContent(disrs.getString("noteid")); */
					%>
					<div class="rawcontent">
						<%=nc.getContent(disrs.getString("noteid"))%>
					</div>
				</div>
				<%
					}
				%>
			</div>
			<%
				if (disrs != null) {
						try {
							disrs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}

					// Now find all the revisions of this thread and
					List<String> threadRevs = opdb.getThreadRevisions(pid, threadName);
			%>
			<script language="javascript" type="text/javascript">
        var i = 0, displayId, id, ts, modified_by;
       // var i = 0, displayId, id, ts;
        $('#thread_history_options').append($('<ul style="width: 100%; height: 100px; overflow-y: auto;">'));
        $('#thread_history_options').width( $('#thread_history_options').parent().width());
        $('#thread_history_options').width("200px");
        $('#thread_history_options').height("150px");
<%for (String rev : threadRevs) {
					String[] cols = rev.split(";");
					String id = cols[0];
					String threadId = cols[1];
					String ts = cols[3];
					String modified_by = cols[4];%>
        id = "<%=id%>";
        ts = "<%=ts%>";
        modified_by = "<%=modified_by%>";
        console.log(modified_by);
        displayId = "H-" + ts;
        $('#thread_history_options ul').append(
            "<li data-modifiedby="+modified_by+"><a  href='#' onclick='threadHistoryInstance.showThreadRevision(\""+id+"\",\""+ts+"\")'>"+ts+"</a></li>");
        
        
       // String [] cols = rev.split(";");
       // String ts = cols[2];
//%>
      
       // $('#thread_history_options ul').append(
        //    "<li><a  href='#' onclick='threadHistoryInstance.showThreadRevision(\""+ts+"\")'>"+ts+"</a></li>");
<%}%>
    
    </script>
			<%
				if (opdb != null) {
						opdb.CloseCon();
					}

					if (opdb_author != null) {
						opdb_author.CloseCon();
					}
				}
			%>

			<!-------------------------------end of the if statement--------------------------------------->
		</div>
	</div>

	<!-- Display thread history data -->
	<div>
		<div id="thread_history_display"></div>
	</div>
	<div>
		<div id="thread_history_chart"></div>
	</div>
	<div id="thread_change_state" data-changed="0"></div>
	
	<script src="../js/en/impress.js"></script>
	<!-- <script>impress().init();</script> -->
	<script>
function viewNotesSlide() {
  var raw_notes = $('#raw_area'),
      slide_div = $('#notes_slide'),
      impress_div = $('#impress');
      x = -1000, y = 0;
  
  impress_div.empty();
  raw_notes.children().each(function() {
    var clone = $(this).children('.rawcontent').clone();
    clone.attr("data-x", x);
    clone.attr("data-y", y);
    clone.addClass('slide step');
    clone.removeClass('rawcontent');
    impress_div.append(clone);
    x += 1000;
  });
  
  var api = impress('impress', 1);
  api.init();
  api.goto(0);
  slide_div.dialog({ width : 900, height : 700 });
}
</script>

</body>
</html>