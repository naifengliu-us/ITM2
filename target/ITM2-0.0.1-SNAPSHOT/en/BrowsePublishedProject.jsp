<%-- 
    Document   : BrowsePublishedProject
    Created on : Apr 25, 2013, 1:16:24 PM
    Author     : Stan
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%>
<%@ page session = "true" %>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
    
<%
String database = request.getParameter("database").toString();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Browse Inquiry Projects Published in the ITM Network</title>
        
        <script src="../js/en/jquery.js" type="text/javascript"></script>
        <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">

        <script src="../js/en/jquery-ui.js" type="text/javascript"></script>
        <script src="../js/en/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="../js/en/jquery.tablesorter.min.js" type="text/javascript"></script>
        
        <LINK REL=StyleSheet HREF="../css/stickyhead.css" TYPE="text/css"></LINK>
        <LINK REL=StyleSheet HREF="../css/BrowsePublishProject.css" TYPE="text/css"></LINK>
        <LINK REL=StyleSheet HREF="../css/jquery.dataTables.css" TYPE="text/css"></LINK>


        <style>
         div.even {
           background-color: #E2E4FF
         }
         
         div.row {
          display: inline-block;
          width: 100%;
         }

         div.content-row {
          padding-left: 1%;
          padding-right: 1%;
          padding-top: 1%;
          padding-bottom: 1%;
         }

         div.col {
          display: table-cell;
         /* border-color: grey;
          border:1px solid #a1a1a1;*/
          height: 60px;
          background-color: white;
         }

         div.row-heading {
           background-color: #333;
           height: 40px;
           margin-top: 1px;
           margin-left: auto;
           margin-right: auto;
           text-align: center;
           vertical-align: middle;
           line-height: 40px;
           color: white;
         }
        
        
.dataTables_wrapper {
    clear: both;
    max-height: 400px;
    overflow: scroll;
    position: relative;
}
        </style>
        <script type="text/javascript" charset="utf-8">
          function downloadFile(elemAnchor) {
            $.ajax({
              type: "GET",
              url: "../TeacherReflectionServlet",
              data: {downloadFilepath: $(elemAnchor).attr('href')},
              success: function(res) {
                console.log(res);
              }, 
              error: function(res) {
                console.log(res)
              }
            });

            return false;
          }

           function openTeacherReflectionView(projName, dbName, timestamp) {
             $.post( "../TeacherReflectionServlet", { getData: "true", projectname: projName, 
               databasename: dbName, timestamp: timestamp}, function(result) {
                 
                 console.log('result is ', result);
                 result = jQuery.parseJSON(result);
                 for (var key in result) {
	                 if (result.hasOwnProperty(key) && result[key] == "" || result[key] === undefined) {
	                   result[key] = "None";
	                 }
                 }
                 
                 var root = $('#teacher_reflection_content');
                 var fullpath = result.attachment_path.replace(/ /g, '\u00a0');
                 root.css('display:');
                 root.find('#bigidea').html(result.bigidea);
                 root.find('#facilitated').html(result.facilitated);
                 root.find('#helpfulactivities').html(result.helpfulActivities);
                 root.find('#lessonslearned').html(result.lessonsLearned);
                 root.find('#attachment').empty();
                 root.find('#attachment').append('<a href="../TeacherReflectionServlet?downloadFilepath='+fullpath+'">'+result.attachment_title+'</a>');

                 root.dialog({width: 1024, height: 700, title: "Teacher Reflection",
                   open: function() {
                     root.css('padding', 0);
                     var parentWidth = root.width();
                     root.find('.col').each(function() {
                       $(this).css('width', parentWidth);
                     });
                   }
                 });

                root.parent().css({"z-index": 9999});
             });
           }
        
            $(document).ready(function(){
                var oTable=$('#pTable').dataTable({
                            "iDisplayLength": 5,
                              "sPaginationType": "full_numbers",
                              	"bLengthChange":false,
                                 "oLanguage": {
                                    "sSearch": "Filter"
                                 }
                            });//end dataTable
                $(':button').click(function(){
//                    $('.search_tr').remove();
                    oTable.fnClearTable();
                    var formData=$('form').serialize();

                    console.log('form data....', formData);
                    
                    $.getJSON("../BrowsePublishServlet", formData, processResponse);
                    function processResponse(data){
                        console.log('data is ', data);
                        $.each(data, function(key, val) {
                            oTable.fnAddData( [val.name, val.timestamp, val.grades,val.teacher,val.school,val.schoolYear,val.db,
                              '<a href="#"  onclick="openTeacherReflectionView(\''+val.name+'\',\''+val.db+'\',\''+val.timestamp+'\')">Teacher Reflection</a><br>' +
                              '<a href="ViewPublishedProject.jsp?database='+val.db+'&projectname='+val.name+'&timestamp='+val.timestamp+
                                  '&dbase='+val.db+'&grades='+val.grades+'&teacher='+val.teacher+'&school='+val.school+
                                  '&fromYear='+val.fromYear+'&toYear='+val.toYear+'&threads='+val.threads+'" target="_blank">View Project</a>'
                        ] );
                      });//end each
                   }
                    
                });//end click
                
            });//end doc
            
            
            $(document).ready(function() { 
              $("#pTable").tablesorter(); 
            });
            
        </script>
    </head>
    <body>
    <div id="sticky">
    	<div id="header">
    <table border="0">
      <tbody>
        <tr>
          <td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
          <td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="15" width="900">&nbsp;&nbsp;
            <center>
              Browse Inquiry Projects Published in the ITM Network
            </center></td>
        </tr>
      </tbody>
    </table>
    <div id = "wrapper" style="overflow:auto">
   	  <p>The ITM network helps students and teachers from around the world to share their knowledge building work through publishing idea threads that involve productive conversations and progress</p>
    	<p>Published inquiry projects:  </p>
    	
    	<form name="form1" method="get" action="">
    	  <label for="project">Search</label>
    	  <input type="search" name="project" id="searchProject" placeholder="Published Topics">
          <label for="location">Range</label>
    	  <select name="location" size="1" >			
				<option value="1">Anywhere</option>
			    <option value="2">Topic</option>
			    <option value="3">Knowledge Forum</option>		
	  </select>
          <input type="button" value="Search"/>
   	  </form>
    	
<table id="pTable">
	<thead>
	<tr>
		<th width="13%">Topic of study</th>
		<th width="5%">Timestamp</th>
		<th width="5%">Grade level</th>
		<th width="8%">Teacher</th>
        <th width="10%">School</th>
        <th width="15%">School year</th>
        <th width="30%">Knowledge Forum host and database name</th>
        <th width="14%">Action</th>
	</tr>
	</thead>
	<tbody class="result">	
	</tbody>
</table>
	<div id="button">
            <a href="New_HomePage.jsp?database=<%=database%>" onclick="" class="close" >Close</a>
	 </div>
    </div>
    </div>
    </div>
  <div id="teacher_reflection_content" style="display:none">
    <div class="container-fluid">
      <div class="row row-heading">What this is inquiry about:"Big ideas" in the curriculum unit(s)</div>
      <div class="row content-row">
        <div class="col" id="bigidea"></div>
      </div>
      <div class="row row-heading">How I facilitated:</div>
      <div class="row content-row">
        <div class="col" id="facilitated"></div>
      </div>
      <div class="row row-heading">Most helpful activities and resources used:</div>
      <div class="row content-row">
        <div class="col" id="helpfulactivities"></div>
      </div>
      <div class="row row-heading">Lessons learned and improvements I want to make in the future:</div>
      <div class="row content-row">
        <div class="col" id="lessonslearned"></div>
      </div>
      <div class="row row-heading">Attachment</div>
      <div class="row content-row">
        <div class="col" id="attachment"></div>
      </div>
    </div>
  </div>
  </body>
</html>