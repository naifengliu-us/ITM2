<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
	/************validate the user session*******************/
	String username = " ";
	String usertype = " ";
	session = request.getSession(false);
	if (session != null) {
		if (session.getAttribute("username") == null) {
			response.sendRedirect("/ITM2/index.jsp");
		} else {
			username = (String) session.getAttribute("username");
			usertype = (String) session.getAttribute("usertype");
		}
	}
%>

<script type="text/javascript">
<%String strsucceed = request.getParameter("ifsucceed");
			String database = request.getParameter("database");
			System.out.println("Database is ....." + database);

			if (strsucceed != null) {
				if (strsucceed.equals("yes")) {
					String strname = request.getParameter("projectname");%>
		alert("This project has been saved!");
		window.opener.location='/ITM2/en/New_HomePage.jsp?projectname=<%=strname%>&database=<%=database%>';
	window.close();
<%} else if (strsucceed.equals("no")) {%>
	alert("The project name already exists!");
<%}
			}%>
	function OnSave() {
		if (document.Create_form.projectname.value == "") {
			window.alert("please input the project name!");
			return false;
		}
		if (document.Create_form.teacher.value == "") {
			window.alert("please input the teacher's name!");
			return false;
		}
		if (document.Create_form.school.value == "") {
			window.alert("please input the school name!");
			return false;
		}
		if (document.Create_form.grade.value == "") {
			window.alert("please choose grade!");
			return false;
		}
		if (document.Create_form.currArea.value == "") {
			window.alert("please select Curriculum Area!");
			return false;
		}

		var from = parseInt(document.Create_form.fromyear.value);
		var to = parseInt(document.Create_form.toyear.value);
		if (from > to) {
			alert("the from-school-year is greater than the to-school-year, please select them again!");
			return false;
		}
		/*if(document.Create_form.group.value=="")
		{
		  window.alert("please choose group!");  
		  return false;  
		}*/
		return true;
	}

	function UpdateDefaultGroup(usrgroup) {
		var index = 0;
		var g = document.getElementById("groupNames");
		//alert(g.length);
		//alert(p[0].innerHTML);
		for (var i = 0; i < g.length; i++) {
			var gName = g[i].innerHTML;
			if (gName == usrgroup) {
				index = i;
			}
		}
		g.selectedIndex = index;
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create New Project</title>

<LINK REL=StyleSheet HREF="../css/stickyhead.css" TYPE="text/css"></LINK>
<LINK REL=StyleSheet HREF="../css/createProject.css" TYPE="text/css"></LINK>
</head>
<%
	// delete status for new project is false 
	int deleted = 0;
	String usrgroup = null;
			String area = null;
			String num = null;
	//connect to database
	sqls s = new sqls();
	Operatedb opdb = new Operatedb(s, database);
	Operatedb opdb_itm = new Operatedb(s, "itm");
	ResultSet rs = null;
	ResultSet rs_currArea = null;
	//get user group name
	String strcon = "group_table.idgroup = group_author.group_id and group_author.author_id = author_table.authorid and username ='"
			+ username + "';";
	String[] tableNames = new String[3];
	tableNames[0] = "group_table";
	tableNames[1] = "group_author";
	tableNames[2] = "author_table";
	rs = opdb.MulGetRecordsFromDB(tableNames, "group_table.title", strcon, 3);
	if (rs.next()) {
		usrgroup = rs.getString(1);
		System.out.println("usergroup is###" + usrgroup);
	}
	//get group from database
	rs = opdb.GetRecordsFromDB("group_table", "title", "");
	//get curriculum Area id, name, parent_id from itm database
	String[] arrayCols = {"name","parent_id"};
	rs_currArea = opdb_itm.GetMultipleRecordsFromDB("curriculum_areas",arrayCols,"");
	//while (rs_currArea.next()) {
	//num = rs_currArea.getString(2);
	//area = rs_currArea.getString(1);
	//System.out.println("Col 2 is "+ num + "....."+ "\n Curr Area is " + area);
	//}


	//get the grade list
	List gradelist = new ArrayList();
	//int i=0;
	/*for(GradeSelect gradesopt:GradeSelect.values()){
		gradelist.add(gradesopt);
		//System.out.println("The "+ i + "th value of the Grade is :" + (GradeSelect)gradelist.get(i));
		//i++;
	}*/

	for (int i = 1; i <= 12; i++) {
		gradelist.add(i);
		//System.out.println(gradelist.get(i-1));
	}
%>

<%
	//get the current year
	int curyear = Calendar.getInstance().get(Calendar.YEAR);
%>

<body onload="UpdateDefaultGroup('<%=usrgroup%>');">
	<div id="sticky">
		<div id="header">
			<table border="0">
				<tr>
					<td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO"
						height="100" width="100" /></td>
					<td
						style="background-color: #01B0F1; color: yellow; font-family: Arial, Helvetica, sans-serif; font-size: 30px;"
						height="60" width="850">&nbsp;&nbsp;Basic Information about
						This Inquiry Project</td>
				</tr>
			</table>

		</div>
	</div>
	<div id="wrapperu">
		<div id="wrapper">
			<form name="Create_form"
				action="../CreateProject?database=<%=database%>&username=<%=username%>&deleted=<%=deleted%>"
				method="post">
				<table>
					<tr>
						<td><b>Topic of study:</b></td>
						<td><input class="fields" name="projectname" id="user"
							type="text" size="18" value="" /></td>
						<td><b>*Grade Level:</b></td>
						<th rowspan="2"><select name="grade" multiple="multiple"
							size="4">
								<option value="K">K</option>
								<%
									int j = 0;
									for (j = 0; j < gradelist.size(); j++) {
								%>
								<option value="<%=(Integer) gradelist.get(j)%>"><%=(Integer) gradelist.get(j)%></option>
								<%
									}
								%>
								<option value="UG">UG</option>
								<option value="PG">PG</option>
								<option value="Other">Other</option>
						</select></th>
					</tr>
					
					<tr>
						<td><b>Teacher:</b></td>
						<td><input class="fields" name="teacher" id="address"
							type="text" size="15" value="" /></td>
						<td></td>
					</tr>
					
					<tr>
						<td><b>School:</b></td>
						<td><input class="fields" name="school" id="year" type="text"
							size="30" value="" /></td>
						<td><b>*Choose Knowledge Forum Group:</b></td>
						<td colspan="2"><select name="group" multiple="multiple"
							id="groupNames" size="4">
								<%
									while (rs.next()) {
								%>
								<option value="<%=rs.getString(1)%>"><%=rs.getString(1)%></option>
								<%
									}
								%>
						</select></td>
					</tr>
					
					<tr>
						<td><b>School Year:</b>
						<td colspan="1" style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;<b>from</b><select
							name="fromyear" style="float: none;">
								<option value="<%=curyear%>"><%=curyear%></option>
								<%
									for (int i = curyear - 1; i > curyear - 20; i--) {
								%>
								<option value="<%=i%>"><%=i%></option>
								<%
									}
								%>
						</select><b>to</b><select name="toyear" style="float: none;">
								<option value="<%=curyear + 1%>"><%=curyear + 1%></option>
								<%
									for (int i = curyear; i > curyear - 20; i--) {
								%>
								<option value="<%=i%>"><%=i%></option>
								<%
									}
								%>
						</select></td>
						<td><b>*Curriculum Area:</b></td>
						<td colspan="3"><select name="currArea" multiple="multiple"
							id="currAreaId" size="6">
									<%
									while (rs_currArea.next()) {
								if (rs_currArea.getInt(2)==0){
								%>
								<option value="<%=rs_currArea.getString(1)%>"><%=rs_currArea.getString(1)%></option>
								<%
									} else { 
								%>
								<option value="<%=rs_currArea.getString(1)%>">&nbsp;&nbsp;&nbsp;&nbsp;<%=rs_currArea.getString(1)%></option>
								<%
								}
									}
								%>
						</select></td> 
					</tr>
					
					
					
					<tr>
						<td></td>
						<td colspan="3">(*)CTRL+click (Windows) or command+click
							(Mac) to choose more than one</td>
					</tr>
				</table>
				<br> <br>
				<center>
					&nbsp;&nbsp;&nbsp;<input name="save" type="submit" id="create_save"
						value="Save" onclick="return OnSave();" />&nbsp;&nbsp;&nbsp;<input
						name="save" type="button" id="create_save" value="Cancel"
						onclick="javascript: window.close();" />&nbsp;&nbsp;&nbsp;
				</center>
			</form>
		</div>
	</div>
</body>
</html>
<%
	//release the connection to database
	if (rs != null) {
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	if (opdb != null) {
		try {
			opdb.CloseCon();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//s.Close();
%>