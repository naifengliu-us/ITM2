<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="itm.controllers.Stemmer"%>
<%@page import="javax.sql.rowset.CachedRowSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*"%>
<html>
<head>
<link rel=stylesheet href="../css/stickyhead.css" type="text/css"></link>
<% 
	/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
		response.sendRedirect("/ITM2/index.jsp");
		}
	}

	/**********read the noteid contained in the current thread from database*******/
   String strproname = request.getParameter("projectname");
   String strfocus = request.getParameter("threadfocus");
   String strdb = request.getParameter("database");
   String proid = null;
   
   ResultSet rs = null;
   CachedRowSet crs = null;
   ResultSet temprs = null;
   ResultSet ntidtime = null;
   
   sqls s = new sqls();
   Operatedb opdb = new Operatedb(s,strdb);
   Operatedb opdb_author = null;
   
 	//get the project id
	temprs=opdb.GetRecordsFromDB("project","idproject","projectname='"+strproname+"';");
	if(temprs.next()){
		proid = temprs.getString(1); 
	}
   
   String strcon = "projectid=" + proid +" and threadfocus='" + strfocus + "';";
   temprs = opdb.GetRecordsFromDB("thread_note","noteid",strcon);
   
   List<String> idlist = new ArrayList<String>();
   while(temprs.next()){
	   idlist.add(temprs.getString(1));
   }
   
   if(temprs != null)
   {
   	 try
     {
	   temprs.close();
     }
   	 catch(SQLException e)
     {
	   e.printStackTrace();
     }
   }
%>

<%//System.out.println("+++++++++++++++++++++++++searchnote.jsp++++++++++++++++++++++++");
//System.out.println("searchnote.jsp the project name is "+request.getParameter("projectname"));
//System.out.println("searchnote.jsp the thread focus name is "+request.getParameter("threadfocus"));%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	<!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="../css/tcal.css" />
	<link rel=stylesheet href="../css/thread.css" type="text/css"/>
	<script type="text/javascript" src="../js/en/tcal.js"></script> 
	<script type="text/javascript" src="../js/en/thread_update.js"></script>

	<script type ="text/javascript">
	var count = 0;
	function UpdateCount(rowCnt)
	{
		count = rowCnt;
		document.getElementById("updatecnt").innerHTML = count;
		//alert(count);
	}
	var cnt = 0;
	function UpdateNoteSelected(rowObject)
	{
		//alert(rowObject);
		//alert(rowObject.checked);
		if(rowObject.checked == true)
		{
		  cnt++;
		}
		if(rowObject.checked == false)
		{
		  cnt--;
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function SendNoteid()
	{
		// FIXME: Remove debug messages
		//alert("Calling SendNoteid in SearchMore.jsp");
		top.document.getElementById("framesetter").rows = "0%,100%";
		var idTimestampMapString = "";
		var num;
		/* if (typeof document.searchnote_form.notecheck.length === 'undefined' || 
		    document.searchnote_form.notecheck !== "" || 
		    typeof document.searchnote_form.notecheck !== 'undefined') {
		  num = 1;
		} else { */
		 num = document.searchnote_form.notecheck.length;
		/* } */
		var TimestampForAddNotes = new String(AddTimeForNotesInThread());
		
		// FIXME: Remove debug messages
		//alert(TimestampForAddNotes);
		//alert(document.searchnote_form.notecheck.length);
		
		var ids="@";
		var putvalue;
		var proname;
		var num=0;
		proname= "<%=request.getParameter("projectname")%>";
		if(typeof document.searchnote_form.notecheck.length === "undefined")
		{
			ids += document.searchnote_form.notecheck.value + "@";
			idTimestampMapString += document.searchnote_form.notecheck.value + "=" + TimestampForAddNotes + ";";
			num++;
		}
		else
		{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++)
			{
				if(document.searchnote_form.notecheck[i].checked)
				{
					num++;
					ids += document.searchnote_form.notecheck[i].value + "@";
					idTimestampMapString += document.searchnote_form.notecheck[i].value + "=" + TimestampForAddNotes + ";";
				   	//alert(document.searchnote_form.notecheck[i].value);
				}
			}
		}
		
		if(num==0)
		{
			alert("Please select note first!");
			return false;
		}
		if(num > 500)
		{
			alert("You selected too many notes, please select again!");
			return false;
		}
		
		// Add the old id list
		<%
		if(idlist.size() > 0)
		{
		  for(int j = 0; j < idlist.size(); j++)
		  { 
		  %>
		    ids += <%=idlist.get(j)%> + "@";
		  <%
		  } 
	    }
		 
	    // Get the noteid and the addtime from thread_note table
	   	String[] columnNames = new String[2];
	    columnNames[0] = "noteid";
	   	columnNames[1] = "addtime";
		ntidtime = opdb.GetMultipleRecordsFromDB("thread_note",columnNames, strcon);
		   
		while(ntidtime.next())
		{
		%>	
		  idTimestampMapString += <%= ntidtime.getString("noteid")%> + "=" + "<%= ntidtime.getString("addtime")%>" + ";" ;
		<%
		}
		
		if(ntidtime != null)
		{
		  try
		  {
		    ntidtime.close();
		  }
		  catch(SQLException e)
		  {
		    e.printStackTrace();
		  }
		}
		%>
		
		//alert("Map is " +  idTimestampMapString);
		
		putvalue = ids;
		//alert(putvalue);
		
	//javascript form
		var f = document.createElement("form");
		f.setAttribute('method',"POST");
		f.setAttribute('action',"/ITM2/en/thread.jsp");
		
		var i = document.createElement("input");
		i.setAttribute('type',"hidden");
		i.setAttribute('name',"database");
		i.setAttribute('value','<%= request.getParameter("database")%>');
		
		var s = document.createElement("input");
		s.setAttribute('type',"hidden");
		s.setAttribute('name',"projectname");
		s.setAttribute('value','<%= request.getParameter("projectname")%>');
		
		var t = document.createElement("input");
		t.setAttribute('type',"hidden");
		t.setAttribute('name',"threadfocus");
		t.setAttribute('value',"<%= request.getParameter("threadfocus")%>");
		
		var u = document.createElement("input");
		u.setAttribute('type',"hidden");
		u.setAttribute('name',"getvalue");
		u.setAttribute('value',putvalue);
		
		var v = document.createElement("input");
		v.setAttribute('type',"hidden");
		v.setAttribute('name',"nidts");
		
		// Replace whitespace with the html code or else the http will truncate the string
		idTimestampMapString = idTimestampMapString.replace(/ /g, "&#32;");
		v.setAttribute('value',idTimestampMapString);
			
		var w = document.createElement("input");
		w.setAttribute('type',"submit");
		w.setAttribute('value',"Submit");
		
		var tc = document.createElement("input");
		tc.setAttribute('type',"hidden");
		tc.setAttribute('name',"threadchanged");
		tc.setAttribute('value',"1");
		
		f.appendChild(i);
		f.appendChild(s);
		f.appendChild(t);
		f.appendChild(u);
		f.appendChild(v);
		f.appendChild(w);
		f.appendChild(tc);
		document.body.appendChild(f);
		f.submit();
	}

	
	function SelectAll(){
	    cnt = 0;
		if(document.searchnote_form.notecheck.length == undefined){
			document.searchnote_form.notecheck.checked = true;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				document.searchnote_form.notecheck[i].checked = true;
				cnt++;
			}
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function DeselectAll(){
		cnt = 0;
		if(document.searchnote_form.notecheck.length == undefined){
			document.searchnote_form.notecheck.checked = false;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				document.searchnote_form.notecheck[i].checked = false;
			}
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function CheckTime(){
		if(document.searchnote_form.fromtime.value=="" && document.searchnote_form.totime.value!="")
			{	
				window.alert("You can't leave the 'From' input box empty!");
				return false;
			
			}
		if(document.searchnote_form.fromtime.value !="" && document.searchnote_form.totime.value=="")
		{	
			window.alert("You can't leave the 'To' input box empty!");
			return false;
		
		}
		
		if(document.searchnote_form.views.value=="")
		{
		  window.alert("please choose one view!");  
		  return false;  
		}
		
		return true;
	}
	function CheckValidate(){
		if(document.searchnote_form.fromtime.value=="")
		{
		  window.alert("please input 'From Time'!");  
		  return false;  
		}
		if(document.searchnote_form.totime.value=="")
		{
		  window.alert("please input 'To Time'!");  
		  return false;  
		}
		if(document.searchnote_form.views.value=="")
		{
		  window.alert("please choose one view!");  
		  return false;  
		}
		
		return true;
	}
	function ShowContent(noteid){
		var body = document.body,
	    html = document.documentElement;

		var height = Math.max( body.scrollHeight, body.offsetHeight, 
	                       html.clientHeight, html.scrollHeight, html.offsetHeight );
// 		alert("Height = "+height);
		window.open('/ITM2/en/New_NoteContent.jsp?database=<%=request.getParameter("database")%>&noteid='+noteid,"notewin","height='+height+', width=750,toolbar=no,scrollbars=yes,menubar=no");
	}
	
	/*function hideDiv()
	{
	   document.getElementById('divNote').style.visibility ='hidden';
	}*/
	/* Hide the number of notes selected and number of notes found until search notes is clicked*/
	function ShowDiv(searchVal) {
	  var s = searchVal;
	  var v = document.getElementById("divNote");
	  if(s == "null")
	  {
	     v.style.visibility='hidden';
	  }
	  else
	  {
	    v.style.visibility='visible';
	  }
	}
	/* Enable Disable select buttons until notes are listed */
	function ShowDivSelect(numNotes) {
	  var sButton = document.getElementById("selbutton").children;
	  //alert(numNotes);
	  //alert(document.getElementById("selbutton").children.length);
	  
	  if(numNotes < 1) {
	    for(var i = 0; i<sButton.length;i++) {
	      sButton[i].removeAttribute("href");
	      sButton[i].removeAttribute("onclick");
	      sButton[i].style.color ="gray";
	    }
	  }
	}
	
	function OpenThreadPage(focus) {
      top.window.location='/ITM2/en/threadLoad.jsp?database=<%=strdb%>&projectname=<%=strproname%>&threadfocus='+focus;          
    }
	</script>
</head>



<%
	/*********************************read all views title from database******************************/
	List vtitlelist = new ArrayList();
	String strtitle;
	//ResultSet rs = null;
	Statement stmt;
	//Operatedb opdb = null;
	String stryear ;
	String strmonth ;
	String strday ;
	String strfrtime=null;
	String strtotime=null;
	String strrange=null;
	String searchval =null;
	String[] strview = null;
	String strword1="", strword2="",strword="";
	stmt= s.Connect(strdb);
	
	try{
			rs = stmt.executeQuery("SELECT * from `view_table` order by title ASC;");
			while(rs.next()){
				strtitle = rs.getString(2);	
				vtitlelist.add(strtitle);			
			}
		}catch(SQLException e){
			e.printStackTrace();		
		}
	/*************************************************************************************************/
	/*************************Get all notes contained in view chosen from database*******************/
	if(request.getParameter("search") != null){ //user clicked on the search buttom
		System.out.println("user click on the search button"+request.getParameter("search"));
	    searchval = request.getParameter("search");
		strfrtime = request.getParameter("fromtime");
		strtotime = request.getParameter("totime");
		strview = request.getParameterValues("views");
		strword = request.getParameter("keywords");
		strrange = "";
		System.out.println("range is "+strrange);
		System.out.println("keywords is "+strword);
		
		if(request.getParameter("range")!= null)
		{
			strrange = request.getParameter("range");
			if(strrange.equals("note title")){
				strword2 = strword;
			}else if(strrange.equals("note content")){
				strword1 = strword;
			}else if(strrange.equals("anywhere")){
				strword1 = strword;
				strword2 = strword;
			}
		}
		
		
		//change the date format if the time period is not empty
		/*
		if( !strfrtime.equals("earliest") && !strtotime.equals("latest")){
			int ifirst = strfrtime.indexOf('/');
			int ilast = strfrtime.lastIndexOf('/');
			stryear = strfrtime.substring(ilast+1,strfrtime.length());
			strmonth = strfrtime.substring(0,ifirst);
			strday = strfrtime.substring(ifirst+1,ilast);
			
			strfrtime = stryear + "-"+strmonth+"-"+strday;
			
		    ifirst = strtotime.indexOf('/');
		    ilast = strtotime.lastIndexOf('/');
			stryear = strtotime.substring(ilast+1,strtotime.length());
			strmonth = strtotime.substring(0,ifirst);
			strday =strtotime.substring(ifirst+1,ilast);
			
			strtotime = stryear + "-"+strmonth+"-"+strday;		
		}*/
		
		if(!strfrtime.equals("earliest")){
			int ifirst = strfrtime.indexOf('/');
			int ilast = strfrtime.lastIndexOf('/');
			stryear = strfrtime.substring(ilast+1,strfrtime.length());
			strmonth = strfrtime.substring(0,ifirst);
			strday = strfrtime.substring(ifirst+1,ilast);
			
			strfrtime = stryear + "-"+strmonth+"-"+strday;
		}
		
		if(!strtotime.equals("latest")){
			int ifirst = strtotime.indexOf('/');
		    int ilast = strtotime.lastIndexOf('/');
			stryear = strtotime.substring(ilast+1,strtotime.length());
			strmonth = strtotime.substring(0,ifirst);
			strday =strtotime.substring(ifirst+1,ilast);
			
			strtotime = stryear + "-"+strmonth+"-"+strday;
		}
		
		
	    //opdb = new Operatedb(s,strdb);
		rs = opdb.GetNotes(strfrtime,strtotime,strview,strword1,strword2,strrange,request.getParameter("match"));
// 		crs = opdb.GetStemmedNotes(strfrtime,strtotime,strview,strword1,strword2,strrange);

	}
	/*************************************************************************************************/
	 
%>

<body>
<div id="wrapperu" style="top:0px;">
<div id="wrapper">
	<form name="searchnote_form" action="/ITM2/en/SearchMore.jsp?database=<%=strdb%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus") %>" method="post">
	
		<div style="background:#1ABDE6">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
			<label>Find Notes: From</label>&nbsp;&nbsp;
			<%if(request.getParameter("fromtime")!=null){%>
				<input type="text" style="background:white" id="ftimeid" name="fromtime" class="tcal" value="<%=request.getParameter("fromtime")%>" />&nbsp;&nbsp;&nbsp;
			<% }else{%>
				<input type="text" style="background:white" id="ftimeid" name="fromtime" class="tcal" value="earliest" />&nbsp;&nbsp;&nbsp;
			<%}%>
			<label>To</label>&nbsp;&nbsp;&nbsp;
			<%if(request.getParameter("totime")!=null){%>
			<input type="text" style="background:white" name="totime" class="tcal" value="<%=request.getParameter("totime") %>" />&nbsp;&nbsp;&nbsp;
			<% }else{%>
			<input type="text" style="background:white" name="totime" class="tcal" value="latest" />&nbsp;&nbsp;&nbsp;
			<%}%>
			<br/>
			
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label>In View</label>&nbsp;&nbsp;
			<select name="views" multiple="multiple" size="7" >
			<%if(request.getParameterValues("views")!=null){
			     String[] strtemp = request.getParameterValues("views");
			     for(int j=0; j<strtemp.length; j++){%>
			    	 <option selected="selected" value="<%=strtemp[j]%>"><%=strtemp[j]%></option>
			     <%}%>
			<% }else{%>
				<option selected="selected" value="all views">All Views</option>
			<%}%>
			<%
				for(int i=0; i<vtitlelist.size();i++){
					strtitle = (String)vtitlelist.get(i);
			%>  <option value="<%=strtitle%>"><%=strtitle%></option>
			<%}%>	
				<option value="all views">All Views</option>
							
			</select>&nbsp;&nbsp;&nbsp;	
			<label>(Shift+click to choose more than one)</label>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label>Key Word:</label>&nbsp;&nbsp;&nbsp;
			<%if(request.getParameter("keywords")!=null){%>
				<input class="fields" name = "keywords" id = "key" type = "text" size = "40" value = "<%=request.getParameter("keywords")%>"/>
			<%}
			  else{%>
			  	<input class="fields" name = "keywords" id = "key" type = "text" size = "40" value =""/>
			 <%}%>
			  &nbsp;&nbsp;
			 <label>in</label>&nbsp;&nbsp;
			 <select name="range" size="1" >
			<%if(request.getParameter("range")!=null){%>
				<option value="<%=request.getParameter("range")%>"><%=request.getParameter("range") %></option>
			<% }else{%>
				<option value="anywhere">anywhere</option>
			<%}%>
				<option value="anywhere">anywhere</option>
			    <option value="note content">note content</option>
			    <option value="note title">note title</option>		
			</select>
			
			<select name="match">
				<option value="exact" selected>Exact Match</option>
				<option value="related" <%=((request.getParameter("match")!=null && request.getParameter("match").contentEquals("related")) ? "selected":"")%>>Related Match</option>
			</select>
			
			
			<input type="submit" style="height:40px" name="search" value="Search more notes" onClick="CheckTime();"/>
			<!-- <input type="button" style="height:40px" value="Cancel search" onclick="javascript: top.document.getElementById('framesetter').rows = '100%,0%';"/> -->
			<input type="button" style="height:40px" value="Cancel search" onclick="OpenThreadPage('<%=strfocus%>')"/>
			<br/>		  
		</div>
		<br/>
		<div id ="divNote" style="visibility:hidden;">
			<label>Number of Notes Found:</label>
	        <span id="updatecnt" >0</span>
	        &nbsp;&nbsp;
			<label>Number of Notes Selected:</label>
	        <span id="updatenoteselected" >0</span>
		</div>
		<br/>
		<Script type="text/javascript">
		   ShowDiv('<%= request.getParameter("search") %>');
		</Script>
		
		<div id = "selbutton" >
			<input type="button" name= "selectall"  onClick="SelectAll()"  id="selectall" value="Select All">
			<input type="button" name= "deselectall"  onClick="DeselectAll()" id="deselectall" value="Deselect All">
			<input type="button" name= "showthread" onClick="SendNoteid()" value="Add To Thread">	
		</div>
		
		<div style="overflow: auto; height: 300px; width:1000px;">
			<table style="width: 1000px;" cellpadding="5" cellspacing="10">
			<tr>
				<th></th>
				<th>Title</th>
				<th>View</th>
				<th>Time Created</th>
				<th>Author</th>
				<th>Content</th>
			 </tr>		
		  <%
		  	int irow=0;
		  	opdb_author = new Operatedb(new sqls(),request.getParameter("database"));
		  	
		  	Stemmer stemmer = new Stemmer();		  	
		  			  	
		  	Outer:
			while(rs.next()){
			
				String ntcontent = stemmer.removeStopWordsAndStem(rs.getString("notecontent"));
				String nttitle = stemmer.removeStopWordsAndStem(rs.getString("notetitle"));
				String stemstrword1 = stemmer.removeStopWordsAndStem(strword1);
				String stemstrword2 = stemmer.removeStopWordsAndStem(strword2);
				
				if(request.getParameter("match").contentEquals("related")){
		    		if(strrange.equals("anywhere")){
								if(!(ntcontent.contains(stemstrword1) || nttitle.contains(stemstrword2)))
									continue Outer;
							}else if(strrange.equals("note title")){
								if(!(nttitle.contains(stemstrword2)))
									continue Outer;
							}else if(strrange.equals("note content")){
								if(!(ntcontent.contains(stemstrword1)))
									continue Outer;
							}
				}
				//System.out.println("irow = " +irow);
				//System.out.println(rs.getString(1));
				//System.out.println(rs.getString(2));
				//System.out.println(rs.getString(3));
				//System.out.println(rs.getString(4));
	    		
	
				if(!idlist.contains(rs.getString(4))){
				  irow++;
				
				%>
				    
				<tr>
					<td>
					<input type="checkbox" class="notecheck" name="notecheck" onClick="UpdateNoteSelected(this);" value="<%=rs.getInt(4)%>"/>
					</td>
					
					<td>
					<%=rs.getString(1)%> <!--display the note's title-->
					</td>
					
					<td>
					<%=rs.getString(2)%> <!--display the view's title-->
					</td>
					
					<td>
					<%=rs.getString(3)%><!--display the note's create time-->
					</td>
					
					<!-- display the note's authors -->
					<td>
					<%String strid=rs.getString(4);
					  List authorList = opdb_author.getMultipleAuthors(strid);
					  for(int i=0;i<authorList.size();i++){
						  String firstname = (String)authorList.get(i);%>
						  <%=firstname %>
						  <%if(i < authorList.size()-1){
						  %>
						  ,&nbsp;
						  <%}%>	  
					  <%}%>
					</td>
					
					<td  style= "cursor:pointer">
					<div onclick="javascript:var a = document.searchnote_form.<%="pre"+rs.getString(4)%>.value;ShowContent(a);">
				    <font color="#1ABDE6"><U><%=(rs.getString(6).replaceAll("<[^>]*>","")).substring(0,40)%>...</U></font><!--display the first 20 words-->
					</div>
					<input type="hidden" name="<%="pre"+rs.getString(4).replaceAll("<[^>]*>", "")%>" value="<%=rs.getString(4).replaceAll("\\<[^>]*>","")%>"/> 
					</td>
				</tr>
				<%}
			 }%>
		</table>	
		</div>	
       
		<script type="text/javascript">
		//No of rows found in search
		UpdateCount('<%= irow%>');
		ShowDivSelect('<%= irow%>');
		
// 		$("#selectall").click(function() {
// 			$(".notecheck").attr('checked', true);
// 		});
// 		$("#deselectall").click(function() {
// 			$(".notecheck").attr('checked', false);
// 		});
		</script>
	 </form>
	</div>
	</div>
</body>
</html>
<%
	//release the connection to the database
	if(rs != null){
		try{
			rs.close();
		}catch(SQLException e){		
			e.printStackTrace();
		}
	}
	if(stmt != null){
		try{
			stmt.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(opdb_author != null){
		opdb_author.CloseCon();
	}
	if(s != null){
		s.Close();
	}
	
%>