<%-- 
    Document   : SuggestThread
    Created on : Apr 26, 2013, 9:05:55 PM
    Author     : Stan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <LINK REL=StyleSheet HREF="../css/BrowsePublishProject.css" TYPE="text/css">
        
        <script src="../js/en/jquery.js" type="text/javascript"></script>
        <script src="../js/en/suggest.thread.js" type="text/javascript"></script>
        </head>
    <body>
    <div id = "wrapper" style="overflow:auto">
        <form id="createThreadForm" action="../addThreadServlet?database=${database}" method="post">
    <input class="fields" id="input_threadfocus" name="thread_focus"  type = "text" size = "25" value ="" onkeydown="return new_thread_searchKeyPress(event);"
        />
<!--    <a href="#" onclick="onCreateThread()" class="close">OK</a>          -->
    <a href="#" onclick="OnFind()" class="close">OK</a> 
    <a href="New_HomePage.jsp?database=${database}" onclick="" class="close">Close</a>    
    </form>
      <div class="styled-select">
        <label>Select a View</label>
     
        &nbsp;&nbsp;
      
        <select id="views" name="views" multiple="multiple" size="7" >
          <option selected="selected" value="All Views">All Views</option>
          <c:forEach var="view" items="${views}">
            <option value="${view}">${view}</option>
          </c:forEach>
        </select>
      </div>
      
      <p>&nbsp; </p>
      <div id="wrapper_ln">
<div id="list_area">
    <ul>
        
    </ul>
</div>
<div id="note_content">
<h3>Please select one from the left idea thread</h3>
</div>
</div>
	
    </div>
    </body>
</html>
