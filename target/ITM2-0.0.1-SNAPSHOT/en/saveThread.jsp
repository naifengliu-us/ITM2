<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*"%>
<%


System.out.println("Saving....");
String foo = request.getParameter("getvalue");
System.out.println(foo);
String[] strvalue = request.getParameter("getvalue").split("@");
String project = request.getParameter("projectname");
String focus = request.getParameter("threadfocus");
String nidts = request.getParameter("nidts");
String thread_owner = null;
int thread_deleted = 0;
String username = request.getParameter("username");
String thread_changed_flag = request.getParameter("thread_changed_flag");
String authorcnt = request.getParameter("authorcount");
System.out.println("username is ##################" + username);
Hashtable<String,String> nid_to_timestamp = new Hashtable<String, String>();

try
{
  if(nidts != null && !nidts.isEmpty() && (nidts.compareTo("null") != 0))
  {
	  nidts = nidts.replaceAll("&#32;", " ");
	  String[] split_nidts = nidts.split(";");
	  for(int i = 0; i < split_nidts.length; ++i) {
		String[] key_value = split_nidts[i].split("=");
		nid_to_timestamp.put(key_value[0], key_value[1]);	 
	  }
  }
}
catch(NullPointerException e)
{
  System.out.println("[ERROR] Parsing note ids with timestamp information.");
  e.printStackTrace();
}

sqls s0 = new sqls();
Operatedb opdb0 = new Operatedb(s0,request.getParameter("database"));
ResultSet rs0 = opdb0.getC().executeQuery("select author,deleted from project_thread where threadfocus='"+focus+"';");
if(rs0.next()) {
	thread_owner = rs0.getString(1);
	thread_deleted = rs0.getInt(2);
}

sqls s = new sqls();
Operatedb opdb = new Operatedb(s,request.getParameter("database"));
ResultSet rs = opdb.getC().executeQuery("select idProject from project where projectname='"+project+"';");
rs.next();
int pid = rs.getInt(1);

//if change of action is 1 then create blob
if(thread_changed_flag.equals("1")) {
  opdb.saveThreadData(pid, focus,username,authorcnt);
}

opdb.getC().executeUpdate("delete from thread_note using thread_note inner join project on project.idProject = thread_note.projectid and thread_note.projectid = '" +pid+ "'and thread_note.threadfocus='"+focus+"';");
opdb.getC().executeUpdate("delete from project_thread where project_thread.projectid = '" +pid+ "'and project_thread.threadfocus = '"+focus+"';");
//System.out.println("insert into project_thread (projectid, threadfocus,author) values("+pid+",'"+focus+"','"+thread_owner+"');");
opdb.getC().executeUpdate("insert into project_thread (projectid, threadfocus,author,deleted) values("+pid+",'"+focus+"','"+thread_owner+"','"+thread_deleted+"');");

for (int i = 1; i < strvalue.length;i++) {
	if (nid_to_timestamp.size() < 0) {
		System.out.println("[ERROR : saveThread.jsp] Note id to timestamp information is empty");	
	}
	
	char mk = strvalue[i].charAt(strvalue[i].length() - 1);
	String tmpmk = strvalue[i].substring(0, strvalue[i].length() - 1);
	if(nid_to_timestamp.get(tmpmk).equals("null")) {
	  System.out.println("adding epoch time to the notes that were added before the add time feature existed");
	  //System.out.println("[INFO : saveThread.jsp] insert into thread_note (projectid,threadfocus,noteid,highlight,addtime) values ("+pid+",'"+focus+"',"+tmpmk+","+ mk +","+ "1970-01-01 00:00:00" +");");
	  opdb.getC().executeUpdate("insert into thread_note (projectid,threadfocus,noteid,highlight,addtime) values ("+pid+",'"+focus+"',"+tmpmk+","+ mk +"," +"'" + "1970-01-01 00:00:00" + "'"+");");
	}
	else {
	  //System.out.println("[INFO : saveThread.jsp] insert into thread_note (projectid,threadfocus,noteid,highlight,addtime) values ("+pid+",'"+focus+"',"+tmpmk+","+ mk +","+ nid_to_timestamp.get(tmpmk) +");");
	  opdb.getC().executeUpdate("insert into thread_note (projectid,threadfocus,noteid,highlight,addtime) values ("+pid+",'"+focus+"',"+tmpmk+","+ mk +","+ "'" + nid_to_timestamp.get(tmpmk)+"'"+");");
	}	
}

if(rs0 != null) {
	try {
		rs0.close();
	} catch(SQLException e) {
		e.printStackTrace();
	}
}

if(opdb0 != null) {
	opdb0.CloseCon();
}

if(s0 != null) {
	s0.Close();
}

if(rs != null) {
	try {
		rs.close();
	} catch(SQLException e) {
		e.printStackTrace();
	}
}

if(opdb != null) {
	opdb.CloseCon();
}

if(s != null) {
	s.Close();
}
%>