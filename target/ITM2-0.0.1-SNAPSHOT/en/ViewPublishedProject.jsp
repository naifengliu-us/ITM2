<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ page session = "true" %>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<%@page import="code.AccessToProjects"%>
<%@page import="code.Connect"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!-- Validate user-session -->
<%
    String struser = " ";
    String usertype = " ";
    String host = " ";
    String database = " ";
    boolean ifvalid = false;
    session = request.getSession(false);
    if(session != null) {
	    if (session.getAttribute("username") == null) {
        response.sendRedirect("/ITM2/index.jsp");
	    } else {
	      struser = (String)session.getAttribute("username");
	      usertype = (String)session.getAttribute("usertype");
	      host = (String)session.getAttribute("host");
	      ifvalid = true;
	    }
    }
     
    String project = request.getParameter("projectname");
    
    /// NOTE Looks like we do have dbase in session as well? But 
    /// it seems to be different. We need to check on that.
    database = request.getParameter("dbase");
    session.setAttribute("project", project);
%>

<!-- Fetch permissions for the existing usertype -->
<%
  String projectn = request.getParameter("projectname");
  String timestamp = request.getParameter("timestamp");
  String grades = request.getParameter("grades");
  String teacher = request.getParameter("teacher");
  String school = request.getParameter("school");
  String fromYear = request.getParameter("fromYear");
  String toYear = request.getParameter("toYear");
  String threads = request.getParameter("threads");
  String[] threadNames = threads.split("\\$");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ITM HOME PAGE</title>

<script language="javascript"type="text/javascript">
  var _projectname = "<%=projectn%>";
  var _database = "<%=database%>";
  var _timestamp = "<%=timestamp%>";
</script>

<script src="../js/en/jquery.js"></script>
<script src="http://d3js.org/d3.v3.min.js" type="text/javascript"></script>
<script src="../js/en/header_mapthread.js"></script>
<script src="../js/en/drawThreadMap.js"></script>
<script src="../js/en/drawMap.js"></script>
<script src="../js/en/loadMap.js"></script>
<script src="../js/en/pie.js"></script>
<script src="../js/en/networkGraph.js"></script>
<script src="../js/en/drawThreadGraph.js"></script>
<script src="../js/en/publishedThread.js"></script>
<script src="../js/en/compositeGraph.js"></script>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.10.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/en/jquery.qtip.min.js"></script>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">

<link href="../css/drawThreadMap.css" rel="stylesheet" type="text/css">
<link href="../css/networkGraph.css" rel="stylesheet" type="text/css">
<link href="../css/compositeGraph.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/jquery.qtip.min.css">
<LINK REL=StyleSheet HREF="../css/menu.css" TYPE="text/css"></LINK>

<style>
  .ui-front {
    z-index:1000000 !important; /* The default is 100. !important overrides the default. */
  }
</style>

<script type="text/javascript">
	// Disable the menu elements if there are no children
	function UpdateMenu() {
	  var existing_proj = document.getElementById("open_edit_proj");
	  var e_proj = document.getElementById("work_on_existing_project");
	  var read_only_proj= document.getElementById("read_proj");
	  var r_proj = document.getElementById("visit_a_project");
	  var del_project = document.getElementById("del_proj");
	  var d_proj = document.getElementById("deleted_projects");
	  
	  if(existing_proj == null || existing_proj.children.length == 0) {
	    e_proj.removeAttribute("href");
	    e_proj.removeAttribute("onclick");
	    e_proj.style.color  = "gray";
	     
	  }
	  
	  if(read_only_proj == null) {
	    r_proj.removeAttribute("href");
	    r_proj.removeAttribute("onclick");
	    r_proj.style.color  = "gray";
	  }
	 
	  if(del_project == null) {
	    d_proj.removeAttribute("href");
	    d_proj.removeAttribute("onclick");
	    d_proj.style.color  = "gray";
	  }
	}

	<%-- function start() {  
	}

	function OnOpenProject(name) {
	  window.location='/ITM2/en/ViewPublishProject.jsp?database=<%=database%>&projectname='+name;
	} --%>
		
	function openPublishedThread(focus) {
	  window.open('ViewPublishedThread.jsp?projectname=<%=projectn%>&timestamp=<%=timestamp%>&threadfocus='+focus);
	}

	function OnRefresh() {
	  window.open('ViewPublishProject.jsp?database=<%=database%>');
  }
	
  // Close layer when click-out
  //document.onclick = mclose;
</script>

</head>
<body> <!--bgcolor="#E3F5F9"  -->
    <div id="sticky">
    <div id="header">
    <table border="0">
      <tr>
        <td rowspan="2"><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100" /></td>
        <td><img src="../img/itmlogo840x70name.gif" alt="ITM LOGO" height="70" width="840" /></td>
      </tr>
      <tr>
        <td style="background-color:#01B0F1;">
          <ul id="sddm">
            <li>
              <a href="#">Project</a>
            </li>
            <%if(projectn != null){%>
            <li>
              <a href="#">Thread</a> 
              <!-- Display thread names -->
              <div class="m1">
              <ul>
	              <%
	               if (threadNames != null) {
	                 int cnt = threadNames.length;
	          
	                 for (int i = 0; i < cnt; ++i) { 
	              %>
	                   <li><a onclick="openPublishedThread('<%=threadNames[i]%>') "href="#" title='<%=threadNames[i]%>'><%=threadNames[i]%></a></li>
	              <%
	                 }
	               }
                %>
              </ul>
              </div>
            </li>
            <li><a href="#">Map</a>
               <div class="m1" id="m3">
               <div id="m3threadlist">
               <form name="get_threads" id="get_threads">
               <%
               int cnt = 0; 
               
               if (threadNames != null) {
                 cnt = threadNames.length;
               }
               
               if(cnt > 0){%>
                Select Threads to show<br>
                -------------<br>
                  <%
                  for (int i = 0; i < cnt; ++i) { %>
	                  <div><input type="checkbox" name="threadcheck" value="<%=threadNames[i]%>"/><%=threadNames[i]%></div>
	                  <% 
                  }
                }%>
               </form>
               </div>
               <div id="choosemapcontral">
               
               <a href="#" onclick="mapselectall()" style="float: left;width:98px;padding: 5px 0px;">Select All</a>
               <a href="#" onclick="mapreset()"style="float: right;width:98px;padding: 5px 0px;">Reset</a>
               </div>
                       
	             <div>
	             <a href="#" onclick="MapLoad(true)">Show Map</a>
               </div>
              </div>         
            </li>
            <% }else{%>
                <li class="active"><a href="#"><font color="gray">Thread</font></a></li>
                <li class="active"><a href="#"><font color="gray">Map</font></a></li>
            <% } %>
            <li><a href="/ITM2/index.jsp?logout=1&host=<%= host%>&dbase=<%= database %>">Logout</a>
             </li>
            </ul>
          </td>
        </tr>
    </table>
    <%if(projectn != null){%>
      <div id="projinfo">
	      <p><font> You are viewing published Project:</font>
	      <a href="infopublishproject.jsp?database=<%=database%>&projectname=<%=projectn%>&grades=<%=grades%>&teacher=<%=teacher%>&school=<%=school%>&fromYear=<%=fromYear%>&toYear=<%=toYear%>&hasPublished=<%=true%>" target="_blank"><%=projectn%></a>
	      (Teacher: <%=teacher%>, School Year: <%=fromYear%> to <%=toYear%>)&nbsp;&nbsp;This project has <%=threadNames.length%> published idea thread(s). Click "Thread" to <bold>view</bold> threads.
            Click "<bold>Map</bold>" to show threads.</p>
	      <hr>
      </div>
    <%} %>
</div>
</div>

<div id="wrapperu">
<div id="wrapper">
<div id="published_thread_display"></div>

<div id="raw_area"></div>
<div id="draw_area"></div>

<div id="slider" style ="width:640px;top:52px;position:absolute;left:245px"></div>
<div id = "networkGraph_pieChart">
   <div id ="draggable1" >
     <div id="network_area" style="width:350px;height: 300px;border-width:1px;border-style:dotted;align:center; background:#AFFFFFF;"></div>
         <button type="button" onclick="toggleNetworkGraphType(this);">Cross-Thread Build-ons</button>
   </div>
   <div id ="draggable2">
     <div id="pie_area" style="width:350px;height: 300px;border-width:1px;border-style:dotted;text-align:center;background:#FFFFFF;"></div>
   </div>
</div>

</body>
</html>
