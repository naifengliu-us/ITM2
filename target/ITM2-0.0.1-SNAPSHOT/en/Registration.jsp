<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page session="true"%>
<%@ page import="java.util.*"%>
<%@ page import="javax.mail.*"%>
<%@ page import="javax.mail.internet.*"%>
<%@ page import="javax.activation.*"%>
<%@ page import="java.util.*,java.io.*"%>

<%@page import="database.Operatedb"%>
<%@page import="code.*"%>
<%@ page import="java.sql.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String strsucceed = request.getParameter("ifsucceed");
	String strlogout = request.getParameter("logout");
	if (strlogout != null) {
		session.invalidate();
	}
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Welcome - Idea Thread Mapper</title>
<script type="text/javascript">
	var info = '<%=strsucceed%>';
	if (info == 'no') {
		alert("Wrong login information,please try again");
	}
	if (info == 'yes') {
		window.alert("Your application has been submitted, you will receive a notification after you application is aproved.");
	}
	
</script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script type="text/javascript"> 
function checkValidate()  
{   
  if(registration_form.fname.value==""||registration_form.fname.value==null)  
  {  	    
	  window.alert("First name cannot be empty!!!");   
	  return false;  
  }  
  
  else if(registration_form.lname.value==""||registration_form.lname.value==null)  
  {    
	  window.alert("Last name cannot be empty!!!");   
	  return false;  
  }  
  
  else if(registration_form.id.value==""||registration_form.id.value==null)  
  {   
	  window.alert("Username cannot be empty!!!");  
	  return false;  
  } 
  
  else if(registration_form.password.value==""||registration_form.password.value==null)  
  {  
	  window.alert("Password cannot be empty!!!");  
	  return false;  
  } 
  
  else if(registration_form.Email.value==""||registration_form.Email.value==null)  
  {   
	  window.alert("E-mail cannot be empty!!!");  
	  return false;  
  } 
  
  else if(registration_form.phone.value==""||registration_form.phone.value==null)  
  {   
	  window.alert("Phone number cannot be empty!!!");   
	  return false;  
  }   
  else if(registration_form.URL.value==""||registration_form.URL.value==null)  
  {  
	  
	  window.alert("URL cannot be empty!!!");    	 
	  return false;  
  } 
  
  
 		//registration_form.url.value=location.href;
 		//alert(registration_form.url.value);
 		///window.alert("Your application has been submitted, you will receive a notification after you application is aproved.");
		return true;
}  
</script>


</head>
<LINK REL=StyleSheet HREF="../css/default.css" TYPE="text/css"></LINK>

<body bgcolor="aliceblue">
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<div id="section" style="display: none;"></div>
	<center>
		<form
			style="border-width: 1px; border-style: solid; border-color: #006600; width: 50%"
			name="registration_form" action="/ITM2Dev/Register" method="post"
			onsubmit="return checkValidate();">
			<br />
			<h2>
				<font size="8" color="#71C2B1" face="Times New Roman"> Idea
					Thread Mapper </font>
			</h2>
			<br>
			<h3>Please Enter Your Knowledge Forum Information</h3>
			<br>
			<table>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>URL</b>
						</label></font></td>
					<td><input class="fields" name="URL" id="URL" type="text"
						size="20" value="" /></td>
				</tr>

				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>URL
									Port:</b> </label></font></td>
					<td><input class="fields" name="port" id="port" type="text"
						size="20" value="80" /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Organization:</b>
						</label></font></td>
					<td><input class="fields" name="org" id="org" type="text"
						size="20" value="" /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Username:</b>
						</label></font></td>
					<td><input class="fields" name="id" id="username" type="text"
						size="20" value="" /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Password:</b>
						</label></font></td>
					<td><input class="fields" name="password" id="password"
						type="password" size="20" value="" /></td>
				</tr>



			</table>
			
			<br>
			<h3>Please Enter Community Information</h3>
			<br>
			<table>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>School Name</b>
						</label></font></td>
					<td><input class="fields" name="school_name" id="school_name" type="text"
						size="20" value="" /></td>
				</tr>
<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>School Phone</b>
						</label></font></td>
					<td><input class="fields" name="school_phone" id="school_phone" type="text"
						size="20" value="" /></td>
				</tr>

				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Grade Level
									</b> </label></font></td>
					<td><input class="fields" name="grade_level" id="grade_level" type="text"
						size="20"  /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Address</b>
						</label></font></td>
					<td><input class="fields" name="org" id="org" type="text"
						size="20" value=""  placeholder="Street"/></td>
				</tr>
				<tr>
				<td><font size="3" color="#71C2B1" face="Times New Roman"></td>
					<td ><input class="fields" name="city" id="city" type="text"
						size="20" value=""  placeholder="City"/></td>
				</tr>
				<tr><td><font size="3" color="#71C2B1" face="Times New Roman"></td>
					<td ><input class="fields" name="state" id="state" type="text"
						size="20" value=""  placeholder="state"/></td>
				</tr>
				<tr><td><font size="3" color="#71C2B1" face="Times New Roman"></td>
					<td ><input class="fields" name="country" id="country" type="text"
						size="20" value=""  placeholder="Country"/></td>
				</tr>
				<tr><td><font size="3" color="#71C2B1" face="Times New Roman"></td>
					<td ><input class="fields" name="zipcode" id="zipcode"
						type="password" size="20" value=""  placeholder="zip code"/></td>
				</tr>



			</table>
			</br>
			<h3>Please Enter Your Personal Information</h3>
			</br>
			<table>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>First
									Name:</b> </label></font></td>
					<td><input class="fields" name="fname" id="fname" type="text"
						size="20" value="" /></td>
				</tr>
				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Last
									Name:</b> </label></font></td>
					<td><input class="fields" name="lname" id="lname" type="text"
						size="20" value="" /></td>
				</tr>

				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>E-mail:</b>
						</label></font></td>
					<td><input class="fields" name="Email" id="Email" type="text"
						size="20" value="@" /></td>
				</tr>

				<tr>
					<td><font size="3" color="#71C2B1" face="Times New Roman"><label><b>Phone
									Number:</b> </label></font></td>
					<td><input class="fields" name="phone" id="phone" type="text"
						size="20" value="" /></td>
				</tr>
			</table>

			<font color="#4889EB" size="3" face="Times New Roman"><b><input
					style="width: 80px; height: 31" name="submit" id="form_submit"
					type="button" value="submit"></b></font> <br /> <br />
			<h6>Idea Thread Mapper (ITM) version 1.13, updated on June 03,
				2013</h6>

			<input type="hidden" name="url" id="url" />

		</form>
	</center>

	<script type="text/javascript">
function enterDatabase(
		database,
		user_name,
		password,
		firstname,
		lastName,
		email,
		org,
		phone,
		url,
		port
		){
	
	

	 $.post("../Register", {
		 'fname': firstname,
		 'id':user_name,
		 'lname': lastName,
		 'password':password,
		 'Email': email,
		 'org':org,
		 'db':database,
		 'phone':phone,
		 'url':url,
		 'port':port
		 },function(result){
		 window.location.replace(result);
	 })
	
}

$("#form_submit").click(function(){
	
	  var user_name=$("#username").val();
	    var password=$("#password").val();
	    var firstname=$("#fname").val();
	    var lastName=$("#lname").val();
	    var email=$("#Email").val();
	    var org=$("#org").val();
	    var phone=$("#phone").val();
	    var url=$("#URL").val();
	    var port=$("#port").val();
	    var school_name=$("#school_name").val();
	    var school_phone=$("#school_phone").val();
		/*   
	enterDatabase(
			  "TestDatabases",
				user_name,
				password,
				firstname,
				lastName,
				email,
				org,
				phone,
				url,
				port
				)
*/
	if(!checkValidate())return;
    var user_name=$("#username").val();
    var password=$("#password").val();
    var firstname=$("#fname").val();
    var lastName=$("#lname").val();
    var email=$("#Email").val();
    var org=$("#org").val();
    var phone=$("#phone").val();
    var url=$("#URL").val();
    var port=$("#port").val();
    
    $.post("../kf5connect", {'user_name': user_name,'password':password}, function(result){
    	if(result!=""&&result!=null){
    		
    		var obj = jQuery.parseJSON(result);
    		var htmlContent=" <ul>";
    		for (i = 0; i < obj.length; i++) { 
    			htmlContent+=" <li><input type='checkbox' name='radioSection' value='"+obj[i].sectionTitle+"'>"+obj[i].sectionTitle+"</li>";
    		}
    		htmlContent+=" </ul>";
    		$("#section").html(htmlContent);
    		  $('#section').dialog({title:"Choose Database",
    			  buttons: [
    			            {
    			              text: "OK",
    			              click: function() {
    			            	  var dialCheckResult="";
    			            	  var index =$("input:checkbox[name='radioSection']:checked").length;

    			            	  for(i = 0; i < index; i++){
    			            		  dialCheckResult+="&"+$("input:checkbox[name='radioSection']:checked")[i].value;
    			            	  }
    			            	  if(dialCheckResult){
    			            		  enterDatabase(
    			            				  dialCheckResult,
    			            					user_name,
    			            					password,
    			            					firstname,
    			            					lastName,
    			            					email,
    			            					org,
    			            					phone,
    			            					url,
    			            					port
    			            					)
    			            		  }else{
    			            			  console.log("No database be chosed")
    			            			  }
    			            	
    			                $( this ).dialog( "close" );
    			              }
    			            }
    			          ]});
    		console.log(obj)
    		
    	}else{
    		alert("Wrong login information,please try again");
    	}
       
    });
   
});

</script>
</body>
</html>