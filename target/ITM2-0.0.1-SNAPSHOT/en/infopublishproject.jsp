<%@page import="itm.models.ProjectModel"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
     pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<% 
/************validate the user session*******************/
String username =" ";
String usertype =" ";
  session = request.getSession(false);
  if(session != null){
    if (session.getAttribute("username") == null){
      response.sendRedirect("/ITM2/index.jsp");
    }
    username = (String)session.getAttribute("username");
    usertype = (String)session.getAttribute("usertype");
  }
%>

<%
/*******************Fetch permissions for the existing usertype***************/
//AccessToProjects atp = new AccessToProjects();

//int ProjectInfoPermission = atp.fetchPermission("ProjectInfo",usertype);
//System.out.println("ProjectInfoPermission" + ProjectInfoPermission);
String projectn = request.getParameter("projectname");
String database = request.getParameter("database");
String project = request.getParameter("projectname");
String grades = request.getParameter("grades");
String teacher = request.getParameter("teacher");
String school = request.getParameter("school");
String fromYear = request.getParameter("fromYear");
String toYear = request.getParameter("toYear");
%>

<script src="../js/en/jquery.js"></script>
<script type="text/javascript">
  function OnSave(){
    if(document.Create_form.projectname.value=="") {
        window.alert("please input the project name!");  
        return false;  
    }
    if(document.Create_form.teacher.value=="") {
      window.alert("please input the teacher's name!");  
      return false;  
    }
    if(document.Create_form.school.value=="") {
      window.alert("please input the school name!");  
      return false;  
    }
    if(document.Create_form.grade.value=="") {
      window.alert("please choose grade!");  
      return false;  
    }
    
    var from = parseInt(document.Create_form.fromyear.value);
    var to = parseInt(document.Create_form.toyear.value);
    if(from > to) {
      alert("the from-school-year is greater than the to-school-year, please select them again!");
      return false;
    }
    /*if(document.Create_form.group.value=="")
    {
      window.alert("please choose group!");  
      return false;  
    }*/ 
    return true;
  } 

  function print(object) {
    var output = '';
    for (property in object) {
      output += property + ': ' + object[property]+'; ';
    }
    console.log(output);
  }
  
  
  
  </script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project Information</title>

<LINK REL=StyleSheet HREF="../css/stickyhead.css" TYPE="text/css"></LINK>
<LINK REL=StyleSheet HREF="../css/createProject.css" TYPE="text/css"></LINK>
</head>

<body>
<div id="sticky">
  <div id="header">
  <table border="0">
    <tr>
      <td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100" /></td>
      <td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="60" width="850">
      &nbsp;&nbsp;Basic Information about This Inquiry Project
      </td>
    </tr>
  </table>
      
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
  
  <table>
    <tr>
      <td><b>Topic of study:</b></td>
      <input name="database" type="hidden" value="<%=database%>"/>
      <input name="preproject" type="hidden" value="<%=project%>"/>
      <td style="text-align:left"><%=project%></td>
      <td><b>*Grade Level:</b><%=grades%></td>
    </tr>
    <tr>
      <td><b>Teacher:</b></td>
      <td style="text-align:left"><%=teacher%></td>
      <td></td>
    </tr>
    <tr>
      <td><b>School:</b></td>
      <td style="text-align:left"><%=school%></td>
      <td><b>Knowledge Forum Group:</b></td>
      <td style="text-align:left">
      </td>
    </tr>
    <tr>
      <td><b>School Year:</b>
      <td colspan="3" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;<b>from</b>
      <%=fromYear%>
        <b>to</b>
      <%=toYear%>
      </td>
    </tr>
    <tr>
      <td></td>
      <td colspan="3"></td>
    </tr>
  </table>
  <br>
  <br>
  <input name="save" type="button" id="create_save" value= "Close" onclick="javascript: window.close();"/>&nbsp;&nbsp;&nbsp;</center>
   
</div>
</div>
</body>
</html>
