<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*"%>
<html>
<head>
<link rel=stylesheet href="css/stickyhead.css" type="text/css"></link>
<% 
	/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
		response.sendRedirect("/ITM2/index.jsp");
		}
	}

	/**********read the noteid contained in the current thread from database*******/
   String strproname = request.getParameter("projectname");
   String strfocus = request.getParameter("threadfocus");
   String strdb = request.getParameter("database");
   String proid = null;
   ResultSet rs = null,temprs=null;
   sqls s = new sqls();
   Operatedb opdb = new Operatedb(s,strdb);
   Operatedb opdb_author = null;
   
 	//get the project id
	temprs=opdb.GetRecordsFromDB("project","idproject","projectname='"+strproname+"';");
	if(temprs.next()){
		proid = temprs.getString(1); 
	}
   
   String strcon = "projectid=" + proid+" and threadfocus='" +strfocus+"';" ;
   temprs = opdb.GetRecordsFromDB("thread_note","noteid",strcon);
   
   List<String> idlist = new ArrayList();
   while(temprs.next()){
	   idlist.add(temprs.getString(1));
   }
   
   if(temprs != null){
	   try{
		   temprs.close();
	   }catch(SQLException e){
		   e.printStackTrace();
	   }
   }
%>

<%//System.out.println("+++++++++++++++++++++++++searchnote.jsp++++++++++++++++++++++++");
//System.out.println("searchnote.jsp the project name is "+request.getParameter("projectname"));
//System.out.println("searchnote.jsp the thread focus name is "+request.getParameter("threadfocus"));%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	<!-- link calendar resources -->
	<link rel="stylesheet" type="text/css" href="css/tcal.css" />
	<script type="text/javascript" src="js/tcal.js"></script> 
	
	<script type ="text/javascript">
	var count = 0;
	function UpdateCount(rowCnt)
	{
		count = rowCnt;
		document.getElementById("updatecnt").innerHTML = count;
		//alert(count);
	}
	var cnt = 0;
	function UpdateNoteSelected(rowObject)
	{
		//alert(rowObject);
		//alert(rowObject.checked);
		if(rowObject.checked == true)
		{
		  cnt++;
		}
		if(rowObject.checked == false)
		{
		  cnt--;
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function SendNoteid(){
		var num = document.searchnote_form.notecheck.length;
		var ids="@";
		var putvalue;
		var proname;
		var num=0;
		proname= "<%=request.getParameter("projectname")%>";
		if(document.searchnote_form.notecheck.length == undefined){
			ids += document.searchnote_form.notecheck.value + "@";
			num++;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				if(document.searchnote_form.notecheck[i].checked){
					num++;
					ids += document.searchnote_form.notecheck[i].value + "@";
				   	//alert(document.searchnote_form.notecheck[i].value);
				} 
			}
		}
		if(num==0){
			alert("Please select note first!");
			return false;
		}
		if(num>500){
			alert("You selected too many notes, please select again!");
			return false;
		}
		
		//add the old id list
		<%
		  if(idlist.size() > 0){
				for(int j=0;j<idlist.size();j++)
				{%>
					ids += <%=idlist.get(j)%> + "@";
				<%
				} 
		 }
		%>
		
		putvalue = ids + "&"+proname;
		//alert(putvalue);
		top.topframe.location='thread.jsp?database=<%=strdb%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus")%>&getvalue=' + putvalue;
		top.document.getElementById("framesetter").rows = "100%,0%"
		return true;
	}
	function SelectAll(){
		cnt=0;
		if(document.searchnote_form.notecheck.length == undefined){
			document.searchnote_form.notecheck.checked = true;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				document.searchnote_form.notecheck[i].checked = true;
				cnt++;
			}
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function DeselectAll(){
		cnt=0;
		if(document.searchnote_form.notecheck.length == undefined){
			document.searchnote_form.notecheck.checked = false;
		}else{
			for(var i=0;i<document.searchnote_form.notecheck.length;i++){
				document.searchnote_form.notecheck[i].checked = false;
			}
		}
		document.getElementById("updatenoteselected").innerHTML = cnt;
	}
	function CheckTime(){
		if(document.searchnote_form.fromtime.value=="" && document.searchnote_form.totime.value!="")
			{	
				window.alert("You can't leave the 'From' input box empty!");
				return false;
			
			}
		if(document.searchnote_form.fromtime.value !="" && document.searchnote_form.totime.value=="")
		{	
			window.alert("You can't leave the 'To' input box empty!");
			return false;
		
		}
		
		if(document.searchnote_form.views.value=="")
		{
		  window.alert("please choose one view!");  
		  return false;  
		}
	}
	function CheckValidate(){
		if(document.searchnote_form.fromtime.value=="")
		{
		  window.alert("please input 'From Time'!");  
		  return false;  
		}
		if(document.searchnote_form.totime.value=="")
		{
		  window.alert("please input 'To Time'!");  
		  return false;  
		}
		if(document.searchnote_form.views.value=="")
		{
		  window.alert("please choose one view!");  
		  return false;  
		}
		
		return true;
	}
	function ShowContent(noteid){
		var body = document.body,
	    html = document.documentElement;

		var height = Math.max( body.scrollHeight, body.offsetHeight, 
	                       html.clientHeight, html.scrollHeight, html.offsetHeight );
// 		alert("Height = "+height);
		window.open('/ITM2/New_NoteContent.jsp?database=<%=request.getParameter("database")%>&noteid='+noteid,"notewin","height='+height+', width=750,toolbar=no,scrollbars=yes,menubar=no");
	}
	
	</script>
</head>



<%
	/*********************************read all views title from database******************************/
	List vtitlelist = new ArrayList();
	String strtitle;
	//ResultSet rs = null;
	Statement stmt;
	//Operatedb opdb = null;
	String stryear ;
	String strmonth ;
	String strday ;
	String strfrtime=null;
	String strtotime=null;
	String[] strview = null;
	String strword1="", strword2="",strword="";
	stmt= s.Connect(strdb);
	
	try{
			rs = stmt.executeQuery("SELECT * from `view_table`;");
			while(rs.next()){
				strtitle = rs.getString(2);	
				vtitlelist.add(strtitle);			
			}
		}catch(SQLException e){
			e.printStackTrace();		
		}
	/*************************************************************************************************/
	/*************************Get all notes contained in view chosen from database*******************/
	if(request.getParameter("search") != null){ //user clicked on the search buttom
		//System.out.println("user click on the search button");
		strfrtime = request.getParameter("fromtime");
		strtotime = request.getParameter("totime");
		strview = request.getParameterValues("views");
		strword = request.getParameter("keywords");
		String strrange = "";
		System.out.println("range is "+strrange);
		System.out.println("keywords is "+strword);
		
		if(request.getParameter("range")!= null)
		{
			strrange = request.getParameter("range");
			if(strrange.equals("note title")){
				strword2 = strword;
			}else if(strrange.equals("note content")){
				strword1 = strword;
			}else if(strrange.equals("anywhere")){
				strword1 = strword;
				strword2 = strword;
			}
		}
		
		
		//change the date format if the time period is not empty
		/*
		if( !strfrtime.equals("earliest") && !strtotime.equals("latest")){
			int ifirst = strfrtime.indexOf('/');
			int ilast = strfrtime.lastIndexOf('/');
			stryear = strfrtime.substring(ilast+1,strfrtime.length());
			strmonth = strfrtime.substring(0,ifirst);
			strday = strfrtime.substring(ifirst+1,ilast);
			
			strfrtime = stryear + "-"+strmonth+"-"+strday;
			
		    ifirst = strtotime.indexOf('/');
		    ilast = strtotime.lastIndexOf('/');
			stryear = strtotime.substring(ilast+1,strtotime.length());
			strmonth = strtotime.substring(0,ifirst);
			strday =strtotime.substring(ifirst+1,ilast);
			
			strtotime = stryear + "-"+strmonth+"-"+strday;		
		}*/
		
		if(!strfrtime.equals("earliest")){
			int ifirst = strfrtime.indexOf('/');
			int ilast = strfrtime.lastIndexOf('/');
			stryear = strfrtime.substring(ilast+1,strfrtime.length());
			strmonth = strfrtime.substring(0,ifirst);
			strday = strfrtime.substring(ifirst+1,ilast);
			
			strfrtime = stryear + "-"+strmonth+"-"+strday;
		}
		
		if(!strtotime.equals("latest")){
			int ifirst = strtotime.indexOf('/');
		    int ilast = strtotime.lastIndexOf('/');
			stryear = strtotime.substring(ilast+1,strtotime.length());
			strmonth = strtotime.substring(0,ifirst);
			strday =strtotime.substring(ifirst+1,ilast);
			
			strtotime = stryear + "-"+strmonth+"-"+strday;
		}
		
		
	    //opdb = new Operatedb(s,strdb);
		
		rs = opdb.GetStemmedNotes(strfrtime,strtotime,strview,strword1,strword2,strrange);
	}
	/*************************************************************************************************/
%>

<body >
<div id="wrapperu" style="top:0px;">
<div id="wrapper">
	<form name="searchnote_form" action="/ITM2/SearchMore.jsp?database=<%=strdb%>&projectname=<%=request.getParameter("projectname")%>&threadfocus=<%=request.getParameter("threadfocus") %>" method="post">
	
		<div style="background:#1ABDE6">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
			<label>Find Notes: From</label>&nbsp;&nbsp;
			<%if(request.getParameter("fromtime")!=null){%>
				<input type="text" style="background:white" id="ftimeid" name="fromtime" class="tcal" value="<%=request.getParameter("fromtime")%>" />&nbsp;&nbsp;&nbsp;
			<% }else{%>
				<input type="text" style="background:white" id="ftimeid" name="fromtime" class="tcal" value="earliest" />&nbsp;&nbsp;&nbsp;
			<%}%>
			<label>To</label>&nbsp;&nbsp;&nbsp;
			<%if(request.getParameter("totime")!=null){%>
			<input type="text" style="background:white" name="totime" class="tcal" value="<%=request.getParameter("totime") %>" />&nbsp;&nbsp;&nbsp;
			<% }else{%>
			<input type="text" style="background:white" name="totime" class="tcal" value="latest" />&nbsp;&nbsp;&nbsp;
			<%}%>
			<br/>
			
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label>In View</label>&nbsp;&nbsp;
			<select name="views" multiple="multiple" size="4" >
			<%if(request.getParameterValues("views")!=null){
			     String[] strtemp = request.getParameterValues("views");
			     for(int j=0; j<strtemp.length; j++){%>
			    	 <option selected="selected" value="<%=strtemp[j]%>"><%=strtemp[j]%></option>
			     <%}%>
			<% }else{%>
				<option selected="selected" value="all views">All Views</option>
			<%}%>
			<%
				for(int i=0; i<vtitlelist.size();i++){
					strtitle = (String)vtitlelist.get(i);
			%>  <option value="<%=strtitle%>"><%=strtitle%></option>
			<%}%>	
				<option value="all views">All Views</option>
							
			</select>&nbsp;&nbsp;&nbsp;	
			<label>(Shift+click to choose more than one)</label>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label>Key Word:</label>&nbsp;&nbsp;&nbsp;
			<%if(request.getParameter("keywords")!=null){%>
				<input class="fields" name = "keywords" id = "key" type = "text" size = "40" value = "<%=request.getParameter("keywords")%>"/>
			<%}
			  else{%>
			  	<input class="fields" name = "keywords" id = "key" type = "text" size = "40" value =""/>
			 <%}%>
			  &nbsp;&nbsp;
			 <label>in</label>&nbsp;&nbsp;
			 <select name="range" size="1" >
			<%if(request.getParameter("range")!=null){%>
				<option value="<%=request.getParameter("range")%>"><%=request.getParameter("range") %></option>
			<% }else{%>
				<option value="anywhere">anywhere</option>
			<%}%>
				<option value="anywhere">anywhere</option>
			    <option value="note content">note content</option>
			    <option value="note title">note title</option>		
			</select>
			<input type="submit" style="height:40px" name="search" value="Search more notes" onClick="return CheckTime();"/>
			<input type="button" style="height:40px" value="Cancel search" onclick="javascript: top.document.getElementById('framesetter').rows = '100%,0%';"/>
			<br/>		  
		</div>
			
		<br/>
		<label>Number of Notes Found:</label>
        <span id="updatecnt" >0</span>
        &nbsp;&nbsp;
		<label>Number of Notes Selected:</label>
        <span id="updatenoteselected" >0</span>
		<br/>
		<input type="button" name= "selectall"  onClick="SelectAll()"  id="selectall" value="Select All">
		<input type="button" name= "deselectall"  onClick="DeselectAll()" id="deselectall" value="Deselect All">
		<input type="button" name= "showthread" onClick="return SendNoteid()" value="Add To Thread">
			
		<div style="overflow: auto; height: 300px; width:1000px;">		
		<table style="width: 1000px;" cellpadding="5" cellspacing="10">
				<tr>
				<th></th>
				<th>Title</th>
				<th>View</th>
				<th>Time Created</th>
				<th>Author</th>
				<th>Content</th>
				</tr>
				
		  <%
		  	int irow=0;
		  	opdb_author = new Operatedb(new sqls(),request.getParameter("database"));
			while(rs.next()){
				//System.out.println("irow = " +irow);
				irow++;
				
				if(!idlist.contains(rs.getString(4))){%>
				<tr>
					<td>
					<input type="checkbox" class="notecheck" name="notecheck" value="<%=rs.getInt(4)%>"/>
					</td>
					
					<td>
					<%=rs.getString(1)%> <!--display the note's title-->
					</td>
					
					<td>
					<%=rs.getString(2)%> <!--display the view's title-->
					</td>
					
					<td>
					<%=rs.getString(3)%><!--display the note's create time-->
					</td>
					
					<!-- display the note's authors -->
					<td>
					<%String strid=rs.getString(4);
					  List authorList = opdb_author.getMultipleAuthors(strid);
					  for(int i=0;i<authorList.size();i++){
						  String firstname = (String)authorList.get(i);%>
						  <%=firstname %>
						  <%if(i < authorList.size()-1){
						  %>
						  ,&nbsp;
						  <%}%>	  
					  <%}%>
					</td>
					
					<td  style= "cursor:pointer">
					<div onclick="javascript:var a = document.searchnote_form.<%="pre"+rs.getString(4)%>.value;ShowContent(a);">
				    <font color="#1ABDE6"><U><%=rs.getString(5)%>...</U></font><!--display the first 20 words-->
					</div>
					<input type="hidden" name="<%="pre"+rs.getString(4) %>" value="<%=rs.getString(4)%>"/> 
					</td>
				</tr>
				<%}
			 }%>
		</table>
		</div>	
		
	
		<script type="text/javascript">
		UpdateCount('<%= irow%>');
// 		$("#selectall").click(function() {
// 			$(".notecheck").attr('checked', true);
// 		});
// 		$("#deselectall").click(function() {
// 			$(".notecheck").attr('checked', false);
// 		});
		</script>

			</form>
	</div>
	</div>
</body>
</html>
<%
	//release the connection to the database
	if(rs != null){
		try{
			rs.close();
		}catch(SQLException e){		
			e.printStackTrace();
		}
	}
	if(stmt != null){
		try{
			stmt.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(opdb_author != null){
		opdb_author.CloseCon();
	}
	if(s != null){
		s.Close();
	}
	
%>