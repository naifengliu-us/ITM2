<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,com.knowledgeforum.k5.common.*,org.zoolib.*,org.zoolib.tuplebase.*,code.*,database.*"%>
    
<%@ page import="itm.models.NoteModel" %>
<%@ page import="itm.controllers.NoteController" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
//request.setCharacterEncoding("UTF-8"); 
/************validate the user session*******************/
String username =" ";
String usertype =" ";
	session = request.getSession(false);
	if(session != null) {
		if (session.getAttribute("username") == null) {
			response.sendRedirect("/ITM2/index.jsp");
		}
		else {
		  username = (String)session.getAttribute("username");
		  usertype = (String)session.getAttribute("usertype");
		}
	}
	
	String projectName = request.getParameter("projectname");
	String threadName = request.getParameter("threadfocus");
	String timestamp = request.getParameter("timestamp");
%>
<html>
<head>
	<title>Thread <%=threadName%>  in Project <%=projectName%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
	<!-- Style sheets -->
	<link href="../css/stickyhead.css" rel="stylesheet" type="text/css">
	<link href="../css/screenLock.css" rel="stylesheet" type="text/css">
	<link href="../css/itm_week_beta.css" rel="stylesheet" type="text/css">
	<link REL=StyleSheet HREF="../css/btn_link.css" TYPE="text/css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="../css/jquery.qtip.min.css">
	<link href="../css/thread.css" rel="stylesheet" type="text/css">
	<link href="../css/impress.css" rel="stylesheet" type="text/css">
	<link href="../css/drawThreadGraph.css" rel="stylesheet" type="text/css">
	
	<style type="text/css">
		/* A class used by the jQuery UI CSS framework for their dialogs. */
		.ui-front {
		    z-index:10000 !important; /* The default is 100. !important overrides the default. */
		}
	</style>
	
	<!-- javascript -->
	<script src="../js/en/jquery.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="http://d3js.org/d3.v3.min.js" type="text/javascript"></script>
	<script src="../js/en/jquery.qtip.min.js" type="text/javascript"></script>
	<script src="../js/en/drawThreadGraph.js" type="text/javascript"></script>
	<script src="../js/en/threadHistory.js" type="text/javascript"></script>
	<script src="../js/en/barChart.js" type="text/javascript"></script>
	<script src="../js/en/thread_update.js"></script>
	<script src="../js/en/drawThreadGraph.js"></script>
	<script src="../js/en/itm_week_beta.js"></script>
	<script src="../js/en/screenLock.js"></script>
	<script src="../js/en/publishedThread.js"></script>
	
	<script>
	 var viewPubThread = {
	     threadGraph : new publishedThread('<%=projectName%>', 
	         '<%=threadName%>', '<%=timestamp%>', "#thread_vis", "#slider"),
	     close :  function() { top.window.close(); }
	 }
	</script>
</head>

<body onload='viewPubThread.threadGraph.showThread()'>
  <!-- alert for no links available -->
  <div id="alertDialog" style="display:none;"> 
    <h5><center>No Links Available</center></center></h5>
  </div> 

  <div id="alertNoNoteDialog" style="display:none;"> 
    <h5><center>None Note Selected</center></center></h5>
  </div> 

  <div id="sticky">
	 <div id="header">
	   <table border="0">
		    <tr>
			   <td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="60" width="60" /></td>
			   <td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;" height="60" width="870">
				   <%if(request.getParameter("ifexist")==null && request.getParameter("threadfocus")==null){%>
				     &nbsp;&nbsp;&nbsp;<font size="5">Add an Idea Thread for Project: <%=request.getParameter("projectname")%></font>
				   <%}else if(request.getParameter("ifexist")!=null && request.getParameter("threadfocus")!=null){ %>
				     &nbsp;&nbsp;&nbsp;<font size="5">Add an Idea Thread for Project: <%=request.getParameter("projectname")%></font>
				   <%}else{ %>
				     &nbsp;&nbsp;&nbsp;<font size="5">Update Idea Thread: <%=threadName%></font><br><font size="3">&nbsp;&nbsp;&nbsp;Project: <%=projectName%></font>
				   <%} %>			
			   </td>
		    </tr>
      </table>
    <div id=td_info>
    <div id="td_name">
    </div><span id="ajax_state"></span>
      <div id="button">
		    <ul id="thread_sddm">
          <li><a href="#" onclick="viewPubThread.close()">Close</a></li>
		    </ul>
	   </div>
    </div>
  </div>
  </div>
  
  <div id="wrapperu">
  <div id="wrapper">

<!--------------------------------display the individual thread map----------------------------------------->	 
  <div id="fm">
	  <div id="threadinfo"></div>
			<ul id="flag">
				<li id="showalltitle">&nbsp;|&nbsp;<a href="javascript:void(0)" onclick="tg.toggleTitles(this)";>Show Title</a>&nbsp;|&nbsp;</li>
				<li id="showallauthor"><a href="javascript:void(0)" onclick="tg.toggleAuthors(this)">Show Author</a>&nbsp;|&nbsp;</li>
				<li id="showalllink">
				<a id ="showalllinkbuildon" href="javascript:void(0)" onclick="tg.toggleLinks(this, 'buildons', $('#alertDialog'));">Show Build-on</a>&nbsp;|&nbsp;
				<a id ="showalllinkreferences" href="javascript:void(0)" onclick="tg.toggleLinks(this, 'references', $('#alertDialog'))">Show Reference</a>&nbsp;|&nbsp;
				</li>
		 </ul>
	</div>
	
  <div id="up_area">
  <div id="wrapper_draw">
	  <div id="right_area">
	     <!-- <div id="draw_area" ondblclick="redraw()" onclick="drawnoteclick(event)"></div>
	     <div id="reference_area"></div>
	     <div id="buildon_area"></div>
	     <div id="annotate_area"></div> -->
	     <!--<div id="background_area"></div>
	     
	     <div id="bar_left" onclick="windowmoveleft()"></div>
	     <div id="bar_area">
	     <div id="bar"></div>
	     <div id="pointer" onmousedown="ptdown(event)" onmouseup="ptup(event)"></div>
	     </div>
	     <div id="bar_right" onclick="windowmoveright()"></div>
	     <div id="time_area"></div>-->
	     <div id="thread_vis"></div>
	     <div id="slider" style="width: 830px;position:absolute;top:392px;left:50px"></div>
	    </div>
  </div>

  <div id="draw_ctrl">
    <ul id="node_contral">
			<li>Note:&nbsp;&nbsp;</li>
			<li><a id="showallhighlighted" href="javascript:void(0)" onclick="greyOutSomeFeatures('HighlightedNotes');tg.toggleHighlightedNotes(this);">Show Highlighted Notes</a>&nbsp;|&nbsp;</li>
			<li><a id="showallhighlightedbuildons" href="javascript:void(0)" onclick="greyOutSomeFeatures('HighlightedBuildons');tg.toggleHighlightedBuildons(this);">Show Highlighted Buildons</a>&nbsp;|&nbsp;</li> 
			<li style="display: none"><a id="viewNotesSlide" href="javascript:void(0)" onclick="viewNotesSlide();">Notes SlideShow</a>&nbsp;&nbsp;</li>   	
    </ul>
    <ul id="zoom">
      <li>Time Range:&nbsp;&nbsp;</li>
	    <li><a href="javascript:void(0)" onclick="tg.resetGraphView()" id="resetGraph">View All</a>&nbsp;&nbsp;&nbsp;</li>
    </ul>
  </div>
  
	<div id="wrapper_ln">
    <div id="list_area" onclick="selectListItem(event)">
      <ul></ul>
    </div>
    <div id="note_content">
      <h3>Select a note to show its content here...</h3>
    </div>
	</div>

	</div>
	  <div id="raw_area"></div>
	</div>
</div>
</body>
</html>