package itm.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class SupportModel extends BaseModel {

    public SupportModel(String dbname) throws InstantiationException,
        IllegalAccessException, ClassNotFoundException, SQLException {
        super(dbname);
        table = "note_table";
        // TODO Auto-generated constructor stub
    }

    public List<String> getSupport(String noteid) throws SQLException {
        List<String> supportList = new ArrayList<String>();
        sql = "SELECT noteid,sn.supportid as supportid,st.text as text " +
              "FROM `support_note` sn " +
              "LEFT JOIN support_table st on sn.supportid=st.supportid " +
              "WHERE sn.noteid = " + noteid;
//		System.out.println(sql);

        ResultSet support = querySelect(sql);

        if(!support.wasNull()) {
            while(support.next()) {
//				System.out.println(support.getString("text"));
                supportList.add(support.getString("text"));
            }
        }

        return supportList;
    }

}
