/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.models;

/**
 *
 * @author Stan
 */
public interface Project {
    String getName();
    void setName(String pName);
    String getDB();
    void setDB(String db);
    String getThreads();
    void setThreads(String threads);
}
