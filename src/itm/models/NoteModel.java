package itm.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

public class NoteModel extends BaseModel {

    public NoteModel(String dbname) throws InstantiationException,
        IllegalAccessException, ClassNotFoundException, SQLException {
        super(dbname);
        table = "note_table";
        // TODO Auto-generated constructor stub
    }

    public ResultSet getNote(String sql) throws SQLException {
        System.out.println(sql);
        return querySelect(sql);
    }

    public String getNoteContent(String noteid) throws SQLException {
        sql = "SELECT notecontent  " +
              "FROM note_table " +
              "WHERE noteid = " + noteid;
        //System.out.println(sql);
        ResultSet rs = querySelect(sql);
        rs.next();
        temps = rs.getString("notecontent");
        rs.close();
        return temps;
    }

    public String getOffset(String noteid) throws SQLException {
        sql = "SELECT offset  " +
              "FROM note_table " +
              "WHERE noteid = " + noteid;
        //System.out.println(sql);
        ResultSet rs = querySelect(sql);
        rs.next();
        temps = rs.getString("offset");
        rs.close();
        return temps;
    }

}
