/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.models;

/**
 *
 * @author Stan
 */
public abstract class PublishProject implements Project {
    private String grade;
    private String userID;
    private String projectName;
    private String db;

    public void setGrade(String grade) {
        this.grade = grade;
    }
    public String getGrade() {
        return grade;
    }
    public String getUserID() {
        return this.userID;
    }
    public void setUserID(String uid) {
        this.userID = uid;
    }
    @Override
    public String getName() {
        return projectName;
    }


    @Override
    public void setName(String pName) {
        this.projectName = pName;
    }
    @Override
    public String getDB() {
        return db;
    }

    @Override
    public void setDB(String db) {
        this.db = db;
    }
}
