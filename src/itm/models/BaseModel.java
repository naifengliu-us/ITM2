package itm.models;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseModel {

    public java.sql.Connection conn;
    protected Statement st;
    protected ResultSet rs;
    protected String sql;
    public String table;
    public String temps;
    public boolean tempb;

    public BaseModel(String dbname) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        // FIXME: The URL should not be hardcoded
        String url = "jdbc:mysql://localhost:3306/";
        String db = dbname;
        String driver = "com.mysql.jdbc.Driver";
        String user = "root";
         String pass="051633b$";
        //String pass = "root";

        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conn = DriverManager.getConnection(url + db, user, pass);
        st = conn.createStatement();
    }

    public void createStructure() throws SQLException {

    }

    public void query(String s) throws SQLException {
        try {
            st.executeQuery(s);
        } finally {
//			st.close();
        }
    }

    public void queryUpdate(String s) throws SQLException {
        System.out.println(s);
        st.executeUpdate(s);
//		st.close();
    }

    public ResultSet querySelect(String s) throws SQLException {
//		System.out.println(s);
        return st.executeQuery(s);
    }


    public String queryInsert(String s) throws SQLException {
        st.execute(s);

        ResultSet rs = st.executeQuery("SELECT max(" + table + "_id) as id from " + table);
        while(rs.next())
            temps = rs.getString("id");
        rs.close();
//		st.getConnection().close();

        if(temps.isEmpty())
            return "-1";
        else
            return temps;
    }


    public void deleteAll() throws SQLException {
        Statement st = conn.createStatement();
        sql = "DELETE FROM " + table;
        st.execute(sql);
    }

    public ResultSet getRow() throws SQLException {
        sql = "SELECT * FROM " + table;
        return querySelect(sql);
    }

    public ResultSet getColumn(String field) throws SQLException {
        sql = "SELECT " + field + " FROM " + table;
        System.out.println(sql);
        return querySelect(sql);
    }

    public ResultSet getdatRow(String key, String value) throws SQLException {
        Statement st = conn.createStatement();
        sql = "SELECT * FROM " + table + " where " + key + " = " + value;
        return st.executeQuery(sql);
    }

    public String lastInserted() throws SQLException {
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT max(" + table + "_id) as id from " + table);

        while(rs.next())
            return rs.getString("id");
        return "-1";
    }

    public boolean exists(String key, String value) throws SQLException {
//		Statement st = conn.createStatement();
        sql = "SELECT * FROM " + table + " where " + key + " = " + value;
        ResultSet rs = st.executeQuery(sql);
        if(rs.wasNull())
            tempb = false;
        else
            tempb = true;
        rs.close();
//	    rs.getStatement().getConnection().close();
        return tempb;
    }

//	public String match(String key, String value) throws SQLException{
//		Statement st = conn.createStatement();
//	    sql = "SELECT id FROM "+table+" where "+key+" = "+value;
//	    ResultSet rs = st.executeQuery(sql);
//		if (!rs.wasNull())
//			return rs.getString("id");
//		return "-1";
//	}

    public String get_id(String key, String value) throws SQLException {
        Statement st = conn.createStatement();
        sql = "SELECT id FROM " + table + " where " + key + " = " + value;
        ResultSet rs = st.executeQuery(sql);
        if (!rs.wasNull())
            return rs.getString("id");
        return "-1";
    }

    public static String implodeArray(String[] inputArray, String glueString) {
        String output = "";
        if (inputArray.length > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(inputArray[0]);

            for (int i = 1; i < inputArray.length; i++) {
                sb.append(glueString);
                sb.append(inputArray[i]);
            }
            output = sb.toString();
        }
        return output;
    }


    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    /*
     * Write records into database
     * @param strtable
     * 		table name
     * @param stropt
     * 		operation string, could be "DELETE","INSERT" and "UPDATE"
     * @param strvalue
     * 		the corresponding field and values need to be changed
     * */
    public void WritebacktoDB(String strtable, String stropt, String strvalue) {
        String strsql = null;

        if(stropt.compareTo("INSERT") == 0)
            strsql = "INSERT INTO " + "`" + strtable + "`" + strvalue;
        else if(stropt.compareTo("UPDATE") == 0) {
        }

        try {
            System.out.println("the sql is :" + strsql);
            queryUpdate(strsql);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

//	public void finalize(){
//		try {
//
////			if(rs!=null)
////				rs.close();
//			if(st!=null)
//				st.close();
//			if(conn!=null)
//				conn.close();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}


}
