/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.servlets;

import code.sqls;

import com.google.gson.Gson;

import itm.controllers.SavedPublishProject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stan, Shweta
 */
@WebServlet("/EditTeacherReflectionServlet")
public class EditTeacherReflectionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
     if (request.getParameter("downloadFilepath") != null) {
        String filePath = request.getParameter("downloadFilepath");
       	//if (request.getParameter("/WEB-INF/upload") != null) {
    	 // String filePath = request.getParameter("/WEB-INF/upload");
        if(filePath == null || filePath.equals("")){
          throw new ServletException("File Name can't be null or empty");
        }
        //File file = new File(request.getServletContext().getAttribute("FILES_DIR")+File.separator+fileName);
        File file = new File(filePath);
        if(!file.exists()){
          throw new ServletException("File doesn't exists on server.");
        }
        System.out.println("File location on server::"+file.getAbsolutePath());
        ServletContext ctx = getServletContext();
        InputStream fis = new FileInputStream(file);
        String mimeType = ctx.getMimeType(file.getAbsolutePath());
        response.setContentType(mimeType != null? mimeType:"application/octet-stream");
        response.setContentLength((int) file.length());
        String fileName = filePath.substring(filePath.lastIndexOf(File.separator) + 1,
                            filePath.length());
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        
        ServletOutputStream os = response.getOutputStream();
        byte[] bufferData = new byte[1024];
        int read=0;
        while((read = fis.read(bufferData))!= -1) {
          os.write(bufferData, 0, read);
        }
        os.flush();
        os.close();
        fis.close();
        System.out.println("File downloaded at client successfully");
      } else {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        
        try {
          String projectName = request.getParameter("projectname");
          String grade = request.getParameter("grade");
          String showID = request.getParameter("showID");
          String getData = request.getParameter("getData");
          String threads_name=request.getParameter("threads_name");
  
          HttpSession session = request.getSession();
          session.setAttribute("grade", grade);
          session.setAttribute("showID", showID);
          session.setAttribute("projectName", projectName);
          session.setAttribute("threads_name", threads_name);
          
  
          if (getData == null || getData.isEmpty()) {
            String url = "/en/EditTeacherReflection.jsp?projectname=" + projectName;
            RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
            rd.forward(request, response);
          } else {
            if (request.getParameter("databasename") != null && 
                request.getParameter("timestamp") != null) {
              Timestamp ts = null;
              String projectname = request.getParameter("projectname").toString();
              String databasename = request.getParameter("databasename").toString();
              String timestamp = request.getParameter("timestamp").toString();
              
              // Feb 27, 2014 8:32:03 PM
              SimpleDateFormat datetimeFormatter = new SimpleDateFormat(
                  "MMM dd, yyyy hh:mm:ss a");
              java.util.Date fromdate;
              try {
                  fromdate = (java.util.Date) datetimeFormatter.parse(timestamp);
                  ts = new Timestamp(fromdate.getTime());
              } catch (ParseException e) {
                e.printStackTrace();
              } finally {
              }
              
              String strsql = "select * from itm.publish, itm.teacher_reflection where publish.publish_id = teacher_reflection.publish_publish_id and projectname='" + projectname + "' and "+
                              "KF_URL='" + databasename + "' and timestamp='" + ts + "';";
              System.out.println(strsql.toString());
              ResultSet rs_problem = null;
              sqls s = new sqls();
              Statement stmt = s.Connect(databasename);
              try {
                rs_problem = stmt.executeQuery(strsql);
                if (rs_problem.next()) {
                  
                  HashMap<String, String> hm = new HashMap<String, String>();
                  hm.put("bigidea", rs_problem.getString("bigidea"));
                  hm.put("facilitated", rs_problem.getString("facilitated"));
                  hm.put("helpfulActivities", rs_problem.getString("helpfulActivities"));
                  hm.put("lessonsLearned", rs_problem.getString("lessonsLearned"));
                  hm.put("attachment_path", rs_problem.getString("attachment_path"));
                  System.out.print("attachment_path");
                  hm.put("attachment_title", rs_problem.getString("attachment_title"));
                 
                  Gson gson = new Gson();
                  out.write(gson.toJson(hm));
                } 
              } catch (SQLException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
              } finally {
                if (rs_problem != null) {
                  try {
                    rs_problem.close();
                  } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                  }
                }
              }
            }
          }
        } finally {
          out.close();
        }
      }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}