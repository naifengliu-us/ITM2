/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.servlets;

import code.sqls;
import database.DBUtil;
import database.ProjectObj;
import database.PublishDB;
import itm.controllers.SavedPublishProject;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stan
 */
public class ShowPublishSave extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String db = session.getAttribute("database").toString();

        sqls sql = new sqls();
        int pid = Integer.parseInt(request.getParameter("id"));
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            /*
             * Show selected save info on a table
             * 1.0 supports editing showID and TR
             */
            ProjectObj pInfo = new ProjectObj();
            LinkedList<String> tr = new LinkedList<String>();
            tr = PublishDB.SelectTeacherReflection(pid, db);

            Connection conn = sql.getConn(db);

            SavedPublishProject save = new SavedPublishProject();
            //jdbc
            String query = "SELECT * FROM publish as a inner join project as b on (a.project_name=b.projectname) where a.publish_id=? ";
            ps = conn.prepareStatement(query);;
            ps.setInt(1, pid);
            rs = ps.executeQuery();
            if(rs.next()) {
                pInfo.setName(rs.getString("project_name"));
                pInfo.SetTeacher(rs.getString("teacher"));
                pInfo.SetSchoolYear(rs.getString("fromyear") + " - " + rs.getString("toyear"));
                pInfo.SetSchool(rs.getString("school"));
                save.setShowID(rs.getBoolean("showID"));
                save.setDate(rs.getDate("date"));
                save.setPID(rs.getInt(1));
                save.setThreads(rs.getString("threads_name"));
                save.setTeacherReflection(tr);
                System.out.println("5555555555555555555555"+ps.toString());
                
            }
            sql.Close();
            String grade = PublishDB.SelectGrade(pInfo.getName(), db);
            save.setGrade(grade);
            //Get other avaliable projects for save to edit
            LinkedList<ProjectObj> otherProjects = PublishDB.SelectOtherProjects(pInfo.getName(), db);
            //           set session
            session.setAttribute("ops", otherProjects);
            session.setAttribute("pInfo", pInfo);
            session.setAttribute("save", save);
            String url = "";
            if((!(Boolean)session.getAttribute("isCN"))) {
                url = "/en/Saves_PublishProject.jsp";
            } else {
                url = "/cn/Saves_PublishProject.jsp";
            }
            RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
            rd.forward(request, response);
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

