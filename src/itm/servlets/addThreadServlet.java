package itm.servlets;

import database.ParameterObj;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Stan
 */
public class addThreadServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	String database=request.getParameter("database");
		String input_threadfocus=request.getParameter("input_threadfocus");
		
		String results = "";
		
		Connection con;
		ResultSet rs;
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		}
		catch(Exception e){
			System.out.print(e);
		} 
		try
		{
			String uri="jdbc:mysql://localhost:3306/"+database;
			System.out.println(uri);
			con=DriverManager.getConnection(uri,"root","051633b$");
			PreparedStatement ps = con.prepareStatement("select * from thread_note where threadfocus='?'");
			ps.setString(1, input_threadfocus);
			rs=ps.executeQuery();
			
			
			if (rs.next()) {  
		          results="find";
		    }  
			con.close();
			
		}
		catch(SQLException e1){
			System.out.print(e1);
		}
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(results);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            HttpSession session = request.getSession();
            String threadName = request.getParameter("thread_focus");
//                     String projectName=request.getParameter("projectname");
            String database = session.getAttribute("database").toString();
            String projectName = session.getAttribute("project").toString();

            ParameterObj pObj = new ParameterObj(database, projectName, threadName);

            session.setAttribute("parameters", pObj);
//         Check language
            String url = "";
                url = "/en/thread_action.jsp?database=" + database + "&projectname=" + projectName + "&threadfocus=" + threadName;
           
            RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
            rd.forward(request, response);
        } finally {
            out.close();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
