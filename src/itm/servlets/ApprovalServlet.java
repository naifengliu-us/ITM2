/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.servlets;

import code.MailUtilGmail;
import code.RegisterDatabase;
import code.sqls;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.albany.edu.IntergrateFactory.FactoryConfiguration;
import org.albany.edu.pugin.KF5Integration;
import org.albany.edu.webservice.ITMService;

/**
 *
 * @author Stan
 */
@WebServlet("/ApprovalServlet")
public class ApprovalServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            //JDBC
            RegisterDatabase rd = new RegisterDatabase();
            String[] pars = new String[11];
            pars[0] = request.getParameter("Fname");
            pars[1] = request.getParameter("Lname");
            pars[2] = request.getParameter("url");
            pars[3] = request.getParameter("db");
            pars[4] = request.getParameter("org");
            pars[5] = request.getParameter("id");
            pars[6] = request.getParameter("Email");
            pars[7] = request.getParameter("phone");
            pars[8] = request.getParameter("password");
            pars[9] = request.getParameter("port");
            pars[10] = request.getParameter("status");
         
            //DB register approved, send email to notify user
            String from = "itm.test.send@gmail.com";
            HttpSession ses = request.getSession();

            String subject = "";
            String body = "";
            String url = "";
            String serverURL = request.getRequestURL().toString();
            serverURL = serverURL.substring(0, serverURL.length() - 15);
            if((!(Boolean)ses.getAttribute("lan"))) {
                subject = "Your Database registration on ITM has been approved. (Do not reply)";
                body = "Dear " + pars[0] + ",\n\n"
                       + "Thanks for registering ITM database, you can go to <a href=\"" + serverURL + "\">ITM</a> to use data from your database now. <br>This is an automated message from ITM, do not reply.<br><br>"
                       + "Idea Thread Mapper";
                url = "/Approval.jsp";
            } else {
                subject = "Your application for registering server is approved(Please do not reply)";
                body = "Respect" + pars[1] + pars[0] + ",<br>"
                       + "Thank you for registering server on ITM, we are pleased to anounce you that you application is approved. Now you could use your registered database on <a href=\"" + serverURL + "\">ITM</a>上使用您所注册的数据库了。\n"
                       + "This email is automatecally generated, please do not reply this one.";
                url = "/Approval.jsp";
            }
            //send email
            try {
                MailUtilGmail.sendMail(pars[6], from, subject, body, true);
            } catch(MessagingException e) {
                this.log(
                    "Unable to send email. \n" +
                    "You may need to configure your system as " +
                    "described in chapter 15. \n" +
                    "Here is the email you tried to send: \n" +
                    "=====================================\n" +
                    "TO: " + pars[6] + "\n" +
                    "FROM: " + from + "\n" +
                    "SUBJECT: " + subject + "\n" +
                    "\n" +
                    body + "\n\n");
            }

           
    		
         
            
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
            
            FactoryConfiguration config = new FactoryConfiguration();
    		config.setEmail(pars[6].trim());
    		config.setFirstName(pars[0].trim());
    		config.setLastName(pars[1].trim());
    		config.setOrg(pars[4].trim().replace('.', '_').replace(" ","_"));
    		config.setPassword(pars[8].trim());
    		config.setProjectName(pars[2]);
    		config.setUserName(pars[5].trim());
    		String URL="https://kf.utoronto.ca:443/kforum/";
    		config.setDb(pars[3].trim().replace("-", "_").replace(" ", "_").replace(".", "_")+"temp");
    		ITMService service = new ITMService();
    		boolean flag=service.login(config,URL,"/network/rit/lab/zhanglab/kf5py-master/login.py");
    		if(flag){
    			System.out.print("Login Success");
    			//service.getDataFromKf5Api(config,URL,"/network/rit/lab/zhanglab/kf5py-master/kf5Api.py");
    		}else{
    			System.out.print("Login failed");
    		}
            /// load database      
           // KF5Integration di = new KF5Integration(config.getOrg()+"_"+config.getDb(),pars[2].replace('.', '_'));
    		//di.Integration(pars[3].trim());
    		System.out.print("rd.Approval_Register");
    		
    		String query = "select * from itm.registration where db='"+pars[3]+"' and id='"+ pars[5]+"' and "
    				+ "password ='"+pars[8]+"' and URL='"+pars[2]+"' and org='"+pars[4]+"';";
    		 sqls sql = new sqls();
    	     Connection conn = sql.getConn("itm");
    	     Statement stmt = conn.createStatement();
			System.out.print(query);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				rd.Approval_Register(pars[0], pars[1], pars[5], pars[8], pars[6], pars[7], pars[2], pars[9], pars[3], pars[4], pars[10],
						rs.getString("school_name"),
						rs.getString("grade_level"),
						rs.getString("street"),
						rs.getString("city"),
						rs.getString("state"),
						rs.getString("country"),
						rs.getString("zipcode"),
						rs.getString("school_phone"));
			}
    		
            //
        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
