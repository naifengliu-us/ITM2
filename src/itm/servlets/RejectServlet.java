/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.servlets;

import code.MailUtilGmail;
import code.RegisterDatabase;
import java.io.IOException;
import java.io.PrintWriter;
import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stan
 */
public class RejectServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            String uid = request.getParameter("id");
            String dbUrl = request.getParameter("url");
            String fName = request.getParameter("Fname");
            String reason = request.getParameter("reason");
            RegisterDatabase rd = new RegisterDatabase();
            rd.Decline_Register(uid, dbUrl);
            //DB register approved, send email to notify user
            String from = "itm.test.send@gmail.com";
            HttpSession ses = request.getSession();
            String to = request.getParameter("Email");
            String subject = "";
            String body = "";
            String url = "";

            if((!(Boolean)ses.getAttribute("lan"))) {
                subject = "Your Database registration on ITM has been Declined. (Do not reply)";
                body = "Dear " + fName + ",<br>"
                       + "We are sorry to notify you, your application on registering database at ITM has been declined. <br>"
                       + "If you have any questions, please contact us by Email: <a href='mailto:itm.test.send@gmail.com'>itm.test.send@gmail.com</a><br>";

                if(!reason.isEmpty())
                    body += "The rejection reason is:<b>" + reason + "</b><br><br>" + "Idea Thread Mapper";
                else
                    body += "<br>" + "Idea Thread Mapper";
                url = "/en/Approval.jsp";
            } else {
                subject = "Your application for registering ITM server is not approved(Please do not reply)";
                body = "Respect" + fName + ",<br>"
                       + "Thank you for registering server on ITM, but we are sorry to notice you that your application is not approved. <br>   If you have any question, please contact us by email<a href='mailto:itm.test.send@gmail.com'>itm.test.send@gmail.com</a><br>"
                       + "This email is automatically generated, please do not reply.<br><br>";

                if(!reason.isEmpty())
                    body += "The refusing reason is:<b>" + reason + "</b><br><br>" + "Idea thread Mapper";
                else
                    body += "<br>" + "idea thread Mapper";
                url = "/Approval.jsp";
            }
            //send email
            try {
                MailUtilGmail.sendMail(to, from, subject, body, true);
            } catch(MessagingException e) {
                this.log(
                    "Unable to send email. \n" +
                    "You may need to configure your system as " +
                    "described in chapter 15. \n" +
                    "Here is the email you tried to send: \n" +
                    "=====================================\n" +
                    "TO: " + to + "\n" +
                    "FROM: " + from + "\n" +
                    "SUBJECT: " + subject + "\n" +
                    "\n" +
                    body + "\n\n");
            }
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

