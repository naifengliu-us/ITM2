/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.servlets;

import com.google.gson.Gson;
import database.PublishDB;
import itm.controllers.SavedPublishProject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Stan
 */
@WebServlet("/SavePublishServlet")
@MultipartConfig
public class SavePublishServlet extends HttpServlet {

    /**
     * Get filename
     */
    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            Gson gson = new Gson();

            HttpSession session = request.getSession();

            ///Get data from json
            String json = null;
            String db = null;

            if (request.getParameter("resetPid") != null) {
                session.setAttribute("pid", null);
                return;
            }

            if (request.getParameter("json") != null) {
                json = request.getParameter("json");
            }
            SavedPublishProject pp = new SavedPublishProject();
            int pid = 0;
            if (json == null) {
                db = IOUtils.toString(request.getPart("database").getInputStream(), "UTF-8");

                LinkedList<String> teacherReflection = new LinkedList<String>();
                teacherReflection.add(IOUtils.toString(request.getPart("BigIdeas").getInputStream(), "UTF-8"));
                teacherReflection.add(IOUtils.toString(request.getPart("Facilitated").getInputStream(), "UTF-8"));
                teacherReflection.add(IOUtils.toString(request.getPart("HelpfulActivities").getInputStream(), "UTF-8"));
                teacherReflection.add(IOUtils.toString(request.getPart("LessonsLearned").getInputStream(), "UTF-8"));

                String projectName = IOUtils.toString(request.getPart("projectname").getInputStream(), "UTF-8");
                String grade = session.getAttribute("grade").toString();
               // String threads_name = IOUtils.toString(request.getPart("threads_name").getInputStream(), "UTF-8");
                String showID = IOUtils.toString(request.getPart("showID").getInputStream(), "UTF-8");

                Part file = request.getPart("fileToUpload");
                String prefix = "";
                String suffix = "";
                String outfilePath = "";
                if (file != null) {
                    String filename = getFilename(file);

                    if (!filename.isEmpty()) {
                        InputStream filecontent = file.getInputStream();
                        prefix = filename;
                        if (filename.contains(".")) {
                            prefix = filename.substring(0, filename.lastIndexOf('.'));
                            suffix = filename.substring(filename.lastIndexOf('.'));
                        }

                        /// Write uploaded file.
                        
                       //File dirs=new File (File.separator + "tmp" );
                       // if ( ! (dirs.exists()) ) {
                        //    dirs.mkdirs();
                                            
                        File dir = new File (File.separator + "Stmp" + File.separator + projectName + System.currentTimeMillis());
                      
                        if (!dir.exists()) {
                          if (!dir.mkdirs()) {
                            System.out.println("Unable to create directory " + dir.getAbsolutePath());
                            return;
                          }
                        }
                        File outfile = new File(dir.getAbsolutePath(), filename);
                        outfilePath = outfile.getAbsolutePath();
                        file.write(outfilePath);

                        System.out.println("File is written here: " + outfilePath);
                    }
                }

                Boolean isShown = false;
                if(showID != null)
                    isShown = true;

                pp.setName(projectName);
                pp.setDB(db);
                pp.setGrade(grade);
                pp.setTeacherReflection(teacherReflection);
                pp.setShowID(isShown);
                pp.setUserID(session.getAttribute("username").toString());
                pp.setAttachmentTitle(prefix);
                pp.setAttachmentPath(outfilePath);

                pid = PublishDB.InsertSavePublish(pp);
                PublishDB.InsertTeacherReflection(pid, pp);
                session.setAttribute("pid", pid);
                String url = "";
                url = "/en/TeacherReflection.jsp?saved=true";
//           if((!(Boolean)session.getAttribute("isCN"))){
//               url="/en/TeacherReflection.jsp?saved=true";
//           }else{
//               url="/cn/TeacherReflection.jsp?saved=true";
//           }
                RequestDispatcher requestDispatcher ;
                requestDispatcher = request.getRequestDispatcher(url) ;
                requestDispatcher.forward( request, response ) ;
            } else {
                db = request.getParameter("database");
                //publish project page save function
                pp = gson.fromJson(json, SavedPublishProject.class);
                //jdbc
                pid = PublishDB.InsertSavePublish(pp);
                session.setAttribute("pid", pid);
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}