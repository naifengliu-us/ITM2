package itm.servlets;

import database.ParameterObj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/ldaTest")
public class LDAIntegrationServlet extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	StringBuffer cmdout = new StringBuffer();
		long startTime = System.currentTimeMillis();
		
		ArrayList<String> result = new ArrayList<String>();//get the result
		boolean IsLdaSuccess=false;
		String user_name="root";
		String password="051633b$";
		String sql_query="select nt.noteid,notetitle,notecontent from builder_ikit_org_GES_2014_2015.note_table as nt,builder_ikit_org_GES_2014_2015.view_note as vn where viewid =\'106\' and nt.noteid=vn.noteid";
		String topic_area="21";
		String topic_num="5";
		String[] cmd2={"/network/rit/lab/zhanglab/custom-python/bin/python","/network/rit/lab/zhanglab/lda_temp/lda_process.py",user_name,password,sql_query,topic_area,topic_num};
		//String[] cmd2 = {"/network/rit/lab/zhanglab/custom-python/bin/python", pythonFileLocation
		//		, URL,  config.getUserName(), config.getPassword(),String.valueOf(startTime)};
		System.out.println("cmd2 :" + cmd2[0]+" "+cmd2[1]+" "+cmd2[2]+" "+cmd2[3]+"\n"+cmd2[4]);
		// Run python api to retrieve data from kf5
		try {
			ProcessBuilder pb = new ProcessBuilder(cmd2);
			pb.redirectErrorStream(true); // equivalent of 2>&1
			Process proc = pb.start();
			proc.waitFor();
			
			InputStream fis = proc.getInputStream();
			InputStream is2 = proc.getErrorStream();  
			   InputStreamReader isr = new  InputStreamReader(is2,  "GBK" );  
			    BufferedReader br2 = new  BufferedReader(isr);  
			    String line2;  
			    while  ((line2 = br2.readLine()) !=  null ) {			    	  
				      //System.out.println("..."+line2);  
			    	}
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line = null;
			while ((line = br.readLine()) != null) {
				cmdout.append(line).append(System.getProperty("line.separator"));
				if(line.startsWith(">>>lda")){
					IsLdaSuccess=true;
				}else if(line.startsWith(">>> [")){
					int start=line.indexOf("[");
					int end=line.indexOf("]");
					String temp=line.substring(start+1, end);					
					for(String str:temp.split(",")){
						result.add(str.trim());
					}
				
				System.out.println("$$$ Lda Success: "+IsLdaSuccess);
				System.out.println("$$$"+result);
			}else{
				//System.out.println("|||"+line);
				}
			}
			if (cmdout.toString().isEmpty() || cmdout.toString().trim().equals("Error")) {
				System.out.println( "Error");
			} else {
				//updateUserPermissionTypeInITM(cmdout.toString());
				//System.out.println( cmdout.toString());
			}
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long endTime = System.currentTimeMillis();
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
