package itm.servlets;

import itm.controllers.SavedPublishProject;
import itm.controllers.NoteController;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import code.sqls;
import database.DBUtil;
import database.ProjectObj;
import database.PublishDB;
import database.Operatedb;
import database.SimpleCrypto;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Servlet implementation class PublishedThread
 */
@WebServlet("/PublishedThread")
public class PublishedThread extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PublishedThread() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    

	
    public static JSONObject prepareData(String data, boolean encrypt) {
    	  String[] rows = data.split("\\$");
        JSONObject obj = new JSONObject();
        JSONArray dataArray = new JSONArray();
        System.out.println(data);
//    obj.put("project_id", projectId);
//    obj.put("thread_name", threadName);
        obj.put("data_array", dataArray);
//    obj.put("modified_by",modifiedBy);
//    obj.put("authorcount",authorcount);
//    obj.put("notecount", rows.length);
        
        String peopleName ="";
        SimpleCrypto sc = new SimpleCrypto();
        for (int i = 0; i < rows.length; ++i) {
            System.out.println("printing rows");
            String[] cols = rows[i].split(";");
            JSONArray dataRow = new JSONArray();
            for (int j = 0; j < cols.length; ++j) {
                // Last two cols should be first name and last name
                if (j >= 5) {
                    try {
                        if (!encrypt) {
                        	peopleName=sc.nameFormate(sc.encrypt("akey", cols[j]));
                        	dataRow.add(peopleName.substring(0, 5));
                        	//dataRow.add(randName(cols[j]));
                        	
                        } else {
                            dataRow.add(cols[j]);
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    dataRow.add(cols[j]);
                }
            }
            dataArray.add(dataRow);
        }
        System.out.println(obj);
        return obj;
    }

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String project_id = request.getParameter("project_id").toString();
        String thread_id = request.getParameter("thread_id").toString();
        String timestamp = request.getParameter("timestamp").toString();

        String data = PublishDB.GetPublishedProjectThreadData(project_id, thread_id, timestamp);
        boolean encrypt = PublishDB.IsPublishedProjectEcrypted(project_id, thread_id, timestamp);
        out.write(PublishedThread.prepareData(data, encrypt).toJSONString());
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        this.processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        this.processRequest(request, response);
    }
}
