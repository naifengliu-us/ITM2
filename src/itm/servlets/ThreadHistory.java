package itm.servlets;

import itm.controllers.SavedPublishProject;
import itm.controllers.NoteController;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import code.sqls;
import database.DBUtil;
import database.ProjectObj;
import database.PublishDB;
import database.Operatedb;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Servlet implementation class ThreadHistory
 */
@WebServlet("/ThreadHistory")
public class ThreadHistory extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreadHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

    public static String prepareData(NoteController nc, Connection conn, String db, Operatedb opdb_author, String id,
                                     String projectId, String threadName, String modifiedBy, String authorcount,
                                     String data) {
        String[] rows = data.split("\\\\");
        JSONObject obj = new JSONObject();
        JSONArray dataArray = new JSONArray();

        obj.put("project_id", projectId);
        obj.put("thread_name", threadName);
        obj.put("data_array", dataArray);
        obj.put("modified_by", modifiedBy);
        obj.put("authorcount", authorcount);
        obj.put("notecount", rows.length);

        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;


        for (int i = 0; i < rows.length; ++i) {
            //System.out.println("printing rows");
            String[] cols = rows[i].split(";");
            JSONArray dataRow = new JSONArray();
            for (int j = 0; j < cols.length; ++j) {
                dataRow.add(cols[j]);
            }
            try {
                System.out.println("note content ......" + nc.getContent(cols[0]));
                dataRow.add(nc.getContent(cols[0]));
                String query = "select notetitle from " + db + ".note_table where noteid=" + cols[0];
                ps = conn.prepareStatement(query);
                rs = ps.executeQuery();
                if(rs.next()) {
                    //get notetitle for individual noteid
                    String notetitle = rs.getString("notetitle");
                    dataRow.add(notetitle);
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            //Get view for the noteid
            try {
                String query = "select title from view_table, view_note where view_table.idview = view_note.viewid and noteid =" + cols[0];
                ps1 = conn.prepareStatement(query);
                rs1 = ps1.executeQuery();
                if(rs1.next()) {
                    //Get view for the noteid
                    String view = rs1.getString("title");
                    dataRow.add(view);
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            //Get Authors for particular noteid
            List authorList = opdb_author.getMultipleAuthors(cols[0]);
            dataRow.add(authorList);

            dataArray.add(dataRow);
        }

        //System.out.println(obj.toJSONString());
        try {
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return obj.toJSONString();
    }

    /**
     *
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String db = session.getAttribute("database").toString();
        String threadhistoryId = request.getParameter("threadhistory_id").toString();
        sqls sql = new sqls();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            NoteController nc = new NoteController(db);
            Operatedb opdb_author = new Operatedb(new sqls(), db);
            Connection conn = sql.getConn(db);
            String query = "select * from " + db + ".thread_history where id=" + threadhistoryId;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next()) {
                String id = rs.getString("id");
                String projectId = rs.getString("projectid");
                String threadName = rs.getString("threadfocus");
                String modified_by = rs.getString("modified_by");
                String authorcount = rs.getString("authorcnt");
                Blob blob = rs.getBlob("thread_data");
                String responseData = prepareData(nc, conn, db, opdb_author, id, projectId, threadName, modified_by, authorcount,
                                                  new String(blob.getBytes(1, (int) blob.length())));
                out.write(responseData);
                out.close();
            }
            sql.Close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        this.processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        this.processRequest(request, response);
    }
}
