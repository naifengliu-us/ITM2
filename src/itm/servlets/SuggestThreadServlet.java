/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.servlets;

import com.google.gson.Gson;
import itm.controllers.NLP_Util;
import itm.controllers.ThreadName;
import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stan
 */
public class SuggestThreadServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     * Input: view name, db name,static file path,language boolean for now
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            String views = request.getParameter("view");
            String db = session.getAttribute("database").toString();
//           String query="";
//           if((views.indexOf("All Views"))==-1){
//               query=views;
//           }
            String path = "/ThreadName.txt";

            String absoluteDiskPath = getServletContext().getRealPath(path);
            InputStream input = new FileInputStream(absoluteDiskPath);

            List<ThreadName> threadNames = new ArrayList<ThreadName>();
            threadNames = NLP_Util.ProcessTXT(input, views, db, false);

            Gson gson = new Gson();
            String json = "";
//           for(ThreadName tn:threadNames){
//              json=gson.toJson(tn);
//              out.println(json);
//           }
            response.setContentType("application/json");
//           json="\"items\":"+gson.toJson(threadNames);
//           out.println(json);
            json = gson.toJson(threadNames);
            out.write(json);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
