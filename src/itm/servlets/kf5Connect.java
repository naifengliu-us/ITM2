package itm.servlets;

import code.Retrieve;
import code.StringUtils;
import code.sqls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.util.*;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;






import org.albany.edu.IntergrateFactory.FactoryConfiguration;
import org.albany.edu.IntergrateFactory.JsonFactory;
import org.albany.edu.IntergrateFactory.Interface.Imp.ReadDataByFileAddress;
import org.albany.edu.model.ModelBuilderByJson;
import org.albany.edu.webservice.ITMService;
import org.apache.log4j.Logger;
import org.json.simple.parser.JSONParser;

import database.*;

import java.sql.*;

import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Connect
 */

@WebServlet("/kf5connect")
public class kf5Connect extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
	final static Logger logger = Logger.getLogger(kf5Connect.class);
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("UTF-8");
		  
		String output = "";
		System.out.println("Inside Connect servlet");
		String strdb = null;
		ITMService service = new ITMService();
		// get the parameters
		String username = request.getParameter("user_name");
		String password = request.getParameter("password");
		String usertype = null;
		int port = 80;
		
//		output=GetDataBaseName(username,password);
//		if(output.equals("[]")){
//			output="";
//		}
		// check user
		// flag use as a flag that user is invalid or valid,-1 represent user is
		// invalid.

		int flag = -1;
		if (username != null) {
			// login in function
			// to do connect to kf 5
			FactoryConfiguration config = new FactoryConfiguration();
			config.setPassword(password);
			config.setUserName(username);
			String URL = "https://kf.utoronto.ca:443/kforum/";
			String filePath = "/network/rit/lab/zhanglab/kf5py-master/login.py";
			logger.info("This is info : " + filePath);
			
			boolean isAccess = false;
			try {
				isAccess = service.login(config, URL, filePath);
			} catch (InterruptedException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if (isAccess) {
				try {
					output+= service.login(URL, username, password, filePath);
					flag = 1;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
			// server login fails
				
				// to do 。。
			}

		}
		
	//	 output="[{\"guid\":\"2ef4aa10-7acc-4634-9c0b-ba6a4405a319\",\"authorInfo\":{\"guid\":\"75ecace9-1c22-46cd-8f69-75acc8bee31b\",\"userName\":\"Jianwei Zhang\",\"lastName\":\"Zhang\",\"firstName\":\"Jianwei\",\"email\":\"jzhang1@albany.edu\"},\"roleInfo\":{\"guid\":\"39746fda-0f7f-4050-a9b7-1778f5d54e5e\",\"name\":\"MANAGER\"},\"sectionId\":\"b47a8a6b-9298-4b1f-b520-65891e9dcdec\",\"sectionTitle\":\"GES 2015-2016\",\"dateCreated\":\"Aug 31, 2015 10:41:11 AM\",\"markedForDelete\":false},{\"guid\":\"0b572638-886f-435e-9972-0c6c89cc747a\",\"authorInfo\":{\"guid\":\"75ecace9-1c22-46cd-8f69-75acc8bee31b\",\"userName\":\"Jianwei Zhang\",\"lastName\":\"Zhang\",\"firstName\":\"Jianwei\",\"email\":\"jzhang1@albany.edu\"},\"roleInfo\":{\"guid\":\"8fe00415-d97a-4056-b4d1-cfd9f3d10d3d\",\"name\":\"WRITER\"},\"sectionId\":\"254ede30-2164-4d22-899e-8a500cfe3d82\",\"sectionTitle\":\"JICS 2015-2016\",\"dateCreated\":\"Aug 25, 2015 12:01:40 PM\",\"markedForDelete\":false},{\"guid\":\"b9e2f9e8-9916-4798-a231-6a35f98fe76e\",\"authorInfo\":{\"guid\":\"75ecace9-1c22-46cd-8f69-75acc8bee31b\",\"userName\":\"Jianwei Zhang\",\"lastName\":\"Zhang\",\"firstName\":\"Jianwei\",\"email\":\"jzhang1@albany.edu\"},\"roleInfo\":{\"guid\":\"8fe00415-d97a-4056-b4d1-cfd9f3d10d3d\",\"name\":\"WRITER\"},\"sectionId\":\"b1f6fed2-a64e-4bd2-ab8b-393fb2ed1f06\",\"sectionTitle\":\"Knowledge Society Network\",\"dateCreated\":\"Aug 25, 2014 1:29:27 PM\",\"markedForDelete\":false},{\"guid\":\"8b67ec9e-795d-46b3-b44e-8c06bb7f1085\",\"authorInfo\":{\"guid\":\"75ecace9-1c22-46cd-8f69-75acc8bee31b\",\"userName\":\"Jianwei Zhang\",\"lastName\":\"Zhang\",\"firstName\":\"Jianwei\",\"email\":\"jzhang1@albany.edu\"},\"roleInfo\":{\"guid\":\"39746fda-0f7f-4050-a9b7-1778f5d54e5e\",\"name\":\"MANAGER\"},\"sectionId\":\"5b5d1d37-3294-40a4-b8c0-43cefcc94ab1\",\"sectionTitle\":\"LA-WEEK\",\"dateCreated\":\"Dec 15, 2014 9:00:44 AM\",\"markedForDelete\":false},{\"guid\":\"722c3dcc-b8b3-42f6-a327-65dab5ee4da5\",\"authorInfo\":{\"guid\":\"75ecace9-1c22-46cd-8f69-75acc8bee31b\",\"userName\":\"Jianwei Zhang\",\"lastName\":\"Zhang\",\"firstName\":\"Jianwei\",\"email\":\"jzhang1@albany.edu\"},\"roleInfo\":{\"guid\":\"39746fda-0f7f-4050-a9b7-1778f5d54e5e\",\"name\":\"MANAGER\"},\"sectionId\":\"bb470d30-977f-43be-b94e-d6c2d7dc19d8\",\"sectionTitle\":\"TaCCL-UAlbany\",\"dateCreated\":\"Aug 20, 2014 10:10:07 AM\",\"markedForDelete\":false},{\"guid\":\"f6293e92-3ffc-4b16-86a5-449631ad6265\",\"authorInfo\":{\"guid\":\"75ecace9-1c22-46cd-8f69-75acc8bee31b\",\"userName\":\"Jianwei Zhang\",\"lastName\":\"Zhang\",\"firstName\":\"Jianwei\",\"email\":\"jzhang1@albany.edu\"},\"roleInfo\":{\"guid\":\"39746fda-0f7f-4050-a9b7-1778f5d54e5e\",\"name\":\"MANAGER\"},\"sectionId\":\"cd3ad110-fa61-4969-8e80-cbc8aef9cd16\",\"sectionTitle\":\"TeckWhye_Sec2\",\"dateCreated\":\"Nov 29, 2015 9:45:30 PM\",\"markedForDelete\":false}]";

		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		response.getWriter().write(output.trim());

	}

	
    
    public String GetDataBaseName(String userName,String pwd){
    	sqls sql = new sqls();
    	Statement st= sql.Connect("itm");
    	String output ="";
   	 String strsql = "select * from itm.section where user_name='"+userName+"' and password='"+pwd+"'";
   	  ResultSet rs = null;
   	 output="[";
   	  try {
			rs = st.executeQuery(strsql);
			  while(rs.next()) {
	              //  output= rs.getInt(1);
				  output += "{ \"roleInfo\":{\"name\":\""+rs.getString("type")+"\"},\"sectionTitle\":\""+rs.getString("section_name")+"\"},";
	            }
			  rs.close();
			  
   	  } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
   	  }
   	  if(output.length()>5){
   	  output=output.substring(0,output.length()-1)+"]";
   	  }else{
   		output="[]"; 
   	  }
   	 return output;
    }
    
    
    
	private UUID createSessionID() {
		return UUID.randomUUID();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		doGet(request, response);// TODO Auto-generated method stub
	}

}
