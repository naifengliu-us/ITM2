package itm.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import code.sqls;
import database.Operatedb;

/**
 * Servlet implementation class AuthorCount
 */
@WebServlet("/AuthorCount")
public class AuthorCount extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthorCount() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        String db = session.getAttribute("database").toString();
        String threadname = request.getParameter("threadname").toString();
        String pid = request.getParameter("project_id").toString();
        String count;
        ResultSet rs = null;
        sqls sql = new sqls();
        Operatedb opdb_ct = new Operatedb(sql, db);
        try {
            rs =  opdb_ct.getC().executeQuery("select count(distinct authorid) from  thread_note inner join  author_note on  thread_note.noteid =  author_note.noteid and  thread_note.threadfocus='" + threadname + "'and thread_note.projectid='" + pid + "'");
            if (rs.next()) {
                count = rs.getString(1);
                out.write(count);
                out.close();
                rs.close();
            }
            sql.Close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        this.processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        this.processRequest(request, response);
    }
}
