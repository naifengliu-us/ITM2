package itm.servlets;

import code.sqls;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;


import org.albany.edu.webservice.ITMService;

import database.*;

import java.sql.*;

import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Connect
 */

@WebServlet("/Connect")
public class Connect extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		
		ITMService service = new ITMService();
		String username = request.getParameter("username");
     	String password = request.getParameter("password");
		String db = request.getParameter("db");
		String usertype = request.getParameter("type");;

		sqls sobj = new sqls();
		Operatedb op = new Operatedb(sobj, "itm");
		String URL = "https://kf.utoronto.ca:443/kforum/";
		String filePath = "/network/rit/lab/zhanglab/kf5py-master/login.py";

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		request.getSession().setAttribute("username", username);
		if( usertype.toLowerCase().equals("writer")|| usertype.toLowerCase().equals("researcher")){
			request.getSession().setAttribute("usertype", "manager");
			request.getSession().setAttribute("usertype2","1");
		}else{
			request.getSession().setAttribute("usertype",usertype.toLowerCase());
			request.getSession().setAttribute("usertype2","2");
		}
		String databaseName =db;
		
//		request.getSession().setAttribute("host", "");
//		
//		String password = "jianweizh";
//		String username = "jianwei zhang";
//		request.getSession().setAttribute("usertype", "manager");
//    	request.getSession().setAttribute("usertype2","1");
//		
//		// database name
//		String databaseName ="GES 2015-2016";
//		String db ="builder_ikit_org_GES_2015_2016"; 
//		
		
		
		
		System.out.println(" connection to itm database ");
		CreateDatabase crtdb = new CreateDatabase(databaseName);
		System.out.println("Check login info ");
		databaseName=crtdb.constructTheDataBase(db,username,password);
		System.out.println("Check Info done and DB is"+databaseName);
		request.getSession().setAttribute("dbase", databaseName);
		if(databaseName.equals("")){
			String response2="Do not have database";
			response.getWriter().write(response2);
			
		}else{
			crtdb.setDbname(databaseName);
			if (!crtdb.CheckDB()) {
				String response2="Do not have database";
				response.getWriter().write(response2);
				
			}else{
				crtdb.CloseCon();
				String url = response.encodeURL("/ITM2/en/New_HomePage.jsp?database="
						+ databaseName);
				response.getWriter().write(url);
			}
		}
		
	}

	private UUID createSessionID() {
		return UUID.randomUUID();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		doGet(request, response);// TODO Auto-generated method stub
	}

}
