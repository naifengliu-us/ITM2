package itm.servlets;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.cmu.side.recipe.ITMPredictor;
//import org.albany.edu.pugin.LightSide;
import edu.cmu.side.recipe.*;

/**
 * Servlet implementation class LightSideServlet
 */
@WebServlet("/LightSideServlet")
public class LightSideServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LightSideServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String text = "";
		  File file = new File("/network/rit/lab/zhanglab/outputtest2.csv");
		  String[] array = null;
		    List<String> lines = Files.readAllLines(file.toPath(), 
		            StandardCharsets.UTF_8);
		    int i = 0;
			  String[][] newone=new String[lines.size()][];
		    for (String line : lines) {
		    	newone[i] = line.split(",");
		        i++;
		    }
		    
		    for(int a=0;(1+5*a)<newone.length-1;a++){
		    	for(int b=0;b<newone[1+5*a].length-1;b++){
		    		text=text+newone[(1+5*a)][b]+",!,";
		    	}
		    	text=text+newone[(1+5*a)][newone[(1+5*a)].length-1]+";!;";		    	
		    }

	    response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
	    response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
	    response.getWriter().write(text);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String finaltext = request.getParameter("finaltext");
		
//		try {
//			ITMPredictor.main(null);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		FileWriter writer = new FileWriter("/network/rit/lab/zhanglab/test.csv");
//		FileWriter writer = new FileWriter("test.csv");
		writer.append("set");
		writer.append(",");
		writer.append("title");
		writer.append(",");
		writer.append("text");
		writer.append("\n");
		writer.append(finaltext);		
	    writer.flush();
	    writer.close();
	    
	    
//		Runtime rt= Runtime.getRuntime();
//		try{
//			//rt.exec(new String[]{"/bin/bash","-c","cd /Users/rksmac/Desktop/lightside && java -cp /Users/rksmac/Desktop/lightside/bin:/Users/rksmac/Desktop/lightside/antlr-2.7.7.jar:/Users/rksmac/Desktop/lightside/bayesianLogisticRegression.jar:/Users/rksmac/Desktop/lightside/cglib-nodep-2.2.jar:/Users/rksmac/Desktop/lightside/chiSquaredAttributeEval.jar:/Users/rksmac/Desktop/lightside/com.sun.rowset.jar:/Users/rksmac/Desktop/lightside/commons-compress-1.10.jar:/Users/rksmac/Desktop/lightside/commons-dbcp-1.4.jar:/Users/rksmac/Desktop/lightside/commons-exec-1.3.jar:/Users/rksmac/Desktop/lightside/commons-io-2.4.jar:/Users/rksmac/Desktop/lightside/commons-logging-1.0.4.jar:/Users/rksmac/Desktop/lightside/commons-math3-3.1.jar:/Users/rksmac/Desktop/lightside/commons-pool-1.5.4.jar:/Users/rksmac/Desktop/lightside/ditchnet-tabs-taglib.jar:/Users/rksmac/Desktop/lightside/dom4j-1.6.1.jar:/Users/rksmac/Desktop/lightside/fastjson-1.1.41.jar:/Users/rksmac/Desktop/lightside/genesis.jar:/Users/rksmac/Desktop/lightside/gnujaxp-1.0.0.jar:/Users/rksmac/Desktop/lightside/gson-2.2.3.jar:/Users/rksmac/Desktop/lightside/gson-2.2.4.jar:/Users/rksmac/Desktop/lightside/hamcrest.jar:/Users/rksmac/Desktop/lightside/hibernate-commons-annotations-4.0.1.Final.jar:/Users/rksmac/Desktop/lightside/hibernate-core-4.1.4.Final.jar:/Users/rksmac/Desktop/lightside/hibernate-jpa-2.0-api-1.0.1.Final.jar:/Users/rksmac/Desktop/lightside/httpclient-4.3.4.jar:/Users/rksmac/Desktop/lightside/httpcore-4.3.2.jar:/Users/rksmac/Desktop/lightside/jackson-annotations-2.5.0.jar:/Users/rksmac/Desktop/lightside/jackson-core-2.5.4.jar:/Users/rksmac/Desktop/lightside/jackson-databind-2.5.4.jar:/Users/rksmac/Desktop/lightside/jackson-xml-databind-0.6.2.jar:/Users/rksmac/Desktop/lightside/java-cup.jar:/Users/rksmac/Desktop/lightside/javassist-3.15.0-GA.jar:/Users/rksmac/Desktop/lightside/jboss-logging-3.1.0.GA.jar:/Users/rksmac/Desktop/lightside/jboss-transaction-api_1.1_spec-1.0.0.Final.jar:/Users/rksmac/Desktop/lightside/jdom-1.1.jar:/Users/rksmac/Desktop/lightside/jettison-1.2.jar:/Users/rksmac/Desktop/lightside/joda-time-1.6.jar:/Users/rksmac/Desktop/lightside/json-20080701.jar:/Users/rksmac/Desktop/lightside/json-20140107-sources.jar:/Users/rksmac/Desktop/lightside/json-20140107.jar:/Users/rksmac/Desktop/lightside/json-simple-1.1.1.jar:/Users/rksmac/Desktop/lightside/jstl-1.2.jar:/Users/rksmac/Desktop/lightside/junit.jar:/Users/rksmac/Desktop/lightside/jython-2.7-b4.jar:/Users/rksmac/Desktop/lightside/jzlib.jar:/Users/rksmac/Desktop/lightside/k5commonnew.jar:/Users/rksmac/Desktop/lightside/kf5Api.jar:/Users/rksmac/Desktop/lightside/kxml2-2.3.0.jar:/Users/rksmac/Desktop/lightside/kxml2-min-2.3.0.jar:/Users/rksmac/Desktop/lightside/liblinear-1.92.jar:/Users/rksmac/Desktop/lightside/LibSVM.jar:/Users/rksmac/Desktop/lightside/lightside.jar:/Users/rksmac/Desktop/lightside/log4j-1.2.12.jar:/Users/rksmac/Desktop/lightside/lucene-core-3.3.0.jar:/Users/rksmac/Desktop/lightside/mail.jar:/Users/rksmac/Desktop/lightside/main.jar:/Users/rksmac/Desktop/lightside/mysql-connector-java-5.0.8.jar:/Users/rksmac/Desktop/lightside/packageManager.jar:/Users/rksmac/Desktop/lightside/py4j0.8.jar:/Users/rksmac/Desktop/lightside/quiet-weka.jar:/Users/rksmac/Desktop/lightside/riverlayout.jar:/Users/rksmac/Desktop/lightside/xml-apis.jar:/Users/rksmac/Desktop/lightside/serializer.jar:/Users/rksmac/Desktop/lightside/simple-5.0.4.jar:/Users/rksmac/Desktop/lightside/slf4j-api-1.6.1.jar:/Users/rksmac/Desktop/lightside/spring-asm-3.0.7.RELEASE.jar:/Users/rksmac/Desktop/lightside/spring-beans-3.0.7.RELEASE.jar:/Users/rksmac/Desktop/lightside/spring-context-3.0.7.RELEASE.jar:/Users/rksmac/Desktop/lightside/spring-core-3.0.7.RELEASE.jar:/Users/rksmac/Desktop/lightside/spring-expression-3.0.7.RELEASE.jar:/Users/rksmac/Desktop/lightside/spring-jdbc-3.0.7.RELEASE.jar:/Users/rksmac/Desktop/lightside/spring-tx-3.0.7.RELEASE.jar:/Users/rksmac/Desktop/lightside/spring-web-3.1.1.RELEASE.jar:/Users/rksmac/Desktop/lightside/spring-webmvc-3.1.1.RELEASE.jar:/Users/rksmac/Desktop/lightside/stanford-parser.jar:/Users/rksmac/Desktop/lightside/stanford-postagger.jar:/Users/rksmac/Desktop/lightside/stax-1.2.0.jar:/Users/rksmac/Desktop/lightside/stax-api-1.0.1.jar:/Users/rksmac/Desktop/lightside/wstx-asl-3.2.7.jar:/Users/rksmac/Desktop/lightside/xerces-2.4.0.jar:/Users/rksmac/Desktop/lightside/xercesImpl-2.11.0.jar:/Users/rksmac/Desktop/lightside/xmlParserAPIs-2.6.2.jar:/Users/rksmac/Desktop/lightside/xmlparserv2.jar:/Users/rksmac/Desktop/lightside/xmlpull-1.1.3.1.jar:/Users/rksmac/Desktop/lightside/xom-1.1.jar:/Users/rksmac/Desktop/lightside/xpp3_min-1.1.4c.jar:/Users/rksmac/Desktop/lightside/xstream-1.4.4.jar:/Users/rksmac/Desktop/lightside/xstream-benchmark-1.4.4.jar:/Users/rksmac/Desktop/lightside/xstream-hibernate-1.4.4.jar:/Users/rksmac/Desktop/lightside/yeritools-min.jar:/Users/rksmac/Desktop/lightside/zoolib.jar test"});
//			rt.exec(new String[]{"/bin/bash","-c","cd /network/rit/lab/zhanglab/lightside && java -cp /network/rit/lab/zhanglab/lightside/bin:/network/rit/lab/zhanglab/lightside/antlr-2.7.7.jar:/network/rit/lab/zhanglab/lightside/bayesianLogisticRegression.jar:/network/rit/lab/zhanglab/lightside/cglib-nodep-2.2.jar:/network/rit/lab/zhanglab/lightside/chiSquaredAttributeEval.jar:/network/rit/lab/zhanglab/lightside/com.sun.rowset.jar:/network/rit/lab/zhanglab/lightside/commons-compress-1.10.jar:/network/rit/lab/zhanglab/lightside/commons-dbcp-1.4.jar:/network/rit/lab/zhanglab/lightside/commons-exec-1.3.jar:/network/rit/lab/zhanglab/lightside/commons-io-2.4.jar:/network/rit/lab/zhanglab/lightside/commons-logging-1.0.4.jar:/network/rit/lab/zhanglab/lightside/commons-math3-3.1.jar:/network/rit/lab/zhanglab/lightside/commons-pool-1.5.4.jar:/network/rit/lab/zhanglab/lightside/ditchnet-tabs-taglib.jar:/network/rit/lab/zhanglab/lightside/dom4j-1.6.1.jar:/network/rit/lab/zhanglab/lightside/fastjson-1.1.41.jar:/network/rit/lab/zhanglab/lightside/genesis.jar:/network/rit/lab/zhanglab/lightside/gnujaxp-1.0.0.jar:/network/rit/lab/zhanglab/lightside/gson-2.2.3.jar:/network/rit/lab/zhanglab/lightside/gson-2.2.4.jar:/network/rit/lab/zhanglab/lightside/hamcrest.jar:/network/rit/lab/zhanglab/lightside/hibernate-commons-annotations-4.0.1.Final.jar:/network/rit/lab/zhanglab/lightside/hibernate-core-4.1.4.Final.jar:/network/rit/lab/zhanglab/lightside/hibernate-jpa-2.0-api-1.0.1.Final.jar:/network/rit/lab/zhanglab/lightside/httpclient-4.3.4.jar:/network/rit/lab/zhanglab/lightside/httpcore-4.3.2.jar:/network/rit/lab/zhanglab/lightside/jackson-annotations-2.5.0.jar:/network/rit/lab/zhanglab/lightside/jackson-core-2.5.4.jar:/network/rit/lab/zhanglab/lightside/jackson-databind-2.5.4.jar:/network/rit/lab/zhanglab/lightside/jackson-xml-databind-0.6.2.jar:/network/rit/lab/zhanglab/lightside/java-cup.jar:/network/rit/lab/zhanglab/lightside/javassist-3.15.0-GA.jar:/network/rit/lab/zhanglab/lightside/jboss-logging-3.1.0.GA.jar:/network/rit/lab/zhanglab/lightside/jboss-transaction-api_1.1_spec-1.0.0.Final.jar:/network/rit/lab/zhanglab/lightside/jdom-1.1.jar:/network/rit/lab/zhanglab/lightside/jettison-1.2.jar:/network/rit/lab/zhanglab/lightside/joda-time-1.6.jar:/network/rit/lab/zhanglab/lightside/json-20080701.jar:/network/rit/lab/zhanglab/lightside/json-20140107-sources.jar:/network/rit/lab/zhanglab/lightside/json-20140107.jar:/network/rit/lab/zhanglab/lightside/json-simple-1.1.1.jar:/network/rit/lab/zhanglab/lightside/jstl-1.2.jar:/network/rit/lab/zhanglab/lightside/junit.jar:/network/rit/lab/zhanglab/lightside/jython-2.7-b4.jar:/network/rit/lab/zhanglab/lightside/jzlib.jar:/network/rit/lab/zhanglab/lightside/k5commonnew.jar:/network/rit/lab/zhanglab/lightside/kf5Api.jar:/network/rit/lab/zhanglab/lightside/kxml2-2.3.0.jar:/network/rit/lab/zhanglab/lightside/kxml2-min-2.3.0.jar:/network/rit/lab/zhanglab/lightside/liblinear-1.92.jar:/network/rit/lab/zhanglab/lightside/LibSVM.jar:/network/rit/lab/zhanglab/lightside/lightside.jar:/network/rit/lab/zhanglab/lightside/log4j-1.2.12.jar:/network/rit/lab/zhanglab/lightside/lucene-core-3.3.0.jar:/network/rit/lab/zhanglab/lightside/mail.jar:/network/rit/lab/zhanglab/lightside/main.jar:/network/rit/lab/zhanglab/lightside/mysql-connector-java-5.0.8.jar:/network/rit/lab/zhanglab/lightside/packageManager.jar:/network/rit/lab/zhanglab/lightside/py4j0.8.jar:/network/rit/lab/zhanglab/lightside/quiet-weka.jar:/network/rit/lab/zhanglab/lightside/riverlayout.jar:/network/rit/lab/zhanglab/lightside/xml-apis.jar:/network/rit/lab/zhanglab/lightside/serializer.jar:/network/rit/lab/zhanglab/lightside/simple-5.0.4.jar:/network/rit/lab/zhanglab/lightside/slf4j-api-1.6.1.jar:/network/rit/lab/zhanglab/lightside/spring-asm-3.0.7.RELEASE.jar:/network/rit/lab/zhanglab/lightside/spring-beans-3.0.7.RELEASE.jar:/network/rit/lab/zhanglab/lightside/spring-context-3.0.7.RELEASE.jar:/network/rit/lab/zhanglab/lightside/spring-core-3.0.7.RELEASE.jar:/network/rit/lab/zhanglab/lightside/spring-expression-3.0.7.RELEASE.jar:/network/rit/lab/zhanglab/lightside/spring-jdbc-3.0.7.RELEASE.jar:/network/rit/lab/zhanglab/lightside/spring-tx-3.0.7.RELEASE.jar:/network/rit/lab/zhanglab/lightside/spring-web-3.1.1.RELEASE.jar:/network/rit/lab/zhanglab/lightside/spring-webmvc-3.1.1.RELEASE.jar:/network/rit/lab/zhanglab/lightside/stanford-parser.jar:/network/rit/lab/zhanglab/lightside/stanford-postagger.jar:/network/rit/lab/zhanglab/lightside/stax-1.2.0.jar:/network/rit/lab/zhanglab/lightside/stax-api-1.0.1.jar:/network/rit/lab/zhanglab/lightside/wstx-asl-3.2.7.jar:/network/rit/lab/zhanglab/lightside/xerces-2.4.0.jar:/network/rit/lab/zhanglab/lightside/xercesImpl-2.11.0.jar:/network/rit/lab/zhanglab/lightside/xmlParserAPIs-2.6.2.jar:/network/rit/lab/zhanglab/lightside/xmlparserv2.jar:/network/rit/lab/zhanglab/lightside/xmlpull-1.1.3.1.jar:/network/rit/lab/zhanglab/lightside/xom-1.1.jar:/network/rit/lab/zhanglab/lightside/xpp3_min-1.1.4c.jar:/network/rit/lab/zhanglab/lightside/xstream-1.4.4.jar:/network/rit/lab/zhanglab/lightside/xstream-benchmark-1.4.4.jar:/network/rit/lab/zhanglab/lightside/xstream-hibernate-1.4.4.jar:/network/rit/lab/zhanglab/lightside/yeritools-min.jar:/network/rit/lab/zhanglab/lightside/zoolib.jar test && mkdir -p /network/rit/lab/zhanglab/testifsuccess"});			
//		}catch(IOException e){
//			
//		}
////		
//		Process p = Runtime.getRuntime().exec((new String[]{"/bin/bash","-c","cd /network/rit/lab/zhanglab/lightside && "
//				+ "java -cp /network/rit/lab/zhanglab/lightside/bin:"
//				+ "/network/rit/lab/zhanglab/lightside/antlr-2.7.7.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/bayesianLogisticRegression.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/cglib-nodep-2.2.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/chiSquaredAttributeEval.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/com.sun.rowset.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/commons-compress-1.10.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/commons-dbcp-1.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/commons-exec-1.3.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/commons-io-2.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/commons-logging-1.0.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/commons-math3-3.1.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/commons-pool-1.5.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/ditchnet-tabs-taglib.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/dom4j-1.6.1.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/fastjson-1.1.41.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/genesis.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/gnujaxp-1.0.0.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/gson-2.2.3.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/gson-2.2.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/hamcrest.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/hibernate-commons-annotations-4.0.1.Final.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/hibernate-core-4.1.4.Final.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/hibernate-jpa-2.0-api-1.0.1.Final.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/httpclient-4.3.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/httpcore-4.3.2.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jackson-annotations-2.5.0.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jackson-core-2.5.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jackson-databind-2.5.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jackson-xml-databind-0.6.2.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/java-cup.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/javassist-3.15.0-GA.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jboss-logging-3.1.0.GA.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jboss-transaction-api_1.1_spec-1.0.0.Final.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jdom-1.1.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jettison-1.2.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/joda-time-1.6.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/json-20080701.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/json-20140107-sources.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/json-20140107.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/json-simple-1.1.1.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jstl-1.2.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/junit.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jython-2.7-b4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/jzlib.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/k5commonnew.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/kf5Api.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/kxml2-2.3.0.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/kxml2-min-2.3.0.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/liblinear-1.92.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/LibSVM.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/lightside.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/log4j-1.2.12.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/lucene-core-3.3.0.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/mail.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/main.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/mysql-connector-java-5.0.8.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/packageManager.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/py4j0.8.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/quiet-weka.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/riverlayout.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xml-apis.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/serializer.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/simple-5.0.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/slf4j-api-1.6.1.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-asm-3.0.7.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-beans-3.0.7.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-context-3.0.7.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-core-3.0.7.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-expression-3.0.7.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-jdbc-3.0.7.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-tx-3.0.7.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-web-3.1.1.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/spring-webmvc-3.1.1.RELEASE.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/stanford-parser.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/stanford-postagger.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/stax-1.2.0.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/stax-api-1.0.1.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/wstx-asl-3.2.7.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xerces-2.4.0.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xercesImpl-2.11.0.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xmlParserAPIs-2.6.2.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xmlparserv2.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xmlpull-1.1.3.1.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xom-1.1.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xpp3_min-1.1.4c.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xstream-1.4.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xstream-benchmark-1.4.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/xstream-hibernate-1.4.4.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/yeritools-min.jar:"
//				+ "/network/rit/lab/zhanglab/lightside/zoolib.jar test && mkdir -p /network/rit/lab/zhanglab/testifsuccess"}));
//		

	    
		doGet(request, response);
	}

	
}



