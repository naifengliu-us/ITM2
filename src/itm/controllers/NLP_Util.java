/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.controllers;

import code.StringUtils;
import code.sqls;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * Read .txt to process suggest thread data
 * For now we only have one data about 'battery', so the arraylist only contain one obj
 * Future we would have multiple threads data, you need to modify the parsing logic to init ThreadName obj
 * Use a loop to push the objs to the ArrayList.
 * @author Stan
 */
public class NLP_Util {
    public static ArrayList<ThreadName> ProcessTXT(InputStream is, String query, String db, boolean isCN) {
        ArrayList<ThreadName> tnList = new ArrayList<ThreadName>();
        try {
            Scanner scan = new Scanner(is);
            scan.useDelimiter("\\Z");
            String content = scan.next();
            String[] bulks = content.split("###");
            String targetLine = bulks[2].trim().split("\n")[1];
            String[] noteIDs = targetLine.split(",");
            Statement st = null;
            sqls sql = new sqls();
            st = sql.Connect(db);
            ResultSet rs = null;
            String statement = "";
            LinkedList<Integer> targetNoteIDs = new LinkedList();
            //set search location by view name
            if(query.indexOf("All Views") == -1) {

                if(query.contains("@ ")) {
                    //mutiple views
                    String[] views = query.split("@ ");
                    for(String v : views) {
                        statement = "SELECT noteid FROM view_note as vn,view_table as vt "
                                    + "where vn.viewid=vt.idview and title='" + v + "' and (";
                        for(String n : noteIDs) {
                            statement += "noteid=" + n + " OR ";
                        }
                        statement = statement.substring(0, statement.length() - 3);
                        statement += ");";
                        rs = st.executeQuery(statement);
                        while(rs.next()) {
                            targetNoteIDs.add(rs.getInt(1));
                        }
                    }
                    rs.close();
                } else {
//                    single view
                    targetNoteIDs = new LinkedList();
                    statement = "SELECT noteid FROM view_note as vn,view_table as vt "
                                + "where vn.viewid=vt.idview and title='" + query + "' and (";
                    for(String n : noteIDs) {
                        statement += "noteid=" + n + " OR ";
                    }
                    statement = statement.substring(0, statement.length() - 3);
                    statement += ");";
                    rs = st.executeQuery(statement);
                    while(rs.next()) {
                        targetNoteIDs.add(rs.getInt(1));
                    }
                }
                if(!targetNoteIDs.isEmpty()) {
                    //Modify the codes below if we are pasing different document or multiple threads data
                    for(String s : bulks) {
                        //Thread Names
                        if(s.contains("Thread Names:")) {
                            String[] threadNames = s.split("\n");
                            int size = threadNames.length;
                            for(int i = 1; i < size; i++) {
                                ThreadName tn = new ThreadName();
                                tn.setThreadName(threadNames[i]);
                                tnList.add(tn);
                            }
                        }
//                    Keywords cluster
                        if(s.contains("Keywords for topic: ")) {
                            String[] keywords = s.trim().split("\n");
                            int size = keywords.length;
                            for(int i = 1; i < size; i++) {
                                tnList.get(i - 1).setKeyWord(keywords[i]);
                            }
                        }
                        //jdbc to get title, author and content
                        int tnIndex = 0;
                        for(int noteID : targetNoteIDs) {
                            statement = "SELECT nt.notetitle,firstname,lastname,SUBSTRING_INDEX(nt.notecontent, ' ', 20) "
                                        + "from note_table as nt inner join (author_note as an cross join author_table as at) ON (nt.noteid=an.noteid and an.authorid=at.authorid)  "
                                        + "where nt.noteid=" + noteID + ";";
                            rs = st.executeQuery(statement);
                            if(rs.next()) {
                                tnList.get(tnIndex).setTitle(rs.getString(1));
                                //Chinese name or English name?
                                String fullName = rs.getString(2) + " " + rs.getString(3);
                                if(isCN)
                                    fullName = StringUtils.AdjustChineseName(fullName);
                                tnList.get(tnIndex).setAuthor(fullName);
                                tnList.get(tnIndex).setContent(rs.getString(4));
                            }
                            tnIndex++;
                        }
                    }
                }

            } else {
                //All views
                for(String s : bulks) {
                    //Thread Names
                    if(s.contains("Thread Names:")) {
                        String[] threadNames = s.split("\n");
                        int size = threadNames.length;
                        for(int i = 1; i < size; i++) {
                            ThreadName tn = new ThreadName();
                            tn.setThreadName(threadNames[i]);
                            tnList.add(tn);
                        }
                    }
                    if(s.contains("Keywords for topic: ")) {
                        String[] keywords = s.trim().split("\n");
                        int size = keywords.length;
                        for(int i = 1; i < size; i++) {
                            tnList.get(i - 1).setKeyWord(keywords[i]);
                        }
                    }
                    if(s.contains("Notes for topic: ")) {
                        //jdbc to get title, author and content

                        int tnIndex = 0;
                        for(String noteID : noteIDs) {
                            statement = "SELECT nt.notetitle,firstname,lastname,SUBSTRING_INDEX(nt.notecontent, ' ', 20) "
                                        + "from note_table as nt inner join (author_note as an cross join author_table as at) ON (nt.noteid=an.noteid and an.authorid=at.authorid)  "
                                        + "where nt.noteid=" + noteID + ";";
                            rs = st.executeQuery(statement);
                            if(rs.next()) {
                                tnList.get(tnIndex).setTitle(rs.getString(1));
                                //Chinese name or English name?
                                String fullName = rs.getString(2) + " " + rs.getString(3);
                                if(isCN)
                                    fullName = StringUtils.AdjustChineseName(fullName);
                                tnList.get(tnIndex).setAuthor(fullName);
                                tnList.get(tnIndex).setContent(rs.getString(4));
                            }
                            tnIndex++;
                        }
                    }
                }
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
        return tnList;
    }
}
