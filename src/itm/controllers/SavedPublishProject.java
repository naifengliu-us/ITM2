/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package itm.controllers;

import itm.models.Project;
import itm.models.PublishProject;
import java.sql.Date;
import java.util.LinkedList;

import code.Publish;

public  class SavedPublishProject extends Publish {
    private int pid;
    //private String[] threadsName;
    private LinkedList<String> teacherReflection;
    private Date date;
    private String threads;
    private String attachmentTitle;
    private String attachmentPath;

    public void SavedPublishProject() {
    }

    public void setPID(int id) {
        this.pid = id;
    }
    public int getPid() {
        return pid;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public Date getDate() {
        return date;
    }

    public void setTeacherReflection(LinkedList<String> tr) {
        this.teacherReflection = tr;
    }
    public LinkedList<String> getTeacherReflection() {
        return teacherReflection;
    }

    @Override
    public String getThreads() {
        return threads;
    }
    @Override
    public void setThreads(String tNames) {
        if (tNames.contains(".and^")) {
            String[] threads = tNames.split(".and^");
            for(String s : threads) {
                this.threads += s + ", ";
            }
            this.threads.subSequence(0, this.threads.length() - 2);
        } else {
            this.threads = threads;
        }
    }

    public String getAttachmentTitle() {
        return this.attachmentTitle;
    }
    public void setAttachmentTitle(String title) {
        this.attachmentTitle = title;
    }

    public String getAttachmentPath() {
        return this.attachmentPath;
    }
    public void setAttachmentPath(String path) {
        this.attachmentPath = path;
    }
}
