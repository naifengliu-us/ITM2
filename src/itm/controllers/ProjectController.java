package itm.controllers;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

import itm.models.*;


public class ProjectController extends BaseController {
    private ProjectModel p;
    public ProjectController(String dbname)
    throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        super();
        p = new ProjectModel(dbname);
    }
    public void updateproject(HttpServletRequest req) throws SQLException {
        try {
            p.updateproject(req);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void deleteproject(HttpServletRequest req) throws SQLException {
        p.DeleteProj(req);
    }

    public void recoverproject(HttpServletRequest req) throws SQLException {
        p.RecoverProj(req);
    }

}
