package itm.controllers;

import java.util.List;


public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        String content = "My two sons and  new line now";

        System.out.println(content);

        System.out.println("1 " + System.getProperty("line.separator") + " 2");

        String str = "This is a\ntest, this\n\nis a test\n";

        for (int pos = str.indexOf("\n"); pos != -1; pos = str.indexOf("\n", pos + 1)) {
            System.out.println("\\n at " + pos);
        }

        str = str.replaceAll("\n", "</p><p>");
        System.out.println(str);
    }

}
