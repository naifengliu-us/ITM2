package LoginModule;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@Path("/UserService")
public class UserService {
	
	public static void main(String arg[]){
		UserService service = new UserService();
		System.out.println(service.getUserInfo("jianwei zhang", "jianweizh").getMsg());
	}

	UserDao userDao = new UserDao();

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseMsg loginByUserNameAndPasswordAndCommunity(@FormParam("username") String userName,
			@FormParam("password") String password,
			@FormParam("community") String community) {
		ResponseMsg msg = new ResponseMsg();
		boolean flag = false;
		boolean flag2 = false;
		if (userName != "" && password != "" && community != "") {
				
			
			// Search User Info From local DB
			for (User user : userDao
					.getUserInfoByUserNamePasswordAndCommunityFromLocalDB(
							userName, password, community)) {
				
				if (user.getUserName().equals(userName)
						&& user.getPassword().equals(password)
						&& user.getLocalDatabases().equals(community)) {
					flag=true;
					break;

				}
			}
			
			// Search User Info From KF
			Type listType = new TypeToken<ArrayList<User>>() {
            }.getType();
            List<User> userList = new Gson().fromJson(userDao.getUserInfoByUserNameAndPasswordFromKF5(userName, password), listType);
			
            for (User user : userList) {
				if (user.getUserName().equals(userName)
						&& user.getPassword().equals(password)
						&& user.getLocalDatabases().equals(community)) {
					flag=true;
					userDao.saveUserInfo(user);
					break;

				}
			}
			
		}
		if(flag){
			msg.setCode("200");
			msg.setMsg("Login Success");
			msg.setStatus("Sucess");
		}else{
			msg.setCode("400");
			msg.setMsg("Invaild User Name or password.");
			msg.setStatus("Failed");
		}
		return msg;
	}
	
	

	@GET
	@Path("/login_get")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseMsg GetloginByUserNameAndPasswordAndCommunity(@QueryParam("username") String userName,
			@QueryParam("password") String password,
			@QueryParam("community") String community) {
		ResponseMsg msg = new ResponseMsg();
		boolean flag = false;
		System.out.println("userName:"+userName+"   password"+password+"  community"+community+"  ");
		if (userName != "" && password != "" && community != "") {
			for (User user : userDao
					.getUserInfoByUserNamePasswordAndCommunityFromLocalDB(
							userName, password, community)) {
				if (user.getUserName().equals(userName)
						&& user.getPassword().equals(password)
						&& user.getLocalDatabases().equals(community)) {
					flag=true;
					break;
				}
			}
		}
		if(flag){
			msg.setCode("200");
			msg.setMsg("Login Success");
			msg.setStatus("Sucess");
		}else{
			msg.setCode("400");
			msg.setMsg("Invaild User Name or password.");
			msg.setStatus("Failed");
		}
		return msg;
	}
	

	

//	@POST
//	@Path("/getUserInfo")
//	@Produces(MediaType.APPLICATION_JSON)
//	public ResponseMsg getUserInfo(@FormParam("username") String userName,
//			@FormParam("password") String password) {
//		ResponseMsg msg = new ResponseMsg();
//		String output="";
//		List<User> users = new ArrayList<User>();
//		if (userName != "" && password != "") {
//			
//			 users=userDao.getUserInfoByUserNameAndPasswordFromLocalDB(userName, password);
//			 
//			if(users.isEmpty()){
//
//				// Search User Info From KF
//				Type listType = new TypeToken<ArrayList<User>>() {
//	            }.getType();
//	            List<User> userList = new Gson().fromJson(userDao.getUserInfoByUserNameAndPasswordFromKF5(userName, password), listType);
//				
//	            for (User user : userList) {
//					if (user.getUserName().equals(userName)
//							&& user.getPassword().equals(password)) {
//						userDao.saveUserInfo(user);
//						break;
//
//					}
//				}
//	            users=userDao.getUserInfoByUserNameAndPasswordFromLocalDB(userName, password);
//	            
//			}
//			output=new Gson().toJson(users);
//		}
//	
//		if(users.isEmpty()){
//			msg.setCode("400");
//			msg.setMsg("Invaild User Name or password.");
//			msg.setStatus("Failed");
//		}else{
//			msg.setCode("200");
//			msg.setMsg(output);
//			msg.setStatus("Sucess");
//		}
//		
//		
//		return msg;
//	}
//	
//	
	

	@POST
	@Path("/getUserInfo")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseMsg getUserInfo(@FormParam("username") String userName,
			@FormParam("password") String password) {
		ResponseMsg msg = new ResponseMsg();
		String output="";
		List<User> users = new ArrayList<User>();
		if (userName != "" && password != "") {
			
			 users=userDao.getUserInfoByUserNameAndPasswordFromLocalDB(userName, password);
			 
			if(users.isEmpty()){

				// Search User Info From KF
				
	          String userInfo = userDao.getUserInfoByUserNameAndPasswordFromKF5(userName, password);
	          List<User> userList = userDao.JosnStrToUserObject(userInfo, userName, password) ;
	            for (User user : userList) {
					if (user.getUserName().equals(userName)
							&& user.getPassword().equals(password)) {
						userDao.saveUserInfo(user);
					

					}
				}
	            users=userDao.getUserInfoByUserNameAndPasswordFromLocalDB(userName, password);
	            
			}
			output=new Gson().toJson(users);
		}
	
		if(users.isEmpty()){
			msg.setCode("400");
			msg.setMsg("Invaild User Name or password.");
			msg.setStatus("Failed");
		}else{
			msg.setCode("200");
			msg.setMsg(output);
			msg.setStatus("Sucess");
		}
		
		
		return msg;
	}
}