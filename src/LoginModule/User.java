package LoginModule;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String userName;
	private String firstName;
	private String lastName;
	private String type;
	private String password;
	private String email;
	private String Kf5Databases;
	private String localDatabases;
	
	
	

	public String getKf5Databases() {
		return Kf5Databases;
	}
	@XmlElement
	public void setKf5Databases(String kf5Databases) {
		Kf5Databases = kf5Databases;
	}

	public String getLocalDatabases() {
		return localDatabases;
	}
	@XmlElement
	public void setLocalDatabases(String localDatabases) {
		this.localDatabases = localDatabases;
	}

	public String getEmail() {
		return email;
	}
	
	@XmlElement
	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	@XmlElement
	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	@XmlElement
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	@XmlElement
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	@XmlElement
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getType() {
		return type;
	}

	@XmlElement
	public void setType(String type) {
		this.type = type;
	}

	public String getPassword() {
		return password;
	}

	@XmlElement
	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public User() {
	}

}