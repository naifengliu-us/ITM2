package LoginModule;

import itm.servlets.kf5Connect;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.albany.edu.IntergrateFactory.FactoryConfiguration;
import org.albany.edu.webservice.ITMService;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import code.sqls;

public class UserDao {

	final static Logger logger = Logger.getLogger(UserDao.class);
	sqls sql = new sqls();
	Connection conn = sql.getConn("itm");

	
	public String getUserInfoByUserNameAndPasswordFromKF5(String UserName,
			String Password) {

		String output = "";
		ITMService service = new ITMService();
		FactoryConfiguration config = new FactoryConfiguration();
		config.setPassword(Password);
		config.setUserName(UserName);
		String URL = "https://kf.utoronto.ca:443/kforum/";
		String filePath = "/network/rit/lab/zhanglab/kf5py-master/login.py";
		//logger.info("This is info : " + filePath);

		boolean isAccess = false;
		try {
			isAccess = service.login(config, URL, filePath);
		} catch (InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (isAccess) {
			try {
				output += service.login(URL, UserName, Password, filePath);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(output.indexOf('[')>1){
			output.substring(output.indexOf('[')-1, output.length());
		}
		return output;
	}
	
	public List<User> getUserInfoByUserNamePasswordAndCommunityFromLocalDB(String userName,String password, String community){
		return getUserInfo(" username='"+userName+"' and password='"+password+"' and community='"+community+"';");
	}
	
	
	
	public List<User> getUserInfoByUserNameAndPasswordFromLocalDB(String userName,
			String password) {
		return getUserInfo(" username='"+userName+"' and password='"+password+"';");
	}
	
	
	public List<User> getUserInfo(String condition){
		 List<User> users = new ArrayList<User>();
	     String insertQuery="select * from itm.author_table where "+condition;
         Statement stmt;
		try {
			stmt = conn.createStatement();
			System.out.print(insertQuery);
			ResultSet rs = stmt.executeQuery(insertQuery);
			
			while(rs.next()){
				User user = new User();
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));
				user.setUserName(rs.getString("username"));
				user.setType(rs.getString("type"));
				user.setPassword(rs.getString("password"));
				user.setLocalDatabases(rs.getString("community"));
				users.add(user);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return users;
	}
	
	/**********************
	Info Example :
	{"guid":"2ef4aa10-7acc-4634-9c0b-ba6a4405a319",
		"authorInfo":{"guid":"75ecace9-1c22-46cd-8f69-75acc8bee31b","userName":"Jianwei Zhang","lastName":"Zhang","firstName":"Jianwei","email":"jzhang1@albany.edu"},
		"roleInfo":{"guid":"39746fda-0f7f-4050-a9b7-1778f5d54e5e","name":"MANAGER"},
		"sectionId":"b47a8a6b-9298-4b1f-b520-65891e9dcdec","sectionTitle":"GES 2015-2016","dateCreated":"Aug 31, 2015 10:41:11 AM","markedForDelete":false}
	*********************/
	
	public List<User> JosnStrToUserObject(String info,String userName, String Password){
		
		List<User> users = new ArrayList<User>();
		JSONParser parser = new JSONParser();
		 Object obj;
		try {
			obj = parser.parse(info);
			JSONArray array = (JSONArray)obj;
		    for( Object o : array){
		    	User user = new User();
		    	JSONObject jsonObj = (JSONObject)o;
		    	user.setUserName(userName);
		    	user.setFirstName(((JSONObject)jsonObj.get("authorInfo")).get("firstName").toString());
		    	user.setLastName(((JSONObject)jsonObj.get("authorInfo")).get("lastName").toString());
		    	user.setPassword(Password);
		    	user.setType(((JSONObject)jsonObj.get("roleInfo")).get("name").toString());
		    	user.setEmail(((JSONObject)jsonObj.get("authorInfo")).get("email").toString());
		    	user.setKf5Databases((jsonObj.get("sectionTitle")).toString());
		    	String query  = " select localdb from itm.connection_table where kfdb='"+(jsonObj.get("sectionTitle")).toString()+"'";
		    	Statement stmt;
		    	try {
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(query);
					if(rs.next()){
						user.setLocalDatabases(rs.getString(1));	
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		    	users.add(user);
		    	
		    }
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return users;
		
	}
	
	public void saveUserInfo(User user){
		 sqls sql = new sqls();
	     Connection conn = sql.getConn("itm");
	     PreparedStatement ps = null;
	   
	    	    String insertQuery="Insert into itm.author_table (firstname,lastname,username,type,password,community) values('"
	   	    		 +user.getFirstName()+"','"
	   	    		 +user.getLastName()+"','"
	   	    		 +user.getUserName()+"','"
	   	    		 +user.getType()+"','"
	   	    		 +user.getPassword()+"','"
	   	    		 +user.getLocalDatabases()+"');"
	   	    		 ;
	   	     // Create blob here
	            Statement stmt;
	   		try {
	   			stmt = conn.createStatement();
	   			stmt.executeUpdate(insertQuery);
	   		} catch (SQLException e) {
	   			// TODO Auto-generated catch block
	   			e.printStackTrace();
	   		} 
	     
	 
        
	}
}
























