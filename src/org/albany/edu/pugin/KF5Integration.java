package org.albany.edu.pugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import database.CreateDatabase;

public class KF5Integration {

	public static final String DB_URL = "jdbc:mysql://localhost:3306/";
	public static final String DB_USER = "root";
	public static final String DB_PW = "051633b$";

	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	public Connection conn = null;
	public String db_name;
	protected String sql;
	Statement st = null;
	String URL = "";

	public KF5Integration() throws ClassNotFoundException, SQLException {
		getCon();
		getStatement();
	}

	public KF5Integration(String databaseName) throws ClassNotFoundException,
			SQLException {
		this.db_name = databaseName;
		getCon();
		getStatement();

	}

	public KF5Integration(String databaseName, String tempDB, String URL)
			throws ClassNotFoundException, SQLException {
		this.db_name = databaseName;
		this.getCon();
		getStatement();

	}

	public KF5Integration(String databaseName, String URL)
			throws ClassNotFoundException, SQLException {
		this.db_name = databaseName;
		this.URL = URL;
		getCon();
		getStatement();

	}

	public Connection getCon() throws ClassNotFoundException, SQLException {
		Class.forName(DB_DRIVER);
		this.conn = DriverManager.getConnection(DB_URL + db_name, DB_USER,
				DB_PW);
		return conn;
	}

	public Statement getStatement() throws ClassNotFoundException, SQLException {
		this.conn = getCon();
		this.st = conn.createStatement();
		return st;
	}

	public ResultSet selectRecords(String sql) throws ClassNotFoundException,
			SQLException {
		ResultSet rs = st.executeQuery(sql);
		return rs;
	}

	public int insertRecords(String sql) throws ClassNotFoundException,
			SQLException {
		int rs = st.executeUpdate(sql);
		return rs;
	}

	public void CleanUp() {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void insertview(String tableName, String database)
			throws SQLException {
		sql = "insert into `"
				+ db_name
				+ "`.`view_kf5`(title,uuid,section_id,guid) select title,uuid,sectionGuid,guid from `"
				+ database + "`.`" + tableName.toLowerCase() + "`;";

		st.execute(sql);

	}

	public void Updateview(String tableName, String database)
			throws SQLException {
		sql = "insert into `"
				+ database
				+ "`.`view_kf5`(title,uuid,section_id,guid) select title,uuid,sectionGuid,guid from `"
				+ database + "`.`" + tableName.toLowerCase() + "`;";

		st.execute(sql);
	}

	public void insertview(String db, String tableName, String database)
			throws SQLException {
		sql = "insert into `"
				+ db_name
				+ "`.`view_kf5`(title,uuid,section_id,guid) select title,uuid,sectionGuid,guid from `"
				+ database + "`.`" + tableName.toLowerCase() + "`;";

		st.execute(sql);

	}

	public void convertViewKF5toViewTable() throws SQLException {
		sql = "insert into `" + db_name
				+ "`.`view_table`(title,idview) select title,idview_kf5 from `"
				+ db_name + "`.`view_kf5`;";

		st.execute(sql);
	}
	
	public void convertViewKF5toViewTable(String db) throws SQLException {
		sql = "insert into `" + db
				+ "`.`view_table`(title,idview) select title,idview_kf5 from `"
				+ db_name + "`.`view_kf5`;";

		st.execute(sql);
	}


	public void createTable() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`buildon_toid` (`idbuildon_toid` INT(11) NOT NULL AUTO_INCREMENT,`toid` INT(11) NULL,PRIMARY KEY (`idbuildon_toid`));";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`author_table_kf5` (`idauthor_table_kf5` INT(11) NOT NULL AUTO_INCREMENT,`firstname` VARCHAR(45) NULL,`lastname` VARCHAR(45) NULL,`username` VARCHAR(45) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800)NULL,`guid` VARCHAR(800)NULL,PRIMARY KEY (`idauthor_table_kf5`))";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`author_table_kf5uni` (`idauthor_table_kf5` INT(11) NOT NULL AUTO_INCREMENT,`firstname` VARCHAR(45) NULL,`lastname` VARCHAR(45) NULL,`username` VARCHAR(45) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800)NULL,`guid` VARCHAR(800)NULL,PRIMARY KEY (`idauthor_table_kf5`))";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`note_kf5` (`idnote_KF5` INT(11) NOT NULL AUTO_INCREMENT,`notetitle` VARCHAR(200) NULL,`notecontent` MEDIUMTEXT NULL,`createtime` VARCHAR(800) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL, PRIMARY KEY (`idnote_KF5`));";

		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`view_kf5` (`idview_kf5` INT(11) NOT NULL AUTO_INCREMENT,`title` VARCHAR(450) NULL,`uuid` VARCHAR(800) NULL,`section_id` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL,PRIMARY KEY (`idview_kf5`));";

		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`note_kf5uni` (`idnote_KF5` INT(11) NOT NULL AUTO_INCREMENT,`notetitle` VARCHAR(200) NULL,`notecontent` MEDIUMTEXT NULL,`createtime` VARCHAR(800) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL, PRIMARY KEY (`idnote_KF5`));";

		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`note_view_con` (`noteid` INT(11) NULL ,`viewid` INT(11) NULL);";

		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`buildon_kf5` (`idbuildon_kf5` INT(11) NOT NULL AUTO_INCREMENT,`fromid` VARCHAR(800) NULL,`toid` VARCHAR(800) NULL,`linktype` VARCHAR(800) NULL,PRIMARY KEY (`idbuildon_kf5`));";

		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`note_note_kf5` (`idnote_note_kf5` INT(11) NOT NULL AUTO_INCREMENT,`fromid` INT(11) NULL,`toid` INT(11) NULL,PRIMARY KEY (`idnote_note_kf5`));";

		st.execute(sql);

		sql = "CREATE TABLE " + db_name + ".`viewpostrefs` ("
				+ "`id` int(11) NOT NULL,"
				+ "`father_id` varchar(800) DEFAULT NULL,"
				+ " `uuid` varchar(800) DEFAULT NULL,"
				+ "`copy` varchar(5) DEFAULT NULL,"
				+ "`viewGuid` varchar(800) DEFAULT NULL,"
				+ "`copierId` varchar(800) DEFAULT NULL,"
				+ "`height` int(16) DEFAULT NULL,"
				+ "`width` int(16) DEFAULT NULL,"
				+ " `rotation` double DEFAULT NULL,"
				+ " `guid` varchar(800) DEFAULT NULL,"
				+ "`target` int(16) DEFAULT NULL,"
				+ "`display` int(16) DEFAULT NULL," + " PRIMARY KEY (`id`)"
				+ ") ENGINE=InnoDB AUTO_INCREMENT=1496 DEFAULT CHARSET=utf8;";
		st.execute(sql);
		sql = "CREATE TABLE " + db_name + ".`attachments_kf5` ("
				+ "`id` int(11) NOT NULL AUTO_INCREMENT,"
				+ "`father_id` varchar(800) DEFAULT NULL,"
				+ "`uuid` varchar(800) DEFAULT NULL,"
				+ "`created` varchar(800) DEFAULT NULL,"
				+ "`title` varchar(800) DEFAULT NULL,"
				+ "`mime` varchar(800) DEFAULT NULL,"
				+ "`fileSize` varchar(800) DEFAULT NULL,"
				+ "`file` varchar(800) DEFAULT NULL,"
				+ "`guid` varchar(800) DEFAULT NULL,"
				+ "`target` int(16) DEFAULT NULL," + "PRIMARY KEY (`id`)"
				+ ") ENGINE=InnoDB AUTO_INCREMENT=414 DEFAULT CHARSET=utf8;";

		st.execute(sql);
	}

	public void createTable(String db_name) throws SQLException {

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`buildon_toid`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`buildon_toid` (`idbuildon_toid` INT(11) NOT NULL AUTO_INCREMENT,`toid` INT(11) NULL,PRIMARY KEY (`idbuildon_toid`));";
		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`author_table_kf5`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`author_table_kf5` (`idauthor_table_kf5` INT(11) NOT NULL AUTO_INCREMENT,`firstname` VARCHAR(45) NULL,`lastname` VARCHAR(45) NULL,`username` VARCHAR(45) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800)NULL,`guid` VARCHAR(800)NULL,PRIMARY KEY (`idauthor_table_kf5`))";
		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`author_table_kf5uni`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`author_table_kf5uni` (`idauthor_table_kf5` INT(11) NOT NULL AUTO_INCREMENT,`firstname` VARCHAR(45) NULL,`lastname` VARCHAR(45) NULL,`username` VARCHAR(45) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800)NULL,`guid` VARCHAR(800)NULL,PRIMARY KEY (`idauthor_table_kf5`))";
		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`note_kf5`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`note_kf5` (`idnote_KF5` INT(11) NOT NULL AUTO_INCREMENT,`notetitle` VARCHAR(200) NULL,`notecontent` MEDIUMTEXT NULL,`createtime` VARCHAR(800) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL, PRIMARY KEY (`idnote_KF5`));";

		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`view_kf5`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`view_kf5` (`idview_kf5` INT(11) NOT NULL AUTO_INCREMENT,`title` VARCHAR(450) NULL,`uuid` VARCHAR(800) NULL,`section_id` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL,PRIMARY KEY (`idview_kf5`));";

		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`note_kf5uni`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`note_kf5uni` (`idnote_KF5` INT(11) NOT NULL AUTO_INCREMENT,`notetitle` VARCHAR(200) NULL,`notecontent` MEDIUMTEXT NULL,`createtime` VARCHAR(800) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL, PRIMARY KEY (`idnote_KF5`));";

		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`note_view_con`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`note_view_con` (`noteid` INT(11) NULL ,`viewid` INT(11) NULL);";

		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`buildon_kf5`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`buildon_kf5` (`idbuildon_kf5` INT(11) NOT NULL AUTO_INCREMENT,`fromid` VARCHAR(800) NULL,`toid` VARCHAR(800) NULL,`linktype` VARCHAR(800) NULL,PRIMARY KEY (`idbuildon_kf5`));";

		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`note_note_kf5`";
		st.execute(sql);

		sql = "CREATE TABLE `"
				+ db_name
				+ "`.`note_note_kf5` (`idnote_note_kf5` INT(11) NOT NULL AUTO_INCREMENT,`fromid` INT(11) NULL,`toid` INT(11) NULL,PRIMARY KEY (`idnote_note_kf5`));";

		st.execute(sql);

		sql = "DROP TABLE IF EXISTS `" + db_name + "`.`attachments_kf5`";
		st.execute(sql);

		sql = "CREATE TABLE " + db_name + ".`attachments_kf5` ("
				+ "`id` int(11) NOT NULL AUTO_INCREMENT,"
				+ "`father_id` varchar(800) DEFAULT NULL,"
				+ "`uuid` varchar(800) DEFAULT NULL,"
				+ "`created` varchar(800) DEFAULT NULL,"
				+ "`title` varchar(800) DEFAULT NULL,"
				+ "`mime` varchar(800) DEFAULT NULL,"
				+ "`fileSize` varchar(800) DEFAULT NULL,"
				+ "`file` varchar(800) DEFAULT NULL,"
				+ "`guid` varchar(800) DEFAULT NULL,"
				+ "`target` int(16) DEFAULT NULL," + "PRIMARY KEY (`id`)"
				+ ") ENGINE=InnoDB AUTO_INCREMENT=414 DEFAULT CHARSET=utf8;";

		st.execute(sql);
		
		sql="CREATE TABLE " + db_name + ".`viewpostrefs2` ("
				+ "  `id` int(11) NOT NULL,"
				+ "  `father_id` varchar(800) DEFAULT NULL,"
				+ "  `uuid` varchar(800) DEFAULT NULL,"
				+ "  `copy` varchar(5) DEFAULT NULL,"
				+ "  `viewGuid` varchar(800) DEFAULT NULL,"
				+ "  `copierId` varchar(800) DEFAULT NULL,"
				+ "  `height` int(16) DEFAULT NULL,"
				+ "  `width` int(16) DEFAULT NULL,"
				 + " `rotation` double DEFAULT NULL,"
				 + " `guid` varchar(800) DEFAULT NULL,"
				+ "  `target` int(16) DEFAULT NULL,"
				 + " `display` int(16) DEFAULT NULL,"
				 + " PRIMARY KEY (`id`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		st.execute(sql);
	}

	public void createDatabase() throws SQLException {
		String sql = "CREATE DATABASE " + db_name;
		st.execute(sql);
	}

	public void dropDatabase() throws SQLException {
		String sql = "DROP schema IF EXISTS " + db_name;
		st.execute(sql);
	}

	// public void insertconnoteview(int id,String dbname) throws SQLException {
	// sql =
	// "insert into "+db_name+".note_view_con(viewid) select idview_kf5 from view_kf5 where guid in (select viewGuid from `"+dbname+"`.`viewpostrefs` where uuid in (select father_id from note_kf5 where idnote_KF5="
	// + id + "))";
	//
	// st.execute(sql);
	// }

	public void insertIntoViewpostrefsFromViewpostrefsByViewGuid(String table,
			String viewGuid) throws ClassNotFoundException, SQLException {
		String sql = "insert into "
				+ db_name
				+ ".viewpostrefs (`id`,"
				+ "`father_id`,"
				+ "`uuid`,"
				+ "`copy`,"
				+ "`viewGuid`,"
				+ "`copierId`,"
				+ "`height`,"
				+ "`width`,"
				+ "`rotation`,"
				+ "`guid`,"
				+ "`display`) select"
				+ " id,father_id,`uuid`, `copy`,`viewGuid`,`copierId`,`height`,`width`,`rotation`, `guid`,`display`"
				+ "from " + table + " where viewGuid='" + viewGuid + "'";
		insertRecords(sql);

	}
	
	public void insertIntoViewpostrefs2FromViewpostrefsByViewGuid(String table,
			String viewGuid) throws ClassNotFoundException, SQLException {
		String sql = "insert into "
				+ db_name
				+ ".viewpostrefs2 (`id`,"
				+ "`father_id`,"
				+ "`uuid`,"
				+ "`copy`,"
				+ "`viewGuid`,"
				+ "`copierId`,"
				+ "`height`,"
				+ "`width`,"
				+ "`rotation`,"
				+ "`guid`,"
				+ "`display`) select"
				+ " id,father_id,`uuid`, `copy`,`viewGuid`,`copierId`,`height`,`width`,`rotation`, `guid`,`display`"
				+ "from " + table + " where viewGuid='" + viewGuid + "'";
		insertRecords(sql);

	}
	

	// db is registration db GES-2015-2016
	public void UpdateDatabases(String db) throws ClassNotFoundException,
			SQLException, ParseException {

		CreateDatabase cd = new CreateDatabase(db_name);
		createTable(db_name);
		System.out.print("create table finish");
		ResultSet rs = selectRecords("SELECT table_name FROM database_table;");
		String current_database = db_name;

		// get table_names
		List<String> table_names = new ArrayList<String>();
		while (rs.next()) {
			table_names.add(rs.getString(1).toLowerCase());
		}
		// Create database and get Section id list and section title list
		ResultSet section = selectRecords("SELECT * FROM section;");
		
		List<String> Section_ids = new ArrayList<String>();
		List<String> Section_name = new ArrayList<String>();
		List<String> Section_id_temp = new ArrayList<String>();
		
		if (section.next()) {
			String[] sectionTitles = section.getString("renamekey").split("&");
			int length = sectionTitles.length;
			for (int i = 1; i < length; i++) {
				Section_id_temp.add(section.getString(3 + i));
			}
			for (int i = 1; i < length; i++) {
				String dbname = sectionTitles[i];
				if (db.equals(dbname)) {
					// Create db
					Section_ids.add(Section_id_temp.get(i - 1));
					Section_name.add(db_name);

				}

			}
		}

		int index = 0;
		System.out.println("Section_id:" + Section_ids);
		for (String sectionId : Section_ids) {
			// find view_guid under this sectionId
			// Use this guid to find viewpostrefer's uuid which is fatherid in
			// the postInfo.
			// Store viewpostrefer into this section
			// Store viewInfo into view_kf5 under this section

			db_name = Section_name.get(index);
			index++;
			List<String> viewGuids = new ArrayList<String>();
			List<String> viewTable = new ArrayList<String>();
			List<String> buildOnTables = new ArrayList<String>();
			// Get all view
			for (String tableName : table_names) {
				// find all view tables
				if (tableName.toLowerCase().startsWith(
						"noteandnotegrouprelationship")
						&& tableName.length() > 30) {
					// find view info
					ResultSet viewInfo = selectRecords("SELECT * FROM "
							+ current_database + "." + tableName.toLowerCase());
					while (viewInfo.next()) {
						// if view is under section_id then transfor this view
						if (viewInfo.getString("sectionGuid").equals(sectionId)) {
							// get view guid
							viewGuids.add(viewInfo.getString("guid"));
							// add table name into viewTable which use for
							// transfor this view under this section
							viewTable.add(tableName.toLowerCase());
						}
					}
				}
				String tbName = "buildon"
						+ sectionId.replace("_", "").replace("-", "")
								.replace("/", "").replace(" ", "");
				if (tableName.equalsIgnoreCase(tbName.toLowerCase())) {
					buildOnTables.add(tableName);
				}
			}// end for
			System.out.println("current_database:" + current_database);
			// Store viewInfo into view_kf5 under this section
			for (String tablename : viewTable) {
				Updateview(tablename.toLowerCase(), current_database);
			}

			// Store viewpostrefer into this section
			for (String viewGuid : viewGuids) {
				insertIntoViewpostrefs2FromViewpostrefsByViewGuid(
						current_database + ".viewpostrefs ", viewGuid);
			}
			sql ="TRUNCATE  table `"
					+ current_database
					+ "`.`viewpostrefs`";
			insertRecords(sql);
			
			String sql3 = "insert into `"
					+ db_name
					+ "`.`viewpostrefs`"
					+ "(id,father_id,uuid,copy,viewGuid,copierId,height,width,rotation,guid,display)"
					+ " select id,father_id,uuid,copy,viewGuid,copierId,height,width,rotation,guid,display "
					+ "from `" + current_database + "`.`viewpostrefs2`";
			
			insertRecords(sql3);
			
			// Get all view uuid
			// viewpostreferes uuid which is fatherid in the postInfo.
			List<String> viewUUIDS = new ArrayList<String>();
			// viewUUIDS
			ResultSet viewpostUUID = selectRecords("SELECT uuid FROM "
					+ db_name + ".viewpostrefs ");
			while (viewpostUUID.next()) {
				viewUUIDS.add(viewpostUUID.getString(1));
			}
			// Store all note under this section
			for (String uuid : viewUUIDS) {
				UpdateNote_kf5FromPostinfoByUUID(current_database, uuid);
			}

			// Get all note uuid
			List<String> noteUUIDS = new ArrayList<String>();
			ResultSet noteUUID = selectRecords("SELECT uuid FROM " + db_name
					+ ".note_kf5 ");
			while (noteUUID.next()) {
				noteUUIDS.add(noteUUID.getString(1));
			}

			
			sql ="TRUNCATE  table `"
					+ current_database
					+ "`.`author_table_kf5`";
			insertRecords(sql);
			
			// Store all author under this section
			for (String uuid : noteUUIDS) {
				UpdateIntoAuthor_Table_KF5FormAuthorsByUUID(current_database,
						uuid);
			}

			// Store all attachment under this section
			for (String uuid : noteUUIDS) {
				UpdateIntoAttachmetn_Kf5fromAttachmentsByUUID(current_database,
						uuid);

			}

			for (String tables : buildOnTables) {
				UpdateBuildOnTablefromBuildOnTable(tables, current_database);
			}
			
			generateAuthorUni(current_database);

			generateNoteUni(current_database);
			
			
			
		//	convertViewKF5toViewTable(current_database);
			
		//	convertNoteKF5toTargetNoteKF(current_database);


		}

	}

	
	public List<String> CreateDatabase(String db)
			throws ClassNotFoundException, SQLException, ParseException {

		ResultSet rs = selectRecords("SELECT table_name FROM database_table;");
		String current_database = db_name;

		// get table_names
		List<String> table_names = new ArrayList<String>();
		while (rs.next()) {
			table_names.add(rs.getString(1).toLowerCase());
		}
		// Create database and get Section id list and section title list
		ResultSet section = selectRecords("SELECT * FROM section;");
		List<String> Section_ids = new ArrayList<String>();
		List<String> Section_name = new ArrayList<String>();
		List<String> Section_id_temp = new ArrayList<String>();
		if (section.next()) {
			String[] sectionTitles = section.getString("renamekey").split("&");
			int length = sectionTitles.length;
			for (int i = 1; i < length; i++) {
				Section_id_temp.add(section.getString(3 + i));
			}
			for (int i = 1; i < length; i++) {
				String dbname = sectionTitles[i];
				if (db.equals(dbname)) {
					// Create db
					Section_ids.add(Section_id_temp.get(i - 1));
					db_name = this.URL
							+ "_"
							+ dbname.replace("-", "_").replace(" ", "_")
									.replace(".", "_");
					Section_name.add(db_name);
					// create ITM database

					CreateDatabase cd = new CreateDatabase(db_name);
					cd.CreateDB();
					// create Integration Tables
					createTable();
					createTable(current_database);
				}

			}
		}

		int index = 0;

		for (String sectionId : Section_ids) {
			// find view_guid under this sectionId
			// Use this guid to find viewpostrefer's uuid which is fatherid in
			// the postInfo.
			// Store viewpostrefer into this section
			// Store viewInfo into view_kf5 under this section

			db_name = Section_name.get(index);
			index++;
			List<String> viewGuids = new ArrayList<String>();
			List<String> viewTable = new ArrayList<String>();
			List<String> buildOnTables = new ArrayList<String>();
			// Get all view
			for (String tableName : table_names) {
				// find all view tables
				if (tableName.toLowerCase().startsWith(
						"noteandnotegrouprelationship")
						&& tableName.length() > 30) {
					// find view info
					ResultSet viewInfo = selectRecords("SELECT * FROM "
							+ current_database + "." + tableName.toLowerCase());
					while (viewInfo.next()) {
						// if view is under section_id then transfor this view
						if (viewInfo.getString("sectionGuid").equals(sectionId)) {
							// get view guid
							viewGuids.add(viewInfo.getString("guid"));
							// add table name into viewTable which use for
							// transfor this view under this section
							viewTable.add(tableName.toLowerCase());
						}
					}
				}
				String tbName = "buildon"
						+ sectionId.replace("_", "").replace("-", "")
								.replace("/", "").replace(" ", "");
				if (tableName.equalsIgnoreCase(tbName.toLowerCase())) {
					buildOnTables.add(tableName);
				}
			}// end for

			// Store viewInfo into view_kf5 under this section
			for (String tablename : viewTable) {
				insertview(tablename.toLowerCase(), current_database);
			}
			convertViewKF5toViewTable();

			// Store viewpostrefer into this section
			for (String viewGuid : viewGuids) {
				insertIntoViewpostrefsFromViewpostrefsByViewGuid(
						current_database + ".viewpostrefs ", viewGuid);
			}

			// Get all view uuid
			// viewpostreferes uuid which is fatherid in the postInfo.
			List<String> viewUUIDS = new ArrayList<String>();
			// viewUUIDS
			ResultSet viewpostUUID = selectRecords("SELECT uuid FROM "
					+ db_name + ".viewpostrefs ");
			while (viewpostUUID.next()) {
				viewUUIDS.add(viewpostUUID.getString(1));
			}
			// Store all note under this section
			for (String uuid : viewUUIDS) {
				insertIntoNote_kf5FromPostinfoByUUID(current_database, uuid);
			}

			// Get all note uuid
			List<String> noteUUIDS = new ArrayList<String>();
			ResultSet noteUUID = selectRecords("SELECT uuid FROM " + db_name
					+ ".note_kf5 ");
			while (noteUUID.next()) {
				noteUUIDS.add(noteUUID.getString(1));
			}

			// Store all author under this section
			for (String uuid : noteUUIDS) {
				insertIntoAuthor_Table_KF5FormAuthorsByUUID(current_database,
						uuid);
			}

		

			// Store all attachment under this section
			for (String uuid : noteUUIDS) {
				insertIntoAttachmetn_Kf5fromAttachmentsByUUID(current_database,
						uuid);

			}

			for (String tables : buildOnTables) {
				insertIntoBuildOnTablefromBuildOnTable(tables, current_database);
			}

		}

		return Section_name;
	}

	public void insertIntoBuildOnTablefromBuildOnTable(String table, String db)
			throws ClassNotFoundException, SQLException {
		String sql = "insert into `"
				+ db_name
				+ "`.`buildon_kf5`(`fromid`,`toid`,`linktype`) select `from`,`to`,`type` from "
				+ db + "." + table;
		insertRecords(sql);

	}

	public void UpdateBuildOnTablefromBuildOnTable(String table, String db)
			throws ClassNotFoundException, SQLException {

		sql = "insert into `"
				+ db
				+ "`.`buildon_kf5`(`fromid`,`toid`,`linktype`) select `from`,`to`,`type` from "
				+ db + "." + table;
		insertRecords(sql);
	}

	public void insertIntoAttachmetn_Kf5fromAttachmentsByUUID(String table,
			String uuid) throws ClassNotFoundException, SQLException,
			ParseException {
		String sql = "insert into `"
				+ db_name
				+ "`.`attachments_kf5`(`id`,`father_id`,`uuid`,created,title,mime,fileSize,file,guid) select `id`,`father_id`,`uuid`,created,title,mime,fileSize,file,guid from "
				+ table + ".`attachments`" + " where father_id='" + uuid + "';";
		insertRecords(sql);

	}

	public void UpdateIntoAttachmetn_Kf5fromAttachmentsByUUID(String table,
			String uuid) throws ClassNotFoundException, SQLException,
			ParseException {
		sql = "insert into `"
				+ table
				+ "`.`attachments_kf5`(`id`,`father_id`,`uuid`,created,title,mime,fileSize,file,guid) select `id`,`father_id`,`uuid`,created,title,mime,fileSize,file,guid from "
				+ table + ".`attachments`" + " where father_id='" + uuid + "';";
		insertRecords(sql);
	}

	public void convertAttachmetnTablefromAttachment_KF5()
			throws ClassNotFoundException, SQLException, ParseException {

		sql = "select *  from `" + db_name + "`.`attachments_kf5`";

		ResultSet rs = selectRecords(sql);
		List<String> insertSql = new ArrayList<String>();
		SimpleDateFormat fromUser = new SimpleDateFormat(
				"MMM dd, yyyy HH:mm:ss a", Locale.ENGLISH);
		SimpleDateFormat spdtformat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		while (rs.next()) {

			String sql2 = "insert into `"
					+ db_name
					+ "`.`attachment_table`(attachmentid,attachmenttitle,attachmentfile,createtime) values("
					+ "'"
					+ rs.getInt("id")
					+ "',"
					+ "\""
					+ rs.getString("title").replace("\"", "'")
					+ "\","
					+ "\""
					+ rs.getString("file").replace("\"", "'")
					+ "\","
					+ "'"
					+ spdtformat
							.format(fromUser.parse(rs.getString("created")))
					+ "'" + ")";
			insertSql.add(sql2);
		}
		for (String sql3 : insertSql) {
			// print(sql3);
			st.execute(sql3);
		}
	}

	public void insertIntoAuthor_Table_KF5FormAuthorsByUUID(String table,
			String uuid) throws ClassNotFoundException, SQLException {
		String sql = "insert into `"
				+ db_name
				+ "`.`author_table_kf5`(idauthor_table_kf5,firstname,lastname,username,father_id,uuid,guid) select id,firstName,lastName,userName,father_id,uuid ,guid from "
				+ table + ".`authors`" + " where father_id='" + uuid + "';";
		insertRecords(sql);

	}

	public void UpdateIntoAuthor_Table_KF5FormAuthorsByUUID(String table,
			String uuid) throws ClassNotFoundException, SQLException {
	
		sql = "insert into `"
				+ table
				+ "`.`author_table_kf5`(idauthor_table_kf5,firstname,lastname,username,father_id,uuid,guid) select id,firstName,lastName,userName,father_id,uuid ,guid from "
				+ table + ".`authors`" + " where father_id='" + uuid + "';";
		insertRecords(sql);

	}

	public void insertIntoNote_kf5FromPostinfoByUUID(String table, String uuid)
			throws ClassNotFoundException, SQLException {
		String sql = "insert into `"
				+ db_name
				+ "`.`note_kf5`(notetitle,notecontent,father_id,uuid,createtime,guid) select title,body,father_id,uuid,created,guid from `"
				+ table + "`.`postinfo`" + "  where father_id='" + uuid + "';";
		insertRecords(sql);

	}

	public void UpdateNote_kf5FromPostinfoByUUID(String table, String uuid)
			throws ClassNotFoundException, SQLException {
		String sql2 = "insert into `"
				+ table
				+ "`.`note_kf5`(notetitle,notecontent,father_id,uuid,createtime,guid) select title,body,father_id,uuid,created,guid from `"
				+ table + "`.`postinfo`" + "  where father_id='" + uuid + "';";
		insertRecords(sql2);
	}

	public void insertIntoConNoteAuthor(int id_author, int id_note)
			throws SQLException {
		sql = "insert into `" + db_name
				+ "`.author_note(authorid,noteid) values (" + id_author + ","
				+ id_note + ")";
		st.execute(sql);
	}

	public void generateAuthorUni() throws SQLException {
		System.out.println("generateAuthorUni");
		sql = "insert into `"
				+ db_name
				+ "`.author_table_kf5uni(firstname,lastname,username,father_id,uuid,guid)"
				+ "select firstname,lastname,username,father_id,uuid,guid from "
				+ "`"
				+ db_name
				+ "`.author_table_kf5 group by guid order by idauthor_table_kf5";
		st.execute(sql);
		System.out.println(sql);

		sql = "insert into `" + db_name
				+ "`.author_table(firstname,lastname,username,authorid)"
				+ "select firstname,lastname,username,idauthor_table_kf5 from "
				+ "`" + db_name + "`.author_table_kf5uni";
		st.execute(sql);
		System.out.println(sql);

	}
	
	
	public void generateAuthorUni(String db) throws SQLException {
		sql = "insert into `"
				+ db
				+ "`.author_table_kf5uni(firstname,lastname,username,father_id,uuid,guid)"
				+ "select firstname,lastname,username,father_id,uuid,guid from "
				+ "`"
				+ db
				+ "`.author_table_kf5 group by guid order by idauthor_table_kf5";
		st.execute(sql);

		
	}

	// public void generateBuidOn() throws SQLException {
	// sql = "insert into `"
	// + db_name
	// + "`.buildon_kf5(idbuildon_kf5,fromid,toid,linktype)"
	// + "select firstname,lastname,username,father_id,uuid,guid from "
	// + "`"
	// + db_name
	// + "`.author_table_kf5 group by guid order by idauthor_table_kf5";
	// st.execute(sql);
	//
	// sql = "insert into `"
	// + db_name
	// + "`.author_table(firstname,lastname,username,authorid)"
	// + "select firstname,lastname,username,idauthor_table_kf5 from "
	// + "`"
	// + db_name
	// + "`.author_table_kf5uni";
	// st.execute(sql);
	//
	// }

	public void generateNoteUni() throws SQLException, ParseException,
			ClassNotFoundException {
		sql = "insert into `"
				+ db_name
				+ "`.`note_kf5uni`(notetitle,notecontent,createtime,father_id,uuid,guid)"
				+ "select notetitle,notecontent,createtime,father_id,uuid,guid from `"
				+ db_name + "`.`note_kf5` group by guid order by idnote_KF5";
		st.execute(sql);

		sql = "select notetitle,notecontent,createtime,idnote_KF5 from `"
				+ db_name + "`.`note_kf5uni`";

		ResultSet rs = selectRecords(sql);
		List<String> insertSql = new ArrayList<String>();
		SimpleDateFormat fromUser = new SimpleDateFormat(
				"MMM dd, yyyy HH:mm:ss a", Locale.ENGLISH);
		SimpleDateFormat spdtformat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		while (rs.next()) {

			String sql2 = "insert into `"
					+ db_name
					+ "`.`note_table`(notetitle,notecontent,createtime,noteid) values("
					+ "\""
					+ rs.getString("notetitle").replace("\"", "'")
					+ "\","
					+ "\""
					+ rs.getString("notecontent").replace("\"", "'")
					+ "\","
					+ "'"
					+ spdtformat.format(fromUser.parse(rs
							.getString("createtime"))) + "'," + "'"
					+ rs.getInt("idnote_KF5") + "'" + ")";
			insertSql.add(sql2);
		}
		for (String sql3 : insertSql) {
			st.execute(sql3);
		}

	}

	public void generateNoteUni(String db) throws SQLException, ParseException,
			ClassNotFoundException {
		sql = "insert into `"
				+ db
				+ "`.`note_kf5uni`(notetitle,notecontent,createtime,father_id,uuid,guid)"
				+ "select notetitle,notecontent,createtime,father_id,uuid,guid from `"
				+ db + "`.`note_kf5` group by guid order by idnote_KF5";
		st.execute(sql);

//		sql = "select notetitle,notecontent,createtime,idnote_KF5 from `" + db
//				+ "`.`note_kf5uni`";
//
//		ResultSet rs = selectRecords(sql);
//		List<String> insertSql = new ArrayList<String>();
//		SimpleDateFormat fromUser = new SimpleDateFormat(
//				"MMM dd, yyyy HH:mm:ss a", Locale.ENGLISH);
//		SimpleDateFormat spdtformat = new SimpleDateFormat(
//				"yyyy-MM-dd HH:mm:ss");
//		while (rs.next()) {
//
//			String sql2 = "insert into `"
//					+ db
//					+ "`.`note_table`(notetitle,notecontent,createtime,noteid) values("
//					+ "\""
//					+ rs.getString("notetitle").replace("\"", "'")
//					+ "\","
//					+ "\""
//					+ rs.getString("notecontent").replace("\"", "'")
//					+ "\","
//					+ "'"
//					+ spdtformat.format(fromUser.parse(rs
//							.getString("createtime"))) + "'," + "'"
//					+ rs.getInt("idnote_KF5") + "'" + ")";
//			insertSql.add(sql2);
//		}
//		for (String sql3 : insertSql) {
//			st.execute(sql3);
//		}

	}

	public void insertConNote(String column, String tableName, int id)
			throws SQLException {
		sql = "insert into `" + db_name + "`.`" + tableName.toLowerCase()
				+ "`(" + column
				+ ") select idnote_KF5 from note_kf5 where guid in (select "
				+ column + " from buildon_kf5 where idbuildon_kf5=" + id + ")";
		st.execute(sql);

	}

	public void updateConNote(int id) throws SQLException {
		sql = "update `" + db_name + "`.`note_note_kf5`set toid=" + id
				+ " where idnote_note_kf5==" + id + "";
		st.execute(sql);

	}

	// public int selectconnoteid(int id) throws SQLException {
	// int idnote_kf5 = 0;
	// sql = "SELECT idnote_KF5 from `"
	// + db_name
	// +
	// "`.note_kf5 where guid in (select toid from buildon_kf5 where idbuildon_kf5=1)";
	// st.execute(sql);
	// return idnote_kf5;
	// }

	public void insertNoteView(int id_note, int id_view) throws SQLException {
		sql = "insert into `" + db_name + "`.view_note(noteid,viewid) values ("
				+ id_note + "," + id_view + ")";
		st.execute(sql);

	}

	public void insertAuthorNote(int id_author, int id_note)
			throws SQLException {
		sql = "insert into `" + db_name
				+ "`.author_note(authorid,noteid) values (" + id_author + ","
				+ id_note + ")";
		st.execute(sql);

	}

	public void insertNoteNote(int from, int to, String type)
			throws SQLException {
		sql = "insert into `" + db_name
				+ "`.note_note(fromnoteid,tonoteid,linktype) values (" + from
				+ "," + to + ",'" + type + "')";
		st.execute(sql);

	}

	public void insertAttachmentNote(int noteId, int attId) throws SQLException {
		sql = "insert into `" + db_name
				+ "`.attachment_note(noteid,attachmentid) values (" + noteId
				+ "," + attId + ")";
		st.execute(sql);

	}

	public ArrayList<String> selectfatherid(int id) throws SQLException,
			ClassNotFoundException {

		ResultSet rs1 = selectRecords("select father_id from `" + db_name
				+ "`.note_KF5 where guid in (select guid from `" + db_name
				+ "`.note_KF5uni where idnote_KF5=" + id + ");");

		ArrayList<String> father_id = new ArrayList<String>();
		while (rs1.next()) {
			father_id.add(rs1.getString("father_id"));
		}
		return father_id;
	}

	//
	// public ArrayList<String> selectauthorfatherid(int id) throws
	// SQLException, ClassNotFoundException{
	//
	// DBConfig db1 = new DBConfig();
	// ResultSet rs1
	// =db1.selectRecords("select father_id from author_table_kf5 where guid in (select guid from author_table_kf5uni where idauthor_table_kf5="+id+");");
	// ArrayList<String> auth_fid = new ArrayList<String>();
	// while(rs1.next()){
	// auth_fid.add(rs1.getString("father_id"));
	// }
	// return auth_fid;
	// }
	//
	// public int selectviewid(String father_id) throws SQLException,
	// ClassNotFoundException{
	// Statement st = conn.createStatement();
	// sql =
	// "select idview_kf5 from view_kf5 where guid in(select viewGuid from `"+db_kf5+"`.`viewpostrefs` where uuid='"+father_id+"')";
	//
	// ResultSet viewid
	// =db1.selectRecords("select * from view_kf5 where guid in(select viewGuid from `"+db_kf5+"`.`viewpostrefs` where uuid='"+father_id+"')");
	// int viewid_kf5=0;
	// viewid.next();
	// viewid_kf5= viewid.getInt("idview_kf5");
	// return viewid_kf5;
	// }
	//
	//
	// public int selectnoteid(String father_id) throws SQLException,
	// ClassNotFoundException{
	// DBConfig db1 = new DBConfig();
	// //get guid
	// ResultSet noteGuid
	// =db1.selectRecords("select * from note_kf5 where uuid='"+father_id+"'");
	// String noteGuidId = "";
	// while(noteid.next()){
	// noteGuidId=noteGuid.getString("guid");
	// }
	// ResultSet noteid
	// =db1.selectRecords("select * from note_kf5uni where guid='"+noteGuidId+"'");
	// // if not find note the result is -1
	// int noteid_kf5=-1;
	// while(noteid.next()){
	// noteid_kf5=noteid.getInt("idnote_KF5");
	// }
	// return noteid_kf5;
	// }
	//

	public int getAuthor_Table_KF5GuidByID(int id) throws SQLException {
		Statement st = conn.createStatement();
		sql = "select guid from `" + db_name
				+ "`.author_table_kf5 where idauthor_table_kf5=" + id + "";
		st.execute(sql);
		return id;
	}

	public void print(String st) {
		System.out.println(st);
	}

	public void Integration(String dbName) throws ClassNotFoundException,
			SQLException, ParseException {

		List<String> sectionNames = CreateDatabase(dbName);
		System.out.print("CreateDatabase Done :" + dbName);
		print("10");
		// for each database
		for (String sectionName : sectionNames) {
			if (sectionName.equals(this.URL
					+ "_"
					+ dbName.replace("-", "_").replace(" ", "_")
							.replace(".", "_"))) {
				db_name = sectionName;
				// Generate author_uni & Note_uni
				generateAuthorUni();
				generateNoteUni();

				// generate author and note relationship

				// get author_uni uuid
				List<String> author_guids = new ArrayList<String>();

				List<Integer> author_ids = new ArrayList<Integer>();
				ResultSet rsAuthorUni = selectRecords("select guid,idauthor_table_kf5 from "
						+ sectionName + ".author_table_kf5uni");
				while (rsAuthorUni.next()) {
					author_guids.add(rsAuthorUni.getString("guid"));
					author_ids.add(rsAuthorUni.getInt("idauthor_table_kf5"));
				}
				// find author fatherId by UUID
				for (int i = 0; i < author_guids.size() - 1; i++) {
					ResultSet rsAuthor = selectRecords("select father_id from "
							+ sectionName + ".author_table_kf5 "
							+ "where guid='" + author_guids.get(i) + "'");
					List<String> author_fids = new ArrayList<String>();
					while (rsAuthor.next()) {
						author_fids.add(rsAuthor.getString("father_id"));
					}

					// get note guid by author father id
					Map<String, String> note_guid = new HashMap<String, String>();
					for (String author_fid : author_fids) {
						ResultSet rsNote_KF5 = selectRecords("select guid from "
								+ sectionName
								+ ".note_kf5 "
								+ "where uuid='"
								+ author_fid + "'");
						while (rsNote_KF5.next()) {
							note_guid.put(rsNote_KF5.getString(1),
									rsNote_KF5.getString(1));
						}
					}

					// get note id from note_kf_uni by guid
					Iterator noteIterator = note_guid.keySet().iterator();
					List<Integer> note_ids = new ArrayList<Integer>();
					while (noteIterator.hasNext()) {
						ResultSet rsNote_KF5_Uni = selectRecords("select idnote_KF5 from "
								+ sectionName
								+ ".note_kf5uni "
								+ "where guid='"
								+ (String) noteIterator.next()
								+ "'");
						if (rsNote_KF5_Uni.next()) {
							note_ids.add(rsNote_KF5_Uni.getInt("idnote_KF5"));
						}
					}

					for (int note_id : note_ids) {
						insertAuthorNote(author_ids.get(i), note_id);

					}
				}
				// end of generate author and note relationship

				// generate view and note relationship
				// get all note guid from note_uni

				List<String> noteGuids = new ArrayList<String>();
				List<Integer> noteIds = new ArrayList<Integer>();

				// get note guids
				ResultSet rsNoteKF5Uni = selectRecords("select guid,idnote_KF5 from "
						+ sectionName + ".note_kf5uni ");
				while (rsNoteKF5Uni.next()) {
					noteGuids.add(rsNoteKF5Uni.getString(1));
					noteIds.add(rsNoteKF5Uni.getInt(2));
				}

				for (int i = 0; i < noteGuids.size() - 1; i++) {
					// get note father id from note_kf5 by note_uni.guid =
					// note_kf5.guid
					ResultSet rsNoteKF5 = selectRecords("select father_id from "
							+ sectionName
							+ ".note_kf5 where guid='"
							+ noteGuids.get(i) + "'");
					List<String> noteFids = new ArrayList<String>();
					while (rsNoteKF5.next()) {
						noteFids.add(rsNoteKF5.getString(1));
					}

					// get view id from view_kf5 by view_kf5.uuid = note father
					// id
					List<String> noteFids2 = new ArrayList<String>();
					for (String notefid : noteFids) {
						ResultSet rsViewKF5 = selectRecords("select father_id from "
								+ sectionName
								+ ".viewpostrefs where uuid='"
								+ notefid + "'");
						while (rsViewKF5.next()) {
							noteFids2.add(rsViewKF5.getString(1));
						}
					}

					// get view id from view_kf5 by view_kf5.uuid = note father
					// id
					List<Integer> viewIds = new ArrayList<Integer>();
					for (String notefid : noteFids2) {
						ResultSet rsViewKF5 = selectRecords("select idview_kf5 from "
								+ sectionName
								+ ".view_kf5 where uuid='"
								+ notefid + "'");
						while (rsViewKF5.next()) {
							viewIds.add(rsViewKF5.getInt(1));
						}
					}

					for (int vid : viewIds) {
						insertNoteView(noteIds.get(i), vid);
					}

				}

				// generate build on

				// get Buildon information and put it into a list and
				// then find from guid's related note id and to guidls related
				// note id
				// put into note-note table
				//
				ArrayList<String> buildonFrom = new ArrayList<String>();
				ArrayList<String> buildonTo = new ArrayList<String>();
				ArrayList<String> type = new ArrayList<String>();
				ResultSet rsBuildOnKF5 = selectRecords("select * from "
						+ sectionName + ".buildon_kf5");
				while (rsBuildOnKF5.next()) {
					buildonFrom.add(rsBuildOnKF5.getString("fromid"));
					buildonTo.add(rsBuildOnKF5.getString("toid"));
					type.add(rsBuildOnKF5.getString("linktype"));
				}

				for (int i = 0; i < buildonFrom.size() - 1; i++) {
					// find note id from note_kf5uni by equal guid
					ResultSet rsNoteKf5 = selectRecords("select * from "
							+ sectionName + ".note_kf5uni where guid='"
							+ buildonFrom.get(i) + "'");
					print("select * from " + sectionName
							+ ".note_kf5uni where guid='" + buildonFrom.get(i)
							+ "'");
					int fromId = -1;
					int toId = -1;
					if (rsNoteKf5.next()) {
						fromId = rsNoteKf5.getInt("idnote_KF5");
					}

					ResultSet rsNoteKf52 = selectRecords("select * from "
							+ sectionName + ".note_kf5uni where guid='"
							+ buildonTo.get(i) + "'");
					print("select * from " + sectionName
							+ ".note_kf5uni where guid='" + buildonTo.get(i)
							+ "'");
					if (rsNoteKf52.next()) {
						toId = rsNoteKf52.getInt("idnote_KF5");
					}
					if (fromId != -1 && toId != -1) {
						insertNoteNote(fromId, toId, type.get(i));
					}

				}

				// End generate build on

				// generate attachement with note

				// get attachment's fatherid which is note_kf5 uuid ,
				// then we could get guid from note_kf5 and get note id in
				// note_kf5uni by compare guid

				// get attachment's fatherid

				ArrayList<String> attachmentsKf5Fids = new ArrayList<String>();

				ArrayList<Integer> attachmentsKf5ids = new ArrayList<Integer>();

				ResultSet rsAttachmentsKf5 = selectRecords("select * from "
						+ sectionName + ".attachments_kf5 ");

				while (rsAttachmentsKf5.next()) {
					attachmentsKf5Fids.add(rsAttachmentsKf5
							.getString("father_id"));
					attachmentsKf5ids.add(rsAttachmentsKf5.getInt("id"));
				}

				for (int i = 0; i < attachmentsKf5ids.size(); i++) {
					ResultSet rsNoteKf5 = selectRecords("select * from "
							+ sectionName + ".note_kf5 where uuid='"
							+ attachmentsKf5Fids.get(i) + "'");
					List<String> noteGuid = new ArrayList<String>();
					while (rsNoteKf5.next()) {
						noteGuid.add(rsNoteKf5.getString("guid"));
					}
					List<Integer> attNoteIds = new ArrayList<Integer>();
					for (String guid : noteGuid) {
						ResultSet rsNoteKf5uni = selectRecords("select * from "
								+ sectionName + ".note_kf5uni where guid='"
								+ guid + "'");
						while (rsNoteKf5uni.next()) {
							attNoteIds.add(rsNoteKf5uni.getInt("idnote_KF5"));
						}
					}
					// attachment_note
					for (int noteId : attNoteIds) {
						insertAttachmentNote(noteId, attachmentsKf5ids.get(i));
					}
				}
				convertAttachmetnTablefromAttachment_KF5();

			}

		}
		System.out.print("Integration Done");
	}

	public static void main(String arg[]) throws ClassNotFoundException,
			SQLException, ParseException {
		KF5Integration di = new KF5Integration("t4",
				"Ualbany");
		di.UpdateDatabases("t2");

	}

}