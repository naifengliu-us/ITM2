package org.albany.edu.pugin;

import itm.servlets.kf5Connect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.albany.edu.IntergrateFactory.FactoryConfiguration;
import org.albany.edu.IntergrateFactory.JsonFactory;
import org.albany.edu.IntergrateFactory.Interface.Imp.ReadDataByFileAddress;
import org.albany.edu.model.ModelBuilderByJson;
import org.apache.log4j.Logger;

/**
 * 
 * @author Yizhen Chen
 * @version 2015-8-18
 * 
 */
public class _ITMService {

	/**
	 * Check User Login
	 * <p>
	 * 
	 * @param FactoryConfiguration
	 *            config config contains project information for connect KF5
	 * 
	 * @param URL
	 *            URL to connect KF5
	 * 
	 * @param pythonFileLocation
	 *            KF5 use python to provide api, this arg indicate location if
	 *            python code
	 * 
	 * @return boolean
	 */
	final static Logger logger = Logger.getLogger(_ITMService.class);
	
	
	public boolean login(FactoryConfiguration config, String URL,
			String pythonFileLocation) throws IOException, InterruptedException {

		String output;
		output = login(URL, config.getUserName(), config.getPassword(),
				pythonFileLocation);
		if (output.equals("Error")) {
			return false;
		} else {
			return true;
		}

	}
	
	public String login(FactoryConfiguration config, String URL,
			String pythonFileLocation, String style) throws IOException, InterruptedException {

		String output;
		output = login(URL, config.getUserName(), config.getPassword(),
				pythonFileLocation);
		return output;

	}

	/**
	 * Check User Login
	 * <p>
	 * 
	 * @param userName
	 * @param passWord
	 * @param URL
	 *            URL to connect KF5
	 * 
	 * @param pythonFileLocation
	 *            KF5 use python to provide api, this arg indicate location if
	 *            python code
	 * 
	 * @return String
	 */

	public String login(String URL, String userName, String passWord,
			String pythonFileLocation) throws IOException, InterruptedException {
		StringBuffer cmdout = new StringBuffer();
		String cmd = "python " + pythonFileLocation + " \"" + URL + "\" \""
				+ userName + "\" \"" + passWord + "\"";
		logger.info("Login in Python CMD Line is " + cmd);
		Process proc = Runtime.getRuntime().exec(cmd);
		InputStream fis = proc.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		proc.waitFor();
		String line = null;
		while ((line = br.readLine()) != null) {
			cmdout.append(line).append(System.getProperty("line.separator"));
		}
		if (cmdout.toString().isEmpty() || cmdout.toString().trim().equals("Error")) {
			return "Error";
		} else {
			updateUserPermissionTypeInITM(cmdout.toString());
			return cmdout.toString();
		}

	}

	public void updateUserPermissionTypeInITM(String userInfo) {
		System.out.println("userInfo:" + userInfo);

	}

	/**
	 * Get data through kF5 python api then construct the javabeans base on genomic model.
	 * The data will store into Mysql .
	 * <p>
	 * 
	 * @param FactoryConfiguration
	 *            config config contains project information for connect KF5
	 * 
	 * @param URL
	 *            URL to connect KF5
	 * 
	 * @param pythonFileLocation
	 *            KF5 use python to provide api, this arg indicate location if
	 *            python code
	 * 
	 */
	public void getDataFromKf5Api(FactoryConfiguration config, String URL,
			String pythonFileLocation) throws InstantiationException,
			IllegalAccessException {

		long startTime = System.currentTimeMillis();

		String cmd = "python " + pythonFileLocation + " \"" + URL + "\" \""
				+ config.getUserName() + "\" \"" + config.getPassword()
				+ "\" \"" + startTime + "\"";
		System.out.println(startTime);
		logger.info(" getDataFromKf5Api Python CMD Line is " + cmd);
		// Run python api to retrieve data from kf5
		try {
			System.out.println(cmd);
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long endTime = System.currentTimeMillis();

		System.out.println(endTime);
		System.out.println((endTime - startTime) / (1000 * 60 * 60));

		ModelBuilderByJson model = new ModelBuilderByJson();
		// init factory
		JsonFactory factory = model.getFactory();

		factory.setConfig(config);
		factory.initFactoryInDB();
		// Retrieve note
		ReadDataByFileAddress reader = new ReadDataByFileAddress();
//			URL[] urls = new URL[] { new URL("file:/"
//					+ System.getProperty("user.dir") + "/src") };
//			System.out.print("file:/"+ System.getProperty("user.dir") + "/src");
//			URL[] urls = new URL[] { new URL("file:/D:\\workspace\\itmdev20150728\\itmdev20150728\\src") };
//			URLClassLoader ul = new URLClassLoader(urls);
	}

	public  static void main(String arg[]) throws IOException, InterruptedException, InstantiationException, IllegalAccessException{
		FactoryConfiguration config = new FactoryConfiguration();
		config.setEmail("jonkiky@gmail.com");
		config.setFirstName("yizhen");
		config.setLastName("chen");
		config.setOrg("SUNYAlbany");
		config.setPassword("jianweizh");
		config.setProjectName("classModelTest");
		config.setUserName("Jianwei Zhang");
		String URL="https://kf.utoronto.ca:443/kforum/";
		config.setDb("GES 2015-2016");
		_ITMService service = new _ITMService();
		boolean flag=service.login(config,URL,"//network//rit//lab//zhanglab//kf5py-master//login.py");
		if(flag){
			System.out.print("Login Success");
			service.getDataFromKf5Api(config,URL,"//network//rit//lab//zhanglab//kf5py-master//kf5Api.py");
		}else{
			System.out.print("Login failed");
		}
		
		
	}
}
