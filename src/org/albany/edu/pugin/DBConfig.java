package org.albany.edu.pugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DBConfig {
	public static final String DB_URL = "jdbc:mysql://localhost:3306/";
	public static final String DB_USER = "root";
	public static final String DB_PW = "051633b$";

	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	public Connection conn = null;
	public Statement stmt = null;
	String db_ITM = "t1";
	String db_kf5 = "temp_knowledge_society_network";

	protected String sql;

	public DBConfig(String db_ITM, String db_kf5) {
		try {
			this.db_ITM = db_ITM;
			this.db_kf5 = db_kf5;
			this.conn = this.getCon();
			this.stmt = this.getStatement();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Connection getCon() throws ClassNotFoundException, SQLException {
		// load driver
		Class.forName(DB_DRIVER);
		// get connection
		this.conn = DriverManager
				.getConnection(DB_URL + db_ITM, DB_USER, DB_PW);
		return conn;
	}

	public Statement getStatement() throws ClassNotFoundException, SQLException {
		this.conn = getCon();
		return conn.createStatement();
	}

	public ResultSet selectRecords(String sql) throws ClassNotFoundException,
			SQLException {
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}

	public int inSertRecords(String sql) throws ClassNotFoundException,
			SQLException {
		int rs = stmt.executeUpdate(sql);
		return rs;
	}

	public void CleanUp() {
		if (stmt != null) {
			try {
				
				
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void createbuildtoid() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`buildon_toid` (`idbuildon_toid` INT(11) NOT NULL AUTO_INCREMENT,`toid` INT(11) NULL,PRIMARY KEY (`idbuildon_toid`));";
		stmt.execute(sql);
	}

	public void createAuthors() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`author_table_kf5` (`idauthor_table_kf5` INT(11) NOT NULL AUTO_INCREMENT,`firstname` VARCHAR(45) NULL,`lastname` VARCHAR(45) NULL,`username` VARCHAR(45) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800)NULL,`guid` VARCHAR(800)NULL,PRIMARY KEY (`idauthor_table_kf5`))";
		stmt.execute(sql);
	}

	public void createAuthorsuni() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`author_table_kf5uni` (`idauthor_table_kf5` INT(11) NOT NULL AUTO_INCREMENT,`firstname` VARCHAR(45) NULL,`lastname` VARCHAR(45) NULL,`username` VARCHAR(45) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800)NULL,`guid` VARCHAR(800)NULL,PRIMARY KEY (`idauthor_table_kf5`))";
		stmt.execute(sql);
	}

	public void createnoteKF5() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`note_kf5` (`idnote_KF5` INT(11) NOT NULL AUTO_INCREMENT,`notetitle` VARCHAR(200) NULL,`notecontent` MEDIUMTEXT NULL,`createtime` VARCHAR(800) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL, PRIMARY KEY (`idnote_KF5`));";

		stmt.execute(sql);
	}

	public void createviewkf5() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`view_kf5` (`idview_kf5` INT(11) NOT NULL AUTO_INCREMENT,`title` VARCHAR(450) NULL,`uuid` VARCHAR(800) NULL,`section_id` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL,PRIMARY KEY (`idview_kf5`));";

		stmt.execute(sql);
	}

	public void createnoteKF5uni() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`note_kf5uni` (`idnote_KF5` INT(11) NOT NULL AUTO_INCREMENT,`notetitle` VARCHAR(200) NULL,`notecontent` MEDIUMTEXT NULL,`createtime` VARCHAR(800) NULL,`father_id` VARCHAR(800) NULL,`uuid` VARCHAR(800) NULL,`guid` VARCHAR(800) NULL,`target` INT(11)  NULL, PRIMARY KEY (`idnote_KF5`));";

		stmt.execute(sql);
	}

	public void createconnoteview() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`note_view_con` (`noteid` INT(11) NULL ,`viewid` INT(11) NULL);";

		stmt.execute(sql);
	}

	public void createbuildonKF5() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`buildon_kf5` (`idbuildon_kf5` INT(11) NOT NULL AUTO_INCREMENT,`fromid` VARCHAR(800) NULL,`toid` VARCHAR(800) NULL,`linktype` VARCHAR(800) NULL,PRIMARY KEY (`idbuildon_kf5`));";

		stmt.execute(sql);
	}

	public void createconnote() throws SQLException {
		sql = "CREATE TABLE `"
				+ db_ITM
				+ "`.`note_note_kf5` (`idnote_note_kf5` INT(11) NOT NULL AUTO_INCREMENT,`fromid` INT(11) NULL,`toid` INT(11) NULL,PRIMARY KEY (`idnote_note_kf5`));";

		stmt.execute(sql);
	}

	public void insertAuthors() throws SQLException {
		sql = "insert into `"
				+ db_ITM
				+ "`.`author_table_kf5`(idauthor_table_kf5,firstname,lastname,username,father_id,uuid,guid) select id,firstName,lastName,userName,father_id,uuid,guid from `kf5`.`authors`;";

		stmt.execute(sql);
	}

	public void insertnote() throws SQLException {
		sql = "insert into `"
				+ db_ITM
				+ "`.`note_kf5`(notetitle,notecontent,father_id,uuid,createtime,guid) select title,body,father_id,uuid,created,guid from `kf5`.`postinfo`;";

		stmt.execute(sql);
	}

	public void insertauthoruni() throws SQLException {
		sql = "insert into author_table_kf5uni(firstname,lastname,username,father_id,uuid,guid)select firstname,lastname,username,father_id,uuid,guid from author_table_kf5 group by guid order by idauthor_table_kf5";

		stmt.execute(sql);
	}

	// ////
	public void insertnoteuni() throws SQLException {
		sql = "insert into `"
				+ db_ITM
				+ "`.`note_kf5uni`(notetitle,notecontent,createtime,father_id,uuid,guid)select notetitle,notecontent,createtime,father_id,uuid,guid from `"
				+ db_ITM + "`.`note_kf5` group by guid order by idnote_KF5";

		stmt.execute(sql);
	}

	public String[] insertview(String tableName) throws SQLException {

		sql = "insert into `"
				+ db_ITM
				+ "`.`view_kf5`(title,uuid,section_id,guid) select title,uuid,sectionGuid,guid from `kf5`.`"
				+ tableName + "`;";

		stmt.execute(sql);

		return null;
	}

	public String[] insertbuildon(String tableName) throws SQLException {

		sql = "insert into `"
				+ db_ITM
				+ "`.`buildon_kf5`(fromid,toid,linktype) select `from` ,`to`,`type` from `kf5`.`"
				+ tableName + "`;";

		stmt.execute(sql);

		return null;
	}

	public String[] insertconnote(String column, String tableName, int id)
			throws SQLException {

		sql = "insert into `" + db_ITM + "`.`" + tableName + "`(" + column
				+ ") select idnote_KF5 from note_kf5 where guid in (select "
				+ column + " from buildon_kf5 where idbuildon_kf5=" + id + ")";

		stmt.execute(sql);

		return null;
	}

	public String[] updateconnote(int id) throws SQLException {

		sql = "update `" + db_ITM + "`.`note_note_kf5`set toid=" + id
				+ " where idnote_note_kf5==" + id + "";

		stmt.execute(sql);

		return null;
	}

	public int selectconattnoteid(int id) throws SQLException {

		int idnote_kf5 = 0;

		sql = "SELECT idnote_KF5 from note_kf5 where uuid in (select father_id from `attachments_kf5` where id=1)";

		stmt.execute(sql);

		return idnote_kf5;
	}

	public String[] insertconnoteview(int id_note, int id_view)
			throws SQLException {

		sql = "insert into note_view_con(noteid,viewid) values (" + id_note
				+ "," + id_view + ")";

		stmt.execute(sql);

		return null;
	}

	public ArrayList<String> selectfatherid(int id) throws SQLException,
			ClassNotFoundException {

		ResultSet rs = this.selectRecords("SELECT * FROM note_kf5uni;");
		ResultSet rs1 = this
				.selectRecords("select father_id from note_KF5 where guid in (select guid from note_KF5uni where idnote_KF5="
						+ id + ");");

		ArrayList<String> father_id = new ArrayList<String>();

		while (rs1.next()) {
			father_id.add(rs1.getString("father_id"));
			// System.out.print(rs1.getString("father_id"));
			// System.out.print("\n");
		}

		/*
		 * if (father_id.size()>1){ System.out.print(father_id.get(0));
		 * System.out.print("\n");}
		 */

		// Statement st = conn.createStatement();

		// sql =
		// "select father_id from note_KF5 where guid in (select guid from note_KF5uni where idnote_KF5="+id+"))";

		// st.execute(sql);

		return father_id;
	}

	public ArrayList<String> selectauthorfatherid(int id) throws SQLException,
			ClassNotFoundException {

		ResultSet rs = this.selectRecords("SELECT * FROM author_table_kf5uni;");
		ResultSet rs1 = this
				.selectRecords("select father_id from author_table_kf5 where guid in (select guid from author_table_kf5uni where idauthor_table_kf5="
						+ id + ");");

		ArrayList<String> auth_fid = new ArrayList<String>();

		while (rs1.next()) {
			auth_fid.add(rs1.getString("father_id"));
			// System.out.print(rs1.getString("father_id"));
			// System.out.print("\n");
		}

		/*
		 * if (father_id.size()>1){ System.out.print(father_id.get(0));
		 * System.out.print("\n");}
		 */

		// Statement st = conn.createStatement();

		// sql =
		// "select father_id from note_KF5 where guid in (select guid from note_KF5uni where idnote_KF5="+id+"))";

		// st.execute(sql);

		return auth_fid;
	}

	public int selectviewid(String father_id) throws SQLException,
			ClassNotFoundException {
		Statement st = conn.createStatement();
		sql = "select idview_kf5 from view_kf5 where guid in(select viewGuid from `viewpostrefs` where uuid='"
				+ father_id + "')";

		// ResultSet rs
		// =db1.selectRecords("SELECT * from `"+db_kf5+"`.`viewpostrefs`");
		ResultSet viewid = this
				.selectRecords("select * from view_kf5 where guid in(select viewGuid from `viewpostrefs` where uuid='"
						+ father_id + "')");
		int viewid_kf5 = 0;
		viewid.next();
		viewid_kf5 = viewid.getInt("idview_kf5");

		// System.out.print(viewid_kf5);
		// System.out.print("\n");

		// st.execute(sql);

		return viewid_kf5;
	}

	public int selectnoteid(String father_id) throws SQLException,
			ClassNotFoundException {

		// ResultSet rs
		// =db1.selectRecords("SELECT * from `"+db_kf5+"`.`viewpostrefs`");
		ResultSet noteid = this
				.selectRecords("select * from note_kf5uni where uuid='"
						+ father_id + "'");
		int noteid_kf5 = 0;
		noteid.next();
		noteid_kf5 = noteid.getInt("idnote_KF5");

		// System.out.print(viewid_kf5);
		// System.out.print("\n");

		// st.execute(sql);

		return noteid_kf5;
	}

	public int selectguid(int id) throws SQLException {
		Statement st = conn.createStatement();
		sql = "select guid from author_table_kf5 where idauthor_table_kf5="
				+ id + "";
		st.execute(sql);
		return id;
	}

	// table is author_table_kf5uni
	public void updateAuthor_KF5(String tableName)
			throws ClassNotFoundException, SQLException, ParseException {

		ResultSet rs1 = selectRecords("select * from " + db_kf5 + "."
				+ tableName + "");
		List<Integer> ids = new ArrayList<Integer>();
		while (rs1.next()) {

			ids.add(rs1.getInt("idauthor_table_kf5"));
		}

		for (int id : ids) {
			sql = "SELECT * FROM `" + db_ITM + "`.`" + tableName
					+ "` where guid in" + "(select guid from " + db_kf5 + "."
					+ tableName + " where idauthor_table_kf5=" + id + ");";
			ResultSet rs2 = selectRecords(sql);
			if (rs2.next()) {
				ResultSet rs = selectRecords("select * from " + db_kf5 + "."
						+ tableName + " where idauthor_table_kf5=" + id + "");
				rs.next();
				String sql1 = "update `" + db_ITM + "`.`" + tableName
						+ "` set " + "firstname='" + rs.getString("firstname")
						+ "' ," + "lastname='" + rs.getString("lastname")
						+ "', " + "father_id='" + rs.getString("father_id")
						+ "'," + "uuid='" + rs.getString("uuid") + "'  "
						+ "where idauthor_table_kf5='" + id + "';";

				System.out.print(sql1);
				stmt.execute(sql1);
			} else {

				String sql2 = "insert into `"
						+ db_ITM
						+ "`.`"
						+ tableName
						+ "`(firstname,lastname,username,father_id,uuid,guid)"
						+ "select firstname,lastname,username,father_id,uuid,guid "
						+ "from `" + db_kf5 + "`.`" + tableName + "`"
						+ "where  idauthor_table_kf5=" + id + ";";

				stmt.execute(sql2);

			}

		}
	}

	public void updateBuildOn() throws SQLException{
		String	sql ="TRUNCATE TABLE " + db_ITM + ".buildon_kf5";
		System.out.print("updateBuildOn SQL:"+sql);
		stmt.execute(sql);

		sql ="insert into `"
					+ db_ITM
					+ "`.`buildon_kf5`(`fromid`,`toid`,`linktype`) select `fromid`,`toid`,`linktype` from "
					+ db_kf5 + ".buildon_kf5";
			stmt.execute(sql);

	}
	public void updateViewpostRefs_KF5() throws ClassNotFoundException,
			SQLException, ParseException {

		String sql1 = "DROP table IF EXISTS " + db_ITM + ".viewpostrefs";

		stmt.execute(sql1);

		String sql2 = "CREATE TABLE `" + db_ITM + "`.`viewpostrefs` ("
				+ " `id` INT(11) NOT NULL," + "`father_id` VARCHAR(800) NULL,"
				+ "`uuid` VARCHAR(800) NULL," + "`copy` VARCHAR(5) NULL,"
				+ "`viewGuid` VARCHAR(800) NULL,"
				+ "`copierId` VARCHAR(800) NULL," + "`height` INT(16) NULL,"
				+ "`width` INT(16) NULL," + " `rotation` DOUBLE NULL,"
				+ "`guid` VARCHAR(800) NULL," + "`display` INT(16) NULL,"
				+ " PRIMARY KEY (`id`));";
		stmt.execute(sql2);

		String sql3 = "insert into `"
				+ db_ITM
				+ "`.`viewpostrefs`"
				+ "(id,father_id,uuid,copy,viewGuid,copierId,height,width,rotation,guid,display)"
				+ " select id,father_id,uuid,copy,viewGuid,copierId,height,width,rotation,guid,display "
				+ "from `" + db_kf5 + "`.`viewpostrefs`";
		stmt.execute(sql3);
	}

	public void updateView_KF5() throws ClassNotFoundException, SQLException,
			ParseException {

		ResultSet rs1 = selectRecords("select * from " + db_kf5 + ".view_kf5");
		List<Integer> ids = new ArrayList<Integer>();
		for (int id = 1; rs1.next(); id++) {
			ids.add(id);
		}
		for (int id : ids) {
			sql = "SELECT * FROM `" + db_ITM + "`.`view_kf5` where guid in"
					+ "(select guid from " + db_kf5
					+ ".view_kf5 where idview_kf5=" + id + ");";
			ResultSet rs2 = selectRecords(sql);

			if (rs2.next()) {

				ResultSet rs = selectRecords("select * from " + db_kf5
						+ ".view_kf5 where idview_kf5=" + id + "");

				rs.next();
				String sql1 = "update `" + db_ITM + "`.`view_kf5`" + " set "
						+ "title=\"" + rs.getString("title").replace("\"", "'")
						+ "\" ," + "uuid='" + rs.getString("uuid") + "', "
						+ "section_id='" + rs.getString("section_id") + "' "
						+ "where guid='" + rs.getString("guid") + "';";

				stmt.execute(sql1);

			} else {

				String sql2 = "insert into `" + db_ITM
						+ "`.`view_kf5`(title,uuid,section_id,guid)"
						+ "select title,uuid,section_id,guid " + "from `"
						+ db_kf5 + "`.`view_kf5`" + "where  idview_kf5=" + id
						+ ";";

				stmt.execute(sql2);

			}

		}
	}

	// public void updateBuildon()throws ClassNotFoundException, SQLException,
	// ParseException {
	// ResultSet rs1 =selectRecords("select * from "+db_kf5+".buildon_kf5");
	// List<Integer> ids = new ArrayList<Integer>();
	// for (int id=1;rs1.next();id++){
	// ids.add(id);
	// }
	// for(int id : ids){
	// sql="SELECT * FROM `"+db_ITM+"`.`buildon_kf5` where guid in"
	// + "(select guid from "+db_kf5+".`buildon_kf5` where idnote_KF5="+id+");";
	// ResultSet rs2 =selectRecords(sql);
	//
	//
	// if (rs2.next()){
	// ResultSet rs
	// =selectRecords("select * from "+db_kf5+".buildon_kf5 where idnote_KF5="+id+"");
	// rs.next();
	// String sql1 ="update `"+db_ITM+"`.`"+tableName+"` set "
	// // + "notetitle=\""+rs.getString("notetitle")+"\","
	// // + "notecontent=\""+rs.getString("notecontent")+"\", "
	// + "createtime='"+rs.getString("createtime")+"',"
	// + "father_id='"+rs.getString("father_id")+"', "
	// + "uuid='"+rs.getString("uuid")+"', "
	// + "guid='"+rs.getString("guid")+"', "
	// + "target='"+rs.getInt("idnote_KF5")+"' "
	// + "where idnote_KF5="+id+";";
	//
	// stmt.execute(sql1);
	//
	//
	// }else{
	//
	// String sql2
	// ="insert into `"+db_ITM+"`.`"+tableName+"`(notetitle,notecontent,createtime,father_id,uuid,guid,target)"
	// +
	// "select notetitle,notecontent,createtime,father_id,uuid,guid,idnote_KF5 "
	// + "from `"+db_kf5+"`.`"+tableName+"`"
	// + "where  idnote_KF5="+id+";";
	//
	// stmt.execute(sql2);
	//
	// }
	//
	// }
	//
	// }

	public void updateNote1_KF5(String tableName)
			throws ClassNotFoundException, SQLException, ParseException {
		ResultSet rs1 = selectRecords("select * from " + db_kf5 + "."
				+ tableName + "");
		List<Integer> ids = new ArrayList<Integer>();
		for (int id = 1; rs1.next(); id++) {
			ids.add(id);
		}
		for (int id : ids) {
			sql = "SELECT * FROM `" + db_ITM + "`.`" + tableName
					+ "` where guid in" + "(select guid from " + db_kf5 + "."
					+ tableName + " where idnote_KF5=" + id + ");";
			ResultSet rs2 = selectRecords(sql);

			if (rs2.next()) {
				ResultSet rs = selectRecords("select * from " + db_kf5 + "."
						+ tableName + " where idnote_KF5=" + id + "");
				rs.next();
				String sql1 = "update `"
						+ db_ITM
						+ "`.`"
						+ tableName
						+ "` set "
						// + "notetitle=\""+rs.getString("notetitle")+"\","
						// + "notecontent=\""+rs.getString("notecontent")+"\", "
						+ "createtime='" + rs.getString("createtime") + "',"
						+ "father_id='" + rs.getString("father_id") + "', "
						+ "uuid='" + rs.getString("uuid") + "'  "
						+ "where guid='" + rs.getString("guid") + "'; ";

				stmt.execute(sql1);

			} else {

				String sql2 = "insert into `"
						+ db_ITM
						+ "`.`"
						+ tableName
						+ "`(notetitle,notecontent,createtime,father_id,uuid,guid)"
						+ "select notetitle,notecontent,createtime,father_id,uuid,guid "
						+ "from `" + db_kf5 + "`.`" + tableName + "`"
						+ "where  idnote_KF5=" + id + ";";

				stmt.execute(sql2);

			}

		}
	}

	public void updateAttachment_KF5() throws ClassNotFoundException,
			SQLException, ParseException {

		ResultSet rs1 = selectRecords("select * from " + db_kf5
				+ ".attachments_kf5");

		List<Integer> ids = new ArrayList<Integer>();
		for (int id = 1; rs1.next(); id++) {
			ids.add(id);
		}
		for (int id : ids) {

			sql = "SELECT * FROM `" + db_ITM
					+ "`.`attachments_kf5` where guid in"
					+ "(select guid from " + db_kf5
					+ ".attachments_kf5 where id=" + id + ");";
			ResultSet rs2 = selectRecords(sql);

			if (rs2.next()) {
				ResultSet rs = selectRecords("select * from " + db_kf5
						+ ".attachments_kf5 where id=" + id + "");
				rs.next();
				String sql1 = "update `"
						+ db_ITM
						+ "`.`attachments_kf5` set "
						// + "firstname='"+rs.getString("firstname")+"' ,"
						// + "lastname='"+rs.getString("lastname")+"', "
						+ "father_id='" + rs.getString("father_id") + "',"
						+ "`uuid`='" + rs.getString("uuid") + "', "
						+ "`created`='" + rs.getString("created") + "' ,"
						+ "title=\"" + rs.getString("title") + "\", "
						+ "`mime`='" + rs.getString("mime") + "' ,"
						+ "`fileSize`='" + rs.getString("fileSize") + "', "
						+ "`file`=\"" + rs.getString("file") + "\", "
						+ "guid='" + rs.getString("guid") + "', "
						+ "`target`='" + rs.getInt("id") + "' " + "where id="
						+ id + ";";

				try {
					System.out.println(sql1);
					stmt.execute(sql1);
				} catch (Exception e) {
					System.out.print("exception" + e);
				}

			} else {

				String sql2 = "insert into `" 
						+ db_ITM
						+ "`.`attachments_kf5`(father_id,uuid,created,title,mime,fileSize,file,guid)"
						+ "select father_id,uuid,created,title,mime,fileSize,file,guid "
						+ "from `" + db_kf5 + "`.`attachments_kf5`"
						+ "where  id=" + id + ";";

				try {
					System.out.println(sql2);
					stmt.execute(sql2);
				} catch (Exception e) {
					System.out.print("exception" + e);
				}

			}

		}
	}

	public ArrayList<String> selectUselessDataId(String tableName)
			throws SQLException, ClassNotFoundException {

		String sql = "select * from tableName where target =null";
		return null;

	}

	public void deleteUselessTableData(String tableName) throws SQLException {

		sql = "delete from " + tableName + " where target=null";
		stmt.execute(sql);
	}

	public void convertViewKF5toViewTable1() throws SQLException, ClassNotFoundException {
		sql ="select * from `" + db_ITM+ "`.`view_table`";
		ResultSet rs = selectRecords(sql);
		List<Integer> ids = new ArrayList<Integer>();
		while(rs.next()){
			ids.add(rs.getInt("idview"));
		}
		sql="select * from `" + db_ITM+ "`.`view_kf5`";
		ResultSet rs2 = selectRecords(sql);
		List<String> insertSql = new ArrayList<String>();
		while(rs2.next()){
			if(ids.contains(rs2.getInt("idview_kf5"))){
				//update
				String sql1 ="UPDATE `"+db_ITM+"`.`view_table`"
						+"  SET"
						+" `title` = \""+rs2.getString("title")+"\" "
						+"WHERE `idview` = "+rs2.getInt("idview_kf5")+";";
				 insertSql.add(sql1);
				
			}else{
				// insert
				String sql1 ="INSERT INTO `"+db_ITM+"`.`view_table` (`idview`,`title`)VALUES("
						+rs2.getInt("idview_kf5")+",\""+rs2.getString("title")+"\");";
						
				 insertSql.add(sql1);
				
			}
		}
		
		for (String sql3 : insertSql) {
			System.out.println(sql3);
			stmt.execute(sql3);
		}
		
	}

	public void convertAttachmetnTablefromAttachment_KF5()
			throws ClassNotFoundException, SQLException, ParseException {

		sql = "select *  from `" + db_ITM + "`.`attachments_kf5`";

		ResultSet rs = selectRecords(sql);
		List<String> insertSql = new ArrayList<String>();
		SimpleDateFormat fromUser = new SimpleDateFormat(
				"MMM dd, yyyy HH:mm:ss a", Locale.ENGLISH);
		SimpleDateFormat spdtformat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		while (rs.next()) {

			String sql2 = "insert into `"
					+ db_ITM
					+ "`.`attachment_table`(attachmentid,attachmenttitle,attachmentfile,createtime) values("
					+ "'"
					+ rs.getInt("id")
					+ "',"
					+ "\""
					+ rs.getString("title").replace("\"", "'")
					+ "\","
					+ "\""
					+ rs.getString("file").replace("\"", "'")
					+ "\","
					+ "'"
					+ spdtformat
							.format(fromUser.parse(rs.getString("created")))
					+ "'" + ")";
			insertSql.add(sql2);
		}
		for (String sql3 : insertSql) {
			// print(sql3);
			stmt.execute(sql3);
		}
	}

	public void insertIntoConNoteAuthor(int id_author, int id_note)
			throws SQLException {
		sql = "insert into `" + db_ITM
				+ "`.author_note(authorid,noteid) values (" + id_author + ","
				+ id_note + ")";
		stmt.execute(sql);
	}

	public void generateAuthorUni() throws SQLException, ClassNotFoundException {

		sql = "select * from `" + db_ITM + "`.`author_table`";

		ResultSet rs2 = selectRecords(sql);
		List<Integer> ids = new ArrayList<Integer>();
		while (rs2.next()) {
			ids.add(rs2.getInt("authorid"));
		}

		sql = "select * from `" + db_ITM + "`.author_table_kf5uni";
		ResultSet rs = selectRecords(sql);
		List<String> insertSql = new ArrayList<String>();
		while (rs.next()) {
			if (ids.contains(rs.getInt("idauthor_table_kf5"))) {
				String sql2 = "UPDATE `" + db_ITM + "`.`author_table`"
						+ " SET " + "`firstname` = '"
						+ rs.getString("firstname") + "'," + "`lastname` = '"
						+ rs.getString("lastname") + "'," + "`username` = '"
						+ rs.getString("username") + "'"
						+ " WHERE `authorid` ="
						+ rs.getInt("idauthor_table_kf5");
				insertSql.add(sql2);

			} else {
				String sql2 = "insert into `"
						+ db_ITM
						+ "`.author_table(firstname,lastname,username,authorid)VALUES"
						+ "('" + rs.getString("firstname") + "','"
						+ rs.getString("lastname") + "','"
						+ rs.getString("username") + "',"
						+ rs.getInt("idauthor_table_kf5") + ")";
				insertSql.add(sql2);
			}

		}

		for (String sql3 : insertSql) {
			System.out.println(sql3);
			stmt.execute(sql3);
		}

	}

	public void convertViewKF5toViewTable() throws SQLException {
		sql = "insert into `" + db_ITM
				+ "`.`view_table`(title,idview) select title,idview_kf5 from `"
				+ db_kf5 + "`.`view_kf5`;";

		stmt.execute(sql);
	}

	public void generateNoteUni() throws SQLException, ParseException,
			ClassNotFoundException {
		// sql="TRUNCATE TABLE " + db_ITM+ ".note_kf5uni";
		// stmt.execute(sql);
		//
		// sql = "insert into `"
		// + db_ITM
		// +
		// "`.`note_kf5uni`(notetitle,notecontent,createtime,father_id,uuid,guid)"
		// +
		// "select notetitle,notecontent,createtime,father_id,uuid,guid from `"
		// + db_ITM + "`.`note_kf5` group by guid order by idnote_KF5";
		// stmt.execute(sql);

		sql = "select noteid from `" + db_ITM + "`.`note_table`";

		ResultSet rs2 = selectRecords(sql);
		List<Integer> ids = new ArrayList<Integer>();

		while (rs2.next()) {
			ids.add(rs2.getInt("noteid"));
		}
		sql = "select notetitle,notecontent,createtime,idnote_KF5 from `"
				+ db_ITM + "`.`note_kf5uni`";

		ResultSet rs = selectRecords(sql);
		List<String> insertSql = new ArrayList<String>();
		SimpleDateFormat fromUser = new SimpleDateFormat(
				"MMM dd, yyyy HH:mm:ss a", Locale.ENGLISH);
		SimpleDateFormat spdtformat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");

		while (rs.next()) {
			if (ids.contains(rs.getInt("idnote_KF5"))) {
				String sql2 = "update `"
						+ db_ITM
						+ "`.`note_table`"
						+ "SET"
						+ "`notetitle` = "
						+ "\""
						+ rs.getString("notetitle").replace("\"", "'")
						+ "\","
						+ "`notecontent` ="
						+ "\""
						+ rs.getString("notecontent").replace("\"", "'")
						+ "\","
						+ "`createtime` ="
						+ "'"
						+ spdtformat.format(fromUser.parse(rs
								.getString("createtime"))) + "'"
						+ "WHERE `noteid` =" + rs.getInt("idnote_KF5") + ";";
				insertSql.add(sql2);

			} else {

				String sql2 = "insert into `"
						+ db_ITM
						+ "`.`note_table`(notetitle,notecontent,createtime,noteid) values("
						+ "\""
						+ rs.getString("notetitle").replace("\"", "'")
						+ "\","
						+ "\""
						+ rs.getString("notecontent").replace("\"", "'")
						+ "\","
						+ "'"
						+ spdtformat.format(fromUser.parse(rs
								.getString("createtime"))) + "'," + "'"
						+ rs.getInt("idnote_KF5") + "'" + ")";
				insertSql.add(sql2);
			}

		}
		for (String sql3 : insertSql) {
			stmt.execute(sql3);
		}

	}

	public void insertAuthorNote(int id_author, int id_note)
			throws SQLException {
		sql = "insert into `" + db_ITM
				+ "`.author_note(authorid,noteid) values (" + id_author + ","
				+ id_note + ")";
		System.out.println("insertAuthorNote:  "+sql);
		stmt.execute(sql);

	}

	public void insertNoteView(int id_note, int id_view) throws SQLException {

		sql = "insert into `" + db_ITM + "`.view_note(noteid,viewid) values ("
				+ id_note + "," + id_view + ")";
		stmt.execute(sql);

	}

	public void insertNoteNote(int from, int to, String type)
			throws SQLException {
		sql = "insert into `" + db_ITM
				+ "`.note_note(fromnoteid,tonoteid,linktype) values (" + from
				+ "," + to + ",'" + type + "')";
		stmt.execute(sql);

	}

	public void insertAttachmentNote(int noteId, int attId) throws SQLException {
		sql = "insert into `" + db_ITM
				+ "`.attachment_note(noteid,attachmentid) values (" + noteId
				+ "," + attId + ")";
		stmt.execute(sql);

	}

	private void convertNoteKF5toTargetNoteKF() throws SQLException {
		sql = "DROP TABLE IF EXISTS `" + db_ITM + "`.`note_kf5`";
		stmt.execute(sql);
		sql = "CREATE TABLE  `" + db_ITM + "`.`note_kf5` ("
				+ " `idnote_KF5` int(11) NOT NULL AUTO_INCREMENT,"
				+ " `notetitle` varchar(200) DEFAULT NULL,"
				+ "  `notecontent` mediumtext,"
				+ "  `createtime` varchar(800) DEFAULT NULL,"
				+ "  `father_id` varchar(800) DEFAULT NULL,"
				+ "  `uuid` varchar(800) DEFAULT NULL,"
				+ "  `guid` varchar(800) DEFAULT NULL,"
				+ "  PRIMARY KEY (`idnote_KF5`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		stmt.execute(sql);
		sql = "insert into `"
				+ db_ITM
				+ "`.`note_kf5`(notetitle,notecontent,createtime,father_id,uuid,guid) "
				+ "select notetitle,notecontent,createtime,father_id,uuid,guid from `"
				+ db_kf5 + "`.`note_kf5`;";
		stmt.execute(sql);
	}

	public     List  removeDuplicate(List list)   { 
		   for  ( int  i  =   0 ; i  <  list.size()  -   1 ; i ++ )   { 
		    for  ( int  j  =  list.size()  -   1 ; j  >  i; j -- )   { 
		      if  (list.get(j).equals(list.get(i)))   { 
		        list.remove(j); 
		      } 
		    } 
		  } 
		   return list;
	} 
	
	public void update()
			throws ClassNotFoundException, SQLException, ParseException {
		

		// generate author and note relationship

		// get author_uni uuid
		List<String> author_guids = new ArrayList<String>();
		

		List<Integer> author_ids = new ArrayList<Integer>();
		ResultSet rsAuthorUni = selectRecords("select guid,idauthor_table_kf5 from "
				+ this.db_ITM + ".author_table_kf5uni");
		while (rsAuthorUni.next()) {
			author_guids.add(rsAuthorUni.getString("guid"));
			author_ids.add(rsAuthorUni.getInt("idauthor_table_kf5"));
		}
		// find author fatherId by UUID
		
		sql = "TRUNCATE TABLE " + db_ITM + ".author_note";
		stmt.execute(sql);
		
		for (int i = 0; i < author_guids.size() - 1; i++) {
			ResultSet rsAuthor = selectRecords("select DISTINCT father_id from "
					+ this.db_ITM + ".author_table_kf5 " + "where guid='"
					+ author_guids.get(i) + "'");
			List<String> author_fids = new ArrayList<String>();
			while (rsAuthor.next()) {
				author_fids.add(rsAuthor.getString("father_id"));
			}
		//	author_fids=removeDuplicate(author_fids);
			// get note guid by author father id
			Map<String, String> note_guid = new HashMap<String, String>();
			for (String author_fid : author_fids) {
				ResultSet rsNote_KF5 = selectRecords("select guid from "
						+ this.db_ITM + ".note_kf5 " + "where uuid='"
						+ author_fid + "'");
				while (rsNote_KF5.next()) {
					note_guid.put(rsNote_KF5.getString(1),
							rsNote_KF5.getString(1));
				}
			}
			// get note id from note_kf_uni by guid
			Iterator noteIterator = note_guid.keySet().iterator();
			List<Integer> note_ids = new ArrayList<Integer>();
			while (noteIterator.hasNext()) {
				ResultSet rsNote_KF5_Uni = selectRecords("select idnote_KF5 from "
						+ this.db_ITM
						+ ".note_kf5uni "
						+ "where guid='"
						+ (String) noteIterator.next() + "'");
				if (rsNote_KF5_Uni.next()) {
					note_ids.add(rsNote_KF5_Uni.getInt("idnote_KF5"));
				}
			}
			
			System.out.println("************Author_Note*************");
			for (int note_id : note_ids) {
				insertAuthorNote(author_ids.get(i), note_id);
			}
		}
		// end of generate author and note relationship

		System.out.println("CleanUp");

	}
	
	
	
	public void update(String dbName, String tempDb)
			throws ClassNotFoundException, SQLException, ParseException {
		// dbName =kf5_ges_kf5_ges_ges_2015_2016
		// tempDb=kf5_ges_kf5_ges_ges_2015_2016_temp
		// KF5Integration kf5 =new KF5Integration(dbName);
		// kf5.Integration()
		this.db_kf5 = tempDb;
		this.db_ITM = dbName;
		System.out.println("db_kf5 is  " + this.db_kf5);
		System.out.println("db_ITM is  " + this.db_ITM);

		this.updateBuildOn();
		
		this.updateViewpostRefs_KF5();
		System.out.println("updateViewpostRefs_KF5  ");

		this.updateAuthor_KF5("author_table_kf5uni");
		System.out.println("update author_table_kf5uni");
		//
		 this.updateAuthor_KF5("author_table_kf5");
		 System.out.println("update author_table_kf5");

		this.updateNote1_KF5("note_kf5uni");
		System.out.println("update note_kf5uni");

		this.updateAttachment_KF5();
		System.out.println("update Attachment_KF5");

		this.updateView_KF5();
		System.out.println("update View_KF5");

		this.generateNoteUni();
		this.generateAuthorUni();

		convertViewKF5toViewTable1();

		convertNoteKF5toTargetNoteKF();

		// generate author and note relationship

		// get author_uni uuid
		List<String> author_guids = new ArrayList<String>();
		

		List<Integer> author_ids = new ArrayList<Integer>();
		ResultSet rsAuthorUni = selectRecords("select guid,idauthor_table_kf5 from "
				+ this.db_ITM + ".author_table_kf5uni");
		while (rsAuthorUni.next()) {
			author_guids.add(rsAuthorUni.getString("guid"));
			author_ids.add(rsAuthorUni.getInt("idauthor_table_kf5"));
		}
		// find author fatherId by UUID
		
		sql = "TRUNCATE TABLE " + db_ITM + ".author_note";
		stmt.execute(sql);
		
		for (int i = 0; i < author_guids.size() - 1; i++) {
			ResultSet rsAuthor = selectRecords("select father_id from "
					+ this.db_ITM + ".author_table_kf5 " + "where guid='"
					+ author_guids.get(i) + "'");
			List<String> author_fids = new ArrayList<String>();
			while (rsAuthor.next()) {
				author_fids.add(rsAuthor.getString("father_id"));
			}

			// get note guid by author father id
			Map<String, String> note_guid = new HashMap<String, String>();
			for (String author_fid : author_fids) {
				ResultSet rsNote_KF5 = selectRecords("select guid from "
						+ this.db_ITM + ".note_kf5 " + "where uuid='"
						+ author_fid + "'");
				while (rsNote_KF5.next()) {
					note_guid.put(rsNote_KF5.getString(1),
							rsNote_KF5.getString(1));
				}
			}
			// get note id from note_kf_uni by guid
			Iterator noteIterator = note_guid.keySet().iterator();
			List<Integer> note_ids = new ArrayList<Integer>();
			while (noteIterator.hasNext()) {
				ResultSet rsNote_KF5_Uni = selectRecords("select idnote_KF5 from "
						+ this.db_ITM
						+ ".note_kf5uni "
						+ "where guid='"
						+ (String) noteIterator.next() + "'");
				if (rsNote_KF5_Uni.next()) {
					note_ids.add(rsNote_KF5_Uni.getInt("idnote_KF5"));
				}
			}
			
			System.out.println("************Author_Note*************");
			for (int note_id : note_ids) {
				insertAuthorNote(author_ids.get(i), note_id);
			}
		}
		// end of generate author and note relationship

		// generate view and note relationship
		// get all note guid from note_uni

		List<String> noteGuids = new ArrayList<String>();
		List<Integer> noteIds = new ArrayList<Integer>();

		// get note guids
		ResultSet rsNoteKF5Uni = selectRecords("select guid,idnote_KF5 from "
				+ this.db_ITM + ".note_kf5uni ");
		while (rsNoteKF5Uni.next()) {
			noteGuids.add(rsNoteKF5Uni.getString(1));
			noteIds.add(rsNoteKF5Uni.getInt(2));
		}
		sql = "TRUNCATE TABLE " + db_ITM + ".view_note";
		stmt.execute(sql);
		for (int i = 0; i < noteGuids.size() - 1; i++) {
			// get note father id from note_kf5 by note_uni.guid =
			// note_kf5.guid
			ResultSet rsNoteKF5 = selectRecords("select father_id from "
					+ this.db_ITM + ".note_kf5 where guid='" + noteGuids.get(i)
					+ "'");
			List<String> noteFids = new ArrayList<String>();
			while (rsNoteKF5.next()) {
				noteFids.add(rsNoteKF5.getString(1));
			}

			// get view id from view_kf5 by view_kf5.uuid = note father
			// id
			List<String> noteFids2 = new ArrayList<String>();
			for (String notefid : noteFids) {
				ResultSet rsViewKF5 = selectRecords("select father_id from "
						+ this.db_ITM + ".viewpostrefs where uuid='" + notefid
						+ "'");
				while (rsViewKF5.next()) {
					noteFids2.add(rsViewKF5.getString(1));
				}
			}

			// get view id from view_kf5 by view_kf5.uuid = note father
			// id
			List<Integer> viewIds = new ArrayList<Integer>();
			for (String notefid : noteFids2) {
				ResultSet rsViewKF5 = selectRecords("select idview_kf5 from "
						+ this.db_ITM + ".view_kf5 where uuid='" + notefid
						+ "'");
				while (rsViewKF5.next()) {
					viewIds.add(rsViewKF5.getInt(1));
				}
			}

			for (int vid : viewIds) {
				insertNoteView(noteIds.get(i), vid);
			}

		}

		// generate build on

		// get Buildon information and put it into a list and
		// then find from guid's related note id and to guidls related
		// note id
		// put into note-note table
		//
		ArrayList<String> buildonFrom = new ArrayList<String>();
		ArrayList<String> buildonTo = new ArrayList<String>();
		ArrayList<String> type = new ArrayList<String>();
		ResultSet rsBuildOnKF5 = selectRecords("select * from " + this.db_ITM
				+ ".buildon_kf5");
		while (rsBuildOnKF5.next()) {
			buildonFrom.add(rsBuildOnKF5.getString("fromid"));
			buildonTo.add(rsBuildOnKF5.getString("toid"));
			type.add(rsBuildOnKF5.getString("linktype"));
		}

		sql = "TRUNCATE TABLE " + db_ITM + ".note_note";
		stmt.execute(sql);

		for (int i = 0; i < buildonFrom.size() - 1; i++) {
			// find note id from note_kf5uni by equal guid
			ResultSet rsNoteKf5 = selectRecords("select * from " + this.db_ITM
					+ ".note_kf5uni where guid='" + buildonFrom.get(i) + "'");
			print("select * from " + this.db_ITM + ".note_kf5uni where guid='"
					+ buildonFrom.get(i) + "'");
			int fromId = -1;
			int toId = -1;
			if (rsNoteKf5.next()) {
				fromId = rsNoteKf5.getInt("idnote_KF5");
			}

			ResultSet rsNoteKf52 = selectRecords("select * from " + this.db_ITM
					+ ".note_kf5uni where guid='" + buildonTo.get(i) + "'");
			print("select * from " + this.db_ITM + ".note_kf5uni where guid='"
					+ buildonTo.get(i) + "'");
			if (rsNoteKf52.next()) {
				toId = rsNoteKf52.getInt("idnote_KF5");
			}
			if (fromId != -1 && toId != -1) {
				insertNoteNote(fromId, toId, type.get(i));
			}

		}

		// End generate build on

		// generate attachement with note

		// get attachment's fatherid which is note_kf5 uuid ,
		// then we could get guid from note_kf5 and get note id in
		// note_kf5uni by compare guid

		// get attachment's fatherid

		ArrayList<String> attachmentsKf5Fids = new ArrayList<String>();

		ArrayList<Integer> attachmentsKf5ids = new ArrayList<Integer>();

		ResultSet rsAttachmentsKf5 = selectRecords("select * from "
				+ this.db_ITM + ".attachments_kf5 ");

		while (rsAttachmentsKf5.next()) {
			attachmentsKf5Fids.add(rsAttachmentsKf5.getString("father_id"));
			attachmentsKf5ids.add(rsAttachmentsKf5.getInt("id"));
		}
		sql = "TRUNCATE TABLE " + db_ITM + ".attachment_note";
		stmt.execute(sql);

		for (int i = 0; i < attachmentsKf5ids.size(); i++) {
			ResultSet rsNoteKf5 = selectRecords("select * from " + this.db_ITM
					+ ".note_kf5 where uuid='" + attachmentsKf5Fids.get(i)
					+ "'");
			List<String> noteGuid = new ArrayList<String>();
			while (rsNoteKf5.next()) {
				noteGuid.add(rsNoteKf5.getString("guid"));
			}
			List<Integer> attNoteIds = new ArrayList<Integer>();
			for (String guid : noteGuid) {
				ResultSet rsNoteKf5uni = selectRecords("select * from "
						+ this.db_ITM + ".note_kf5uni where guid='" + guid
						+ "'");
				while (rsNoteKf5uni.next()) {
					attNoteIds.add(rsNoteKf5uni.getInt("idnote_KF5"));
				}
			}
			// attachment_note
			for (int noteId : attNoteIds) {
				insertAttachmentNote(noteId, attachmentsKf5ids.get(i));
			}
		}

		sql = "TRUNCATE TABLE " + db_ITM + ".attachment_table";
		stmt.execute(sql);
		convertAttachmetnTablefromAttachment_KF5();

		this.CleanUp();
		System.out.println("CleanUp");

	}
	
	

	public void print(String st) {
		System.out.println(st);
	}

	public static void main(String arg[]) throws ClassNotFoundException,
			SQLException, ParseException {
		 DBConfig db = new DBConfig("t1", "t3");
		 //db.updateAuthor_KF5("author_table_kf5");
		 db.update();
		// KF5Integration kf5 =new KF5Integration();
		// db.updateViewpostRefs_KF5();
		// db.updateAuthor_KF5("author_table_kf5uni");
		// db.updateAuthor_KF5("author_table_kf5");
		// db.updateNote1_KF5("note_kf5");
		// db.updateNote1_KF5("note_kf5uni");
		// db.updateAttachment_KF5();
		// db.updateView_KF5();
		//
		// db.CleanUp();
	}

}
