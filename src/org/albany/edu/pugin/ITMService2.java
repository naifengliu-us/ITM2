package org.albany.edu.pugin;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.albany.edu.IntergrateFactory.JsonFactory;
import org.albany.edu.IntergrateFactory.Interface.Imp.ReadDataByFileAddress;
import org.albany.edu.model.ModelBuilderByJson;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

/**
 * 
 * @author Yizhen Chen
 * @version 2015-8-18
 * 
 */
public class ITMService2 {

	/**
	 * Check User Login
	 * <p>
	 * 
	 * @param FactoryConfiguration
	 *            config config contains project information for connect KF5
	 * 
	 * @param URL
	 *            URL to connect KF5
	 * 
	 * @param pythonFileLocation
	 *            KF5 use python to provide api, this arg indicate location if
	 *            python code
	 * 
	 * @return boolean
	 */

	public boolean login(_FactoryConfiguration config, String URL,
			String pythonFileLocation) throws IOException, InterruptedException {

		String output;
		output = login(URL, config.getUserName(), config.getPassword(),
				pythonFileLocation);
		if (output.equals("Error")) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * Check User Login
	 * <p>
	 * 
	 * @param userName
	 * @param passWord
	 * @param URL
	 *            URL to connect KF5
	 * 
	 * @param pythonFileLocation
	 *            KF5 use python to provide api, this arg indicate location if
	 *            python code
	 * 
	 * @return String
	 */

	public String login(String URL, String userName, String passWord,
			String pythonFileLocation) throws IOException, InterruptedException {

		try {
			StringBuffer cmdout = new StringBuffer();
			
			String[] cmd2 = {"/network/rit/lab/zhanglab/custom-python/bin/python", pythonFileLocation
					, URL, userName, passWord};
			
//			String[] cmd2 = {"python", pythonFileLocation
//					, URL, userName, passWord};
			
			ProcessBuilder builder = new ProcessBuilder(cmd2);
			Process proc = builder.start();
			proc.waitFor();
			InputStream fis = proc.getInputStream();
			InputStream is2 = proc.getErrorStream();  
			   InputStreamReader isr = new  InputStreamReader(is2,  "GBK" );  
			    BufferedReader br2 = new  BufferedReader(isr);  
			    String line2;  
			    while  ((line2 = br2.readLine()) !=  null ) {  
				      System.out.println(line2);  }
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line = null;
			while ((line = br.readLine()) != null) {
				cmdout.append(line).append(System.getProperty("line.separator"));
			}
			if (cmdout.toString().isEmpty() || cmdout.toString().trim().equals("Error")) {
				return "Error";
			} else {
				updateUserPermissionTypeInITM(cmdout.toString());
				return cmdout.toString();
			}
			
		} catch (Exception e) {
			return e.toString();
		}
	}

	public void updateUserPermissionTypeInITM(String userInfo) {
		System.out.println("userInfo:" + userInfo);

	}

	

	public static void main(String arg[]) throws IOException,
			InterruptedException, InstantiationException,
			IllegalAccessException {
		_FactoryConfiguration config = new _FactoryConfiguration();
		config.setEmail("jonkiky@gmail.com");
		config.setFirstName("yizhen");
		config.setLastName("chen");
		config.setOrg("SUNYAlbany");
		config.setPassword("jianweizh");
		config.setProjectName("classModelTest");
		config.setUserName("Jianwei Zhang");
		String URL = "https://kf.utoronto.ca:443/kforum/";
		config.setDb("TeckWhye_Sec2");
		ITMService2 service = new ITMService2();
		boolean flag = service.login(config, URL,
				"D://kf5py-master//login.py");

		if (flag) {
			System.out.print("Login Success");
		} else {
			System.out.print("Login failed");
		}

		// service.login(URL,"Jianwei Zhang","jianweizh","//network//rit//lab//zhanglab//kf5py-master//login.py");
	}
}
