package itmdev.webservice.client;



import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

public class demo {

	private CloseableHttpClient httpClient = HttpClients.createDefault();

	private String host;
	
	public demo(String host) {
		setHost(host);
	}
	
	public void setHost(String host) {
		if (!host.endsWith("/")) {
			host = host + "/";
		}
		this.host = host;
	}
	
	public JSONArray getData() throws Exception {
		HttpGet method = new HttpGet(host);
		return getJSON(method);
	}

	
	
	private JSONArray getJSON(HttpUriRequest request) throws Exception {
		CloseableHttpResponse response = httpClient.execute(request);
		int status = response.getStatusLine().getStatusCode();
		if (status != 200) {
			//throw new KF5ServiceException(request.getURI() + " failed.", status);
		}
		String contentStr = EntityUtils.toString(response.getEntity(), "UTF-8");
		if (!contentStr.startsWith("[")) {
			contentStr = "[" + contentStr + "]";
		}
		JSONArray json = new JSONArray(contentStr);
		return json;
	}
	
	public static void main(String arg[]) throws Exception{
		 demo service = new demo("http://localhost:8080/hw");
		 JSONArray regs = service.getData();
		 System.out.println(regs);
	}
}
