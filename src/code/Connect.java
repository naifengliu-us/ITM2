package code;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

//import org.ikit.kfdatabridge.BindableZTB;
import org.zoolib.*;
import org.zoolib.tuplebase.*;

import com.knowledgeforum.k5.common.K5TBConnector;
import database.*;
import java.sql.*;
/**
 * Servlet implementation class Connect
 */
public class Connect extends HttpServlet {
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Inside Connect servlet");
		String strdb = null;
		//get the parameters
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String host = request.getParameter("host"); 
		String weburl = request.getParameter("url"); 
		String db = request.getParameter("db");
		String usertype = null;
				
		//keeping default port number as 80
		int port = 80;		
		/*if(host.equalsIgnoreCase("kf48.sol.edu.hku.hk"))
		{
		   port = 8084;
		}*/
		//Set the correct port number stored in the database.
		sqls sobj = new sqls();
        Operatedb op = new Operatedb(sobj,"itm");
        String strcon = "URL='"+host+"' and kfdb='"+db+"';";
        ResultSet rs = op.GetRecordsFromDB("connection_table","distinct(port)",strcon);
		try {
			while(rs.next())
			{
				port = rs.getInt("port");
				System.out.println(port);
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ZTB tb = null;
			
		//try { //port = Integer.parseInt(request.getParameter("port")); } catch (Exception e){};
		
		
		    System.out.println("URL is " + weburl);
        System.out.println("db is " + db);
        System.out.println("username is " + username);
        System.out.println("password is " + password);
        System.out.println("host is " + host);
        System.out.println("Testing connect.java");
        System.out.println("port is " + port);
        //if the username is "jianwei", then don't connect to the KF
        if(username != null){
        	
        	// NOTE: We no longer treat jianwei as special username
        	//if(!username.equals("jianwei")){
        	
        	K5TBConnector.Options options = new K5TBConnector.Options(5, (long)500); // retry count and retry interval in milliseconds
        	ZID[] sessionIDs = new ZID[1];
        	K5TBConnector.HostInfo hostInfo = new K5TBConnector.HostInfo(host, port, db);
        	tb = K5TBConnector.sGetTB_HTTP_UserName(hostInfo, options, username, password, sessionIDs);
        	System.out.println("SessionId is " + sessionIDs[0]);
        	System.out.println("tb is connect in  source package "+tb);
        	//}
        }
        
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			if (tb != null) // we have a valid connection to TB
			{
					System.out.println("Valid User");
					ZTxn txn = new ZTxn();
					
					//store the user name and database name in session
					request.getSession().setAttribute("username", username);
					System.out.println("weburl is"+ weburl);
					if(StringUtils.CheckIfDevelopment(weburl))
					{
						strdb = host.replace('.','_')+"_"+db.replace("-","_").replace(" ","_").replace(".", "_")+"_dev";
					}
					else
					{
					   strdb = host.replace('.','_')+"_"+db.replace("-","_").replace(" ","_").replace(".", "_");
					}
					
					System.out.println("strdb value is "+ strdb);
					/*Shifted session variables down*/
				//Store the user type in session
					op = new Operatedb(sobj,strdb);
			        strcon = "username='"+ username +"';";
			        rs = op.GetRecordsFromDB("author_table","type",strcon);
					
			    if (rs != null)
			    {
				    try 
						{
							while(rs.next())
							{
								usertype = rs.getString("type");
								System.out.println("usertype is "+ usertype);
							}	
						
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Set the session attributes
						request.getSession().setAttribute("usertype", usertype);
						request.getSession().setAttribute("host", host);
						request.getSession().setAttribute("dbase", db);
						request.getSession().setAttribute("strdb", strdb);
					//ToDo check if this is correct
						request.getSession().setAttribute("isCN",0);
			    }
					
					// Check if the database already exists
					CreateDatabase crtdb = new CreateDatabase(strdb);
					if(!crtdb.CheckDB()){ //the database does not exist
						crtdb.CreateDB();
//						record the connection information
						crtdb.RecordConInfor(host, db, username, password,strdb,port);
											
						DownloadViews(tb,strdb); //download all views from KF
						Downloadnotes(tb,strdb); //download all notes from KF
						DownLoadNoteViewLink(tb,strdb); //download view_note link from KF
						DownloadAttachment(tb,strdb);
						DownloadAttachmentNoteLink(tb,strdb);
						Downloadauthor(tb,strdb);
						this.DownLoadAuthorNoteLink(tb,strdb);
						this.DownLoadNoteNoteLink(tb,strdb);
						this.DownLoadGroupAuthorLink(tb,strdb);
						DownloadGroups(tb,strdb);
						downloadSupportObjects(tb,strdb); //download support objects from KF
						downloadSupportNoteLink(tb,strdb);
					}
					
					System.out.println("I am here.............");
					
					

					crtdb.CloseCon();
					
					if(tb != null){
						tb.close();
					}
					
				
					
					String url =response.encodeURL("/ITM2/en/New_HomePage.jsp?database="+strdb);
					response.sendRedirect(url);				
					return;								
				
			}
			else
			{
			System.out.println("invalid username or password");	
			String url =response.encodeURL("/ITM2/index.jsp?ifsucceed=no&username="+username+"&dbase="+db+"&host="+host);
			out.println("Invalid Username or Password");
			response.sendRedirect(url);
			return;
			}
			
			
}
	
	private UUID createSessionID(){
		return UUID.randomUUID();
	}
	
	private void DownloadAttachmentNoteLink(ZTB tb,String strdb)
	{
		System.out.println("Baibhav downloading the attachment_note link from KF");
		Links linkobj;
		int i;
		String sqlvalue;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get  support_note link from KF
		List linkList = rtrv.GetAttachmentNoteLink(tb);
		 
		System.out.println("Size of attachment note links = "+linkList.size());
		//add all support_note links into support_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  sqlvalue = " (`noteid`,`attachmentid`) VALUES ("+linkobj.GetToID()+","+ linkobj.GetFromID()+")";
//			  System.out.println("DownloadAttachmentNoteLink sql is "+sqlvalue);
			  dbobject.WritebacktoDB("attachment_note","INSERT",sqlvalue);	  
		}
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
	}
	
	/**
	 * Download all views from KF and write the views into database
	 * @param tb
	 * 		  tuplebase connection
	 * @param strdb
	 * 		  database name
	 * @return 
	 * 		  void
	 */
	private void DownloadAttachment(ZTB tb,String strdb)
	{
		System.out.println("downloading Attachments from KF");
		AttachmentObject attachmentobj;
		int i;
		String sqlvalue,strcretime;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/*********************write the view objects into database********************/
		List viewList = rtrv.getAttachments(tb);//get all views from KF
		 
		//add all view objects into view_table
		for(i=0; i<viewList.size();i++)
		{
			  attachmentobj = (AttachmentObject)viewList.get(i);
			  strcretime = spdtformat.format(attachmentobj.GetCretime());
			  sqlvalue = " (`attachmentid`, `attachmenttitle`,`attachmentfile`,`createtime`) VALUES ("+
				  		attachmentobj.GetID()+","+"'"+ 
			  			attachmentobj.GetTitle()+"','"+attachmentobj.GetFile()+"','"+strcretime+"')";
			 // System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("attachment_table","INSERT",sqlvalue);	  
		}
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
		
	}
	
	/**
	 * Download all views from KF and write the views into database
	 * @param tb
	 * 		  tuplebase connection
	 * @param strdb
	 * 		  database name
	 * @return 
	 * 		  void
	 */
	private void DownloadViews(ZTB tb,String strdb)
	{
		System.out.println("downloading views from KF");
		ViewObject viewobj;
		int i;
		String sqlvalue;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		/*********************write the view objects into database********************/
		List viewList = rtrv.GetViewObject(tb);//get all views from KF
		 
		//add all view objects into view_table
		for(i=0; i<viewList.size();i++)
		{
			  viewobj = (ViewObject)viewList.get(i);
			  sqlvalue = " (`idview`, `title`) VALUES ("+viewobj.GetID()+","+"'" +viewobj.GetTitle()+"');";
			 // System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("view_table","INSERT",sqlvalue);	  
		} 
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
		
	}
	
	/**
	 * download support objects from KF
	 * @param tb
	 * 		  tuplebase connection
	 * @param strdb
	 * 		  database name
	 * 
	 */
	private void downloadSupportObjects(ZTB tb,String strdb){
		System.out.println("downloading support objects from KF");
		SupportObject supportobj;
		int i;
		String sqlvalue;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
		/*********************write the support objects into database********************/
		List supportList = rtrv.getSupports(tb);//get all support objects from KF
		
		//add all support objects into support_table
		for(i=0; i<supportList.size();i++)
		{
			  supportobj = (SupportObject)supportList.get(i);
			  sqlvalue = " (`supportid`, `text`) VALUES ("+supportobj.getID()+","+"'" +supportobj.getText()+"');";
			  //System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("support_table","INSERT",sqlvalue);	  
		}
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
	}
	
	
	/**
	 * Download support-note links from KF and write the them into database
	 * @param tb
	 * 		  tuplebase connection
	 * @param strdb
	 * 		  database name
	 * @return 
	 * 		  void
	 */
	private void downloadSupportNoteLink(ZTB tb,String strdb)
	{
		System.out.println("downloading the support_note link from KF");
		Links linkobj;
		int i;
		String sqlvalue;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get  support_note link from KF
		List linkList = rtrv.getSupportNoteLink(tb);
		 
		//add all support_note links into support_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  sqlvalue = " (`supportid`, `noteid`,`text`) VALUES ("+linkobj.GetFromID()+","+ linkobj.GetToID()+",'"+linkobj.GetText()+"');";
//			  System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("support_note","INSERT",sqlvalue);	  
		} 
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
	}
	
	
	/**
	 * Download all groups from KF and write the groups into database
	 * @param tb
	 * 		  tuplebase connection
	 * @return 
	 * 		  void
	 */
	private void DownloadGroups(ZTB tb,String strdb)
	{
		System.out.println("downloading groups from KF");
		GroupObject groupobj;
		String sqlvalue;
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		List Gtitlelist = new ArrayList();
		Retrieve rtrv=new Retrieve();
		List groupList = rtrv.GetGroupObject(tb); //get groups from KF

		for(int i=0; i<groupList.size();i++)
		{
			  groupobj = (GroupObject)groupList.get(i);
			  Gtitlelist.add(groupobj.GetTitle());
			  //System.out.println("the "+i+"th"+" group title is: "+groupobj.GetTitle());
			  sqlvalue = " (`idgroup`, `title`) VALUES ("+groupobj.GetID()+","+"'" +groupobj.GetTitle()+"');";
			  //System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("group_table","INSERT",sqlvalue);	  
		} 
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
		
	}
	
	/**
	 * Download all notes from KF and write the views into database
	 * @param tb
	 * 		  tuplebase connection
	 * @return 
	 * 		  void
	 */
	private void Downloadnotes(ZTB tb,String strdb)
	{
		System.out.println("downloading the notes from KF");
		NoteObject noteobj;
		String sqlvalue, strcretime;
		//ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//SimpleDateFormat spdtformat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		
		List noteList = rtrv.GetNoteObject(tb);//get all notes from KF
		
		//add all view objects into view_table
		 for(int i=0; i<noteList.size();i++)
		  {
			  noteobj = (NoteObject)noteList.get(i);
			  strcretime = spdtformat.format(noteobj.GetCretime());
			  sqlvalue = " (`noteid`, `notetitle`,`notecontent`,`offset`,`createtime`) VALUES ("+noteobj.GetID()+","+"'" 
			  +noteobj.GetTitle()+"','"+noteobj.GetContent()+"','"+noteobj.offsetString+"','"+strcretime+"');";
			  //System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("note_table","INSERT",sqlvalue);	  
		  } 
		
		 if(dbobject != null){
				dbobject.CloseCon();
		}
		
	}
	
	/**
	 * Download all links of notes and views from KF and write the data into database
	 * @param tb
	 * 		  tuplebase connection
	 * @return 
	 * 		  void
	 */
	private void DownLoadNoteViewLink(ZTB tb,String strdb)
	{
		System.out.println("downloading the view_note link from KF");
		Links linkobj;
		int i;
		String sqlvalue;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get all view_note link from KF
		List linkList = rtrv.GetViewNoteLink(tb);
		 
		//add all view_note links into view_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  sqlvalue = " (`noteid`, `viewid`) VALUES ("+linkobj.GetToID()+","+ +linkobj.GetFromID()+");";
			  //System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("view_note","INSERT",sqlvalue);	  
		} 
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
		
	}
	
	/**
	 * Download author object from KF and write the them into database
	 * @param tb
	 * 		  tuplebase connection
	 * @return 
	 * 		  void
	 */
	private void Downloadauthor(ZTB tb,String strdb)
	{
		System.out.println("downloading the information of author from KF");
		AuthorObject authorobj;
		String sqlvalue, strcretime;
		//ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
				
		List authorList = rtrv.GetAuthorObject(tb);//get author from KF
		   
		//add author objects into author_table
		 for(int i=0; i<authorList.size();i++)
		  {
			  authorobj = (AuthorObject)authorList.get(i);
			  sqlvalue = " (`authorid`, `firstname`,`lastname`,`username`,`type`) VALUES ("+authorobj.GetID()+","+"'" 
			  +authorobj.GetFstname()+"','"+authorobj.GetLstname()+"','"+authorobj.GetUsname()+"','"+authorobj.GetType()+"');";
			  //System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("author_table","INSERT",sqlvalue);	  
		  } 
		
		 if(dbobject != null){
				dbobject.CloseCon();
		}
	}
	
	/**
	 * Download all links of notes and authors from KF and write the them into database
	 * @param tb
	 * 		  tuplebase connection
	 * @return 
	 * 		  void
	 */
	private void DownLoadAuthorNoteLink(ZTB tb,String strdb)
	{
		System.out.println("downloading the author_note link from KF");
		Links linkobj;
		int i;
		String sqlvalue;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get all author_note link from KF
		List linkList = rtrv.GetAuthorNoteLink(tb);
		 
		//add all view_note links into view_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  sqlvalue = " (`noteid`, `authorid`) VALUES ("+linkobj.GetToID()+","+ +linkobj.GetFromID()+");";
			 // System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("author_note","INSERT",sqlvalue);	  
		} 
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
	}
	
	/**
	 * Download note_to_note links from KF and write them into database
	 * @param tb
	 * 		  tuplebase connection
	 * @return 
	 * 		  void
	 */
	private void DownLoadNoteNoteLink(ZTB tb,String strdb)
	{
		System.out.println("downloading the note_to_note link from KF");
		Links linkobj;
		int i;
		String sqlvalue;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get all note_to_note link from KF
		List linkList = rtrv.GetNoteNoteLink(tb);
		 
		//add all view_note links into view_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  sqlvalue = " (`fromnoteid`, `tonoteid`,`linktype`) VALUES ("+linkobj.GetFromID()+","+linkobj.GetToID()+",'" + linkobj.GetLink()+"');";
			  //System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("note_note","INSERT",sqlvalue);	  
		} 
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
		
	}
	
	
	/**
	 * Download contains link of group and author from KF and write the them into database
	 * @param tb
	 * 		  tuplebase connection
	 * @return 
	 * 		  void
	 */
	private void DownLoadGroupAuthorLink(ZTB tb,String strdb)
	{
		System.out.println("downloading the group_author link from KF");
		Links linkobj;
		int i;
		String sqlvalue;
		Retrieve rtrv=new Retrieve();
		Operatedb dbobject = new Operatedb(new sqls(),strdb); //get one database operation object
		
	    //get all group_author link from KF
		List linkList = rtrv.GetGroupAuthorLink(tb);
		 
		//add all group_author links into view_note table
		for(i=0; i<linkList.size();i++)
		{
			  linkobj = (Links)linkList.get(i);
			  sqlvalue = " (`author_id`, `group_id`) VALUES ("+linkobj.GetToID()+","+ +linkobj.GetFromID()+");";
			 // System.out.println("the sqlvalue is "+sqlvalue);
			  dbobject.WritebacktoDB("group_author","INSERT",sqlvalue);	  
		} 
		
		if(dbobject != null){
			dbobject.CloseCon();
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);// TODO Auto-generated method stub
	}

}
