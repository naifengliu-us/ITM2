package code;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;


//import org.ikit.kfdatabridge.BindableZTB;
import org.zoolib.*;
import org.zoolib.tuplebase.*;

import com.knowledgeforum.k5.common.K5TBConnector;

import database.*;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 * import ping
 */
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.DatagramPacket;
import java.net.UnknownHostException;
import java.io.IOException;

import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class Connect
 */

@WebServlet("/Register")
public class Register extends HttpServlet {

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String serverURL = request.getRequestURL().toString();
        serverURL = serverURL.substring(0, serverURL.length() - 8);
        System.out.println("Inside Register servlet");
        String strdb = null;
        //get the parameters
        String FirstName = request.getParameter("fname");
        String LastName = request.getParameter("lname");

        String ID = request.getParameter("id");
        String PWD = request.getParameter("password");

        String Email = request.getParameter("Email");
        String Org ="kf5_"+request.getParameter("org").trim();
        // list of database spilt by &
        String DB = request.getParameter("db");
        String TEL = request.getParameter("phone");
        String Port=request.getParameter("port");
        String Status = "pending"; 
        
        String schoolName=request.getParameter("school_name");
        String gradeLevel=request.getParameter("grade_level");
        String street=request.getParameter("street");
        String city=request.getParameter("city");
        String state=request.getParameter("state");
        String country =request.getParameter("country");
        String zipcode= request.getParameter("zipcode");
        String school_phone= request.getParameter("school_phone");
        String weburl = request.getParameter("url");
        System.out.println("weburl=" + weburl);


        RegisterDatabase crtdb = new RegisterDatabase();

        System.out.println("Done123");
        final String username = "itm.test.send@gmail.com";
        final String password = "ITM@2014";


        Exception failure = null;


        if (failure == null) {
            response.setContentType("text/html");
            if (DB != null) { // we have a valid connection to TB

            	String[] dbList=DB.split("&");

            	for(String db : dbList){
            		if(!db.equals("undefined")&&!db.equals("")&&!db.equals("null")){
            			crtdb.RecordConInfor_Register(FirstName, LastName, ID, PWD, Email, TEL, weburl, Port, db, Org, Status,
            					 schoolName,
            	         gradeLevel,
            	         street,
            	         city,
            	         state,
            	         country, 
            	         zipcode,school_phone);
            		}
            		 
            	}
               

                Properties props = new Properties();
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.host", "smtp.gmail.com");
                props.put("mail.smtp.port", "587");

                Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });


                try {
                    String url = "";

                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress("itm.test.send@gmail.com"));
                    message.setRecipients(Message.RecipientType.TO,
                                          InternetAddress.parse("itm.test.send@gmail.com"));
                    message.setSubject(ID + "'s" + "Application of Database Registraion");
                    message.setText("Dear System Administrator,\n\n" + "  " + FirstName + " " + LastName + "   would like to add new database."
                                    + "\n\n");
                        url = "index.jsp";
                        message.setContent("Please click the below link to view his/her registration information.<br/><a href=\"" + serverURL + "en/Approval.jsp\">E-mail</a>", "text/html; charset=UTF-8");
                    Transport.send(message);

                    Transport.send(message);

                    System.out.println("Done");
//
                    response.sendRedirect(url);

                } catch (MessagingException e) {
                  //  throw new RuntimeException(e);
                }


            } else {
                System.out.println("invalid username or password");
                String url = "";
                    url = response.encodeURL("/ITM2/en/Registration.jsp?ifsucceed=no");
                response.sendRedirect(url);
                return;
            }

        } 




    }
    private UUID createSessionID() {
        return UUID.randomUUID();
    }
//
//    private void DownloadAttachmentNoteLink(ZTB tb, String strdb) {
//        System.out.println("Baibhav downloading the attachment_note link from KF");
//        Links linkobj;
//        int i;
//        String sqlvalue;
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//
//        //get  support_note link from KF
//        List linkList = rtrv.GetAttachmentNoteLink(tb);
//
//        System.out.println("Size of attachment note links = " + linkList.size());
//        //add all support_note links into support_note table
//        for(i = 0; i < linkList.size(); i++) {
//            linkobj = (Links)linkList.get(i);
//            sqlvalue = " (`noteid`,`attachmentid`) VALUES (" + linkobj.GetToID() + "," + linkobj.GetFromID() + ")";
////		  System.out.println("DownloadAttachmentNoteLink sql is "+sqlvalue);
//            dbobject.WritebacktoDB("attachment_note", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//    }
//
//    /**
//     * Download all views from KF and write the views into database
//     * @param tb
//     * 		  tuplebase connection
//     * @param strdb
//     * 		  database name
//     * @return
//     * 		  void
//     */
//    private void DownloadAttachment(ZTB tb, String strdb) {
//        System.out.println("downloading Attachments from KF");
//        AttachmentObject attachmentobj;
//        int i;
//        String sqlvalue, strcretime;
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//        SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        /*********************write the view objects into database********************/
//        List viewList = rtrv.getAttachments(tb);//get all views from KF
//
//        //add all view objects into view_table
//        for(i = 0; i < viewList.size(); i++) {
//            attachmentobj = (AttachmentObject)viewList.get(i);
//            strcretime = spdtformat.format(attachmentobj.GetCretime());
//            sqlvalue = " (`attachmentid`, `attachmenttitle`,`attachmentfile`,`createtime`) VALUES (" +
//                       attachmentobj.GetID() + "," + "'" +
//                       attachmentobj.GetTitle() + "','" + attachmentobj.GetFile() + "','" + strcretime + "')";
//            // System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("attachment_table", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//
//    }
//
//    /**
//     * Download all views from KF and write the views into database
//     * @param tb
//     * 		  tuplebase connection
//     * @param strdb
//     * 		  database name
//     * @return
//     * 		  void
//     */
//    private void DownloadViews(ZTB tb, String strdb) {
//        System.out.println("downloading views from KF");
//        ViewObject viewobj;
//        int i;
//        String sqlvalue;
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//        /*********************write the view objects into database********************/
//        List viewList = rtrv.GetViewObject(tb);//get all views from KF
//
//        //add all view objects into view_table
//        for(i = 0; i < viewList.size(); i++) {
//            viewobj = (ViewObject)viewList.get(i);
//            sqlvalue = " (`idview`, `title`) VALUES (" + viewobj.GetID() + "," + "'" + viewobj.GetTitle() + "');";
//            // System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("view_table", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//
//    }
//
//    /**
//     * download support objects from KF
//     * @param tb
//     * 		  tuplebase connection
//     * @param strdb
//     * 		  database name
//     *
//     */
//    private void downloadSupportObjects(ZTB tb, String strdb) {
//        System.out.println("downloading support objects from KF");
//        SupportObject supportobj;
//        int i;
//        String sqlvalue;
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//
//        /*********************write the support objects into database********************/
//        List supportList = rtrv.getSupports(tb);//get all support objects from KF
//
//        //add all support objects into support_table
//        for(i = 0; i < supportList.size(); i++) {
//            supportobj = (SupportObject)supportList.get(i);
//            sqlvalue = " (`supportid`, `text`) VALUES (" + supportobj.getID() + "," + "'" + supportobj.getText() + "');";
//            //System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("support_table", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//    }
//
//
//    /**
//     * Download support-note links from KF and write the them into database
//     * @param tb
//     * 		  tuplebase connection
//     * @param strdb
//     * 		  database name
//     * @return
//     * 		  void
//     */
//    private void downloadSupportNoteLink(ZTB tb, String strdb) {
//        System.out.println("downloading the support_note link from KF");
//        Links linkobj;
//        int i;
//        String sqlvalue;
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//
//        //get  support_note link from KF
//        List linkList = rtrv.getSupportNoteLink(tb);
//
//        //add all support_note links into support_note table
//        for(i = 0; i < linkList.size(); i++) {
//            linkobj = (Links)linkList.get(i);
//            sqlvalue = " (`supportid`, `noteid`,`text`) VALUES (" + linkobj.GetFromID() + "," + linkobj.GetToID() + ",'" + linkobj.GetText() + "');";
////		  System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("support_note", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//    }
//
//
//    /**
//     * Download all groups from KF and write the groups into database
//     * @param tb
//     * 		  tuplebase connection
//     * @return
//     * 		  void
//     */
//    private void DownloadGroups(ZTB tb, String strdb) {
//        System.out.println("downloading groups from KF");
//        GroupObject groupobj;
//        String sqlvalue;
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//        List Gtitlelist = new ArrayList();
//        Retrieve rtrv = new Retrieve();
//        List groupList = rtrv.GetGroupObject(tb); //get groups from KF
//
//        for(int i = 0; i < groupList.size(); i++) {
//            groupobj = (GroupObject)groupList.get(i);
//            Gtitlelist.add(groupobj.GetTitle());
//            //System.out.println("the "+i+"th"+" group title is: "+groupobj.GetTitle());
//            sqlvalue = " (`idgroup`, `title`) VALUES (" + groupobj.GetID() + "," + "'" + groupobj.GetTitle() + "');";
//            //System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("group_table", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//
//    }
//
//    /**
//     * Download all notes from KF and write the views into database
//     * @param tb
//     * 		  tuplebase connection
//     * @return
//     * 		  void
//     */
//    private void Downloadnotes(ZTB tb, String strdb) {
//        System.out.println("downloading the notes from KF");
//        NoteObject noteobj;
//        String sqlvalue, strcretime;
//        //ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//        SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        //SimpleDateFormat spdtformat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
//
//        List noteList = rtrv.GetNoteObject(tb);//get all notes from KF
//
//        //add all view objects into view_table
//        for(int i = 0; i < noteList.size(); i++) {
//            noteobj = (NoteObject)noteList.get(i);
//            strcretime = spdtformat.format(noteobj.GetCretime());
//            sqlvalue = " (`noteid`, `notetitle`,`notecontent`,`offset`,`createtime`) VALUES (" + noteobj.GetID() + "," + "'"
//                       + noteobj.GetTitle() + "','" + noteobj.GetContent() + "','" + noteobj.offsetString + "','" + strcretime + "');";
//            //System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("note_table", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//
//    }
//
//    /**
//     * Download all links of notes and views from KF and write the data into database
//     * @param tb
//     * 		  tuplebase connection
//     * @return
//     * 		  void
//     */
//    private void DownLoadNoteViewLink(ZTB tb, String strdb) {
//        System.out.println("downloading the view_note link from KF");
//        Links linkobj;
//        int i;
//        String sqlvalue;
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//
//        //get all view_note link from KF
//        List linkList = rtrv.GetViewNoteLink(tb);
//
//        //add all view_note links into view_note table
//        for(i = 0; i < linkList.size(); i++) {
//            linkobj = (Links)linkList.get(i);
//            sqlvalue = " (`noteid`, `viewid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
//            //System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("view_note", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//
//    }
//
//    /**
//     * Download author object from KF and write the them into database
//     * @param tb
//     * 		  tuplebase connection
//     * @return
//     * 		  void
//     */
//    private void Downloadauthor(ZTB tb, String strdb) {
//        System.out.println("downloading the information of author from KF");
//        AuthorObject authorobj;
//        String sqlvalue, strcretime;
//        //ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//
//        List authorList = rtrv.GetAuthorObject(tb);//get author from KF
//
//        //add author objects into author_table
//        for(int i = 0; i < authorList.size(); i++) {
//            authorobj = (AuthorObject)authorList.get(i);
//            sqlvalue = " (`authorid`, `firstname`,`lastname`,`username`,`type`) VALUES (" + authorobj.GetID() + "," + "'"
//                       + authorobj.GetFstname() + "','" + authorobj.GetLstname() + "','" + authorobj.GetUsname() + "','" + authorobj.GetType() + "');";
//            //System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("author_table", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//    }
//
//    /**
//     * Download all links of notes and authors from KF and write the them into database
//     * @param tb
//     * 		  tuplebase connection
//     * @return
//     * 		  void
//     */
//    private void DownLoadAuthorNoteLink(ZTB tb, String strdb) {
//        System.out.println("downloading the author_note link from KF");
//        Links linkobj;
//        int i;
//        String sqlvalue;
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//
//        //get all author_note link from KF
//        List linkList = rtrv.GetAuthorNoteLink(tb);
//
//        //add all view_note links into view_note table
//        for(i = 0; i < linkList.size(); i++) {
//            linkobj = (Links)linkList.get(i);
//            sqlvalue = " (`noteid`, `authorid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
//            // System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("author_note", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//    }
//
//    /**
//     * Download note_to_note links from KF and write them into database
//     * @param tb
//     * 		  tuplebase connection
//     * @return
//     * 		  void
//     */
//    private void DownLoadNoteNoteLink(ZTB tb, String strdb) {
//        System.out.println("downloading the note_to_note link from KF");
//        Links linkobj;
//        int i;
//        String sqlvalue;
//        Retrieve rtrv = new Retrieve();
//        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
//
//        //get all note_to_note link from KF
//        List linkList = rtrv.GetNoteNoteLink(tb);
//
//        //add all view_note links into view_note table
//        for(i = 0; i < linkList.size(); i++) {
//            linkobj = (Links)linkList.get(i);
//            sqlvalue = " (`fromnoteid`, `tonoteid`,`linktype`) VALUES (" + linkobj.GetFromID() + "," + linkobj.GetToID() + ",'" + linkobj.GetLink() + "');";
//            //System.out.println("the sqlvalue is "+sqlvalue);
//            dbobject.WritebacktoDB("note_note", "INSERT", sqlvalue);
//        }
//
//        if(dbobject != null) {
//            dbobject.CloseCon();
//        }
//
//    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        doGet(request, response); // TODO Auto-generated method stub
    }


    public static void main(String[] args) {
        System.out.println("Done123");
        final String username = "itm.test.send@gmail.com";
        final String password = "HK8825252";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
        new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("itm.test.send@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                                  InternetAddress.parse("itm.test.send@gmail.com"));
            message.setSubject("Testing Subject");
            message.setText("Dear Mail test,"
                            + "\n\n No spam to my email, please!");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public class MyPing {
        static final int echoPort = 7;
        static final int maxPingTime = 3000;     // Milliseconds
        static final int pingPollInterval = 100; // Milliseconds

        public void ping(InetAddress dest) {
            String message = "test ping";
            byte[] outmsg = message.getBytes();
            int length = message.getBytes().length;
            byte[] inmsg = new byte[length];
            DatagramPacket outPacket = new DatagramPacket(outmsg, length);
            DatagramPacket inPacket = new DatagramPacket(inmsg, length);

            try {
                DatagramSocket socket = new DatagramSocket(echoPort, dest);
                outPacket.setAddress(dest);
                outPacket.setPort(echoPort);
                socket.send(outPacket);
                socket.setSoTimeout(5000);
                socket.receive(inPacket);
                System.out.println(new String(inmsg));
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}