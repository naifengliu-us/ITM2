/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import itm.models.Project;
import itm.models.PublishProject;

public class Publish implements Project {
    private String projectName;
    private Boolean showID;
    private String userID;
    private String grade;
    private String db;
    private String[] threadsName;
    private String[] highlightsOption;

    /**
     * Constructor
     */
    public Publish() {
    }

    /**
     * Get grade
     *
     * @return
     */
    public String getGrade() {
        return grade;
    }
    /**
     * Set grade
     * @param grade
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * Get user ID
     * @return
     */
    public String getUserID() {
        return this.userID;
    }
    /**
     * Set user ID
     * @param uid
     */
    public void setUserID(String uid) {
        this.userID = uid;
    }

    /**
     * Get state of showID
     * @return
     */
    public boolean getShowID() {
        return showID;
    }
    /**
     * Set state of showID
     * @param showid
     */
    public void setShowID(Boolean showid) {
        this.showID = showid;
    }

    /**
     * Get name of the project
     */
    public String getProjectName() {
        return projectName;
    }
    /**
     * Set name of the project
     */
    public void setProjectName(String pName) {
        this.projectName = pName;
    }

    /**
     * Get database
     */
    public String getDB() {
        return db;
    }
    /**
     * Set database
     */
    public void setDB(String db) {
        this.db = db;
    }

    /**
     * Get threads
     */
    public String getThreads() {
        // TODO Auto-generated method stub
        return null;
    }
    /**
     * Set threads
     */
    public void setThreads(String threads) {
        // TODO Auto-generated method stub
    }

    /*Set threadNames
     */
    public void setThreadsName(String tNames) {
        // TODO Auto-generated method stub
        String[] threads = tNames.split(",");
        this.threadsName = threads;
    }

    /*
     * Get Thread Names
     */

    public String[] getThreadsName() {
        return threadsName;
    }

    @Override
    public String getName() {
        return this.projectName;
    }

    @Override
    public void setName(String pName) {
        this.projectName = pName;
    }
    
    public String[] getHighlightsOption() {
      return this.highlightsOption;
    }
    public void setHighlightsOption(String hgtsOnly) {
        this.highlightsOption = hgtsOnly.split(",");
    }
}