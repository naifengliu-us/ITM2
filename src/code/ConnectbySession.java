package code;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;


//import org.ikit.kfdatabridge.BindableZTB;
import org.zoolib.*;
import org.zoolib.tuplebase.*;

import com.knowledgeforum.k5.common.K5TBConnector;
import com.knowledgeforum.k5.common.K5TBConnector.Options;

import database.*;

/**
 * Servlet implementation class Connect
 */

public class ConnectbySession extends HttpServlet {

    private static final String PROPERTY_USER_ID = "userID";

    /**the session tuple**/
    private ZTuple fSessionTuple = null;
    /** the tuplebase connection */
    private ZTB fTB;
    /** id of current tuplebase session */
    private ZID fSessionZID ;
    /** a K5TBConnector.Options object */
    private Options fHostOptions;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long TEMPORARY_TIMEOUT = 2000; // ZTB does not block on start()
        int port = 80;
        String username = null;
        String password = null;
        //get host
        String host = request.getParameter("host");
        //System.out.println("host is "+host);

        //get database name
        String db = request.getParameter("db").replace('@', ' ');
        //System.out.println("db is "+db);

        //get session id
        String strid = request.getParameter("sessionid");
        //System.out.println("sessionid is "+strid);
        System.out.println("Testing connect by session.java");
        if (strid == null) {
            System.err.println("Parameter Warning: The Applet parameter < PARAM_SESSION_ID > was not found.");
        } else {
            try {
                fSessionZID = new ZID(Long.parseLong(strid));
            } catch(Exception e) {
                System.err.println("Error creating sessionID: " + e.getMessage());
            }
        }

        //connect to the tuplebase using session ID
        fHostOptions = new Options(5, (long)5000);
        fTB = K5TBConnector.sGetTB_HTTP_Session(new K5TBConnector.HostInfo(host, port, db), fHostOptions, fSessionZID);
        try {
            java.lang.Thread.sleep(TEMPORARY_TIMEOUT);
        } catch(Throwable t) {} // Make sure ZClient.start() has time to finish

        //ZTB fTB = K5TBConnector.sGetTB_HTTP_UserName(new K5TBConnector.HostInfo("builder.ikit.org", port, "ICS 2012"),null, "jianwei", "jianweiz",null);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (fTB != null) { // we have a valid connection to TB
            System.out.println("Valid User");

            //create a new transaction
            ZTxn txn = new ZTxn();
            //create an object to encapsulate the tuplebase and the transaction
            ZTBTxn tbTxn = new ZTBTxn(txn, fTB);

            //get the author ID
            ZID authorid = this.getAuthorID();
            //ZID authorid = new ZID(Long.parseLong("9118"));
            ZTBQuery authorQ = new ZTBQuery(authorid);
            ZTBIter authorIter = new ZTBIter(tbTxn, authorQ);
            if (authorIter.hasNext()) {
                ZTuple authorTuple = authorIter.get();
                username = authorTuple.getString("unam");
                password = authorTuple.getString("pass");
                //System.out.println("the username is "+username);
                //System.out.println("the password is "+password);
            }

            //store the user name into session
            request.getSession().setAttribute("username", username);

            String strdb = host.replace('.', '_') + "_" + db.replace("-", "_").replace(" ", "_");
            System.out.println("strdb..." + strdb);
            //check if the database already exists
            CreateDatabase crtdb = new CreateDatabase(strdb);
            if(!crtdb.CheckDB()) { //the database does not exist
                crtdb.CreateDB();
                //record the connection information
                crtdb.RecordConInfor(host, db, username, password, strdb, port);
                DownloadViews(fTB, strdb); //download all views from KF
                Downloadnotes(fTB, strdb); //download all notes from KF
                DownLoadNoteViewLink(fTB, strdb); //download view_note link from KF
                Downloadauthor(fTB, strdb);
                this.DownLoadAuthorNoteLink(fTB, strdb);
                this.DownLoadNoteNoteLink(fTB, strdb);
                this.DownLoadGroupAuthorLink(fTB, strdb);
                DownloadGroups(fTB, strdb);


            }

            crtdb.CloseCon();

            fTB.close();
            String url = response.encodeURL("/ITM2/New_HomePage.jsp?database=" + strdb);
            response.sendRedirect(url);
            return;

        } else {
            System.out.println("invalid username or password");
            String url = response.encodeURL("/ITM2/index.jsp?ifsucceed=no&username=" + username + "&database=" + db + "&host=" + host);
            out.println("Invalid Username or Password");
            response.sendRedirect(url);
            return;
        }

    }

    /**
     * retrieve the ID of the logged in author from
     * the session tuple.
     * @return the id
     */
    public ZID getAuthorID() {
        ZID oZID = null;
        //TODO: test the result of failed getID() call. Jul 21, 2007 -- ctax
        ZTuple theSession = getSessionTuple();
        System.out.println("dump session tuple: " + theSession);
        oZID = new ZID(theSession.getID(PROPERTY_USER_ID));
        return oZID;
    }

    public ZTuple getSessionTuple() {
        if ( fSessionTuple == null ) {
            if ( !doQuery_SessionTuple() ) {
                System.out.println("Cannot get session tuple.");
            }
        }
        return fSessionTuple;
    }

    private boolean doQuery_SessionTuple() {
        while (true) {
            ZTxn txn = new ZTxn();
            fSessionTuple = fTB.get(txn, fSessionZID);
            if (txn.commit())
                break;
        }
        return ( fSessionTuple != null );
    }

    private UUID createSessionID() {
        return UUID.randomUUID();
    }

    /**
     * Download all views from KF and write the views into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    private void DownloadViews(ZTB tb, String strdb) {
        System.out.println("downloading views from KF");
        ViewObject viewobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        /*********************write the view objects into database********************/
        List viewList = rtrv.GetViewObject(tb);//get all views from KF

        //add all view objects into view_table
        for(i = 0; i < viewList.size(); i++) {
            viewobj = (ViewObject)viewList.get(i);
            sqlvalue = " (`idview`, `title`) VALUES (" + viewobj.GetID() + "," + "'" + viewobj.GetTitle() + "');";
            // System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("view_table", "INSERT", sqlvalue);
        }

    }
    /**
     * Download all groups from KF and write the groups into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    private void DownloadGroups(ZTB tb, String strdb) {
        System.out.println("downloading groups from KF");
        GroupObject groupobj;
        String sqlvalue;
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        List Gtitlelist = new ArrayList();
        Retrieve rtrv = new Retrieve();
        List groupList = rtrv.GetGroupObject(tb); //get groups from KF

        for(int i = 0; i < groupList.size(); i++) {
            groupobj = (GroupObject)groupList.get(i);
            Gtitlelist.add(groupobj.GetTitle());
            //System.out.println("the "+i+"th"+" group title is: "+groupobj.GetTitle());
            sqlvalue = " (`idgroup`, `title`) VALUES (" + groupobj.GetID() + "," + "'" + groupobj.GetTitle() + "');";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("group_table", "INSERT", sqlvalue);
        }

    }

    /**
     * Download all notes from KF and write the views into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    private void Downloadnotes(ZTB tb, String strdb) {
        System.out.println("downloading the notes from KF");
        NoteObject noteobj;
        String sqlvalue, strcretime;
        //ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat spdtformat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

        List noteList = rtrv.GetNoteObject(tb);//get all notes from KF

        //add all view objects into view_table
        for(int i = 0; i < noteList.size(); i++) {
            noteobj = (NoteObject)noteList.get(i);
            strcretime = spdtformat.format(noteobj.GetCretime());
            sqlvalue = " (`noteid`, `notetitle`,`notecontent`,`createtime`) VALUES (" + noteobj.GetID() + "," + "'"
                       + noteobj.GetTitle() + "','" + noteobj.GetContent() + "','" + strcretime + "');";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("note_table", "INSERT", sqlvalue);
        }


    }

    /**
     * Download all links of notes and views from KF and write the data into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    private void DownLoadNoteViewLink(ZTB tb, String strdb) {
        System.out.println("downloading the view_note link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all view_note link from KF
        List linkList = rtrv.GetViewNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`noteid`, `viewid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("view_note", "INSERT", sqlvalue);
        }

    }

    /**
     * Download author object from KF and write the them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    private void Downloadauthor(ZTB tb, String strdb) {
        System.out.println("downloading the information of author from KF");
        AuthorObject authorobj;
        String sqlvalue, strcretime;
        //ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        List authorList = rtrv.GetAuthorObject(tb);//get author from KF

        //add author objects into author_table
        for(int i = 0; i < authorList.size(); i++) {
            authorobj = (AuthorObject)authorList.get(i);
            sqlvalue = " (`authorid`, `firstname`,`lastname`,`username`,`type`) VALUES (" + authorobj.GetID() + "," + "'"
                       + authorobj.GetFstname() + "','" + authorobj.GetLstname() + "','" + authorobj.GetUsname() + "','" + authorobj.GetType() + "');";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("author_table", "INSERT", sqlvalue);
        }


    }

    /**
     * Download all links of notes and authors from KF and write the them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    private void DownLoadAuthorNoteLink(ZTB tb, String strdb) {
        System.out.println("downloading the author_note link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all author_note link from KF
        List linkList = rtrv.GetAuthorNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`noteid`, `authorid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
            // System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("author_note", "INSERT", sqlvalue);
        }

    }

    /**
     * Download note_to_note links from KF and write them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    private void DownLoadNoteNoteLink(ZTB tb, String strdb) {
        System.out.println("downloading the note_to_note link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all note_to_note link from KF
        List linkList = rtrv.GetNoteNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`fromnoteid`, `tonoteid`,`linktype`) VALUES (" + linkobj.GetFromID() + "," + linkobj.GetToID() + ",'" + linkobj.GetLink() + "');";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("note_note", "INSERT", sqlvalue);
        }

    }

    /**
     * Download contains link of group and author from KF and write the them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    private void DownLoadGroupAuthorLink(ZTB tb, String strdb) {
        System.out.println("downloading the group_author link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all group_author link from KF
        List linkList = rtrv.GetGroupAuthorLink(tb);

        //add all group_author links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`author_id`, `group_id`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
            // System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("group_author", "INSERT", sqlvalue);
        }

        if(dbobject != null) {
            dbobject.CloseCon();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response); // TODO Auto-generated method stub
    }

}
