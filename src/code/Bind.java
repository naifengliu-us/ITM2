package code;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;
import org.zoolib.tuplebase.ZTB;
public class Bind implements HttpSessionBindingListener {
    private ZTB fTB;
    static Logger log = Logger.getLogger("code");

    public Bind(ZTB tb) {
        fTB = tb;
    }

    public ZTB getZTB() {
        return fTB;
    }
    public void valueBound(HttpSessionBindingEvent e) {
        // TODO Auto-generated method stub

    }
    @Override
    public void valueUnbound(HttpSessionBindingEvent e) {
        fTB.close();
        fTB = null;
        // TODO Auto-generated method stub

    }
}
