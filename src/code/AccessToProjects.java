package code;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import database.Operatedb;

/**
*
*@author Shweta
* User right control
*/
public class AccessToProjects {

    Hashtable<String, Vector<String>> accessToProject = new Hashtable<String, Vector<String>>();
    Hashtable<String, Vector<Integer>> permissions = new Hashtable<String, Vector<Integer>>();

    enum Permission {
        CREATE (8),
        READ (4),
        EDIT (2),
        PUBLISH (1),
        NONE (0);

        private int value;

        Permission(int val) {
            this.value = val;
        }

        int getValue() {
            return this.value;
        }

    }

    /*
     * Constructor
     */
    public AccessToProjects() {
        this.constructProjectPermissions();
    }

    /*
     * Create project permissions table
     */
    public void constructProjectPermissions() {
        // Store Strings for the key and Vectors of
        // Strings for the values
        Vector<String> r = new Vector<String>();
        r.add("visitor");
        r.add("reader");
        r.add("writer");
        r.add("editor");
        r.add("manager");
        r.add("researcher");
        accessToProject.put("Role", r);

        Vector<Integer> p = new Vector<Integer>();
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue());
        permissions.put("Published", p);

        p = new Vector<Integer>();
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue());
        p.add(Permission.EDIT.getValue() + Permission.READ.getValue() + Permission.PUBLISH.getValue());
        p.add(Permission.EDIT.getValue() + Permission.READ.getValue() + Permission.PUBLISH.getValue());
        p.add(Permission.READ.getValue());
        permissions.put("Current_KF_db", p);

        p = new Vector<Integer>();
        p.add(Permission.NONE.getValue());
        p.add(Permission.NONE.getValue());
        p.add(Permission.READ.getValue() + Permission.EDIT.getValue());
        p.add(Permission.EDIT.getValue() + Permission.READ.getValue() + Permission.PUBLISH.getValue());
        p.add(Permission.EDIT.getValue() + Permission.READ.getValue() + Permission.PUBLISH.getValue());
        p.add(Permission.READ.getValue() + Permission.EDIT.getValue());
        permissions.put("Group", p);

        p = new Vector<Integer>();
        p.add(Permission.NONE.getValue());
        p.add(Permission.NONE.getValue());
        p.add(Permission.CREATE.getValue() + Permission.READ.getValue() + Permission.EDIT.getValue());
        p.add(Permission.CREATE.getValue() + Permission.READ.getValue() + Permission.EDIT.getValue() + Permission.PUBLISH.getValue());
        p.add(Permission.CREATE.getValue() + Permission.READ.getValue() + Permission.EDIT.getValue() + Permission.PUBLISH.getValue());
        p.add(Permission.CREATE.getValue() + Permission.READ.getValue() + Permission.EDIT.getValue());
        permissions.put("Own", p);

        p = new Vector<Integer>();
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue());
        p.add(Permission.READ.getValue() + Permission.EDIT.getValue());
        p.add(Permission.READ.getValue() + Permission.EDIT.getValue());
        p.add(Permission.READ.getValue() + Permission.EDIT.getValue());
        p.add(Permission.READ.getValue() + Permission.EDIT.getValue());
        permissions.put("ProjectInfo", p);

    }

    /*Check if the type of access is available
     * for the respective category in that role
     */
    public boolean checkAuthentication(Permission type, String Category, String role) {
        int index = 0;
        String key = "Role";
        Vector<String> r = this.accessToProject.get(key);

        for (int i = 0; i < r.size(); ++i) {
            if(r.get(i).equalsIgnoreCase(role)) {
                index = i;
                break;
            }
        }


        Vector<Integer> p = permissions.get(Category);
        int valueAtLocation = p.get(index);

        if ((type.getValue() & valueAtLocation) == 1) {
            return true;
        } else {
            return false;
        }
    }

    public int fetchPermission(String Category, String usertype) {
        System.out.println("Usertype is " + usertype);
        System.out.println("Category is " + Category);
        int index = 0;
        String key = "Role";
        Vector<String> r = this.accessToProject.get(key);

        for (int i = 0; i < r.size(); ++i) {
            if(r.get(i).equalsIgnoreCase(usertype)) {
                index = i;
                break;
            }
        }
        Vector<Integer> p = permissions.get(Category);
        int valueAtLocation = p.get(index);

        return valueAtLocation;
    }

    /* Check the access permission for user */
    public boolean canEdit(String username, String usertype, String projectname, String database) {
        // Here is how it works:
        // Get the role (index) of the person logged in
        // - First check under the Own title if edit is allowed
        // - Second check under the Group if edit is allowed
        // - Check under the Current KF db
        boolean canEdit = false;

        //Special case
        if(projectname == null) {
            return canEdit;
        }

        System.out.println("Usertype is " + usertype);
        int roleIndex = 0;
        String key = "Role";
        Vector<String> r = this.accessToProject.get(key);

        for (int i = 0; i < r.size(); ++i) {
            if(r.get(i).equalsIgnoreCase(usertype)) {
                // Get the index of the role
                roleIndex = i;
                System.out.println("Role index is " + i);
                break;
            }
        }

        // We don't know the owner yet
        String projOwner = null;
        // A project can belong to multiple groups
        // An author can belong to 0 or 1 group


        String[] authorTableNames = new  String[2];
        String[] projectTableNames = new String[2];

        ArrayList<Integer> userGroupIds = new ArrayList<Integer>();
        ArrayList<Integer> projectGroupIds = new ArrayList<Integer>();
        sqls sobj = new sqls();
        Operatedb op = new Operatedb(sobj, database);



        String strcon = "projectname='" + projectname + "';";
        ResultSet rs = op.GetRecordsFromDB("project", "projectowner", strcon);

        try {
            while(rs.next()) {
                projOwner = rs.getString("projectowner");
                System.out.println("Project Owner" + projOwner);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }

        if(username.equalsIgnoreCase(projOwner)) {
            Vector<Integer> p = permissions.get("Own");

            int valueAtLocation = p.get(roleIndex);

            if( (valueAtLocation & 2) == 2) {
                //System.out.println("@@@@@@@@@@@@@@owner is true is@@@@@@@@@@"+ projOwner);
                canEdit = true;
                return canEdit;
            }
        }

        /* Store the author id in an array */
        //rs = op.MulGetRecordsFromDB(tableNames, "count(group_author.group_id)", strcon, 4);
        try {
            projectTableNames[0] = "project";
            projectTableNames[1] = "project_group";
            authorTableNames[0] = "author_table";
            authorTableNames[1] = "group_author";

            strcon = "author_id = authorid AND username='" + username + "';";
            rs = op.MulGetRecordsFromDB(authorTableNames, "group_id", strcon, 2);

            while(rs.next()) {
                userGroupIds.add(rs.getInt("group_id"));
                System.out.println("author's group_id  is" + userGroupIds);
            }

            strcon = "projectid = idproject AND projectname='" + projectname + "';";
            rs = op.MulGetRecordsFromDB(projectTableNames, "groupid", strcon, 2);

            while(rs.next()) {
                projectGroupIds.add(rs.getInt("groupid"));
                System.out.println("project groupid is" + projectGroupIds);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(usertype.equalsIgnoreCase("visitor") || usertype.equalsIgnoreCase("reader")) {
            //do nothing
        } else if(userGroupIds.size() < 1 || projectGroupIds.size() < 1 ) {
            //do nothing
        } else {
            System.out.println(" project group id size =" + projectGroupIds.size());

            for(int i = 0; i < projectGroupIds.size(); i++) {
                if(projectGroupIds.get(i).equals(userGroupIds.get(0))) {
                    //System.out.println("@@@@@@@@@ group permission is true is @@@@@@@@");
                    canEdit = true;
                    return canEdit;
                }
            }
        }

        Vector<Integer> p = permissions.get("Current_KF_db");
        int valueAtLocation = p.get(roleIndex);
        if((valueAtLocation & 2) == 2) {
            //System.out.println("@@@@@@@@@ kf db is true is @@@@@@@@");
            canEdit = true;
            return canEdit;
        }

        return canEdit;
    }
}



