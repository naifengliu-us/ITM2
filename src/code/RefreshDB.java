package code;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.albany.edu.IntergrateFactory.FactoryConfiguration;
import org.albany.edu.pugin.DBConfig;
import org.albany.edu.pugin.KF5Integration;
import org.albany.edu.webservice.ITMService;
import org.zoolib.tuplebase.ZTB;

import com.knowledgeforum.k5.common.K5TBConnector;

import database.*;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.zoolib.*;
import org.zoolib.tuplebase.*;

import com.knowledgeforum.k5.common.K5TBConnector;
/**
 * Servlet implementation class RefreshDB
 */
@WebServlet("/RefreshDB")
public class RefreshDB extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RefreshDB() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        String strdb = request.getParameter("database");
       //String strdb="kf5_ges_kf5_ges_ges_2015_2016";
        try {
        	sqls sobj = new sqls();
        	Operatedb op = new Operatedb(sobj, "itm");
        	FactoryConfiguration config = new FactoryConfiguration();
        	String tempDB="";
        	String kfDB="";

        	ResultSet rs =op.GetRecordsFromDB("connection_table", "*", " localdb='"+strdb+"'");
        	if(rs.next()){
        		String url=rs.getString("URL");
        		System.out.println("Refresh Databases:"+rs.getString("password"));
        		System.out.println("Refresh Databases:"+rs.getString("username"));
        		config.setPassword(rs.getString("password"));
        		config.setUserName(rs.getString("username"));
        		kfDB=rs.getString("kfdb");
        	}
        	
        	ResultSet rs2 =op.GetRecordsFromDB("registration", "*", " db='"+kfDB+"'");
        	if(rs2.next()){
        		tempDB=rs2.getString("db").trim().replace(".","_").replace(" ", "_").replace("-", "_")+"temp";
        		config.setOrg(rs2.getString("org").trim());
        		String url=rs2.getString("URL");
        		config.setDb(tempDB);
        		tempDB=rs2.getString("org").trim().replace(".","_").replace(" ", "_")+"_"
				+tempDB;
        	}
        
     		String URL="https://kf.utoronto.ca:443/kforum/";
     		ITMService service = new ITMService();
     		boolean flag;
			try {
				flag = service.login(config,URL,"/network/rit/lab/zhanglab/kf5py-master/login.py");
				if(flag){
	     			System.out.print("Login Success");
	     			service.getDataFromKf5Api(config,URL,"/network/rit/lab/zhanglab/kf5py-master/kf5Api.py");
	     		}else{
	     			System.out.print("Login failed");
	     		}
			} catch (InterruptedException | InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
			System.out.print("tempDB :" +tempDB);
			KF5Integration di = new KF5Integration(tempDB);
	    	di.UpdateDatabases(kfDB);;
	    	 DBConfig db= new DBConfig(strdb,tempDB);	
        	System.out.println("RefreshDB Start");
        	//System.out.println("DB is " + strdb);
			db.update(strdb,tempDB);
			//UpdateRefreshTime(strdb);
		} catch (ClassNotFoundException | SQLException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//        String host = null, db = null, username = null, password = null;
//        int port = 0;
//        sqls s = new sqls();
//        Operatedb pqdb = new Operatedb(s, "itm");
//        String strcolum = "URL,kfdb,username,password,port";
//        String strcon = "localdb='" + strdb + "'";
//        ResultSet rs = null;
//        try {
//            rs = pqdb.GetRecordsFromDB("connection_table", strcolum, strcon);
//            if(rs.next()) {
//                host = rs.getString(1);
//                db = rs.getString(2);
//                username = rs.getString(3);
//                password = rs.getString(4);
//                port = rs.getInt(5);
//            }
//
//        } catch(SQLException e) {
//            e.printStackTrace();
//        } finally {
//            if(rs != null) {
//                try {
//                    rs.close();
//                } catch(SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//            if(pqdb != null) {
//                pqdb.CloseCon();
//            }
//            if(s != null) {
//                //s.Close();
//            }
//
//        }
       
        
        // ???????????????????????????????????????????
        // add update function
        // ?????????????????????????????????????????
        
        
//        ZTB tb = K5TBConnector.sGetTB_HTTP_UserName(new K5TBConnector.HostInfo(host, port, db), null, username, password, null);
//        if(tb != null) {
//            UpdateViews(tb, strdb);
//            UpdateNote(tb, strdb);
//            UpdateNoteViewLink(tb, strdb);
//            Updateauthor(tb, strdb);
//            UpdateAuthorNoteLink(tb, strdb);
//            UpdateNoteNoteLink(tb, strdb);
//            UpdateGroups(tb, strdb);
//            UpdateGroupAuthorLink(tb, strdb);
//            UpdateRefreshTime(strdb);
//            tb.close();
//        }

        if(request.getParameter("projectname").equals("null")) {
            String strpage = "/ITM2/en/New_HomePage.jsp?database=" + request.getParameter("database");
            String url = response.encodeURL(strpage);
            response.sendRedirect(url);
        } else {
            String strpage = "/ITM2/en/New_HomePage.jsp?database=" + request.getParameter("database") + "&projectname=" + request.getParameter("projectname");
            String url = response.encodeURL(strpage);
            response.sendRedirect(url);
        }
    }
    /**
     * update the refresh time
     */
    private void UpdateRefreshTime(String strdb) {

        sqls s = new sqls();
        String strtime = null;
        String strsql = null;

        //get current time
        java.util.Date curdate = new java.util.Date();
        SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        strtime = spdtformat.format(curdate);

        Statement stmt = s.Connect("itm");
        String strcon = "dbname='" + strdb + "'";

        strsql = "UPDATE db_table SET refreshtime='" + strtime + "' WHERE dbname='" + strdb + "'";
        System.out.println(strsql);
        try {
            stmt.executeUpdate(strsql);
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            if(stmt != null) {
                try {
                    stmt.close();
                } catch(SQLException e) {
                    e.printStackTrace();
                }

            }
            if(s != null) {
                s.Close();
            }
        }
        return;
    }
    /**
     * update view_table
     * @param tb
     * @param strdb
     */
    private void UpdateViews(ZTB tb, String strdb) {
        System.out.println("updating views from KF");
        ViewObject viewobj;
        int i = 0, num = 0;
        String sqlvalue = null, strcon = null;
        long iid;
        ResultSet rs = null;
        sqls s = new sqls();
        Statement stmt = s.Connect(strdb);
        ResultSet view_id_db = null;
        Retrieve rtrv = new Retrieve();
        //sqls s = new sqls();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        //Get another operation object
        Operatedb dbob = new Operatedb(new sqls(), strdb);
        /*********************write the view objects into database********************/
        List viewList = rtrv.GetViewObject(tb);//get all views from KF

        //get all the view id from the view_table
        String strsql_viewids = "Select idview from view_table";
        try {
            view_id_db = stmt.executeQuery(strsql_viewids);
        } catch(SQLException e) {
            e.printStackTrace();
        }

        int flag = 0;

        //Store the view ids in hash map
        Map<Integer, Integer> check_id = new HashMap<Integer, Integer> ();
        try {
            while(view_id_db.next()) {
                check_id.put(view_id_db.getInt(1), flag);
                //System.out.println(check_id.get(view_id_db.getInt(1)));
            }
        } catch (SQLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        //update views
        for(i = 0; i < viewList.size(); i++) {
            viewobj = (ViewObject)viewList.get(i);
            iid = viewobj.GetID();
            strcon = "idview=" + iid;

            //check if the view id is present in hashmap ,if yes then set flag to 1.
            if(check_id.containsKey((int)iid)) {
                check_id.put((int)iid, 1);
            }
            try {
                rs = dbobject.GetRecordsFromDB("view_table", "count(*)", strcon);
                if(rs.next()) {
                    num = rs.getInt(1);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }

            if(num == 0) {
                sqlvalue = " (`idview`, `title`) VALUES (" + viewobj.GetID() + "," + "'" + viewobj.GetTitle() + "');";
                // System.out.println("the sqlvalue is "+sqlvalue);
                dbobject.WritebacktoDB("view_table", "INSERT", sqlvalue);
            }
        }

        ArrayList<Integer> unwanted_viewids = new ArrayList<Integer>();
        //check for all the hash map keys with flag/value set to 0. Delete those from the database.
        for(Map.Entry<Integer, Integer> entry : check_id.entrySet()) {
            if(entry.getValue() == 0) {
                unwanted_viewids.add(entry.getKey());
            }
        }
        //Delete the unwanted view ids from database
        for(int j = 0; j < unwanted_viewids.size(); j++) {
            String strsql_del = "delete from view_table where idview=" + unwanted_viewids.get(j);
            try {
                stmt.execute(strsql_del);
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        if(rs != null) {
            try {
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(dbobject != null) {
            dbobject.CloseCon();
        }
        return;

    }

    /**
     * update note_table
     */
    private void UpdateNote(ZTB tb, String strdb) {
        System.out.println("updating the note_table");
        NoteObject noteobj;
        String sqlvalue, strcretime, strcon;
        Retrieve rtrv = new Retrieve();
        ResultSet rs = null;
        long iid;
        int num = 0;
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        List noteList = rtrv.GetNoteObject(tb);//get all notes from KF

        //add all view objects into view_table
        for(int i = 0; i < noteList.size(); i++) {
            noteobj = (NoteObject)noteList.get(i);
            iid = noteobj.GetID();
            strcon = "noteid=" + iid;
            try {
                rs = dbobject.GetRecordsFromDB("note_table", "count(*)", strcon);
                if(rs.next()) {
                    num = rs.getInt(1);
                }

                if(num == 0) {
                    strcretime = spdtformat.format(noteobj.GetCretime());
                    sqlvalue = " (`noteid`, `notetitle`,`notecontent`,`createtime`) VALUES (" + noteobj.GetID() + "," + "'"
                               + noteobj.GetTitle() + "','" + noteobj.GetContent() + "','" + strcretime + "');";
                    dbobject.WritebacktoDB("note_table", "INSERT", sqlvalue);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }

        }
        if(rs != null) {
            try {
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(dbobject != null) {
            dbobject.CloseCon();
        }

        return;

    }
    /**
     *update note_view table
     */
    private void UpdateNoteViewLink(ZTB tb, String strdb) {

        System.out.println("update the view_note table ");
        Links linkobj;
        int i, num = 0;
        long noteid, viewid;
        String sqlvalue, strcon;
        ResultSet rs = null;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all view_note link from KF
        List linkList = rtrv.GetViewNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            viewid = linkobj.GetFromID(); //view id
            noteid = linkobj.GetToID();//note id
            strcon = "viewid=" + viewid + " and noteid=" + noteid;
            try {
                rs = dbobject.GetRecordsFromDB("view_note", "count(*)", strcon);
                if(rs.next()) {
                    num = rs.getInt(1);
                }

                if(num == 0) {
                    sqlvalue = " (`noteid`, `viewid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
                    //System.out.println("the sqlvalue is "+sqlvalue);
                    dbobject.WritebacktoDB("view_note", "INSERT", sqlvalue);
                }

            } catch(SQLException e) {
                e.printStackTrace();
            }

        }
        if(rs != null) {
            try {
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(dbobject != null) {
            dbobject.CloseCon();
        }

        return;

    }

    /**
     * update note_note table
     * @param tb
     * @param strdb
     */
    private void UpdateNoteNoteLink(ZTB tb, String strdb) {
        System.out.println("updating the note_to_note table");
        Links linkobj;
        int i, num = 0;
        long fromid, toid;
        String sqlvalue, strcon, strtype;
        ResultSet rs = null;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all note_to_note link from KF
        List linkList = rtrv.GetNoteNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            fromid = linkobj.GetFromID();
            toid = linkobj.GetToID();
            strtype = linkobj.GetLink();
            strcon = "fromnoteid=" + fromid + " and tonoteid=" + toid + " and linktype='" + strtype + "'";

            try {
                rs = dbobject.GetRecordsFromDB("note_note", "count(*)", strcon);
                if(rs.next()) {
                    num = rs.getInt(1);
                }

                if(num == 0) {
                    sqlvalue = " (`fromnoteid`, `tonoteid`,`linktype`) VALUES (" + linkobj.GetFromID() + "," + linkobj.GetToID() + ",'" + linkobj.GetLink() + "');";
                    dbobject.WritebacktoDB("note_note", "INSERT", sqlvalue);
                }

            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(rs != null) {
            try {
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(dbobject != null) {
            dbobject.CloseCon();
        }
        return;
    }

    /**
     * update author_note table
     * @param tb
     * @param strdb
     */
    private void UpdateAuthorNoteLink(ZTB tb, String strdb) {

        System.out.println("updating author_note table");
        Links linkobj;
        int i, num = 0;
        long noteid, authorid;
        String sqlvalue, strcon;
        Retrieve rtrv = new Retrieve();
        ResultSet rs = null;
        //sqls s = new sqls();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all author_note link from KF
        List linkList = rtrv.GetAuthorNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            noteid = linkobj.GetToID();
            authorid = linkobj.GetFromID();
            strcon = "noteid=" + noteid + " and authorid=" + authorid;
            try {
                rs = dbobject.GetRecordsFromDB("author_note", "count(*)", strcon);
                if(rs.next()) {
                    num = rs.getInt(1);
                }

                if(num == 0) {
                    sqlvalue = " (`noteid`, `authorid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
                    dbobject.WritebacktoDB("author_note", "INSERT", sqlvalue);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }

        if(rs != null) {
            try {
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(dbobject != null) {
            dbobject.CloseCon();
        }

        return;

    }

    /**
     * update author_table
     * @param tb
     * @param strdb
     */
    private void Updateauthor(ZTB tb, String strdb) {

        System.out.println("updating author_table");
        AuthorObject authorobj;
        String sqlvalue, strcon;
        ResultSet rs = null;
        int num = 0;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        List authorList = rtrv.GetAuthorObject(tb);//get author from KF

        //add author objects into author_table
        for(int i = 0; i < authorList.size(); i++) {
            authorobj = (AuthorObject)authorList.get(i);
            strcon = "authorid=" + authorobj.GetID();
            try {
                rs = dbobject.GetRecordsFromDB("author_table", "count(*)", strcon);
                if(rs.next()) {
                    num = rs.getInt(1);
                    sqlvalue = "SET `firstname` ='" + authorobj.GetFstname() + "',`lastname` ='" + authorobj.GetLstname() + "',`username` ='" + authorobj.GetUsname() + "',`type`='" + authorobj.GetType() + "'where `authorid` ='" + authorobj.GetID() + "'";
                    dbobject.WritebacktoDB("author_table", "UPDATE", sqlvalue);
                    System.out.println("Updating_author_table..............................");
                }

                if(num == 0) {
                    sqlvalue = " (`authorid`, `firstname`,`lastname`,`username`,`type`) VALUES (" + authorobj.GetID() + "," + "'"
                               + authorobj.GetFstname() + "','" + authorobj.GetLstname() + "','" + authorobj.GetUsname() + "','" + authorobj.GetType() + "');";
                    dbobject.WritebacktoDB("author_table", "INSERT", sqlvalue);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }

        }
        if(rs != null) {
            try {
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(dbobject != null) {
            dbobject.CloseCon();
        }
        return;
    }

    /**
     * update group_table
     * @param tb
     * @param strdb
     */
    private void UpdateGroups(ZTB tb, String strdb) {

        System.out.println("updating group_table");
        GroupObject groupobj;
        String sqlvalue, strcon;
        ResultSet rs = null;
        long id;
        int num = 0;
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        List Gtitlelist = new ArrayList();
        Retrieve rtrv = new Retrieve();
        List groupList = rtrv.GetGroupObject(tb); //get groups from KF

        for(int i = 0; i < groupList.size(); i++) {
            groupobj = (GroupObject)groupList.get(i);
            Gtitlelist.add(groupobj.GetTitle());
            id = groupobj.GetID();
            strcon = "idgroup=" + id;
            try {
                rs = dbobject.GetRecordsFromDB("group_table", "count(*)", strcon);
                if(rs.next()) {
                    num = rs.getInt(1);
                }

                if(num == 0) {
                    //System.out.println("the "+i+"th"+" group title is: "+groupobj.GetTitle());
                    sqlvalue = " (`idgroup`, `title`) VALUES (" + groupobj.GetID() + "," + "'" + groupobj.GetTitle() + "');";
                    //System.out.println("the sqlvalue is "+sqlvalue);
                    dbobject.WritebacktoDB("group_table", "INSERT", sqlvalue);
                }

            } catch(SQLException e) {
                e.printStackTrace();
            }
        }

        if(rs != null) {
            try {
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(dbobject != null) {
            dbobject.CloseCon();
        }

        return;
    }

    /**
     * update author_note table
     * @param tb
     * @param strdb
     */
    private void UpdateGroupAuthorLink(ZTB tb, String strdb) {

        System.out.println("updating group_author table");
        Links linkobj;
        int i, num = 0;
        long author_id, group_id;
        String sqlvalue, strcon;
        Retrieve rtrv = new Retrieve();
        ResultSet rs = null;
        //sqls s = new sqls();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all group_author link from KF
        List linkList = rtrv.GetGroupAuthorLink(tb);

        //add all group_author links into group_author table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            author_id = linkobj.GetToID();
            group_id = linkobj.GetFromID();
            strcon = "author_id=" + author_id + " and group_id=" + group_id;
            try {
                rs = dbobject.GetRecordsFromDB("group_author", "count(*)", strcon);
                if(rs.next()) {
                    num = rs.getInt(1);
                }

                if(num == 0) {
                    sqlvalue = " (`author_id`, `group_id`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
                    dbobject.WritebacktoDB("group_author", "INSERT", sqlvalue);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }

        if(rs != null) {
            try {
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        if(dbobject != null) {
            dbobject.CloseCon();
        }

        return;

    }
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response); // TODO Auto-generated method stub
    }

}
