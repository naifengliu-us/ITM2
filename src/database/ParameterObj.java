/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Stan
 */

public class ParameterObj {
    private String database;
    private String projectName;
    private String threadName;

    public ParameterObj(String database, String projectName, String threadName) {
        this.database = database;
        this.projectName = projectName;
        this.threadName = threadName;
    }
    public ParameterObj() {
        this.database = "";
        this.projectName = "";
        this.threadName = "";
    }

    public String getDatabase() {
        return database;
    }
    protected void setDatabase(String db) {
        this.database = db;
    }

    public String getProjectName() {
        return projectName;
    }
    protected void setProjectName(String pn) {
        this.projectName = pn;
    }

    public String getThreadName() {
        return threadName;
    }
    public void setThreadName(String tn) {
        threadName = tn;
    }


}
