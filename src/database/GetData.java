package database;
/*****this class is used to download data from KF for ITM applet*******/
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import org.zoolib.*;
import org.zoolib.tuplebase.*;

import com.knowledgeforum.k5.common.K5TBConnector;
import database.*;
import code.*;

public class GetData {

    /**
     * Download all views from KF and write the views into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void DownloadViews(ZTB tb, String strdb) {
        System.out.println("downloading views from KF");
        ViewObject viewobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        /*********************write the view objects into database********************/
        List viewList = rtrv.GetViewObject(tb);//get all views from KF

        //add all view objects into view_table
        for(i = 0; i < viewList.size(); i++) {
            viewobj = (ViewObject)viewList.get(i);
            sqlvalue = " (`idview`, `title`) VALUES (" + viewobj.GetID() + "," + "'" + viewobj.GetTitle() + "');";
            // System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("view_table", "INSERT", sqlvalue);
        }

    }
    /**
     * Download all groups from KF and write the groups into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void DownloadGroups(ZTB tb, String strdb) {
        System.out.println("downloading groups from KF");
        GroupObject groupobj;
        String sqlvalue;
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        List Gtitlelist = new ArrayList();
        Retrieve rtrv = new Retrieve();
        List groupList = rtrv.GetGroupObject(tb); //get groups from KF

        for(int i = 0; i < groupList.size(); i++) {
            groupobj = (GroupObject)groupList.get(i);
            Gtitlelist.add(groupobj.GetTitle());
            //System.out.println("the "+i+"th"+" group title is: "+groupobj.GetTitle());
            sqlvalue = " (`idgroup`, `title`) VALUES (" + groupobj.GetID() + "," + "'" + groupobj.GetTitle() + "');";
//            System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("group_table", "INSERT", sqlvalue);
        }

    }

    /**
     * Download all notes from KF and write the views into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void Downloadnotes(ZTB tb, String strdb) {
        System.out.println("downloading the notes from KF");
        NoteObject noteobj;
        String sqlvalue, strcretime;
        //ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat spdtformat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

        List noteList = rtrv.GetNoteObject(tb);//get all notes from KF

        //add all view objects into view_table
        for(int i = 0; i < noteList.size(); i++) {
            noteobj = (NoteObject)noteList.get(i);
            strcretime = spdtformat.format(noteobj.GetCretime());
            sqlvalue = " (`noteid`, `notetitle`,`notecontent`,`createtime`) VALUES (" + noteobj.GetID() + "," + "'"
                       + noteobj.GetTitle() + "','" + noteobj.GetContent() + "','" + strcretime + "');";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("note_table", "INSERT", sqlvalue);
        }


    }

    /**
     * Download all links of notes and views from KF and write the data into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void DownLoadNoteViewLink(ZTB tb, String strdb) {
        System.out.println("downloading the view_note link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all view_note link from KF
        List linkList = rtrv.GetViewNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`noteid`, `viewid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("view_note", "INSERT", sqlvalue);
        }

    }

    /**
     * Download author object from KF and write the them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void Downloadauthor(ZTB tb, String strdb) {
        System.out.println("downloading the information of author from KF");
        AuthorObject authorobj;
        String sqlvalue, strcretime;
        //ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        List authorList = rtrv.GetAuthorObject(tb);//get author from KF

        //add author objects into author_table
        for(int i = 0; i < authorList.size(); i++) {
            authorobj = (AuthorObject)authorList.get(i);
            sqlvalue = " (`authorid`, `firstname`,`lastname`,`username`,`type`) VALUES (" + authorobj.GetID() + "," + "'"
                       + authorobj.GetFstname() + "','" + authorobj.GetLstname() + "','" + authorobj.GetUsname() + "','" + authorobj.GetType() + "');";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("author_table", "INSERT", sqlvalue);
        }


    }

    /**
     * Download all links of notes and authors from KF and write the them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void DownLoadAuthorNoteLink(ZTB tb, String strdb) {
        System.out.println("downloading the author_note link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all author_note link from KF
        List linkList = rtrv.GetAuthorNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`noteid`, `authorid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
            // System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("author_note", "INSERT", sqlvalue);
        }

    }

    /**
     * Download note_to_note links from KF and write them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void DownLoadNoteNoteLink(ZTB tb, String strdb) {
        System.out.println("downloading the note_to_note link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all note_to_note link from KF
        List linkList = rtrv.GetNoteNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`fromnoteid`, `tonoteid`,`linktype`) VALUES (" + linkobj.GetFromID() + "," + linkobj.GetToID() + ",'" + linkobj.GetLink() + "');";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("note_note", "INSERT", sqlvalue);
        }

    }


    /**
     * Download all links of author and group from KF and write the them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void DownLoadGroupAuthorLink(ZTB tb, String strdb) {
        System.out.println("downloading the group_author link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all author_note link from KF
        List linkList = rtrv.GetGroupAuthorLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`author_id`, `group_id`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
            // System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("group_author", "INSERT", sqlvalue);
        }

    }

    /**
     * Download all notes from KF and write the views into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void DownloadAttachments(ZTB tb, String strdb) {
        System.out.println("downloading the attachments from KF");
        AttachmentObject attachmentobj;
        String sqlvalue, strcretime;
        //ZTB t=((Bind)session.getAttribute("tb")).getZTB(); //get a tuplebase connection
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object
        SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat spdtformat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

        List noteList = rtrv.getAttachments(tb);//get all notes from KF

        //add all view objects into view_table
        for(int i = 0; i < noteList.size(); i++) {
            attachmentobj = (AttachmentObject)noteList.get(i);
            strcretime = spdtformat.format(attachmentobj.GetCretime());
            sqlvalue = " (`attachmentid`, `attachmenttitle`,`attachmentfile`,`createtime`) VALUES (" +
                       attachmentobj.GetID() + "," + "'" +
                       attachmentobj.GetTitle() + "','" + attachmentobj.GetFile() + "','" + strcretime + "')";
            //System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("attachment_table", "INSERT", sqlvalue);

        }
    }

    /**
     * Download all links of notes and attachments from KF and write the them into database
     * @param tb
     * 		  tuplebase connection
     * @return
     * 		  void
     */
    public void DownLoadAttachmentNoteLink(ZTB tb, String strdb) {
        System.out.println("Baibhav downloading the Attachment_note link from KF");
        Links linkobj;
        int i;
        String sqlvalue;
        Retrieve rtrv = new Retrieve();
        Operatedb dbobject = new Operatedb(new sqls(), strdb); //get one database operation object

        //get all author_note link from KF
        List linkList = rtrv.GetAttachmentNoteLink(tb);

        //add all view_note links into view_note table
        for(i = 0; i < linkList.size(); i++) {
            linkobj = (Links)linkList.get(i);
            sqlvalue = " (`noteid`, `attachmentid`) VALUES (" + linkobj.GetToID() + "," + +linkobj.GetFromID() + ");";
            // System.out.println("the sqlvalue is "+sqlvalue);
            dbobject.WritebacktoDB("attachment_note", "INSERT", sqlvalue);
        }

    }


}
