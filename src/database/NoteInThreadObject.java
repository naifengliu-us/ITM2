package database;

import java.util.*;

public class NoteInThreadObject {

    private String strID;
    private String title;
    private String content;
    private String create_time;
    private String firstname;
    private String lastname;
    private int ishightlight;

    private Vector<Integer> anvec = new Vector<Integer>();
    private Vector<Integer> buvec = new Vector<Integer>();
    private Vector<Integer> revec = new Vector<Integer>();

    public NoteInThreadObject() {
        anvec = new Vector<Integer>();
        buvec = new Vector<Integer>();
        revec = new Vector<Integer>();
    }

    public void  AddToAnVector(int id) {
        this.anvec.add(id);
    }
    public Vector<Integer> GetAnVector() {
        return this.anvec;
    }

    public void  AddToBuVector(int id) {
        this.buvec.add(id);
    }
    public Vector<Integer> GetBuVector() {
        return this.buvec;
    }

    public void  AddToReVector(int id) {
        this.revec.add(id);
    }
    public Vector<Integer> GetReVector() {
        return this.revec;
    }

    public void SetID(String strID) {
        this.strID = strID;
    }

    public String GetID() {
        return this.strID;
    }

    public void SetTitle(String title) {
        this.title = title;
    }

    public String GetTitle() {
        return this.title;
    }

    public void SetContent(String content) {
        this.content = content;
    }

    public String GetContent() {
        return this.content;
    }

    public void SetCretime(String Cretime) {
        this.create_time = Cretime;
    }

    public String GetCretime() {
        return this.create_time;
    }

    public void SetFirstName(String strfirst) {
        this.firstname = strfirst;
    }

    public String GetFirstName() {
        return this.firstname;
    }

    public void SetLastName(String strlast) {
        this.lastname = strlast;
    }

    public String GetLastName() {
        return this.lastname;
    }

    public void SetHighLight(int highlight) {
        this.ishightlight = highlight;
    }

    public int GetHighLight() {
        return this.ishightlight;
    }

}
