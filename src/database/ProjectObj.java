package database;

import java.sql.Timestamp;

import itm.models.Project;

public class ProjectObj implements Project {
    private String[] groups;
    private String name;
    private String teacher;
    private String school;
    private String[] grades;
    private String schoolYear;
    private String db;
    private int idPublish;
    private String grade;
    private String fromYear;
    private String toYear;
    private String threads;
    private Timestamp timestamp;

    public void SetPublishId(int idPublish) {
        this.idPublish = idPublish;
    }
    public int GetPublishId() {
        return this.idPublish;
    }

    public void SetTeacher(String strteacher) {
        this.teacher = strteacher;
    }

    public String getTeacher() {
        return this.teacher;
    }

    public void SetSchool(String strschool) {
        this.school = strschool;
    }

    public String getSchool() {
        return this.school;
    }

    public void SetGroups(String[] strgroup) {
        for(int i = 0; i < strgroup.length; i++) {
            this.groups[i] = strgroup[i];
        }
    }

    public String[] GetGroups() {
        return this.groups;
    }

    public void setGrades(String[] strgrades) {
        this.grades = new String[strgrades.length];
        int length = grades.length;
        int i = 0;
        while(i < length) {
            grades[i] = strgrades[i];
            i++;
        }
    }

    public String[] getGrades() {
        return this.grades;
    }

    public void setGrade(String[] strgrades) {
        this.grades = new String[strgrades.length];
        int length = grades.length;
        int i = 0;
        this.grade = "";
        while(i < length) {
            grade += strgrades[i] + " ";
            i++;
        }
        grade = grade.trim();
    }

    public String getGrade() {
        return this.grade;
    }

    public void SetSchoolYear(String strschool) {
        this.schoolYear = strschool;
    }
    public String getSchoolYear() {
        return this.schoolYear;
    }

    public void SetFromYear(String fromYear) {
        this.fromYear = fromYear;
    }
    public String getFromYear() {
        return this.fromYear;
    }

    public void SetToYear(String toYear) {
        this.toYear = toYear;
    }
    public String getToYear() {
        return this.toYear;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String pName) {
        this.name = pName;
    }

    @Override
    public String getDB() {
        return db;
    }

    @Override
    public void setDB(String db) {
        this.db = db;
    }

    @Override
    public String getThreads() {
        return threads;
    }

    @Override
    public void setThreads(String threads) {
        this.threads = threads;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp ts) {
        timestamp = ts;
    }
}