/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import code.Publish;
import code.sqls;
import itm.controllers.SavedPublishProject;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.sql.rowset.serial.SerialBlob;

/**
 * publish project DB
 */
public class PublishDB {
    public  static int InsertSavePublish(SavedPublishProject pp) {
        sqls sql = new sqls();
        Connection conn = sql.getConn(pp.getDB());
        PreparedStatement ps = null;  
        PreparedStatement pss = null; 
        String query;
        String query1;
        ResultSet rs = null;
       
        
        //MayDo Check if project already existed on publish table
        /*
        
        query="select publish_id from itm.publish where user_id=? && project_name=? order by publish_id DESC limit 1;";
        int pid;
        try {       	
            
			ps = conn.prepareStatement(query);
			ps.setString(1, pp.getUserID());
            ps.setString(2, pp.getProjectName());
            System.out.println("33333333333333333333333333333333333333");
            rs = ps.executeQuery();
           
            
            System.out.println("22222222222222222222");
            
            if(rs.next()) {         
            pid=rs.getInt(1);          
            query1 = "insert into itm.publish (project_name,showID,threads_name,date,user_id, attachment_title, attachment_path, encrypt,published) values (?,?,?,NOW(),?, ?, ?, ?,0)";
            String threadsName = "";
            String[] threads = pp.getThreadsName();
            if(threads != null) {
               if (threads.length > 1) {
                   for (String t : threads) {
                       threadsName += t + ".and^";
                   }
                   threadsName = threadsName.substring(0, threadsName.length() - 5);
               } else if(threads.length == 1) {
                   threadsName = threads[0];
               }
            }
           // try{
            pss = conn.prepareStatement(query1);
            pss.setString(1, pp.getProjectName());
            pss.setBoolean(2, pp.getShowID());
            pss.setString(3, threadsName);
            pss.setString(4, pp.getUserID());
            pss.setString(5, pp.getAttachmentTitle());
            pss.setString(6, pp.getAttachmentPath());
            pss.setBoolean(7, !pp.getShowID());
            System.out.println(pss.toString());
            pss.executeUpdate();
       
            query1="update itm.publish set publish_id="+pid+" where user_id=? && project_name=? order by publish_id DESC limit 1;";
            pss = conn.prepareStatement(query1);
            pss.executeUpdate();
            DBUtil.closePreparedStatement(pss);
           /*    }
            finally {
                DBUtil.closePreparedStatement(ps);
                sql.Close();
            }
            return pid;
            }
            else{
            	
         query = "insert into itm.publish (project_name,showID,threads_name,date,user_id, attachment_title, attachment_path, encrypt,published) values (?,?,?,NOW(),?, ?, ?, ?,0)";
         //System.out.print("  //MayDo Check if project already existed on publish table");
         System.out.print(query);
         String threadsName = "";
         String[] threads = pp.getThreadsName();
         if(threads != null) {
            if (threads.length > 1) {
                for (String t : threads) {
                    threadsName += t + ".and^";
                }
                threadsName = threadsName.substring(0, threadsName.length() - 5);
            } else if(threads.length == 1) {
                threadsName = threads[0];
            }
        }
        pid=0;
        try {
            ps = conn.prepareStatement(query);
            ps.setString(1, pp.getProjectName());
            ps.setBoolean(2, pp.getShowID());
            ps.setString(3, threadsName);
            ps.setString(4, pp.getUserID());
            ps.setString(5, pp.getAttachmentTitle());
            ps.setString(6, pp.getAttachmentPath());
            ps.setBoolean(7, !pp.getShowID());
            System.out.println(ps.toString());
            ps.executeUpdate();
            DBUtil.closePreparedStatement(ps);

            //get pid
            query = "SELECT publish_id FROM itm.publish where user_id=? && project_name=? order by publish_id DESC limit 1;";
            ps = conn.prepareStatement(query);
            ps.setString(1, pp.getUserID());
            ps.setString(2, pp.getProjectName());
            System.out.println(ps.toString());
            rs = ps.executeQuery();

            if(rs.next())
                pid = rs.getInt("publish_id");
            return pid;
        } catch(SQLException e) {
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            sql.Close();
        }
            }
            }catch(SQLException e) {
                    return 0;
                } finally {
                    DBUtil.closePreparedStatement(ps);
                    sql.Close();
                }
       	
            	
            	
            }
           
        */
		
    
 
        query = "insert into itm.publish (project_name,showID,threads_name,date,user_id, attachment_title, attachment_path, encrypt,published) values (?,?,?,NOW(),?, ?, ?, ?,0)";
        //System.out.print("  //MayDo Check if project already existed on publish table");
        System.out.print(query);
        String threadsName = "";
        String[] threads = pp.getThreadsName();
        if(threads != null) {
            if (threads.length > 1) {
                for (String t : threads) {
                    threadsName += t + ".and^";
                }
                threadsName = threadsName.substring(0, threadsName.length() - 5);
            } else if(threads.length == 1) {
                threadsName = threads[0];
            }
        }
        int pid=0;
        try {
            ps = conn.prepareStatement(query);
            ps.setString(1, pp.getProjectName());
            ps.setBoolean(2, pp.getShowID());
            ps.setString(3, threadsName);
            ps.setString(4, pp.getUserID());
            ps.setString(5, pp.getAttachmentTitle());
            ps.setString(6, pp.getAttachmentPath());
            ps.setBoolean(7, !pp.getShowID());
            System.out.println(ps.toString());
            ps.executeUpdate();
            DBUtil.closePreparedStatement(ps);

            //get pid
            query = "SELECT publish_id FROM itm.publish where user_id=? && project_name=? order by publish_id DESC limit 1;";
            ps = conn.prepareStatement(query);
            ps.setString(1, pp.getUserID());
            ps.setString(2, pp.getProjectName());
            System.out.println(ps.toString());
            rs = ps.executeQuery();
         
            if(rs.next())
                pid = rs.getInt("publish_id");
            return pid;
        } catch(SQLException e) {
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            sql.Close();
        }
    }

    public static int InsertTeacherReflection(int pid, SavedPublishProject pp) {
        //insert teacherRef data
        sqls sql = new sqls();
        Connection conn = sql.getConn(pp.getDB());
        PreparedStatement ps = null;
        String query = "";
        
        query = "insert into itm.teacher_reflection (bigIdea,facilitated,helpfulActivities,lessonsLearned,publish_publish_id) values (?,?,?,?,?);";
        LinkedList<String> tr2 = pp.getTeacherReflection();
        String query2="UPDATE itm.project  SET bigIdea="+tr2.get(0)+",facilitated="+tr2.get(1)+",helpfulActivities="+tr2.get(2)+", lessonsLearned="+tr2.get(3)+", WHERE some_column=some_value; ";
       
       try {
            ps = conn.prepareStatement(query);
            LinkedList<String> tr = pp.getTeacherReflection();
            for(int i = 0; i < 4; i++) {
                ps.setString(i + 1, tr.get(i));
            }
            ps.setInt(5, pid);
//            ps.setString(1, );
            System.out.println(ps.toString());
            return ps.executeUpdate();
        }   catch(SQLException e) {
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            sql.Close();
        }
    }

    public static int InsertPublishedProject(Publish spp, String id, int pid, String db, SavedPublishProject sdpp) throws Exception {
        String bigIdea = "";
        String lessonsLearned = "";
        String helpfulActivities = "";
        String facilitated = "";
        String attachmentPath = "";
        String attachmentTitle = "";

         System.out.println("======InsertPublishedProject=================================================");
        LinkedList<String> tr = PublishDB.SelectTeacherReflection(pid, db);
        
        System.out.println("PublishDB.SelectTeacherReflection :"+tr.size());
        if (tr != null) {
            for (int i = 0; i < tr.size(); ++i) {
                switch (i) {
                case 0:
                    bigIdea = tr.get(i);
                    break;
                case 1:
                    facilitated = tr.get(i);
                    break;
                case 2:
                    helpfulActivities = tr.get(i);
                    break;
                case 3:
                    lessonsLearned = tr.get(i);
                    break;
                case 4:
                    attachmentPath = tr.get(i);
                    break;
                case 5:
                    attachmentTitle = tr.get(i);
                    break;
                }
            }
        }

        sqls sql = new sqls();
        Connection conn = sql.getConn(spp.getDB());
        PreparedStatement ps = null;
        PreparedStatement ss=null;

        // Get all published threads
        String threadsName[] = spp.getThreadsName();

        // Get highlighted option
        String highlightsOnly[] = spp.getHighlightsOption();
        
        ResultSet rs = null;
        ResultSet rss = null;
        /// NOTE Show ID is used only for the encryption
        Boolean showID = spp.getShowID();
        String query = "";
        String query1="";
        String query2="";
        
        String projectData = "";
        try {

        	///d Create blob of thread.
            for (int i = 0; i < threadsName.length; i++) {
                String threadData = "";
                query ="select * from (select thread_note.projectid,threadfocus,thread_note.noteid,highlight,notetitle," +
                        "notecontent,offset,createtime, firstname, lastname from thread_note,note_table,author_table, author_note," +
                        "project where thread_note.noteid = note_table.noteid and author_note.authorid = author_table.authorid and " +
                        "author_note.noteid = note_table.noteid and thread_note.threadfocus ='" + threadsName[i] +
                        "' and projectName='" + spp.getProjectName() + "')" + "AS T group by noteid";
                if (highlightsOnly[i] == "true") {
                	
                    System.out.println("Highlight is on for " +  threadsName[i] + "  " +  highlightsOnly[i]);
                    query += " and highlight=1";
                }
                System.out.println("InsertPublishedProject qurery is " + query);

                // Create blob here
                // Create blob here
                Statement stmt = conn.createStatement();
                rs = stmt.executeQuery(query);
         
                while (rs.next()) {
                    System.out.println(rs.getString("notetitle"));
                    System.out.println(rs.getString("notecontent"));
                    threadData += rs.getString("noteid") + ";" + rs.getString("highlight") + ";" +
                                  rs.getString("createtime") + ";" + rs.getString("notetitle") + ";" +
                                  rs.getString("notecontent") + ";" + rs.getString("firstname") + ";" +
                                  rs.getString("lastname") + ";$";
                }
                projectData = threadsName[i] + "=" + threadData + "@" + projectData;
            }

            
            // show ID
            query="select projectname,teacher,school,fromyear,toyear,projectowner from project where  projectName='" + spp.getProjectName() + "'";
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
           String[] opt = new String[6];
            while (rs.next()) {
                opt[0]=rs.getString("projectname");
                opt[1]=rs.getString("teacher");
                opt[2]=rs.getString("school");
                opt[3]=rs.getString("fromyear");
                opt[4]=rs.getString("toyear");
                opt[5]=rs.getString("projectowner");
                           
            }
            /*
            query1="select itm.project.idPublish from itm.project where projectname ='"+spp.getProjectName()+"'";
            stmt = conn.createStatement();
            rss = stmt.executeQuery(query1);
            if(rss.next()) {         
               // pid=rss.getInt("idPublish"); 
                query="Delete from itm.project where projectname ='"+spp.getProjectName()+"'";
            ss = conn.prepareStatement(query);
            ss.executeUpdate();
             }
            */
            query = "insert into itm.project (projectname,teacher,school,fromyear,toyear,projectowner,KF_url,idPublish,grade,data,timestamp,bigIdea,lessonsLearned,helpfulActivities,facilitated,attachment_path,attachment_title,encrypt) values("
            		+ "'"+spp.getProjectName()+"','"+opt[1]+"','"+opt[2]+"','"+opt[3]+"','"+opt[4]+"','"+opt[5]+"','"+spp.getDB()+"','"+pid+"','"+spp.getGrade()+"',?,'"+new Timestamp(System.currentTimeMillis())+"','"
            		+bigIdea+"','"+lessonsLearned+"','"+helpfulActivities+"','"+facilitated+"','"+attachmentPath.replace("\\", "\\\\")+"','"+attachmentTitle+"',?)";
           
            //System.out.println("PublishDB.SelectTeacherReflection query:["+query+"]");
            
           // System.out.println("InsertPublishedProject qurery is " + query);
           // System.out.println("address is "+attachmentPath);
            ps = conn.prepareStatement(query);
            ps.setBlob(1, new SerialBlob(projectData.getBytes()));
            ps.setBoolean(2, !showID );
            
            ps.executeUpdate();
            
            
            ///new lines for lili Guo
            
           query1="Delete from itm.project2 where projectname ='"+spp.getProjectName()+"'";             
            	
            ss = conn.prepareStatement(query1);
            ss.executeUpdate();
            System.out.println("deletequrery is " + query1);
            
             
          //  query2 = "insert into itm.project2 (projectname,teacher,school,fromyear,toyear,projectowner,KF_url,idPublish,grade,data,timestamp,bigIdea,lessonsLearned,helpfulActivities,facilitated,attachment_path,attachment_title,encrypt) values("
          //  		+ "'"+spp.getProjectName()+"','"+opt[1]+"','"+opt[2]+"','"+opt[3]+"','"+opt[4]+"','"+opt[5]+"','"+spp.getDB()+"','"+pid+"','"+spp.getGrade()+"',?,'"+new Timestamp(System.currentTimeMillis())+"','"
          //  		+bigIdea+"','"+lessonsLearned+"','"+helpfulActivities+"','"+facilitated+"','"+attachmentPath.replace("\\", "\\\\")+"','"+attachmentTitle+"',?)";
      //
            query2="insert into itm.project2 select * from itm.project where itm.project.idProject in(select Max(idProject) from itm.project);";
            ps = conn.prepareStatement(query2);
          // ps.setBlob(1, new SerialBlob(projectData.getBytes()));
           // ps.setBoolean(2, !showID );
            ps.executeUpdate();
            
            
           ////// 
            
        
            /// NOTE Show ID is used only for the encryption
            // else {
            //     query = "insert into itm.project (projectname,teacher,school,fromyear,toyear,KF_url,idPublish,projectowner,grade) " +
            //             "select projectname,teacher,school,fromyear,toyear,?,?,?,? from project where projectname=?;";
            //     ps = conn.prepareStatement(query);
            //     ps.setString(1, spp.getDB());
            //     ps.setInt(2, pid);
            //     ps.setString(3, id);
            //     ps.setString(4, spp.getGrade());
            //     ps.setString(5, spp.getProjectName());
            // }
            //ps.executeUpdate();
            
            

            // Add blob data to the entry
          /*  query = "update itm.project set data=?, timestamp=?, bigIdea=?, lessonsLearned=?, " +
                    "helpfulActivities=?, facilitated=?, attachment_path=?, attachment_title=?, " +
                    "encrypt=? where projectname=? and idProject=?";
            ps = conn.prepareStatement(query);
            ps.setBlob(1, new SerialBlob(projectData.getBytes()));
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setString(3, bigIdea);
            ps.setString(4, lessonsLearned);
            ps.setString(5, helpfulActivities);
            ps.setString(6, facilitated);
            ps.setString(7, attachmentPath);
            ps.setString(8, attachmentTitle);
            ps.setBoolean(9, !showID);
            ps.setString(10, spp.getProjectName());
            ps.setInt(11, pid);
            ps.executeUpdate(); */

            query="select idproject,projectowner from itm.project where projectname='"+spp.getProjectName()+"' and idPublish='"+pid+"'";
            Statement st = conn.createStatement();
            rs=st.executeQuery(query);
            int idProject=-1;
            String author="";
            while(rs.next()){
            	idProject=rs.getInt(1);
            	author=rs.getString(2);
            }
            if(idProject==-1){
            	throw new Exception("The project has not been published");
            }else{
            	///d Create blob of thread.
                for (int i = 0; i < threadsName.length; i++) {
                    String threadData = "";
                    query = "select thread_note.projectid,threadfocus,thread_note.noteid,highlight,notetitle," +
                            "notecontent,offset,createtime, firstname, lastname from thread_note,note_table,author_table, author_note," +
                            "project where thread_note.noteid = note_table.noteid and author_note.authorid = author_table.authorid and " +
                            "author_note.noteid = note_table.noteid and thread_note.threadfocus ='" + threadsName[i] +
                            "' and projectName='" + spp.getProjectName() + "'";
                    if (highlightsOnly[i] == "true") {
                        System.out.println("Highlight is on for " +  threadsName[i] + "  " +  highlightsOnly[i]);
                        query += " and highlight=1";
                    }
                    System.out.println("InsertPublishedProject qurery is " + query);

                    // Create blob here
                
                    rs = stmt.executeQuery(query);
                    List<String> note_tables =new ArrayList<String>();
                    List<String> project_threads = new ArrayList<String>();
                    List<String> thread_notes =new ArrayList<String>();
                   
                    
                 
                    String project_thread="";
                    String thread_note="";
                    
                    String note_table="";
                    
                    project_thread ="insert into itm.project_thread (projectid,threadfocus,author,projectname) values("+idProject+",'"+
                			threadsName[i].replace("'", "\\'").replace("\"", "\\'")+"','"
                        	+author+"','"
                        	+spp.getProjectName() +"')";
                	project_threads.add(project_thread);
                	  System.out.println("project_thread qurery is " + project_thread);
                	
                    while (rs.next()) {
                    	
                    	note_table="insert into itm.note_table (noteid,notetitle,notecontent,offset,createtime,projectid,threadname)values("+rs.getInt("noteid")+",'"+
                    			rs.getString("notetitle").replace("'", "\\'").replace("\"", "\\'")
                    			+"','"+
                    			rs.getString("notecontent").replace("'", "\\'").replace("\"", "\\'")
                    			+"','"+
                    			rs.getString("offset")
                    			+"','"+
                    			rs.getDate("createtime")
                    			+"',"+idProject+",'"+threadsName[i]+"');";
                    	System.out.println("note_note qurery is " + note_table);
                    	note_tables.add(note_table);
                    	
                    	thread_note ="insert into itm.thread_note (projectid,threadfocus,note_id,highlight,addtime) values("+idProject+",'"+
                    	rs.getString("threadfocus").replace("'", "\\'").replace("\"", "\\'")+"',"
                    	+rs.getInt("noteid")+","
                    	+ rs.getInt("highlight")+",'"
                    	+rs.getDate("createtime")+"');";
                    	thread_notes.add(thread_note);
                    	
                    	
                    	// note_note="insert into itm.note_note select * from builder_ikit_org_ges_2015_2016.note_note where fromnoteid and tonoteid in(select note_id from itm.thread_note)";
                        // note_notes.add(note_note); 
                    	
                   } 
                  
                    List<String> thread_notes_Without_Dup = new ArrayList<String>(new HashSet<String>(thread_notes));
                    List<String> project_threads_Without_Dup = new ArrayList<String>(new HashSet<String>(project_threads));
                    List<String> note_tables_Without_Dup = new ArrayList<String>(new HashSet<String>(note_tables));
                   // List<String> note_notes_Without_Dup = new ArrayList<String>(new HashSet<String>(note_notes));
                   
                    for (String insertSql : thread_notes_Without_Dup){
                    	System.out.println(insertSql);
                    	  stmt.executeUpdate(insertSql);
                    }
                    
                    for (String insertSql : project_threads_Without_Dup){
                    	System.out.println("project_threads_Without_Dup"+insertSql);
                  	  stmt.executeUpdate(insertSql);
                  }
                    
                    for (String insertSql : note_tables_Without_Dup){
                    	System.out.println(insertSql);
                    	  stmt.executeUpdate(insertSql);
                    }
                    
                   // for (String insertSql : note_notes_Without_Dup){
                    //	System.out.println(insertSql);
                    //	  stmt.executeUpdate(insertSql);
                  //  }
               
                }
            }
                        // update save list
            DBUtil.closePreparedStatement(ps);
            //liliguo
            /*
            ArrayList<Integer> hh = new ArrayList<Integer>();
            query="select note_id from itm.thread_note ";
            Statement stt = conn.createStatement();
            rs=stt.executeQuery(query);
            while (rs.next()) {
            System.out.println("noteid is "+rs.getInt("note_id"));
            hh.add(rs.getInt("note_id"));
                              }
            
            System.out.println("noteid is "+hh);
            ArrayList<String> gg = new ArrayList<String>();
            String kk="";
            query="select fromnoteid, tonoteid from builder_ikit_org_ges_2015_2016.note_note ";
            Statement sttt = conn.createStatement();
            rs=sttt.executeQuery(query);
            while (rs.next()) {
            //System.out.println("fronoteid is "+rs.getInt("fromnoteid")+rs.getInt("tonoteid"));
            kk=rs.getString("fromnoteid")+"#"+rs.getInt("tonoteid");
            //System.out.println("fromtonoteid is"+kk);
            gg.add(kk);
           
                              }
            System.out.println("fromtonoteid is"+gg);
            */
          
            query="insert into itm.note_note (fromnoteid, tonoteid, linktype) select fromnoteid, tonoteid, linktype from note_note where fromnoteid and tonoteid in(select note_id from itm.thread_note) And fromnoteid not in(select fromnoteid from itm.note_note) And tonoteid not in(select tonoteid from itm.note_note);";
           ss = conn.prepareStatement(query);
           ss.executeUpdate();
           System.out.println("note_note qurery is " + query);
           
            
            query="insert into itm.publish_author_note select * from author_note where noteid in(select note_id from itm.thread_note) and noteid not in(select noteid from itm.publish_author_note)";
            ss = conn.prepareStatement(query);
            ss.executeUpdate();
            System.out.println("author_note qurery is " + query);
            
            query="insert into itm.publish_author_table (authorid, firstname, lastname, username, type) select authorid, firstname, lastname, username, type from author_table where authorid in(select authorid from itm.publish_author_note) and authorid not in(select authorid from itm.publish_author_table )";
            ss = conn.prepareStatement(query);
            ss.executeUpdate();
            //////
            
            
            
            query = "UPDATE itm.publish SET published=1 WHERE publish_id=?;";
            ps = conn.prepareStatement(query);
            ps.setInt(1, pid);
            return ps.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }
    }

    public static String extractThreadNames(Blob blob) {
        if (blob == null) {
            return null;
        }

        try {
            String data = new String(blob.getBytes(1, (int) blob.length()));
            String[] projectsData = data.split("@");
            String threadNames = "";
            for (int i = 0; i < projectsData.length; ++i) {
                String[] threadsData = projectsData[i].split("$");
                for (int j = 0; j < threadsData.length; ++j) {
                    String[] threadData = threadsData[j].split("=");

                    if (threadData != null) {
                        threadNames += threadData[0] + "$";
                    }
                }
            }

            return threadNames;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public static LinkedList<ProjectObj> SelectPublishedProject(String pname, String loc) {
        LinkedList<ProjectObj> projects = new LinkedList<ProjectObj>();
        sqls sql = new sqls();
        Connection conn = sql.getConn("itm");
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        pname = "%" + pname + "%";
        //Parse search location
        int search = 0;
        search = Integer.parseInt(loc);
        try {
            switch(search) {
            case 1:
                //anywhere
                query = "select projectname,teacher,school,fromyear,toyear,grade,KF_URL,idPublish, data, timestamp from itm.project "
                        + "where (projectname like ?"
                        + "or KF_URL like ?) and timestamp in (select max(timestamp) from itm.project group by projectname, KF_URL);";
                ps = conn.prepareStatement(query);
                System.out.print("query:"+query);
                System.out.print("pname:"+pname);
                ps.setString(1, pname);
                ps.setString(2, pname);
                break;
            case 2:
                //topic
                query = "select projectname,teacher,school,fromyear,toyear,grade,KF_URL,idPublish, data, timestamp from itm.project where projectname like ? " +
                        "and timestamp in (select max(timestamp) from itm.project group by projectname, KF_URL);";
                ps = conn.prepareStatement(query);
                ps.setString(1, pname);
                System.out.print("query:"+query);
                System.out.print("pname:"+pname);
                break;
            case 3:
                //KF
                query = "select projectname,teacher,school,fromyear,toyear,grade,KF_URL,idPublish, data, timestamp from itm.project where KF_URL like ? " +
                        "and timestamp in (select max(timestamp) from itm.project group by projectname, KF_URL);";
                ps = conn.prepareStatement(query);
                ps.setString(1, pname);
                System.out.print("query:"+query);
                System.out.print("pname:"+pname);
                break;
            }

            rs = ps.executeQuery();
            while(rs.next()) {
                ProjectObj project = new ProjectObj();
                project.setName(rs.getString(1));
                project.SetTeacher(rs.getString(2));
                project.SetSchool(rs.getString(3));
                project.SetSchoolYear(ModifyDate(rs.getString(4), rs.getString(5)));
                project.SetFromYear(rs.getString(4));
                project.SetToYear(rs.getString(5));
                String grade = rs.getString(6);
                String[] grades = new String[1];
                grades[0] = grade;
                project.setGrades(grades);
                project.setDB(rs.getString(7));
                project.SetPublishId(rs.getInt(8));
                project.setThreads(PublishDB.extractThreadNames(rs.getBlob(9)));
                project.setTimestamp(rs.getTimestamp("timestamp"));
                projects.add(project);
            }
            return projects;
        } catch(SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }
    }

    public static String GetPublishedProjectThreadData(String pname, String threadname,
            String timestamp) {
        ProjectObj projects = new ProjectObj();
        sqls sql = new sqls();
        Connection conn = sql.getConn("itm");
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        query = "select data from itm.project where projectname = ? and timestamp = ?";
        System.out.println(query);
        String threadData = "";
        try {
            ps = conn.prepareStatement(query);
            ps.setString(1, pname);

            // Feb 27, 2014 8:32:03 PM
            SimpleDateFormat datetimeFormatter1 = (SimpleDateFormat) SimpleDateFormat.getDateInstance(0, Locale.ENGLISH);

  		  datetimeFormatter1.applyPattern(  "MMM dd, yyyy hh:mm:ss a");
            //SimpleDateFormat datetimeFormatter1 = new SimpleDateFormat(
            //    "MMM dd, yyyy hh:mm:ss a");
            java.util.Date fromdate;
            try {
                fromdate = (java.util.Date) datetimeFormatter1.parse(timestamp);
                Timestamp ts = new Timestamp(fromdate.getTime());
                ps.setTimestamp(2, ts);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                //ps.setString(2, null);
            }

            rs = ps.executeQuery();
            if(rs.next()) {
                Blob data = rs.getBlob(1);
               
                String projectData = new String(data.getBytes(1,  (int)data.length())).replaceAll("\\<.*?>","");
               System.out.println("projectData:/*/*/*  "+projectData);
                String[] threadsData = projectData.split("@");
                System.out.println("projectData:/*/*/*  "+threadsData.length);
                for (int i = 0; i < threadsData.length; ++i) {
                	
                    String[] threadKeyValue = threadsData[i].split("=");
                    System.out.println("projectData:/*/*/*  "+threadKeyValue[0]);
                    if (threadKeyValue[0].equals(threadname)) {
                        threadData = threadKeyValue[1];

                        break;
                    }
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }
        return threadData;
    }

    public static boolean IsPublishedProjectEcrypted(String pname, String threadname,
            String timestamp) {
        sqls sql = new sqls();
        Connection conn = sql.getConn("itm");
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        boolean encrypt = false;
        query = "select encrypt from itm.project where projectname = ? and timestamp = ?";
        try {
            ps = conn.prepareStatement(query);
            ps.setString(1, pname);

            // Feb 27, 2014 8:32:03 PM
            SimpleDateFormat datetimeFormatter1 = (SimpleDateFormat) SimpleDateFormat.getDateInstance(0, Locale.ENGLISH);

    		  datetimeFormatter1.applyPattern(  "MMM dd, yyyy hh:mm:ss a");
            java.util.Date fromdate;
            try {
                fromdate = (java.util.Date) datetimeFormatter1.parse(timestamp);
                Timestamp ts = new Timestamp(fromdate.getTime());
                ps.setTimestamp(2, ts);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                //ps.setString(2, null);
            }

            rs = ps.executeQuery();
            if(rs.next()) {
              encrypt = rs.getBoolean("encrypt");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }
        return encrypt;
    }

    public static int InsertGrade(int pid, String grade) {
        sqls sql = new sqls();
        Connection conn = sql.getConn("itm");
        PreparedStatement ps = null;
        String query = "";
        try {
            query = "insert into itm.project_grade (projectid,grade) values (?,?);";
            ps = conn.prepareStatement(query);;
            ps.setInt(1, pid);
            ps.setString(2, grade );
            return ps.executeUpdate();
        } catch(SQLException e) {
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            sql.Close();
        }
    }


    public static LinkedList<SavedPublishProject> SelectSavesList(String user, String db) {
        sqls sql = new sqls();
        Connection conn = sql.getConn(db);
        PreparedStatement ps = null;
        ResultSet rs = null;
        LinkedList<SavedPublishProject> saves = new LinkedList<SavedPublishProject>();
        try {
            String query = "SELECT project_name,date,publish_id FROM itm.publish where user_id=? and published!=1 order by date DESC";
            ps = conn.prepareStatement(query);
            ps.setString(1, user);
            rs = ps.executeQuery();
            while(rs.next()) {
                SavedPublishProject spp = new SavedPublishProject();
                spp.setName(rs.getString(1));
                //spp.setShowID(rs.getBoolean(3));
                spp.setDate(rs.getDate(2));
                spp.setPID(rs.getInt(3));
                //spp.setThreadsName(rs.getString(5));
                saves.add(spp);
            }
            return saves;
        } catch(SQLException e) {
            return null;
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }

    }


    public static LinkedList<ProjectObj> SelectOtherProjects(String pname, String db) {
        LinkedList<ProjectObj> otherProjects = new LinkedList<ProjectObj>();
        sqls sql = new sqls();
        Connection conn = sql.getConn(db);
        PreparedStatement ps = null;
        ResultSet rs = null;
        ProjectObj project = new ProjectObj();
        try {
            String query = "Select projectname,grade,teacher,school,fromyear,toyear from itm.project,project_grade where project.idproject = project_grade.projectid and projectname!=?;";
            ps = conn.prepareStatement(query);
            ps.setString(1, pname);
            rs = ps.executeQuery();
            while(rs.next()) {
                project.setName(rs.getString(1));
                String[] grades = new String[1];
                String grade = rs.getString(2);
                grades[0] = grade;
                project.setGrade(grades);
                project.SetTeacher(rs.getString(3));
                project.SetSchool(rs.getString(4));
                project.SetSchoolYear(ModifyDate(rs.getString(5), rs.getString(6)));
                otherProjects.add(project);
            }
            return otherProjects;
        } catch(SQLException e) {
            return null;
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }
    }

    public static String SelectGrade(String pname, String db) {
        String grade = "";
        sqls sql = new sqls();
        Connection conn = sql.getConn(db);
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String query = "SELECT grade FROM project_grade as g inner join itm.project as p on(g.projectid=idproject) where projectname=?;";
            ps = conn.prepareStatement(query);
            ps.setString(1, pname);
            rs = ps.executeQuery();
            if(rs.next()) {
                grade = rs.getString(1);
            }
            return grade;
        } catch(SQLException e) {
            return null;
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }
    }

    /**
     * Get teacher reflection given a publish id and database
     *
     * @param pid
     * @param db
     * @return
     */
    public static LinkedList<String> SelectTeacherReflection(int pid, String db) {
        LinkedList<String> tr = new LinkedList<String>();
        sqls sql = new sqls();
        Connection conn = sql.getConn(db);
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String query = "SELECT * FROM itm.teacher_reflection, itm.publish where teacher_reflection.publish_publish_id=? " +
                           "and publish.publish_id = teacher_reflection.publish_publish_id;";
            ps = conn.prepareStatement(query);
            System.out.println(ps.toString());
            ps.setInt(1, pid);
            rs = ps.executeQuery();
            if(rs.next()) {
                tr.add(rs.getString("bigidea"));
                tr.add(rs.getString("facilitated"));
                tr.add(rs.getString("helpfulActivities"));
                tr.add(rs.getString("lessonsLearned"));
                tr.add(rs.getString("attachment_path"));
                tr.add(rs.getString("attachment_title"));
            }
            return tr;
        } catch(SQLException e) {
            return null;
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }
    }

    public static int SelectPid(String pname, String db) {
        int pid = 0;
        sqls sql = new sqls();
        Connection conn = sql.getConn("itm");
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String query = "SELECT idproject FROM itm.project where projectname=? and KF_url=?;";
            ps = conn.prepareStatement(query);;
            ps.setString(1, pname);
            ps.setString(2, db);
            rs = ps.executeQuery();
            if(rs.next())
                pid = rs.getInt(1);
            return pid;
        } catch(SQLException e) {
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            DBUtil.closeResultSet(rs);
            sql.Close();
        }
    }

    public static int DeleteSave(int pid, String db) {
        sqls sql = new sqls();
        Connection conn = sql.getConn(db);
        PreparedStatement ps = null;
        String query = "";
        try {
            query = "Delete from itm.publish where publish_id=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, pid);
            return ps.executeUpdate();
        } catch(SQLException e) {
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            sql.Close();
        }
    }

    protected static String ModifyDate(String from, String to) {
        String date = "";
        date = from + " - " + to;
        return date;
    }
}
