package database;
import java.util.*;

public class AttachmentObject {
    private long ID;
    private String title;
    private String file;
    private Date create_time;

    public void SetID(long ID) {
        this.ID = ID;
    }

    public long GetID() {
        return this.ID;
    }

    public void SetTitle(String title) {
        this.title = title;
    }

    public String GetTitle() {
        return this.title;
    }

    public void SetFile(String file) {
        this.file = file;
    }

    public String GetFile() {
        return this.file;
    }

    public void SetCretime(Date Cretime) {
        this.create_time = Cretime;
    }

    public Date GetCretime() {
        return this.create_time;
    }


}
