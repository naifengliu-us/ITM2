package database;

public class GroupAuthorLink {
    private long FromID;
    private long ToID;
    private String strLink;  //link type


    public void SetFromID(long fromid) {
        this.FromID = fromid;
    }

    public long GetFromID() {
        return this.FromID;
    }

    public void SetToID(long toid) {
        this.ToID = toid;
    }

    public long GetToID() {
        return this.ToID;
    }

    public void SetLink(String strLink) {
        this.strLink = strLink;
    }

    public String GetLink() {
        return this.strLink;
    }

}
