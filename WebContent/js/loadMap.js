var _load_map = null;
function MapLoad(e){
	var threadl = document.get_threads.threadcheck;
	var threadnames="@";
	if (threadl.length > 1){
		for (var i=0;i<threadl.length;i++){
			if (threadl[i].checked == true){
					threadnames = threadnames + threadl[i].value+"@"
			}
		}
	}
	else {
		if (threadl.checked == true){
			threadnames = threadnames+threadl.value+"@";
		}
			}
	if (threadnames == "@") return;
	var root_draw = document.getElementById("draw_area");
	var root_raw = document.getElementById("raw_area");
	var cnt = root_draw.children.length;
	//clean up
	for (var i = 0;i<cnt;i++){
		root_draw.removeChild(root_draw.children[0]);
	}
	cnt = root_raw.children.length;
	for (var i = 0;i<cnt;i++){
		root_raw.removeChild(root_raw.children[0]);
	}
	_load_map = initXMLHttpObject();
	if (_load_map == null){
		alert("your browser does not support Ajax");
		return;
	}
	var url = "loadMap.jsp";
	url = url + "?projectname=" + _projectname;
	url = url + "&database="+ _database;
	url = url + "&threadname=" + threadnames;
	_load_map.onreadystatechange = getOutput;
	_load_map.open("GET",url,true);
	_load_map.send(null);
}

function initXMLHttpObject(){
	var xr = null;
	if(window.XMLHttpRequest){
		xr = new XMLHttpRequest();
	}
	else if(window.ActiveXObject){
		xr = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xr;
}

function getOutput(){
	var ready = _load_map.readyState;
	var root_raw = document.getElementById("raw_area");
	if (ready != 4){
		//TODO
	}
	else if(ready == 4){
		root_raw.innerHTML =  _load_map.responseText;
		drawMap();
	}
}

