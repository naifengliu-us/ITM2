var _itm_req = null;
var _itm_req_lock = 0;

function AddTimeForNotesInThread(){
	var currentTime = new Date();
	var year = currentTime.getFullYear();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	if (minutes < 10) {
      minutes = "0"+minutes;
	}
	var seconds = currentTime.getSeconds();
	var timestamp = (year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds);
	return timestamp;
}

function ShowRenameThread() {
	var mask_content = apply_screen_mask();
	//resize
	mask_content.style.width = "300px";
	mask_content.style.height = "130px";
	mask_content.style.marginLeft = "-150px";
	mask_content.style.marginTop = "-65px";
	//fill with content
	var ttl = document.createElement("div");
	ttl.setAttribute("id","mask_title")
	ttl.appendChild(document.createTextNode("Rename Thread"));//TODO
	mask_content.appendChild(ttl);
	ttl = document.createElement("div");
	mask_content.appendChild(ttl);
	ttl.setAttribute("id","mask_detail");
	ttl.appendChild(document.createTextNode("Old thread name: "+_threadfocus));

	ttl.appendChild(document.createElement("br"));
	ttl.appendChild(document.createTextNode("New thread name: "));
	var tmpform = document.createElement("form");
	ttl.appendChild(tmpform);
	tmpform.setAttribute("name","form_renamethread");
	var tmp=document.createElement("input");
	tmp.setAttribute("type","text");
	tmp.setAttribute("name","new_name");
	tmp.setAttribute("onkeydown","return searchKeyPress(event);");
	tmpform.appendChild(tmp);
	//mast_link
	ttl = document.createElement("div");
	ttl.setAttribute("id","mask_link");
	mask_content.appendChild(ttl);

	//ul
	var tmpul = document.createElement("ul");
	ttl.appendChild(tmpul);
	// li a check 
	var tmpli = document.createElement("li");
	tmpul.appendChild(tmpli);
	var tmpahref = document.createElement("a");
	tmpahref.setAttribute("href","#");
	tmpli.appendChild(tmpahref);
	tmpahref.appendChild(document.createTextNode("Check Thread Name"));
	tmpahref.setAttribute("onclick",'OnCheckThreadName()');
	// li a save
	tmpli = document.createElement("li");
	tmpli.setAttribute("id","rename_thread_button");
	tmpli.style.backgroundColor = "#C4DED1";
	tmpul.appendChild(tmpli);
	tmpli.appendChild(document.createTextNode("Save"));
	/*
	tmpahref = document.createElement("a");
	tmpahref.setAttribute("href","#");
	tmpli.appendChild(tmpahref);
	tmpahref.appendChild(document.createTextNode("Save"));
	*/
	//li a cancel
	tmpli = document.createElement("li");
	tmpul.appendChild(tmpli);
	tmpahref = document.createElement("a");
	tmpahref.setAttribute("href","#");
	tmpli.appendChild(tmpahref);
	tmpahref.appendChild(document.createTextNode("Cancel"));
	tmpahref.setAttribute('onclick','remove_screen_mask()');
}

function ShowDeleteThread(){
	var mask_content = apply_screen_mask();
	//resize
	mask_content.style.width = "300px";
	mask_content.style.height = "70px";
	mask_content.style.marginLeft = "-150px";
	mask_content.style.marginTop = "-35px";
	//fill with content
	//mask_title
	var ttl = document.createElement("div");
	ttl.setAttribute("id","mask_title");
	ttl.appendChild(document.createTextNode("Do you want to delete this thread?"));//TODO
	mask_content.appendChild(ttl);
	//mast_link
	ttl = document.createElement("div");
	ttl.setAttribute("id","mask_link");
	mask_content.appendChild(ttl);

	//ul
	var tmpul = document.createElement("ul");
	ttl.appendChild(tmpul);
	// li a save
	tmpli = document.createElement("li");
	tmpul.appendChild(tmpli);
	tmpahref = document.createElement("a");
	tmpahref.setAttribute("href","#");
	tmpli.appendChild(tmpahref);
	tmpahref.appendChild(document.createTextNode("Delete"));
	tmpahref.setAttribute('onclick','OnRemoveThread()');
	//li a cancel
	tmpli = document.createElement("li");
	tmpul.appendChild(tmpli);
	tmpahref = document.createElement("a");
	tmpahref.setAttribute("href","#");
	tmpli.appendChild(tmpahref);
	tmpahref.appendChild(document.createTextNode("Cancel"));
	tmpahref.setAttribute('onclick','remove_screen_mask()');
}

function ShowRestoreThread(){
  var mask_content = apply_screen_mask();
  //resize
  mask_content.style.width = "300px";
  mask_content.style.height = "70px";
  mask_content.style.marginLeft = "-150px";
  mask_content.style.marginTop = "-35px";
  //fill with content
  //mask_title
  var ttl = document.createElement("div");
  ttl.setAttribute("id","mask_title");
  ttl.appendChild(document.createTextNode("Do you want to restore this thread?"));//TODO
  mask_content.appendChild(ttl);
  //mast_link
  ttl = document.createElement("div");
  ttl.setAttribute("id","mask_link");
  mask_content.appendChild(ttl);

  //ul
  var tmpul = document.createElement("ul");
  ttl.appendChild(tmpul);
  // li a save
  tmpli = document.createElement("li");
  tmpul.appendChild(tmpli);
  tmpahref = document.createElement("a");
  tmpahref.setAttribute("href","#");
  tmpli.appendChild(tmpahref);
  tmpahref.appendChild(document.createTextNode("Restore"));
  tmpahref.setAttribute('onclick','OnRestoreThread()');
  //li a cancel
  tmpli = document.createElement("li");
  tmpul.appendChild(tmpli);
  tmpahref = document.createElement("a");
  tmpahref.setAttribute("href","#");
  tmpli.appendChild(tmpahref);
  tmpahref.appendChild(document.createTextNode("Cancel"));
  tmpahref.setAttribute('onclick','remove_screen_mask()');
}

//progress on transfers from the server to the client (downloads)
function updateProgress(evt) {
  if (evt.lengthComputable) {
    var percentComplete = evt.loaded / evt.total;
    console.log(percentComplete);
  } else {
    // Unable to compute progress information since the total size is unknown
  }
}
 
function transferComplete(evt) {
  alert("The transfer is complete.");
}
 
function transferFailed(evt) {
  alert("An error occurred while transferring the file.");
}
 
function transferCanceled(evt) {
  alert("The transfer has been canceled by the user.");
}


function OnSave(threadHasChanged)
{
  checkAjaxStatus();
  _itm_req_lock = 1;
  var threadChangedFlag = 0;
  
  // First thing we will do is save the data if 
  // thread has been changed
  if (threadHasChanged) {
    threadChangedFlag = 1;
  }
  
  var notes = document.getElementById("raw_area");
  if (notes == null) {
    return;
  }
  var cnt = notes.children.length;
  if (cnt == 0) {
    return;
  }
  //alert(notes.children.length);
  var strv = '@';
  for (var i = 0; i < cnt; i++) {
    var cnode = notes.children[i];
    var nid = cnode.getAttribute("noteid");
    var mk;
    if (cnode.getAttribute("highlight") == "0") {
      mk="0";
    }
    else if (cnode.getAttribute("highlight") == "1") {
      mk="1";
    }
    strv = strv+nid+mk+"@";
  }
  
  // NOTE Old code replaced by AJAX calls
  //  _itm_req = initXMLHttpObject();
  //  if (_itm_req == null){
  //    alert("Your browser does not support Ajax");
  //    _itm_req_lock = 0;
  //    return;
  //  }else {
  //    _itm_req_lock = 1;}
  //  var url = "saveThread.jsp";
  //  url=url + "?projectname=" + _projectname;
  //  url=url + "&threadfocus=" + _threadfocus;
  //  url=url + "&database=" + _database;
  //  url=url + "&getvalue="+strv;
  //  url=url + "&nidts=" + document.getElementById("noteid_with_timestamp").getAttribute("value");
  //  
  //  // FIXME: Remove debug message
  //  console.log(url);
  //  
  //  _itm_req.onreadystatechange = outputSaveThread;
  //  _itm_req.open("GET",url,true);
  //  _itm_req.send(null);
  //capture the value of change of action
//set the change of action back to 0 if it is 1
  
  var idTimestampMapString = document.getElementById("noteid_with_timestamp").getAttribute("value");
	// Replace whitespace with the html code or else the http will truncate the string
	idTimestampMapString = idTimestampMapString.replace(/ /g, "&#32;");
	//alert("OnSave " + idTimestampMapString);
	var myurl = "/ITM2/en/saveThread.jsp";
	$.ajax({
	  type: "POST",
	  url: myurl,
	  async: false,
	  data: { database: _database,
	          projectname: _projectname,
	          project: _projectname,
	          threadfocus: _threadfocus,
	          getvalue: strv,
	          nidts: idTimestampMapString,
	          username: username,
	          thread_changed_flag: threadChangedFlag,
	          authorcount:authorcnt
    },
    beforeSend: function (request, settings) {
      changeAjaxStatus("Saving");
      return true;
    }})
	  .done(function(msg){
	    changeAjaxStatus("Saved");
	    var t = setTimeout("removeAjaxStatus()",1000);
	    _itm_req_lock = 0;
	  })
	  .fail(function( msg ) {
	    alert( "Failed to post data to URL " + myurl);
	    _itm_req_lock = 0;
	  });
}


function OnCheckThreadName(e){
	checkAjaxStatus();
	var txt = document.form_renamethread.new_name.value;
	_itm_req = initXMLHttpObject();
	if (_itm_req == null){
		alert("Your browser does not support Ajax");
		_itm_req_lock = 0;
		return;
	}else {
		_itm_req_lock = 1;}
	var url = "checkNameThread.jsp";
	url=url + "?projectname=" + _projectname;
	url=url + "&oldname=" + _threadfocus;
	url=url + "&database=" + _database;
	url=url + "&newname=" + txt;
	_itm_req.onreadystatechange = outputCheckName;
	_itm_req.open("GET",url,true);
	_itm_req.send(null);
}

function OnRenameThread(){
	checkAjaxStatus();
	var txt = document.form_renamethread.new_name.value;
	_itm_req = initXMLHttpObject();
	if (_itm_req == null){
		alert("Your browser does not support Ajax");
		return;
	}else {
		_itm_req_lock = 1;}
	var url = "renameThread.jsp";
	url=url + "?projectname=" + _projectname;
	url=url + "&oldname=" + _threadfocus;
	url=url + "&database=" + _database;
	url=url + "&newname=" + txt;
	_itm_req.onreadystatechange = outputRenameThread;
	_itm_req.open("GET",url,true);
	_itm_req.send(null);
}

function OnClose(e) {
  //alert("hello OnClose() called");
  checkAjaxStatus();
  _itm_req_lock = 1;
  var threadChangedFlag = 0;
  
  var notes = document.getElementById("raw_area");
  if (notes == null){
    removeDeleteStatus();
  }
  var cnt = notes.children.length;
  var strv = '@';
  for (var i = 0;i < cnt;i++) {
    var cnode = notes.children[i];
    var nid = cnode.getAttribute("noteid");
    var hlstate = cnode.getAttribute("highlight");
    console.log(hlstate); 
    strv = strv+nid+hlstate+"@";
  }
  
  // NOTE: Old code replaced by JQuery AJAX
//  _itm_req = initXMLHttpObject();
//  if (_itm_req == null){
//    alert("Your browser does not support Ajax");
//    _itm_req_lock = 0;
//    return;
//  }else {
//    _itm_req_lock = 1;}
//  var url = "saveThread.jsp";
//  url=url + "?projectname=" + _projectname;
//  url=url + "&threadfocus=" + _threadfocus;
//  url=url + "&database=" + _database;
//  url=url + "&getvalue="+strv;
//  url=url + "&nidts=" + document.getElementById("noteid_with_timestamp").getAttribute("value");

//  _itm_req.onreadystatechange = outputCloseThread;
//  _itm_req.open("GET",url,true);
//  _itm_req.send(null);
  var flag =1;
	var idTimestampMapString = document.getElementById("noteid_with_timestamp").getAttribute("value");
	// Replace whitespace with the html code or else the http will truncate the string
	idTimestampMapString = idTimestampMapString.replace(/ /g, "&#32;");
	//alert("OnClose",idTimestampMapString);
  var myurl = "/ITM2/en/saveThread.jsp";
  $.ajax({
    type: "POST",
    url: myurl,
    asych: true,
    data: { database: _database,
            projectname: _projectname,
            threadfocus: _threadfocus,
            getvalue: strv,
            nidts: idTimestampMapString,
            username: username,
            thread_changed_flag: threadChangedFlag,
            authorcount:authorcnt
    },
    beforeSend: function (request, settings) {
      changeAjaxStatus("Saving");
    }
    }).done(function(msg){
      top.window.opener.location.reload();
      top.window.close(); 
      changeAjaxStatus("Saved");
      var t = setTimeout("removeDeleteStatus()",1000);
      _itm_req_lock = 0;
    }).fail(function(msg) {
      alert( "Failed2: " + msg );
    });  
}

function OnRemoveThread(e){
	checkAjaxStatus();
	remove_screen_mask();
	_itm_req = initXMLHttpObject();
	if (_itm_req == null){
		alert("Your browser does not support Ajax");
		_itm_req_lock = 0;
		return;
	}else {
		_itm_req_lock = 1;}
	var url = "deleteThread.jsp";
	url=url + "?projectname=" + _projectname;
	url=url + "&threadfocus=" + _threadfocus;
	url=url + "&database=" + _database;
	_itm_req.onreadystatechange = outputDeleteThread;
	_itm_req.open("GET",url,true);
	_itm_req.send(null);
}


function OnRestoreThread(e){
  checkAjaxStatus();
  remove_screen_mask();
  _itm_req = initXMLHttpObject();
  if (_itm_req == null){
    alert("Your browser does not support Ajax");
    _itm_req_lock = 0;
    return;
  }else {
    _itm_req_lock = 1;}
  var url = "restoreThread.jsp";
  url=url + "?projectname=" + _projectname;
  url=url + "&threadfocus=" + _threadfocus;
  url=url + "&database=" + _database;
  _itm_req.onreadystatechange = outputRestoreThread;
  _itm_req.open("GET",url,true);
  _itm_req.send(null);
}

function outputSaveThread(){
	var ready = _itm_req.readyState;
	var nod = document.getElementById("ajax_state");
	if (nod.firstChild!= null) nod.removeChild(nod.firstChild);
	
	if (ready != 4){
		changeAjaxStatus("Saving");
	}
	else if(ready == 4){
		changeAjaxStatus("Saved");
		var t=setTimeout("removeAjaxStatus()",1000);
		_itm_req_lock = 0;
	}
}

function outputCheckName(){
	var ready = _itm_req.readyState;
	var nod = document.getElementById("ajax_state");
	if (ready != 4){
		var fm = document.form_renamethread.new_name.parentNode;
		if (fm.children.length >1){
			fm.removeChild(fm.children[1]);
		}
		var fm_img = document.createElement("img");
		fm_img.setAttribute("src","../img/loading_small.gif");
		fm_img.setAttribute("alt","loading");
		fm_img.setAttribute("width","20px");
		fm_img.setAttribute("height","20px");
		fm.appendChild(fm_img);
		fm = 4;
	}
	else if(ready == 4){
		var cnt=_itm_req.responseText;
		if (cnt.charAt(0) == "0"){
			//set check
			var fm = document.form_renamethread.new_name.parentNode;
			if (fm.children.length >1){
				fm.removeChild(fm.children[1]);
			}
			var fm_img = document.createElement("img");
			fm_img.setAttribute("src","../img/check.png");
			fm_img.setAttribute("alt","loading");
			fm_img.setAttribute("width","20px");
			fm_img.setAttribute("height","20px");
			fm.appendChild(fm_img);
			//set button
			var buttn = document.getElementById("rename_thread_button");
			buttn.style.backgroundColor = "#5A88A8";
			buttn.removeChild(buttn.firstChild);
			var tmpahref = document.createElement("a");
			tmpahref.setAttribute("href","#");
			tmpahref.appendChild(document.createTextNode("Save"));
			tmpahref.setAttribute('onclick','OnRenameThread()');
			buttn.appendChild(tmpahref)
		}
		else {
			//set check
			var fm = document.form_renamethread.new_name.parentNode;
			if (fm.children.length >1){
				fm.removeChild(fm.children[1]);
			}
			var fm_img = document.createElement("img");
			fm_img.setAttribute("src","../img/checkx.png");
			fm_img.setAttribute("alt","loading");
			fm_img.setAttribute("width","20px");
			fm_img.setAttribute("height","20px");
			fm.appendChild(fm_img);
			//set button
			var buttn = document.getElementById("rename_thread_button");
			buttn.removeChild(buttn.firstChild);
			buttn.appendChild(document.createTextNode("Save"));
			buttn.style.backgroundColor = "#C4DED1";
		}
		_itm_req_lock = 0;
	}
}

function outputRenameThread(){
	var ready = _itm_req.readyState;
	var nod = document.getElementById("ajax_state");
	if (nod.firstChild!= null) nod.removeChild(nod.firstChild);
	
	if (ready != 4){
		var fm = document.form_renamethread.new_name.parentNode;
		if (fm.children.length >1){
			fm.removeChild(fm.children[1]);
		}
		var fm_img = document.createElement("img");
		fm_img.setAttribute("src","../img/loading_small.gif");
		fm_img.setAttribute("alt","loading");
		fm_img.setAttribute("width","20px");
		fm_img.setAttribute("height","20px");
		fm.appendChild(fm_img);
		fm = 4;
	}
	else if(ready == 4){
		_itm_req_lock = 0;
		var fm = document.form_renamethread.new_name.parentNode;
		if (fm.children.length >1){
			fm.removeChild(fm.children[1]);
		}
		var fm_img = document.createElement("img");
		fm_img.setAttribute("src","../img/check.png");
		fm_img.setAttribute("alt","loading");
		fm_img.setAttribute("width","20px");
		fm_img.setAttribute("height","20px");
		fm.appendChild(fm_img);
		top.window.location="threadLoad.jsp?database="+_database+"&projectname="+_projectname+"&threadfocus="+document.form_renamethread.new_name.value;
	}
}

function outputCloseThread(){
	var ready = _itm_req.readyState;
	var nod = document.getElementById("ajax_state");
	if (nod.firstChild!= null) nod.removeChild(nod.firstChild);
	
	if (ready != 4){
		changeAjaxStatus("Saving");
	}
	else if(ready == 4){
		changeAjaxStatus("Saved");
		var t=setTimeout("removeDeleteStatus()",1000);
		_itm_req_lock = 0;
	}
}

function outputDeleteThread(){
	var ready = _itm_req.readyState;
	var nod = document.getElementById("ajax_state");
	if (nod.firstChild!= null) nod.removeChild(nod.firstChild);
	
	if (ready != 4){
		changeAjaxStatus("Deleting");
	}
	else if(ready == 4){
		changeAjaxStatus("Deleted");
		_itm_req_lock = 0;
		var t=setTimeout("removeDeleteStatus()",1000);
	}
}

function outputRestoreThread(){
  var ready = _itm_req.readyState;
  var nod = document.getElementById("ajax_state");
  if (nod.firstChild!= null) nod.removeChild(nod.firstChild);
  
  if (ready != 4){
    changeAjaxStatus("Restoring");
  }
  else if(ready == 4){
    changeAjaxStatus("Restored");
    _itm_req_lock = 0;
    var t=setTimeout("removeRestoreStatus()",1000);
  }
}

function removeDeleteStatus(){
	removeAjaxStatus();
	top.window.opener.location.reload();
	top.window.close();	
}

function removeRestoreStatus(){
  removeAjaxStatus();
  top.window.opener.location.reload();
  top.window.close(); 
}

function initXMLHttpObject(){
	var xr = null;
	if(window.XMLHttpRequest){
		xr = new XMLHttpRequest();
	}
	else if(window.ActiveXObject){
		xr = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xr;
}

function changeAjaxStatus(e){
	var nod = document.getElementById("ajax_state");
	while (nod.childNodes.length > 0){
		nod.removeChild(nod.firstChild);
	}
	nod.appendChild(document.createTextNode(e));
	remove_screen_mask();
	loading_screen_mask();
}

function removeAjaxStatus(){
	var nod = document.getElementById("ajax_state");
	if (nod.childNodes.length != 0){
		nod.removeChild(nod.firstChild);
	}
	remove_screen_mask();
}

function checkAjaxStatus(){
	while(_itm_req_lock==1){
		t=setTimeout("changeAjaxStatus(\"Server Busy\")",200);
	}
}
function searchKeyPress(e)
{
    if (typeof e == 'undefined' && window.event) { e = window.event; }
    if (e.keyCode == 13)
    {
    	OnCheckThreadName();
    	return false;
    }
    else {
    	var fm = document.form_renamethread.new_name.parentNode;
		if (fm.children.length >1){
			fm.removeChild(fm.children[1]);
		}
		//set button
		var buttn = document.getElementById("rename_thread_button");
		buttn.removeChild(buttn.firstChild);
		buttn.appendChild(document.createTextNode("Save"));
		buttn.style.backgroundColor = "#C4DED1";
    }
}
function new_thread_searchKeyPress(e){
	if (typeof e == 'undefined' && window.event) { e = window.event; }
    if (e.keyCode == 13)
    {
    	OnFind();
    	return false;
    }
}
