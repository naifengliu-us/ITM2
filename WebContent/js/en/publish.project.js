//PublishProject.jsp JQuery
    $(document).ready(function(){        
        var msg='do not select idea thread';
        var projectName="",selectedGrade,showID;        
      
        $("#project").change(function(){            
            var str="", temp;
            str=$(this).val();
            projectName=str.split("(");
            projectName=projectName[0];            
            temp=str.split('Grade:');
            selectedGrade=temp[1][0];
        });//end change
        $("#main_table a").click(function(){
            //disable TF form when no project is selected
            if(projectName.length>0){
            //enable publish btn
            $('#publish').removeAttr('disabled');
            var threadsName=[];
           showID=$('#check1').prop('checked');
            $('#hor-minimalist-a tr').has(':checkbox:checked').find('td:eq(1)').each(function() {
                threadsName.push($(this).text());
                });
                //clear hiden inputs               
                $('#li_save').remove();
               //make parameters               
               $('#tr_form').append('<input type="hidden" name="projectname" value="'+projectName+'" />');               
                            
                if(showID){
                   $('#tr_form').append('<input type="hidden" name="showID" value="'+showID+'" />'); 
                }
                $('#tr_form').append('<input type="hidden" name="grade" value="'+selectedGrade+'" />'); 
               
//                var teacherLink='TeacherReflection.jsp?database=<%=database%>';
//                $("#main_table a").attr('href',teacherLink);
            $('#tr_form').submit();
            }
            return false;
        });//end click
        $('#save').click(function(e){
//           JSON data { "projectName": "name", "showID": boolean ,
//                       "threadsName": [
//                          { "threadName":"battery" , "threadName":"Electron" }
//                                       ]   
//                     }                
           var threadsName=[];
            $('#publish').removeAttr('disabled');
           showID=$('#check1').prop('checked');
            $('#hor-minimalist-a tr').has(':checkbox:checked').find('td:eq(1)').each(function() {
                threadsName.push($(this).text());
                });
//            if(threadsName.length>0){
               //make JSON
                var saveObj=new Object();
                var username="${username}";
                saveObj.projectName=projectName;
                saveObj.showID=showID;
                saveObj.threadsName=threadsName;               
                saveObj.userID=username;
                saveObj.grade=selectedGrade;
                saveObj.db="${database}";
                var json=JSON.stringify(saveObj);

                //post data to db
                $.post("../SavePublishServlet", {json:json},  function(data){
                        $('.outter div:eq(1)').insertAfter( $(this) )
                                .fadeIn('slow')
                                .animate({opacity: 1.0}, 3000)
                                .fadeOut('slow');
                        e.preventDefault();
                    });//end post
                    //uncomment if else statement for" select threads is mandatory."
//                } else{   
//                    $('.outter div:eq(0)').fadeIn('slow')
//                                        .animate({opacity: 1.0}, 3000)
//                                        .fadeOut('slow');
//            }            
           return false;
        });//end click
        $('#publish').click(function(){
            //todo get teacher reflection data
            if(projectName.length>0){
                //selected a project   
                var saveObj=new Object();
                var username="${username}";
                showID=$('#check1').prop('checked');
                saveObj.projectName=projectName;
                saveObj.showID=showID;                              
                saveObj.userID=username;
                saveObj.grade=selectedGrade;
                saveObj.db="${database}";
                var json=JSON.stringify(saveObj);
                $.post('../PublishProjectServlet', {json:json}, function(){
                    $('.outter div:last').insertAfter( $(this) )
                                .fadeIn('slow')
                                .animate({opacity: 1.0}, 3000)
                                .fadeOut('slow');
                });//end post
            }
             $('#publish').attr('disabled','disabled');
            return false;
        });//end click
       /*
       * Cancel btn
       * Go back to newhomepage
        */
        $('#cancel').click(function() {
            window.location = "New_HomePage.jsp?database=${database}";
            });//end cancel click
       
        $( ".sideBar_right" ).accordion({
                active: false,
		collapsible: true,
                heightStyle: "fill"
		});//end accordion
                
        $.getJSON('/ITM2/PublishSaves',{'user':"${username}"},processResponse);
                    function processResponse(data){
                        $.each(data, function(key, val) {
                               var tableHTML='<li><p><a href="../ShowPublishSave?id='+val.pid+'">';
                                tableHTML+=val.projectName+'<br>';                               
                                tableHTML+=val.date+'</a></p></li>'; 
                               $('.sideBar_right ul').append(tableHTML);                          
                        });//end each
                    }
     
    });     
