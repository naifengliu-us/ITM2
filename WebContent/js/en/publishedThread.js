// We have defined tg global as the itm_* scripts expect it.
var tg;
var publishedThread = function(projectId, threadId, timestamp, nodeId, sliderId) {
  var m_projectId = projectId, 
      m_threadId = threadId,
      m_timestamp = timestamp;
  
  this.showThread = function() {
    // Get the data from the thread history table
    var myurl = "/ITM2/PublishedThread",
        threadDisplayDiv = $(nodeId);
    
    threadDisplayDiv.empty();
    
    $.ajax({
      type: "POST",
      url: myurl,
      async: false,
      data: { 
        project_id: m_projectId,
        thread_id: m_threadId,
        timestamp: m_timestamp
      },
      success : function(response) {
        try {
          var threadRawData = jQuery.parseJSON(response),
              formattedData = {}, nodes = [], links = [], i;
          for (i = 0; i < threadRawData.data_array.length; ++i) {
            nodes.push({"author": (threadRawData.data_array[i][5] + " " + threadRawData.data_array[i][6]), 
                        "highlight": threadRawData.data_array[i][1],
                        "id": threadRawData.data_array[i][3], 
                        "noteid": threadRawData.data_array[i][0], 
                        "title": threadRawData.data_array[i][3], 
                        "content": threadRawData.data_array[i][4], 
                        "time": parseDT(threadRawData.data_array[i][2]),
                        //"view":threadRawData.data_array[i][5],
                        "timestamp":threadRawData.data_array[i][2]});
          }} catch(err) {
            console.log("[error] " + err);
          }
        
        //  Update text content
        $('#list_area li').remove();
        $('#note_content').children().remove();
        for (i = 0; i < nodes.length; ++i) {
          $('#list_area ul').append('<li noteid=' + nodes[i].noteid + '>' + nodes[i].title + '</li>');    
          var new_note_data = $(document.createElement('div'));
          new_note_data.attr('highlighted', nodes[i].highllight);
          new_note_data.attr('noteid', nodes[i].noteid);
          new_note_data.attr('view', nodes[i].view);
          new_note_data.attr('author', nodes[i].author);
          new_note_data.attr('firstn', "");
          new_note_data.attr('date', nodes[i].timestamp);
          new_note_data.attr('id', nodes[i].title);
       
          new_note_data.append('<div class="references">');
          new_note_data.append('<div class="buildsons">');
          new_note_data.append('<div class="annotates">');
          new_note_data.append('<div class="rawtitle">' + nodes[i].title);
          new_note_data.append('<div class="rawcontent">' + nodes[i].content);
          $('#raw_area').append(new_note_data);
        }
        
        formattedData["nodes"] = nodes;
        formattedData["links"] = links;
        console.log(formattedData);
          
        var width = 930;
        var height = 390;
        var dataDiv = $(document.createElement('div'));
        dataDiv.attr('id', 'published_thread_graph');
        dataDiv.attr('width', width + 'px');
        dataDiv.attr('height', height + 'px');
        
        threadDisplayDiv.append(dataDiv);
        threadDisplayDiv.css('z-index', 10000);
        
        tg = new threadGraph($(sliderId), width, height);
        tg.readData(formattedData, "#published_thread_graph");
        
        //Toggle the title names and author names
        tg.toggleTitles(this);
        tg.toggleAuthors(this);
        
        // Now update highlight state
        for (i = 0; i < threadRawData.data_array.length; ++i) {
          var noteid, highlightState;
          noteid = threadRawData.data_array[i][0];
          highlightState = threadRawData.data_array[i][1];
          if (highlightState === "1") {
            tg.toggleHiglightNodeById(noteid);
          }
        }
      },
      failure : function(response) {
        console.log('[error] Failed to get thread data ', response);
      }
    });
  };
};

