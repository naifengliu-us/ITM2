<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<%
String proj_name = request.getParameter("projectname");
String thread_name = request.getParameter("threadname");
sqls s = new sqls();
Operatedb opdb = new Operatedb(s,request.getParameter("database"));
ResultSet focusrs = opdb.getC().executeQuery("select idProject from  project where projectname='"+proj_name+"';");
focusrs.next();
int pid = focusrs.getInt(1);
if(thread_name.equals("all")){
	focusrs = opdb.getC().executeQuery("select threadfocus from  project_thread where  project_thread.projectid="+pid+";");
}
else {
	String[] focuses = thread_name.split("@");
	String query = "select threadfocus from  project_thread where  project_thread.projectid="+pid+" and (";
	for (int i = 0;i < focuses.length;i++){
		query = query + "  project_thread.threadfocus = '"+focuses[i]+"' or ";
	}
	query = query.substring(0,(query.length()-3));
	query = query + " ) order by project_thread.threadfocus ASC;";
	focusrs = opdb.getC().executeQuery(query);
}
String[] focus = new String[1000];
int tmpi = 0;
while(focusrs.next()){
	focus[tmpi] = focusrs.getString(1);
	tmpi++;
}

if(focusrs != null){
	try{
		focusrs.close();
	}catch(SQLException e){
		e.printStackTrace();
	}
}
int focusmax = tmpi;
for (int kk = 0;kk<tmpi;kk++) {
	
   String query = "select thread_note.noteid, author_table.firstname, author_table.lastname, " + 
       "note_table.notetitle, note_table.notecontent, note_table.createtime, view_table.title, thread_note.highlight from  " + 
       "thread_note inner join  note_table on  thread_note.noteid =  note_table.noteid " +
       "inner join  author_note on  thread_note.noteid =  author_note.noteid inner join " + 
       "author_table on  author_note.authorid =  author_table.authorid inner join " +
       "view_note on note_table.noteid = view_note.noteid inner join view_table on view_note.viewid = view_table.idview " +
       "and thread_note.threadfocus='"+focus[kk]+"' and thread_note.projectid='"+pid+"';";
       
	ResultSet noters = opdb.getC().executeQuery(query);
	String[] vecnode = new String[1000];
	String[] vecauthor = new String[1000];
	String[] vectitle = new String[1000];
	String[] veccontent = new String[1000];
	String[] vecdate = new String[1000];
	String[] vecview = new String[1000];
	String[] vechighlight = new String[1000];
	int veccnt = 0;
	while(noters.next() && veccnt<1000) {
		vecnode[veccnt] = noters.getString(1);
		vecauthor[veccnt] = noters.getString(2) + " " + noters.getString(3);
		vectitle[veccnt] = noters.getString(4);
		veccontent[veccnt] = noters.getString(5);
		vecdate[veccnt] = noters.getString(6);
		vecview[veccnt] = noters.getString(7);
		vechighlight[veccnt] = noters.getString(8);
		veccnt++;
	}
	//count distinct noteid's
	ResultSet countnoteid = opdb.getC().executeQuery(
			"select  count(distinct thread_note.noteid) from  thread_note inner join  note_table on  thread_note.noteid =  note_table.noteid inner join  author_note on  thread_note.noteid =  author_note.noteid inner join  author_table on  author_note.authorid =  author_table.authorid and  thread_note.threadfocus='"+focus[kk]+"'and thread_note.projectid='"+pid+"';");
	String counter = null;
	if(countnoteid.next())
	{
		counter = countnoteid.getString(1);
	}
	
	if(noters != null){
		try{
			noters.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	//count distinct authorid's
	ResultSet tars = opdb.getC().executeQuery(
			"select count(distinct authorid) from  thread_note inner join  author_note on  thread_note.noteid =  author_note.noteid and  thread_note.threadfocus='"+focus[kk]+"'and thread_note.projectid='"+pid+"';"
			);
	String authorcnt = null;
	if (tars.next()){
		authorcnt = tars.getString(1);
	}
	
	if(tars != null){
		try{
			tars.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	%>
	<!-- <div class="rawfocus" focusname="//TODO" -->
	<div class="rowfocus" focusname="<%=focus[kk]%>" au_num="<%=authorcnt%>" nt_num="<%=counter%>">
		<%
		veccnt = 0;
		while (vecnode[veccnt] != null) {
			String tmpnode = vecnode[veccnt];
			String tmpcontent = veccontent[veccnt];
			String tmpdate = vecdate[veccnt];
			String tmptitle = vectitle[veccnt];
			String tmpauthor = vecauthor[veccnt];
			String tmpview = vecview[veccnt];
			String tmphighlight = vechighlight[veccnt];
			while(vecnode[veccnt].equals(vecnode[veccnt + 1])) {
				tmpauthor += ","+ vecauthor[veccnt+1];
				veccnt++;
			}
		%>
		<!-- <div class="rawnote" noteid="//1" author="//2+3" date="//6"-->
		<div class="rawnote" noteid="<%=tmpnode%>" author="<%=tmpauthor%>" date="<%=tmpdate%>" view="<%=tmpview%>" highlight="<%=tmphighlight%>">
			<div class="references">
				<%
				ResultSet refrs = opdb.getC().executeQuery("select tonoteid from  note_note where fromnoteid="+tmpnode+" and linktype = 'references';");
				while(refrs.next()) {
					%>
						<div class="reference" target="<%=refrs.getString(1)%>"></div>
					<%
				}
				
				if (refrs != null) {
					try {
						refrs.close();
					} catch(SQLException e) {
						e.printStackTrace();
					}
				}
				%>
			</div> <!-- class="references" -->
			<div class="buildsons">
				<%
				ResultSet bufrs = opdb.getC().executeQuery("select tonoteid from  note_note where fromnoteid="+tmpnode+" and linktype = 'buildson';");
				while (bufrs.next()) {
				%>
				<div class="buildson" target="<%=bufrs.getString(1)%>"></div>
					<%
					}
					if (bufrs != null) {
						try {
							bufrs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						} catch (Exception e) {
						  e.printStackTrace();
						}
					}
					%>			
             </div> <!-- class="buildsons" -->
			 <div class="annotates">
				<%
				ResultSet anfrs = opdb.getC().executeQuery("select tonoteid from  note_note where fromnoteid="+tmpnode+" and linktype = 'annotates';");
				while (anfrs.next()) {
					%>
						<div class="annotate" target="<%=anfrs.getString(1)%>"></div>
					<%
				}
		
				if(anfrs != null){
					try {
						anfrs.close();
					} catch(SQLException e) {
						e.printStackTrace();
					} catch(Exception e) {
					   e.printStackTrace();  
					}
				}
				%>	
		      </div> <!-- class="annotates" -->
			  <div class="rawtitle"><%=tmptitle%></div>
			  <div class="rawcontent"><%=tmpcontent%></div>
		</div> <!-- class=rawnote"" -->
		<%
		veccnt++;
		}
		%>
	</div> <!-- class="rowfocus" -->
<%
}
%>
<%
	if(opdb != null){
		opdb.CloseCon();
	}
	if(s != null){
		//s.Close();
	}
   %>