<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*,java.text.SimpleDateFormat,java.io.*;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
String user =" ";
String usertype =" ";
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM/index.jsp");
		}
		else
		{
		    user = (String)session.getAttribute("username");
			usertype = (String)session.getAttribute("usertype");
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Teacher Reflection on This Inquiry Project</title>
<style type="text/css">
.inButtonColor {
 background-image: url(images/button.jpg);
 width:197px;
 height:72px;}
.divinput{
margin: 0px 0px 0px 10px;
width:700px;
float: left;
}
.divinput
{
display:block;
} 
#atable {
	margin-left:auto;
	margin-right:auto;
}

#atable td {
	padding-left:40px;
	padding-right:40px;
	padding-top:10px;
	padding-bottom:10px;
}

.btable {
	margin-left:auto;
	margin-right:auto;
	border-spacing:0;
	text-align: center;
}

.btable td {
	border: 1px solid black;
	padding-left:40px;
	padding-right:40px;
	padding-top:10px;
	padding-bottom:10px;
}

.temp_alert {
	/*    show temp msg after a server call is made. Stan*/
        position: absolute;
        float: end;
	width: 15%;
	margin:5em 20em;
	padding: .2em 2em;	
	background: #ffa;
	border: 1px solid #FF6;
	color: #333;
	font-weight: bold;
	display: none;
	text-align: center;	
}

</style>
<script src="js/en/jquery.js"></script>
<script type="text/javascript" >
    $(document).ready(function(){
         $('.outter div').insertAfter( $(this) )
                                .fadeIn('slow')
                                .animate({opacity: 1.0}, 3000)
                                .fadeOut('slow',function(){
//                                     $('#save').attr('disabled', 'disabled');
                                    window.close();
                                });//end fadeout    
                                  
    });
    
    function updateTeacherReflection(bigidea, facilitated, helpfulAct, lessonsLearned) {
      
    }
</script>
<LINK REL=StyleSheet HREF="css/stickyhead.css" TYPE="text/css"/>
</head>
<% 
  String strname = "";            
	String strfocus = "";
	String strdb ="";
  String strsql;
	String strproject;
  String strFacilitated = null;
	String strIdea = null;
	String strActitivies = null;
	String strLessons = null;
	String attachmentTitle = null;
	String attachmentPath = null;
	
	
	
  Operatedb opdb = null;
  sqls s = new sqls();
  
  if (session.getAttribute("database") != null) {
    strdb = session.getAttribute("database").toString();
  }
  
	Statement stmt = s.Connect(strdb);
	
  if(request.getParameter("saved") == null) {
	strname = request.getParameter("projectname");
	strfocus = "";
	
	 //String showID=session.getAttribute("showID").toString();
       // String userID="Anonymous";
        //if(showID.equalsIgnoreCase("true"))
        ///  userID=session.getAttribute("username").toString();
	Operatedb opdb_author = new Operatedb(new sqls(),strdb);
	AccessToProjects atp = new AccessToProjects();
	
	boolean caneditjourney = atp.canEdit(user,usertype,strname,strdb);
	System.out.println("CanEditJourney is "+ caneditjourney);
	
	if (request.getParameter("save")!=null && caneditjourney)
	{
		String strcontent = request.getParameter("ProblemContent");
		String ideacon = request.getParameter("IdeaContent");
		String morecon = request.getParameter("MoreContent");
		strcontent = strcontent.replace("'","''").replace("\\","\\\\");
		ideacon = ideacon.replace("'","''").replace("\\","\\\\");
		morecon = morecon.replace("'","''").replace("\\","\\\\");
		
		//store the three text sections into database
		String strtime;
	    java.util.Date curdate = new java.util.Date();
		opdb = new Operatedb(s,strdb);
		SimpleDateFormat spdtformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		//get the projectid
		ResultSet temprs = null;
		temprs = opdb.GetRecordsFromDB("project","idproject","projectname='"+strname+"';");//get the project id
		temprs.next();
		strproject = temprs.getString(1);
		if (temprs != null) {
			try {
				temprs.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		//get current time
		strtime = spdtformat.format(curdate);
		
		//get the author
		String username=null;
		session = request.getSession(false);
		if(session != null) {
			username = (String)session.getAttribute("username");
		}
		
		//store the big ideas into database
		strsql = " (`threadfocus_problem`, `createtime`,`problem`,projectid,author_problem) VALUES ( '"+strfocus+"','" + strtime+"','" + strcontent+"',"+strproject+",'"+username+"');"; 
		opdb.WritebacktoDB("thread_problem","INSERT",strsql);
		System.out.println("write into thread_problem table is :"+strsql);
		
		//store how i facilitated  into database
		strsql = " (`threadfocus_idea`, `createtime`,`idea`,projectid,author_idea) VALUES ( '"+strfocus+"','" + strtime+"','" + ideacon+"',"+strproject+",'"+username+"');";  
		opdb.WritebacktoDB("thread_idea","INSERT",strsql);
		
		//store helpful activities and resources used into database
		strsql = " (`threadfocus_more`, `createtime`,`more`,projectid,author_more) VALUES ( '"+strfocus+"','" + strtime+"','" + morecon+"',"+strproject+",'"+username+"');"; 
		opdb.WritebacktoDB("thread_more","INSERT",strsql);
		
		//store lessons learned and improvements i want to make in future into database
		strsql = " (`threadfocus_more`, `createtime`,`more`,projectid,author_more) VALUES ( '"+strfocus+"','" + strtime+"','" + morecon+"',"+strproject+",'"+username+"');"; 
	    opdb.WritebacktoDB("thread_more","INSERT",strsql);
	}	
}
%>

<%
  // Ge the current saved data from the database. 
  //if (request.getParameter("republish")) {
    //String pid = session.getAttribute("pid").toString();
    
    //strsql = "SELECT * FROM itm.publish,itm.teacher_reflection WHERE published = 1 and publish.publish_id = teacher_reflection.publish_publish_id and project_name='"+request.getParameter("projectname").toString()+"' ORDER BY publish_id DESC LIMIT 1;";
    System.out.println("!!!!"+request.getParameter("saveorpublish").toString());
    if(request.getParameter("saveorpublish").toString().equals("save"))
    	strsql = "SELECT * FROM itm.publish,itm.teacher_reflection WHERE published = 0 and publish.publish_id = teacher_reflection.publish_publish_id and project_name='"+request.getParameter("projectname").toString()+"' ORDER BY publish_id DESC LIMIT 1;";
    else
    	strsql = "SELECT * FROM itm.publish,itm.teacher_reflection WHERE published = 1 and publish.publish_id = teacher_reflection.publish_publish_id and project_name='"+request.getParameter("projectname").toString()+"' ORDER BY publish_id DESC LIMIT 1;";
    System.out.println(request.getParameter("projectname").toString());
    System.out.println(strsql.toString());
 
		 ResultSet rs_problem = null;
		 rs_problem = stmt.executeQuery(strsql);
		
		if (rs_problem.next()) {
			strIdea = rs_problem.getString("bigidea");
			strFacilitated = rs_problem.getString("facilitated");
			strActitivies = rs_problem.getString("helpfulActivities");
			strLessons = rs_problem.getString("lessonsLearned");
			attachmentPath = rs_problem.getString("attachment_path");
			attachmentTitle = rs_problem.getString("attachment_title");
		}
		if (rs_problem != null) {
			try {
				rs_problem.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
  //}
	
  s.Close();
  DBUtil.closeStatement(stmt);
%>
<body>
<script>
/// Save teacher reflectiond data    
function OnSave() {
  $.ajax({
    type: "POST",
    url: "/ITM/SavePublishServlet",
    data: new FormData(document.getElementById('summary_form')),
    processData: false,
    contentType: false,
    success: function(res) {
      // Do nothing
    }
  });
}
 
 function OnClose() {
  window.close();
 }
 
 function fileSelected() {
   var file = document.getElementById('fileToUpload').files[0];
   if (file) {
     var fileSize = 0;
     if (file.size > 1024 * 1024)
       fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
     else
       fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
           
     document.getElementById('fileName').innerHTML = 'Name: ' + file.name;
     document.getElementById('fileSize').innerHTML = 'Size: ' + fileSize;
     document.getElementById('fileType').innerHTML = 'Type: ' + file.type;
   }
 }
 
 function uploadFile() {
   var xhr = new XMLHttpRequest();
   var fd = new FormData(document.getElementById('summary_form'));

   xhr.upload.addEventListener("progress", uploadProgress, false);
   xhr.addEventListener("load", uploadComplete, false);
   xhr.addEventListener("error", uploadFailed, false);
   xhr.addEventListener("abort", uploadCanceled, false);
  
   xhr.open("POST", "uploadFile");
   xhr.send(fd);
 }
 
 function uploadProgress(evt) {
   if (evt.lengthComputable) {
     var percentComplete = Math.round(evt.loaded * 100 / evt.total);
     document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
   }
   else {
     document.getElementById('progressNumber').innerHTML = 'unable to compute';
   }
 }

 function uploadComplete(evt) {
   /* This event is raised when the server send back a response */
   alert(evt.target.responseText);
 }

 function uploadFailed(evt) {
   alert("There was an error attempting to upload the file.");
 }

 function uploadCanceled(evt) {
   alert("The upload has been canceled by the user or the browser dropped the connection.");
 }  
</script>

<div id="sticky">
	<div id="header">
	<table border="0">
		<tbody><tr>
                        <td><img src="img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="60" width="850">
			&nbsp;&nbsp;Teacher Reflection On This Inquiry Project: <%=strname%>
			</td>
		</tr>
	</tbody></table>			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
<div>
    <%
            if(request.getParameter("saved")!=null){
                %>
                <div class="outter">                    
                    <div class="inner temp_alert">Saved</div>
               </div> 
                <%
            }   %>
<p>Write down your reflection below and/or upload files(e.g. lesson plan,classroom video,photo) to record your reflection on these issues. Sharing images and videos that involve students requires that you obtain permissions from the students and their parents.</p>
<form id="summary_form" name="summary_form" action="/ITM/SavePublishServlet?projectname=<%=strname%>" enctype="multipart/form-data" method="post">
<div class="divinput">
<font face="verdana" color="green">What this is inquiry about:"Big ideas" in the curriculum unit(s):</font><br>
<!--<img src="img/problem.gif" alt="We want to understand" title="We want to understand"  onmouseup="OnTag1()"height="42" width="42"><br>-->
	<% if(strIdea != null){%>
		<textarea onclick="" name="BigIdeas" id="bigideas" rows="20" cols="90">
<%=strIdea %>
		</textarea>
	<%}else{%>
		<textarea onclick="" name="BigIdeas" id="bigideas" rows="20" cols="90"></textarea>
		<%}%>
</div>
<div class="divinput">
<font face="verdana" color="green">How I facilitated:</font><br><br>
<!--<img src="img/problem.gif" alt="We want to understand" title="We want to understand"  onmouseup="OnTag1()"height="42" width="42"><br>-->
	<% if(strFacilitated != null){%>
		<textarea onclick="" name="Facilitated" id="facilitated" rows="20" cols="90">
<%=strFacilitated %>
		</textarea>
	<%}else{%>
		<textarea onclick="" name="Facilitated" id="facilitated" rows="20" cols="90"></textarea>
		<%}%>
</div>
<div class="divinput">
<font face="verdana" color="green">Most helpful activities and resources used:</font><br><br>
<!--  <img src="img/think.gif" alt="We used to think" title="We used to think"onmouseup="OnTag2()"height="42" width="42"> -->
<!--  <img src="img/insight.gif" alt="We now understand" title="We now understand" onmouseup="OnTag3()"height="42" width="42"><br> -->
<% if(strActitivies != null){%>	
		<textarea onclick="" name="HelpfulActivities" id="helpfulactivities" rows="20" cols="90">
<%=strActitivies %>
		</textarea>
		<%}else{%>	
		<textarea onclick="" name="HelpfulActivities" id="helpfulactivities" rows="20" cols="90"></textarea>
		<%}%>
</div>
<div class="divinput">
<font face="verdana" color="green">Lessons learned and improvements I want to make in the future:</font><br>
<!--  <img src="img/think.gif" alt="We used to think" title="We used to think"onmouseup="OnTag2()"height="42" width="42"> -->
<!--  <img src="img/insight.gif" alt="We now understand" title="We now understand" onmouseup="OnTag3()"height="42" width="42"><br> -->
<% if(strLessons != null){%>	
		<textarea onclick="" name="LessonsLearned" id="lessonslearned" rows="20" cols="90">
<%=strLessons %>
		</textarea>
		<%}else{%>	
		<textarea onclick="" name="LessonsLearned" id="lessonslearned" rows="20" cols="90"></textarea>
		<%}%>
</div>
<div id="fileupload_form" class="divinput">
  <div>
      <label for="fileToUpload">Select a File to Upload</label><br />
      <input type="file" name="fileToUpload" id="fileToUpload" onchange="fileSelected();"/>
  </div>
  <% if(attachmentPath != null){%>
    <div id="fileName"><%=(new File(attachmentPath)).getName()%></div>
  <%}else{%>  
     <div id="fileName"></div>
  <%}%>  
  <div id="fileSize"></div>
  <div id="fileType"></div>
  <div id="progressNumber"></div>
</div>
<input type="hidden" name="database" value="<%=strdb%>" />
<input type="hidden" name="projectname" value="<%=strname%>" />
<input type="hidden" name="showID" value="" />

<div class="divinput">
<br>
<center>
<input type="button" name="save" id="save" value="Save" onclick="OnSave()"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" name="" id="close" value="Close" onclick="OnClose()"/>

</center>
</div>
</form>
</div>
</div>
</div>
</body>
</html>
<%
	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(stmt != null){
		stmt.close();
	}
	
	if(s != null){
		s.Close();
	}
%>