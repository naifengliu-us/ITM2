<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%>
<%@ page session = "true" %>
<%@page import="code.AccessToProjects"%>
<%@page import="itm.servlets.Connect"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*,java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<%
/************validate the user session*******************/
String user =" ";
String usertype =" ";
String dbase=" ";
String strdb =" ";
        session = request.getSession(false);
        if(session != null){
                if (session.getAttribute("username") == null){
                        response.sendRedirect("/ITM2/index.jsp");
                }
                else
                {
                    user = (String)session.getAttribute("username");
                        usertype = (String)session.getAttribute("usertype");
                }
        }
%>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Publish an exploration topic to ITM</title>
<%
String database = request.getParameter("database");
System.out.println("Database is ........................."+database);
%>

<link href="../css/jquery-ui.css" rel="stylesheet">
<script src="../js/en/jquery.js" type="text/javascript"></script>  
<script src="../js/en/jquery-ui.js"></script> 

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="../js/en/bootstrap.min.js"></script>

<style type="text/css">
.inButtonColor {
 background-image: url(../images/button.jpg);
 width:197px;
 height:72px;}
.divinput{
margin: 0px 0px 0px 10px;
width: 300px;
float: left;
}

/* make sidebar nav vertical */ 
.ui-menu .ui-menu-icon {
  float: left;
}

.ui-menu { 
  width: 150px; 
}

.ui-menu .ui-menu-item a {
  background-color: #428bca;
  border-color: #357ebd;
  margin-bottom: 1px;
}

</style>

<script type="text/javascript" >

function checkByParent(aId, aChecked) {
  aChecked = aChecked === undefined ? false : aChecked;
  $(aId).find('.checkbox').has("input[id='option1']").find('input').each(function() {
    $(this).attr('checked', aChecked);  
  }); 
}

function check() {
  document.getElementById("check1").checked=true;
}
function uncheck() {
  document.getElementById("check1").checked=false;
}

function updateList(projectname,teacher,school,grade,fromyear,toyear,group) {
  var p = document.getElementById("project");
  var pname = projectname +"( Teacher："+teacher + ",  School："+school +",  Grade："+grade+",  Group："+group+",  From： "+fromyear+"  To "+toyear+" )";
  //alert(pname);
  p.options.add(new Option(pname));
} 

//Project of javascript objects as threads with notecount ,authorcount,fromdate,todate
//Initialize a global javascript object
var projectThreadInfo = {};
var threadInfo ={};

function UpdateProjectThreadList(projectnm,thread_name,threadvals) {
  if(!projectThreadInfo.hasOwnProperty(projectnm)) {
    projectThreadInfo[projectnm] = {};
  }
  projectThreadInfo[projectnm][thread_name] = threadvals; 
}

function SelectionChange(value,f) {         
  // Invalidate the last pid
  $.post("/ITM2/SavePublishServlet", {resetPid:true});
	var new_div = document.createElement("div");
	var data_div = document.getElementById("thread_info");
	new_div.style.height= 150;
	new_div.style.overflow ='auto';
	//var text = null;      
	var p = document.getElementById("project");
	//Get the current project selected in drop down
	var str = p.value;
	//split to extract the project name
	var n;
	if(f==2){ 
		n = value
		}else{
			n= str.split("( Teacher",1);
	}
	
	//var thread_str = null;   
	//alert(n);
	for(var proj in projectThreadInfo)
	  {
	    if(proj == n)
	    {
	      var table = document.createElement('table');
	      var tableth = document.createElement('thead');
	      
	      var tr1 = document.createElement('tr');
	      
	      var th1a = document.createElement('th');
	      th1a.setAttribute('scope', 'col');
	      th1a.appendChild(document.createTextNode('All'));

        var th1b = document.createElement('th');
        th1b.setAttribute('scope', 'col');
        th1b.appendChild(document.createTextNode('Highlights Only'));
	      
	      var th2 = document.createElement('th');
	      th2.setAttribute('scope', 'col');
	      th2.appendChild(document.createTextNode('Idea Thread'));
	      
	      var th3 = document.createElement('th');
	      th3.setAttribute('scope', 'col');
	      th3.appendChild(document.createTextNode('Note(s)'));
	      
	      var th4 = document.createElement('th');
	      th4.setAttribute('scope', 'col');
	      th4.appendChild(document.createTextNode('Author(s)'));
	      
	      var th5 = document.createElement('th');
	      th5.setAttribute('scope', 'col');
	      th5.appendChild(document.createTextNode('From'));
	      
	      var th6 = document.createElement('th');
	      th6.setAttribute('scope', 'col');
	      th6.appendChild(document.createTextNode('To'));
	      
	      //var th7 = document.createElement('th');
	      //th7.setAttribute('scope', 'col');
	      //th7.appendChild(document.createTextNode('To'));
	      
	      tr1.appendChild(th2);
	      tr1.appendChild(th3);
	      tr1.appendChild(th4);
	      tr1.appendChild(th5);
	      tr1.appendChild(th6);
	      tr1.appendChild(th1a);
        tr1.appendChild(th1b);

	      tableth.appendChild(tr1);
	      table.setAttribute('id', 'hor-minimalist-a');
	      
	      table.appendChild(tableth);
	      table.appendChild(document.createElement('tbody'));
	      new_div.appendChild(table);
	      populateTable(table, projectThreadInfo, n);
	    }
	  }
	data_div.innerHTML = '';
	data_div.appendChild(new_div);
}

function toggleSelection(obj1, obj2id) {
  var id="input[id='"+obj2id+"']", obj1 = $(obj1), obj2 = obj1.parent().parent().parent().find(id);
  if (obj1.attr('checked') === 'checked' && 
      obj2.attr('checked') === 'checked') {
    obj2.attr('checked', false);
  }
}

function populateTable(table, projectThreadInfo, n) {
  tbody = table.children[1];
  for (var thread in projectThreadInfo[n]) {
          row = document.createElement("TR");  

          cell0a = document.createElement("TD");
          /* var cb = document.createElement('input');
          cb.type ="checkbox"; */
          $(cell0a).append('<div class="checkbox">\
            <input type="checkbox" name="options" id="option1" onclick="toggleSelection(this, \'option2\')"> \
          </div>');

           cell0b = document.createElement("TD");
          /* var cb = document.createElement('input');
          cb.type ="checkbox"; */
          $(cell0b).append('<div class="checkbox">\
            <input type="checkbox" name="options" id="option2" onclick="toggleSelection(this, \'option1\')"> \
          </div>');
          
          cell1 = document.createElement("TD");
          var span = document.createElement('span');
          span.innerHTML = thread.link("");
          
          /// On click open the thread when it happens
          span.setAttribute('onclick', 'openthread(\"'+n+'\",\"'+thread+'\");return false;');
          
          cell1.appendChild(span);
          
          cell2 = document.createElement("TD");
          cell2.appendChild(document.createTextNode(projectThreadInfo[n][thread].notecount));
          
          cell3 = document.createElement("TD");
          cell3.appendChild(document.createTextNode(projectThreadInfo[n][thread].authorcount));
          
          //cell4 = document.createElement("TD");
          //cell4.appendChild(document.createTextNode(projectThreadInfo[n][thread].notecount));
          
          cell4 = document.createElement("TD");
          var fromDate = new Date(projectThreadInfo[n][thread].fromdate);
          cell4.appendChild(document.createTextNode(
            ("0" + fromDate.getDate()).slice(-2)+"/"+
            ("0" + (fromDate.getMonth() + 1)).slice(-2)+1+"/"+
            fromDate.getFullYear()));
          
          cell5 = document.createElement("TD");
          var toDate = new Date(projectThreadInfo[n][thread].todate);
          cell5.appendChild(document.createTextNode(
            ("0" + toDate.getDate()).slice(-2)+"/"+
            ("0" + (toDate.getMonth() + 1)).slice(-2)+1+"/"+
            toDate.getFullYear()));
            
          row.appendChild(cell1); // 0
          row.appendChild(cell2);
          row.appendChild(cell3);
          row.appendChild(cell4);
          row.appendChild(cell5);
          row.appendChild(cell0a); // 6
          row.appendChild(cell0b); // 7
          tbody.appendChild(row); 
          
          row1 = document.createElement("TR");
          cell6 = document.createElement("TD");   
          /* var cr = document.createElement('input');
          cr.type ="checkbox";
          cell6.appendChild(cr); */
  
          row1.appendChild(cell6);
          tbody.appendChild(row1);      
  };
}

function openthread(projectname, threadname) {
  //alert(projectname);
  //alert(threadname);
  window.open('../en/threadLoad.jsp?database=<%=database%>&projectname=' +projectname+'&threadfocus='+threadname);      
}

</script>
<script>
    function findSavedProjects(callback) {
      $.getJSON('/ITM2/PublishSaves',{'user':"${username}",'database':"${database}"}, processResponse);
      function processResponse(data) {
        var ulelem = $(document.createElement('ul'));
        $('#saved-projects ul').remove();
        $('#saved-projects a').after(ulelem);
        $.each(data, function(key, val) { 
          ulelem.append('<li><a href="javascript:clickSaved('+"'"+val.projectName+"',"+"'"+val.date+"'"+')">' + val.projectName + ' ' + val.date + '</a></li>');
        });

        if (callback !== undefined) {
          callback();
        }
      }
    }
    

    function findPublishedProjects(callback) {
      $.getJSON("../BrowsePublishServlet", "project=${database}&location=1", processResponse);
      function processResponse(data) {  
        var ulelem = $(document.createElement('ul'));
        $('#published-projects ul').remove();
        $('#published-projects a').after(ulelem);
        $.each(data, function(key, val) { 
          ulelem.append('<li><a href="javascript:clickSaved('+"'"+val.name+"',"+"''"+')">' + val.name + '</a></li>');
        });

        if (callback !== undefined) {
          callback();
        }
      }
    }


    function findSavedAndPublishedProjects() {
      findPublishedProjects(function() {
        findSavedProjects(function() {
          $( "#menu" ).menu({ position: { my: "left top", at: "left-150 top+5" }, icons: { submenu: " ui-icon-carat-1-w" } });
        });
      });
    }

    $(document).ready(function(){ 
        var ShowID, highlightedOnly = false;       
        var msg='do not select idea thread';
        var projectName="",selectedGrade,showID;        
      
        $("#project").change(function(){            
            var str="", temp;
            str=$(this).val();
            projectName=str.split("(");
            projectName=projectName[0];            
            temp=str.split('Grade：');
            selectedGrade=temp[1][0];
        });//end change
        
        $("#main_table a").click(function(){
            //disable TF form when no project is selected
            if(projectName.length>0){
            //enable publish btn
            $('#publish').removeAttr('disabled');
            var threadsName=[];
           showID=$('#check1').prop('checked');
            $('#hor-minimalist-a tr').has(':checkbox:checked').find('td:eq(0)').each(function() {
              threadsName.push($(this).text());
            });
                //clear hiden inputs               
                $('#li_save').remove();
               //make parameters               
               $('#tr_form').append('<input type="hidden" name="projectname" value="'+projectName+'" />');               
                            
                if(showID){
                   $('#tr_form').append('<input type="hidden" name="showID" value="'+showID+'" />'); 
                }
                $('#tr_form').append('<input type="hidden" name="grade" value="'+selectedGrade+'" />'); 
               
                var teacherLink="../en/TeacherReflection.jsp?database=<%=database%>";
               // $("#main_table a").attr('href',teacherLink);
            $('#tr_form').submit();
            }
            return false;
        });//end click
        $('#save').click(function(e) {
          var threadsName = [], highlightsOption = [];

          $('#publish').removeAttr('disabled');
          showID = $('#check1').prop('checked');
          
          $('#hor-minimalist-a tr').has('input[id=option1]:checked').find('td:eq(0)').each(function() {
             threadsName.push($(this).text());
             highlightsOption.push(false);
           });

          $('#hor-minimalist-a tr').has('input[id=option2]:checked').find('td:eq(0)').each(function() {
            threadsName.push($(this).text());
            highlightsOption.push(true);
           });

          // make JSON
          console.log('highlightsOption ', highlightsOption);
          var saveObj = new Object();
          var username = "${username}";
          saveObj.highlightsOption = highlightsOption;
          saveObj.projectName = projectName;
          saveObj.showID = showID;
          saveObj.threadsName = threadsName;               
          saveObj.userID = username;
          saveObj.grade = selectedGrade;
          saveObj.db = '<%=database%>';
          var json = JSON.stringify(saveObj);

          //post data to db
          $.post("/ITM2/SavePublishServlet", {json:json,'database':'<%= database%>'},  function(data){
              $('.outter div:eq(1)').insertAfter( $(this) )
                .fadeIn('slow')
                .animate({opacity: 1.0}, 3000)
                .fadeOut('slow');
              e.preventDefault();
          });//end post
          alert("Project Setting has been saved");
          return false;
        });//end click
        $('#publish').click(function(){
          var threadsName = [], highlightsOption = [];
          
          $('#hor-minimalist-a tr').has('input[id=option1]:checked').find('td:eq(0)').each(function() {
             threadsName.push($(this).text());
             highlightsOption.push(false);
           });

          $('#hor-minimalist-a tr').has('input[id=option2]:checked').find('td:eq(0)').each(function() {
            threadsName.push($(this).text());
            highlightsOption.push(true);
           });


          //todo get teacher reflection data
          if(projectName.length>0){
              //selected a project   
              var saveObj=new Object();
              var username="${username}";
              showID=$('#check1').prop('checked');
              saveObj.projectName=projectName;
              saveObj.showID=showID;                              
              saveObj.userID=username;
              saveObj.grade=selectedGrade;
              saveObj.db='<%= database%>';
              saveObj.threadsName = threadsName;
              saveObj.highlightsOption = highlightsOption;
              var json=JSON.stringify(saveObj);
              $.post('/ITM2/PublishProjectServlet', {json:json, 'database':'<%=database%>'}, function(){
                  $('.outter div:last').insertAfter( $(this) )
                              .fadeIn('slow')
                              .animate({opacity: 1.0}, 3000)
                              .fadeOut('slow');
              });//end post
          }
           $('#publish').attr('disabled','disabled');
           alert("Project Setting has been publish");
          return false;
        });//end click
       /*
       * Cancel btn
       * Go back to newhomepage
        */
        $('#cancel').click(function() {
            window.location = "/ITM2/en/New_HomePage.jsp?database=<%= database%>";
            });//end cancel click
       
        $( ".sideBar_right" ).accordion({
                active: false,
                collapsible: true,
                heightStyle: "fill"
                });//end accordion
    });       

    $(function() {
       findSavedAndPublishedProjects();       		
    });
    
    var clickSaved=function(pname,date) {
    	SelectionChange(pname,2);
    	$("#project option").filter(function(){
    		  return ($(this).text().split("( Teacher",1)==pname); 
    	}).prop('selected', true);
    	};     
  
    
</script>

<LINK REL=StyleSheet HREF="../css/stickyhead.css" TYPE="text/css"></LINK>
<LINK REL=StyleSheet HREF="../css/PublishProject.css" TYPE="text/css"></LINK>
</head> 

<body class="hidbody">
<div id="sticky">
        <div id="header">
        <table border="0">
                <tbody><tr>
                        <td><img src="../img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
                        <td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="15" width="900">
                        &nbsp;&nbsp;<center>Publish a project to ITM</center>
                        </td>
                </tr>
        </tbody></table>
    </div>
   </div>                       
<%
String r_thread[] = new String[1000];
Operatedb opdb_thread = null;
ResultSet rs_thread = null;
/*******************read current project's thread*************************************/
    

        

%>
<div id = "main">
<br />
<p>The ITM network helps students and teachers from around the world to share their knowledge building work through publishing idea threads that involve productive conversations and progress</p>
<table id="main_table">
  <tr>
    <td><p>1.Choose an inquiry project to be shared with classrooms</p>
      <p>
  <select name ="project" id ="project" class="form-control" style="width:70%" OnChange ="SelectionChange(value,1)" >
    <option selected="selected"><b>Choose exploration topic</b></option>
  </select> 
        <%
        /*************read all projects from database****************/
        sqls sobj = new sqls();
        //Operatedb op = new Operatedb(sobj,database);
        ResultSet rs = null;
        ResultSet rs_grade = null;
        ResultSet rs_group = null;
        ResultSet rs_projectid = null;
        ResultSet countnoteid = null;
        ResultSet countauthors = null;
        ResultSet noteid_from_date = null;
        ResultSet noteid_to_date = null;
        
        ResultSet noteids = null;
        
        //String[] columnNames = new String[4];
        //columnNames[0] = "projectname";
        //columnNames[1] = "teacher";
        //columnNames[2] = "fromyear";
        //columnNames[3] = "toyear";
        Statement stmt =  sobj.Connect(database);
        Statement stmt0 = sobj.Connect(database);
        Statement stmt01 = sobj.Connect(database);
        Statement stmt02 = sobj.Connect(database);
        
        String strsql_project = "Select idProject, projectname, teacher, school, fromyear, toyear from project";
        //project_grade where project.idproject = project_grade.projectid";
        //rs = op.GetMultipleRecordsFromDB("project", columnNames,"");
        rs = stmt.executeQuery(strsql_project);
        //stmt.close();
         //get existing threads
        
        String[] tablearray = new String[2];
        opdb_thread = new Operatedb(sobj,database);
        
        tablearray[0]="project";tablearray[1]="project_thread";
        
        String projectname="";
        while(rs.next())
        {
                projectname = rs.getString("projectname");
                String teacher = rs.getString("teacher");
                String school = rs.getString("school");
                int fromyear = rs.getInt("fromyear");
                int toyear = rs.getInt("toyear");
                //String grade = rs.getString("grade");
                int idProject = rs.getInt("idProject");
		        String strsql_grade = "select grade from project_grade where projectid ="+idProject;
		        rs_grade = stmt0.executeQuery(strsql_grade);
		        
		        //get the grade
		        String grade = "";
		        while(rs_grade.next())
		        {
		          if(rs_grade.isFirst())
		          {
		             grade = rs_grade.getString("grade");
		          }
		          else
		          {
		            grade += ","+rs_grade.getString("grade");
		          }
		        }
                //Get the project group
                String strsql_group = "select title from group_table, project_group where"+ 
                " group_table.idgroup = project_group.groupid and project_group.projectid="+idProject;
                rs_group = stmt01.executeQuery(strsql_group);
                String group = "";
                while(rs_group.next())
                {
                  if(rs_group.isFirst())
                  {
                     group = rs_group.getString("title");
                  }
                  else
                  {
                    group += ","+rs_group.getString("title");
                  }
                }
                //Get the project threads
                String strcon = "projectname='"+ projectname +"' and idproject=projectid group by threadfocus";
               // Debugger;
                rs_thread = opdb_thread.MulGetRecordsFromDB(tablearray,"threadfocus",strcon,2);
                //console.log(rs_thread);
                int cr_thread = 0;
                String rs_thread_nm = null;
                String notecount = null;
                String authorcount = null;
                String fromdate = null;
                String todate = null;
                
                while (rs_thread.next()){            
                rs_thread_nm = rs_thread.getString(1);
                
                Statement stmt1 = sobj.Connect(database);
                String strsql_noteid ="select  count(distinct thread_note.noteid) from  thread_note inner join  note_table on  thread_note.noteid =  note_table.noteid inner join  author_note on  thread_note.noteid =  author_note.noteid inner join  author_table on  author_note.authorid =  author_table.authorid and  thread_note.threadfocus='"+rs_thread_nm +"'";
                countnoteid= stmt1.executeQuery(strsql_noteid);
                while(countnoteid.next())
                {
                        notecount = countnoteid.getString(1);
                        System.out.println("notecount"+notecount);
                }
                stmt1.close();
                if(countnoteid != null)
                {
                        try{
                                countnoteid.close();
                        }catch(SQLException e){
                                e.printStackTrace();
                        }
                }
                
                Statement stmt2 = sobj.Connect(database);
                String strsql_author ="select count(distinct authorid) from  thread_note inner join  author_note on  thread_note.noteid =  author_note.noteid and  thread_note.threadfocus='"+rs_thread_nm+"'";
                countauthors = stmt2.executeQuery(strsql_author);
                while(countauthors.next())
                {
                        authorcount = countauthors.getString(1);
                        System.out.println("authorcount"+authorcount);
                }
                stmt2.close();
                if(countauthors != null)
                {
                        try{
                                countauthors.close();
                        }catch(SQLException e){
                                e.printStackTrace();
                        }
                }
                
                String strarr1[] = new String[2];
                String from = null;
                Statement stmt3 = sobj.Connect(database);
                String strsql_fromdate ="select min(createtime) from  note_table,thread_note  where note_table.noteid = thread_note.noteid and  thread_note.threadfocus='"+rs_thread_nm+"'";
                noteid_from_date = stmt3.executeQuery(strsql_fromdate);
                while(noteid_from_date.next())
                {
                        from = noteid_from_date.getString(1);
                        if(from != null)
                        {
                                strarr1 = from.split(" ", 2);
                        }
                        else
                        {
                                strarr1[0] = null;
                        }
                        fromdate = strarr1[0];
                        System.out.println("fromdate"+fromdate);
                }
                stmt3.close();
                if(noteid_from_date != null)
                {
                        try{
                                noteid_from_date.close();
                        }catch(SQLException e){
                                e.printStackTrace();
                        }
                }
                
                String strarr2[] = new String[2];
                String to = null;
                Statement stmt4 = sobj.Connect(database);
                String strsql_todate ="select max(createtime) from note_table, thread_note where note_table.noteid = thread_note.noteid and  thread_note.threadfocus='"+rs_thread_nm+"'";
                noteid_to_date = stmt4.executeQuery(strsql_todate);
                while(noteid_to_date.next())
                {
                        to = noteid_to_date.getString(1);
                        if(to != null)
                        {
                                strarr2 = from.split(" ", 2);
                        }
                        else
                        {
                                strarr2[0] = null;
                        }
                        todate = strarr2[0];
                        //todate = noteid_to_date.getString(1);
                        System.out.println("todate"+todate);
                }
                stmt4.close();
                if(noteid_to_date != null)
                {
                        try{
                                noteid_to_date.close();
                        }catch(SQLException e){
                                e.printStackTrace();
                        }
                }
                //Get the notes information for each thread.
                
                %>
        <script type ="text/javascript">
                          UpdateProjectThreadList('<%=projectname%>','<%= rs_thread_nm %>',{"notecount" :'<%= notecount%>',"authorcount":'<%= authorcount %>',"fromdate":'<%= fromdate%>',"todate":'<%= todate%>'});
                        </script>
        <%
            sobj.Close();
                }
                if(rs_thread != null){
                        try{
                                rs_thread.close();
                        }catch(SQLException e){
                                e.printStackTrace();
                        }
                }               
                %>
        <script type ="text/javascript">
                        var projectname = "<%= projectname %>";
                        var teacher = "<%= teacher %>";
                        var fromyear = "<%= fromyear %>";
                        var toyear = "<%= toyear %>";
                        var grade = "<%= grade %>";
                        var school = "<%= school %>";
                        var group = "<%=group %>";
                        updateList(projectname,teacher,school,grade,fromyear,toyear,group); 
                </script>
        <% 
      
       //sobj.Close();
        }
     %>
      </p>
     
    </td>
    <td>
      <ul id="menu">
        <li id='saved-projects'>
          <a >Saved</a>
        </li>
        <li id='published-projects'>
          <a >Published</a>
        </li>
      <ul>
    </td>
  </tr>
 <tr>
   <td><p style="width:70%">2. Please choose idea thread(s) to be shared. For each idea thread, 
             you can chose to share all the notes in the thread or to share 
             the highlighted notes only.</p>
     <div>
     <!--  NOTE This is not required as of now -->
      <!--  <input type="button" class="btn btn-primary" value="Select All" onclick="checkByParent('.thread_info', true);">
       <input type="button" class="btn btn-primary" value="Cancel" onclick="checkByParent('.thread_info', false);"> -->
     </div>
     <div class ="thread_info" id ="thread_info"> 
  </div></td>
 </tr>
 <tr>
   <td><br />3. Privacy Settings  
      <input type="checkbox" id="check1" checked="checked" >Shows all users id.
      <br />
 </td>
 </tr>
<tr>
    <!--<td>4. share the teacher comments<a href="#">Record/View the teacher commments</a>-->
    <td>4. share the teacher comments <a href="#">Record/View the teacher commments</a>
     <form method="get" action="../TeacherReflectionServlet?id=${save.pid}" id="tr_form" target="target">
       <p>&nbsp;</p>
     </form>      
    </td>
</tr>

<tr>
<td>
<div id="button" style ="padding-left:300px;float:left">
<ul>
    <li><button id="cancel" class="btn btn-primary">Cancel</button></li>
    <li id="li_save"><button id="save" class="btn btn-primary">Save</button></li>
    <li><button disabled="disabled" value="Publish" id="publish" class="btn btn-primary">Publish</button></li>
</ul>
</div>

</td>
</tr>
</table>


</div>



</body>
<div class="outter">
        <div class="inner temp_alert">Unselected idea threads</div>
        <div class="inner temp_alert">Saved draft</div> 
         <div class="inner temp_alert">Project published</div>  
</div>
</html>