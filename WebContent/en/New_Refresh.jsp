<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM2/index.jsp");
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Refresh Database</title>
<script type="text/javascript">
function OnCancel(){
	window.location= "/ITM2/en/New_HomePage.jsp?database=<%=request.getParameter("database")%>";
}
</script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
</head>

<%
/*********************read the database refresh time***********************************/
	String database = request.getParameter("database");
	String strrefresh = null;
	String strcon = "dbname='"+database+"'";
	ResultSet rs = null;
	Operatedb opdb_refresh = new Operatedb(new sqls(),"itm");
	try{
		rs = opdb_refresh.GetRecordsFromDB("db_table","refreshtime",strcon);
		if(rs.next()){
		strrefresh= rs.getString(1);		
		}
		opdb_refresh.CloseCon();
	}catch(SQLException e){
		e.printStackTrace();
	}
%>
<body>
	<div class="loading" style="display:none;"><img src="../img/ajax-loader.gif"/>Update Database Processing .....</div>
	The database was updated at <%=strrefresh %> 
	<br/>
	Do you want to update the database data now? It may take a few minutes....
	<br/>
	<form name="refresh_from" action="/ITM2/RefreshDB?database=<%=database%>&projectname=<%=request.getParameter("projectname")%>" method="post">
	<input type="button" value="Update" id="submit"/>
	<input type="button" value="Cancel" onclick="OnCancel()"/>
	</form>
</body>


<script type="text/javascript">

$("#submit").click(function(){
	 $(".loading").css("display", "block");
	 $("input").css("display", "none");
	 $.post('/ITM2/RefreshDB?database=<%=database%>&projectname=<%=request.getParameter("projectname")%>', "",
			 function(result){
		 $(".loading").css("display", "none");
				
	 })
});
</script>

</html>