<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*,com.google.gson.*, itm.servlets.*"%>
<%
String proj_name = request.getParameter("projectname");
String thread_name = request.getParameter("threadname");
String timestamp = request.getParameter("timestamp");

sqls s = new sqls();
Operatedb opdb = new Operatedb(s,request.getParameter("database"));
ResultSet focusrs = opdb.getC().executeQuery("select idProject from  project where projectname='"+proj_name+"';");
focusrs.next();
int pid = focusrs.getInt(1);
if(thread_name.equals("all")){
	focusrs = opdb.getC().executeQuery("select threadfocus from  project_thread where  project_thread.projectid="+pid+";");
}
else {
	String[] focuses = thread_name.split("@");
	String query = "select threadfocus from  project_thread where  project_thread.projectid="+pid+" and (";
	for (int i = 0;i < focuses.length;i++){
		query = query + "  project_thread.threadfocus = '"+focuses[i]+"' or ";
	}
	query = query.substring(0,(query.length()-3));
	query = query + ");";
	focusrs = opdb.getC().executeQuery(query);
}
String[] focus = new String[1000];
int tmpi = 0;
while(focusrs.next()){
	focus[tmpi] = focusrs.getString(1);
	tmpi++;
}

if(focusrs != null){
	try{
		focusrs.close();
	}catch(SQLException e){
		e.printStackTrace();
	}
}
int focusmax = tmpi;
for (int kk = 0;kk<tmpi;kk++) {
  
  String[] vecnode = new String[1000];
  String[] vecauthor = new String[1000];
  String[] vectitle = new String[1000];
  String[] veccontent = new String[1000];
  String[] vecdate = new String[1000];
  String[] vecview = new String[1000];
  String[] vechighlight = new String[1000];
  int veccnt = 0;
  
  int counter = 0;
  int authorcnt = 0;
  
  String data = PublishDB.GetPublishedProjectThreadData(proj_name, focus[kk], timestamp);
  JSONObject obj = PublishedThread.prepareData(data, false);
  System.out.println(obj);
  JSONArray dataArray = (JSONArray)obj.get("data_array");
  
  /// Currenlty assuming that noteids are unique (which may not be true)
  counter = dataArray.size();
  HashSet<String> uniqueAuthors = new HashSet<String>();
  while(veccnt < dataArray.size()) {
    JSONArray threadData = (JSONArray)dataArray.get(veccnt);
    int jIndex = 0;
    System.out.println(threadData.get(jIndex));
    vecnode[veccnt] = threadData.get(jIndex).toString();
    vecauthor[veccnt] = threadData.get(jIndex + 5).toString() + " " + threadData.get(jIndex + 6).toString();
    vectitle[veccnt] = threadData.get(jIndex + 3).toString();
    veccontent[veccnt] = threadData.get(jIndex + 4).toString();
    vecdate[veccnt] = threadData.get(jIndex + 2).toString();
    vecview[veccnt] = threadData.get(jIndex + 1).toString();
    vechighlight[veccnt] = threadData.get(jIndex + 1).toString();
    
    /// Authors are listed from column 5.
    for (int jj = 5; jj < threadData.size(); ++jj) {
      uniqueAuthors.add(threadData.get(jj).toString());
    }
    
    veccnt++;
  }
  authorcnt = uniqueAuthors.size();
  /* 
  {"data_array":[["33760","1","2013-02-26 14:05:22.0","how does under ground water get back into  lakes and rivers?","how does under ground water get back into lakes and rivers?  ","Isaac","Feiner"],["36443","1","2013-03-26 13:36:15.0","asid"," is the rain has asid in it","Kingsley","Stein Quao"],["39277","1","2013-04-09 13:54:17.0","pipes in the ground","  is  that   the  water leiks into the ground and goes through a pipe in the ground and goes back under the lake.    ","Anna","Salamon"],["46926","1","2013-04-30 13:59:37.0","moisture","I think water is wet because of the moisture  ","Clara","Wa Parkin"]]}
  in the Close function of sqls and the connum is 11 */
	
   /* String query = "select thread_note.noteid, author_table.firstname, author_table.lastname, " + 
       "note_table.notetitle, note_table.notecontent, note_table.createtime, view_table.title, thread_note.highlight from  " + 
       "thread_note inner join  note_table on  thread_note.noteid =  note_table.noteid " +
       "inner join  author_note on  thread_note.noteid =  author_note.noteid inner join " + 
       "author_table on  author_note.authorid =  author_table.authorid inner join " +
       "view_note on note_table.noteid = view_note.noteid inner join view_table on view_note.viewid = view_table.idview " +
       "and thread_note.threadfocus='"+focus[kk]+"' and thread_note.projectid='"+pid+"';";
       
	ResultSet noters = opdb.getC().executeQuery(query);
	String[] vecnode = new String[1000];
	String[] vecauthor = new String[1000];
	String[] vectitle = new String[1000];
	String[] veccontent = new String[1000];
	String[] vecdate = new String[1000];
	String[] vecview = new String[1000];
	String[] vechighlight = new String[1000];
	int veccnt = 0;
	while(noters.next() && veccnt<1000) {
		vecnode[veccnt] = noters.getString(1);
		vecauthor[veccnt] = noters.getString(2) + " " + noters.getString(3);
		vectitle[veccnt] = noters.getString(4);
		veccontent[veccnt] = noters.getString(5);
		vecdate[veccnt] = noters.getString(6);
		vecview[veccnt] = noters.getString(7);
		vechighlight[veccnt] = noters.getString(8);
		veccnt++;
	}
	//count distinct noteid's
	ResultSet countnoteid = opdb.getC().executeQuery(
			"select  count(distinct thread_note.noteid) from  thread_note inner join  note_table on  thread_note.noteid =  note_table.noteid inner join  author_note on  thread_note.noteid =  author_note.noteid inner join  author_table on  author_note.authorid =  author_table.authorid and  thread_note.threadfocus='"+focus[kk]+"'and thread_note.projectid='"+pid+"';");
	String counter = null;
	if(countnoteid.next())
	{
		counter = countnoteid.getString(1);
	}
	
	if(noters != null){
		try{
			noters.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	//count distinct authorid's
	ResultSet tars = opdb.getC().executeQuery(
			"select count(distinct authorid) from  thread_note inner join  author_note on  thread_note.noteid =  author_note.noteid and  thread_note.threadfocus='"+focus[kk]+"'and thread_note.projectid='"+pid+"';"
			);
	String authorcnt = null;
	if (tars.next()){
		authorcnt = tars.getString(1);
	}
	
	if(tars != null){
		try{
			tars.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	} */
	
	%>
	<!-- <div class="rawfocus" focusname="//TODO" -->
	<div class="rowfocus" focusname="<%=focus[kk]%>" au_num="<%=authorcnt%>" nt_num="<%=counter%>">
		<%
		veccnt = 0;
		while (vecnode[veccnt] != null) {
			String tmpnode = vecnode[veccnt];
			String tmpcontent = veccontent[veccnt];
			String tmpdate = vecdate[veccnt];
			String tmptitle = vectitle[veccnt];
			String tmpauthor = vecauthor[veccnt];
			String tmpview = vecview[veccnt];
			String tmphighlight = vechighlight[veccnt];
			while(vecnode[veccnt].equals(vecnode[veccnt + 1])) {
				tmpauthor += ","+ vecauthor[veccnt+1];
				veccnt++;
			}
		%>
		<!-- <div class="rawnote" noteid="//1" author="//2+3" date="//6"-->
		<div class="rawnote" noteid="<%=tmpnode%>" author="<%=tmpauthor%>" date="<%=tmpdate%>" view="<%=tmpview%>" highlight="<%=tmphighlight%>">
			<div class="references">
				<%
				ResultSet refrs = opdb.getC().executeQuery("select tonoteid from  note_note where fromnoteid="+tmpnode+" and linktype = 'references';");
				while(refrs.next()) {
					%>
						<div class="reference" target="<%=refrs.getString(1)%>"></div>
					<%
				}
				
				if (refrs != null) {
					try {
						refrs.close();
					} catch(SQLException e) {
						e.printStackTrace();
					}
				}
				%>
			</div> <!-- class="references" -->
			<div class="buildsons">
				<%
				ResultSet bufrs = opdb.getC().executeQuery("select tonoteid from  note_note where fromnoteid="+tmpnode+" and linktype = 'buildson';");
				while (bufrs.next()) {
				%>
				<div class="buildson" target="<%=bufrs.getString(1)%>"></div>
					<%
					}
					if (bufrs != null) {
						try {
							bufrs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						} catch (Exception e) {
						  e.printStackTrace();
						}
					}
					%>			
             </div> <!-- class="buildsons" -->
			 <div class="annotates">
				<%
				ResultSet anfrs = opdb.getC().executeQuery("select tonoteid from  note_note where fromnoteid="+tmpnode+" and linktype = 'annotates';");
				while (anfrs.next()) {
					%>
						<div class="annotate" target="<%=anfrs.getString(1)%>"></div>
					<%
				}
		
				if(anfrs != null){
					try {
						anfrs.close();
					} catch(SQLException e) {
						e.printStackTrace();
					} catch(Exception e) {
					   e.printStackTrace();  
					}
				}
				%>	
		      </div> <!-- class="annotates" -->
			  <div class="rawtitle"><%=tmptitle%></div>
			  <div class="rawcontent"><%=tmpcontent%></div>
		</div> <!-- class=rawnote"" -->
		<%
		veccnt++;
		}
		%>
	</div> <!-- class="rowfocus" -->
<%
}
%>
<%
	if(opdb != null){
		opdb.CloseCon();
	}
	if(s != null){
		//s.Close();
	}
   %>