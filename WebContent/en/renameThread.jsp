<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	/************validate the user session*******************/
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM2/index.jsp");
		}
	}

	String strdb = request.getParameter("database");
	String strproject = request.getParameter("projectname");
	String strold = request.getParameter("oldname");
	String strnew = request.getParameter("newname");
	
	sqls s = new sqls();
	Statement stmt = s.Connect(strdb);
	String strsql_pro = null, strsql_note = null,strsql_more=null,strsql_idea=null,strsql_problem=null;
	String strid = null;
	Operatedb opdb = new Operatedb(s,strdb);
	
	//get the projectid
	ResultSet temprs=null;
	temprs=opdb.GetRecordsFromDB("project","idproject","projectname='"+strproject+"';");
	temprs.next();
	strid = temprs.getString(1);
	if(temprs != null){
		try{
			temprs.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	if(opdb != null){
		opdb.CloseCon();
	}
	
	strsql_pro = "UPDATE project_thread SET threadfocus='"+strnew+"' WHERE projectid="+strid+
	" AND threadfocus='"+strold+"'";
	strsql_note = "UPDATE thread_note SET threadfocus='"+strnew+"' WHERE projectid="+strid+
	" AND threadfocus='"+strold+"'";
	strsql_more = "UPDATE thread_more SET threadfocus_more='"+strnew+"' WHERE projectid="+strid+
	" AND threadfocus_more='"+strold+"'";
	strsql_idea = "UPDATE thread_idea SET threadfocus_idea='"+strnew+"' WHERE projectid="+strid+
	" AND threadfocus_idea='"+strold+"'";
	strsql_problem = "UPDATE thread_problem SET threadfocus_problem='"+strnew+"' WHERE projectid="+strid+
	" AND threadfocus_problem='"+strold+"'";
	try{
		stmt.executeUpdate(strsql_pro);
		stmt.executeUpdate(strsql_note);
		stmt.executeUpdate(strsql_more);
		stmt.executeUpdate(strsql_idea);
		stmt.executeUpdate(strsql_problem);
	}catch(SQLException e){
		e.printStackTrace();
	}finally{
		if(stmt != null){
			try{
				stmt.close();	
			}catch(SQLException e){
				e.printStackTrace();
			}
			
		}
		if(s != null){
			s.Close();
		}
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

</body>
</html>