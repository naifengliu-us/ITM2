<%-- 
    Document   : Saves_PublishProject
    Created on : May 12, 2013, 8:39:59 PM
    Author     : Stan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Publish Saved Project</title>
        
        <link href="css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
        <LINK REL=StyleSheet HREF="css/stickyhead.css" TYPE="text/css"></LINK>
        <LINK REL=StyleSheet HREF="css/PublishProject.css" TYPE="text/css"></LINK>
        <style>           
            h2{
                padding: 0;
                font-family: "Whitney Cond A","Whitney Cond B","ronnia-condensed",sans-serif;
                font-weight: 700;
                font-style: normal;
                line-height: 1.1;
            }
        </style>
        <script src="js/en/jquery.js" type="text/javascript"></script>	
        <script src="js/en/jquery-ui-1.10.3.custom.min.js"></script>
        <script>
        var projectName="${pInfo.name}",selectedGrade,showID;
        function toOption(pname,teacher,grade,schoolYear,school){
            selectedGrade=grade;
            var option=pname+'( Teacher: '+teacher+'. Grade: '+grade
                +'. School Year: '+schoolYear+'. School: '+school+' )';
            return option;
        }

//add threads logic goes here, if we need this function again.

       
            $(document).ready(function(){                
                $( "#publish_dialog" ).dialog({
                autoOpen: false
                });
                $( "#delete_dialog" ).dialog({
                autoOpen: false
                });
                
                $( "#button button:eq(1)" ).click(function() {
                    $.get("OperateSave", {deleted:true,pid:"${save.pid}"}, callback).fail(function(){ alert ("Delete failed")});
                    function callback(){
                        $( "#delete_dialog" ).dialog( "open" );  
                         setTimeout(function() {
                            window.location = "en/PublishProject.jsp?database=${database}";
                                                }, 3000);      
                    }      
                    return false;
                });//end delete click
                
                $('#button button:first').click(function(){
                    var saveObj=new Object();
                    var username="${username}";
                    showID=$('#check1').prop('checked');
                    saveObj.projectName=projectName;
                    saveObj.showID=showID;                              
                    saveObj.userID=username;
                    saveObj.grade=selectedGrade;
                    saveObj.db="${database}";
                    var json=JSON.stringify(saveObj);
                    $.post('PublishProjectServlet', {json:json},processResponse);//end post
                    function processResponse(){
                        $( "#publish_dialog" ).dialog( "open" );  
                         setTimeout(function() {
                            window.location = "en/PublishProject.jsp?database=${database}";
                                                }, 3000);   
                    }
                    return false;
                });//end publish click
                 $( "#button button:last" ).click(function() {                    
                            window.location = "en/PublishProject.jsp?database=${database}"; 
                            return false;
                });//end close click
                
        $('select option:first').text(toOption('${pInfo.name}','${pInfo.teacher}','${save.grade}','${pInfo.schoolYear}', '${pInfo.school}'));
        $('#check1').attr('checked', '${save.showID}');//asign show user ID value
      
        $("#project").change(function(){            
            var str="", temp;
            str=$(this).val();
            projectName=str.split("(");
            projectName=projectName[0];            
            temp=str.split('Grade: ');
            selectedGrade=temp[1][0];
        });//end change
        
         $("#main_table a").click(function(){
            var threadsName=[];
            showID=$('#check1').prop('checked');
            $('#button button:first').removeAttr('disabled');
            $('#hor-minimalist-a tr').has(':checkbox:checked').find('td:eq(1)').each(function() {
                threadsName.push($(this).text());
                });
               //make parameters               
               $('#tr_form').append('<input type="hidden" name="projectname" value="'+projectName+'" />');               
                            
                if(showID){
                   $('#tr_form').append('<input type="hidden" name="showID" value="'+showID+'" />'); 
                }
                $('#tr_form').append('<input type="hidden" name="grade" value="'+selectedGrade+'" />'); 
                $('#tr_form').append('<input type="hidden" name="id" value="'+'${save.pid}'+'" />'); 
            $('#tr_form').submit();
            return false;
        });//end TR click 
        
        $( ".sideBar_right" ).accordion({
                active: false,
		collapsible: true,
                heightStyle: "fill"
		});//end accordion
        $('.sideBar_right h3').click(function(){            
                   $.getJSON('PublishSaves',{'user':"${username}"},processResponse);
                        function processResponse(data){
                            $.each(data, function(key, val) {
                                var tableHTML='<li><a href="ShowPublishSave?id='+val.pid+'"><h4>';
                                    tableHTML+=val.projectName+'</h4><p>';                               
                                    tableHTML+=val.date+'</p></a></li>'; 
                                $('.sideBar_right ul').append(tableHTML);                          
                            });//end each
                        } 
         });//end saves click       
                      
     });
        </script>
    </head>
        <body class="hidbody">
            <div id="sticky">
                    <div id="header">
                    <table border="0">
                            <tbody><tr>
                                    <td><img src="img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
                                    <td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="15" width="900">
                                    &nbsp;&nbsp;<center>Post a topic to explore ITM network</center>
                                    </td>
                            </tr>
                    </tbody></table>
                </div>
            </div>			
   
    <div id = "main">
        <p>The ITM network helps students and teachers from around the world to share their knowledge building work through publishing idea threads that involve productive conversations and progress </p>
        <h2>You are editing the publish saved at:  ${save.date}</h2>
        <table width="717" id="main_table">
        <tr>
            <td width="709"><p>Choose an exploration project to share with classes in other school</p>
            <p>
        <select name ="project" id ="project"  style = "width:500px" OnChange ="SelectionChange(value)" >
            <option selected="selected" style="width:500px "></option>
            <c:forEach var="item" items="${ops}">                            
                                <option style="width:500px ">
                                    <c:out value="${item.name}"/> ( Teacher: <c:out value="${item.teacher}"/>.
                                    Grade: <c:out value="${item.grade}"/>.
                                    School Year: <c:out value="${item.schoolYear}"/>.
                                    School: <c:out value="${item.school}"/> )
                                </option>
            </c:forEach>
        </select>
            </p>

        </td>
        </tr>

        <tr>
        <td>Privacy Settings</td>
        </tr>
        <tr>
        <td>
                <form>
                    <input type="checkbox" id="check1" checked="checked" >Shows all users id.
                </form>
        After release topic, your responsibility is to manually check the essay content to ensure that no sensitive content and topic
        </td>
        </tr>
        <tr>
            <td>Share the teacher comments<a href="#">Record/View the teacher comments</a>
                <form method="get" action="TeacherReflectionServlet" id="tr_form" target="target"> 
                </form>
            </td>
        </tr>
        </table>
        <div id="accordion-resizer">
            <div class="sideBar_right">
                <h3>Saves</h3>
                <div>
                    <div id="dialog" title="Saves"></div>
                    <ul></ul>
                </div>  
            </div>
        </div>
    </div>
        <div id="button" style ="padding-left:40em;float:left">              
            <ul>                
                <li id="li_save"><button disabled="disabled" >Publish</button></li>
                <li><button >Delete</button></li>
                <li><button >Close</button></li>
            </ul>
         </div>
           
        <div id="publish_dialog" title="Publish Save">
         <p>Published</p>
        </div>
        <div id="delete_dialog" title="Delete Save">
         <p>Deleted</p>
        </div>
    </body>
</html>
