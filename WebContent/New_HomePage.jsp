<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ page session = "true" %>
<%@ page import="java.util.*,java.sql.*,code.*,database.*"%>
<%@page import="code.AccessToProjects"%>
<%@page import="code.Connect"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<%
/************validate the user session*******************/
	String struser = " ";
    String usertype =" ";
    String host =" ";
    String dbase=" ";
	boolean ifvalid = false;
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM2/index.jsp");
		}else{
			struser = (String)session.getAttribute("username");
			usertype = (String)session.getAttribute("usertype");
			host = (String)session.getAttribute("host");
			dbase = (String)session.getAttribute("dbase");
			ifvalid = true;
		}
	}
	
%>

<%
/*******************Fetch permissions for the existing usertype***************/
AccessToProjects atp = new AccessToProjects();
int publishedPermission = atp.fetchPermission("Published",usertype);
int currentKFdbPermission = atp.fetchPermission("Current_KF_db",usertype);
int groupPermission = atp.fetchPermission("Group", usertype);
int ownPermission = atp.fetchPermission("Own", usertype);
String projectn = request.getParameter("projectname");
String db = request.getParameter("database");

System.out.println("Host name is "+host);
System.out.println("Database is "+dbase);

boolean canEditThread = false;
if( projectn == null )
{
  //do nothing
}
else
{
    System.out.println("Username is" + struser);
	System.out.println("Usertype is" + usertype);
	System.out.println("Project  name is" + projectn);
	System.out.println("Database name is" + db);
    canEditThread = atp.canEdit(struser,usertype,projectn,db);
}

System.out.println("Username is" + struser);
System.out.println("Usertype is" + usertype);
System.out.println("Project  name is" + projectn);
System.out.println("Database name is" + db);
System.out.println("Published Permission" + publishedPermission);
System.out.println("currentKFdbPermission" + currentKFdbPermission);
System.out.println("groupPermission" + groupPermission);
System.out.println("ownPermission" + ownPermission);
System.out.println("############Can edit thread" +canEditThread);

%>
<%
		String database = request.getParameter("database");
		ResultSet rs = null,rs_exist = null;
		ResultSet rs_deleted = null,rs_read = null,rs_author_group_id = null;
		
		Operatedb opdb = null,opdb_thread=null,opdb_deleted = null,opdb_existng = null;
		sqls s = new sqls();
		String author_group_id = null;
		String delete = "1";
		/********************read all existing projects from database*************************/
		if(database != null){
			Statement stmt = s.Connect(database);
			String strcon = null;
			if(usertype.equalsIgnoreCase("manager") || usertype.equalsIgnoreCase("editor"))
			{
			   
			   String strsql_existing = "SELECT projectname,teacher,fromyear,toyear FROM project where deleted = 0";
			   rs = stmt.executeQuery(strsql_existing);	
			}
			else if(usertype.equalsIgnoreCase("reader") || usertype.equalsIgnoreCase("visitor"))
			{
				
				String strsql_read = "SELECT projectname,teacher,fromyear,toyear FROM project where deleted = 0";
				rs_read = stmt.executeQuery(strsql_read);
			}
			else if(usertype.equalsIgnoreCase("writer") || usertype.equalsIgnoreCase("researcher"))
			{
				
				/* 
				String strsql_author_group = "Select group_id from author_table,group_author where author_table.authorid = group_author.author_id and username ='"+ struser +"';";
				System.out.println(strsql_author_group);
				//rs_author_group_id = stmt.executeQuery(strsql_author_group);
				rs_author_group_id = stmt.executeQuery(strsql_author_group);
				if(rs_author_group_id.next())
				{
				   author_group_id = rs_author_group_id.getString("group_id");
				}
				System.out.println("author group id is "+ author_group_id);
				Statement stmt1 = s.Connect(database);
				String strsql_existing = "SELECT projectname,teacher,fromyear,toyear FROM project,project_group where deleted = 0 AND project.idproject = project_group.projectid AND project_group.groupid ='"+author_group_id+"' or project.projectowner = '"+ struser+"' group by project.projectname;";
				rs = stmt1.executeQuery(strsql_existing);
				Statement stmt2 = s.Connect(database);
				String strsql_read = "SELECT projectname,teacher,fromyear,toyear from project where deleted =0 and project.projectname NOT IN(SELECT projectname from project,project_group where deleted = 0 and project.idproject = project_group.projectid and project_group.groupid ='"+author_group_id+"' or project.projectowner = '"+ struser+"' group by project.projectname);";
			    rs_read = stmt2.executeQuery(strsql_read);*/
			    
			    String strsql_existing = "SELECT projectname,teacher,fromyear,toyear FROM project where deleted = 0";
				   rs = stmt.executeQuery(strsql_existing);	
			}
			else 
			{
				//do nothing for now.
			}
			Statement stmt1 = s.Connect(database);
			String strsql_deleted = "SELECT projectname,teacher,fromyear,toyear FROM project where deleted = 1";
			rs_deleted = stmt1.executeQuery(strsql_deleted);
		}
		
		ResultSet rs_thread = null;
		String r_thread[] = new String[1000];
		String teachername = null;
	   /**********************read current project information *******************************/
		String strname = request.getParameter("projectname");
		String strcon = null;
		if(strname != null){
			opdb = new Operatedb(s,database);
			
			strcon = "projectname='"+strname+"';";
			rs_exist = opdb.GetRecordsFromDB("project","projectname,teacher,school",strcon);
			if(rs_exist.next()){
				teachername = rs_exist.getString(2);
			}
		}
		

	/*******************read current project's thread*************************************/
	if(strname != null){
		//get existing threads
		String[] tablearray = new String[2];
		opdb_thread = new Operatedb(s,database);
		strcon = "projectname='"+strname+"' and idproject=projectid group by threadfocus";
		tablearray[0]="project";tablearray[1]="project_thread";
		rs_thread = opdb_thread.MulGetRecordsFromDB(tablearray,"threadfocus",strcon,2);
		int cr_thread = 0;
		while (rs_thread.next()){
			r_thread[cr_thread] = rs_thread.getString(1);
			cr_thread++;
		
		}
		if(rs_thread != null){
			try{
				rs_thread.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ITM HOME PAGE</title>
<LINK REL=StyleSheet HREF="css/menu.css" TYPE="text/css"></LINK>
<script language="javascript"type="text/javascript">
  var _projectname = "<%=request.getParameter("projectname")%>";
  var _database = "<%=request.getParameter("database")%>";
</script>
<script src="js/header_mapthread.js"></script>
<script src="js/drawMap.js"></script>
<script src="js/loadMap.js"></script>
<script type="text/javascript">

/*set permissions for various usertype*/
function updateControls(publishedPermission, currentKFdbPermission, groupPermission, ownPermission)
{
	var selp = document.getElementById("Create_new_Project");
	
	if ((parseInt(ownPermission, 10) & 8) != 8)
	{
		console.log("[DEBUG] User does not has create new project permissions");
		
		// Disable create functionality
		selp.removeAttribute("href");
		selp.removeAttribute("onclick");
		selp.style.color  = "gray";
	}
}

/*Control update database and publish project by manager and editor*/
function updateDatabaseControl(usertype)
{
	var selc = document.getElementById("update_db");
	// var selp = document.getElementById("publish_project");
	if(usertype != "editor" && usertype != "manager")
	{
		console.log("[DEBUG] User does not has update database permissions");
		//Disable update Database Functionality.
		//selc.removeAttribute("href");
		//selc.removeAttribute("onclick");
		//selc.setAttribute("id", "Create_new_Project_disabled");
		selc.style.color  = "gray";
		
	    // console.log("[DEBUG] User does not has publish project permissions");
		// Disable publish project Functionality.
		// selp.removeAttribute("href");
		// selp.removeAttribute("onclick");
		// selc.setAttribute("id", "Create_new_Project_disabled");
		// selp.style.color  = "gray"; 
		
	}
}

function updateDeletedProjectsControl(usertype)
{
  var del = document.getElementById("deleted_projects");
  var delp = document.getElementById("delete_p");
  if(usertype !="editor" && usertype != "manager")
  {
    console.log("[DEBUG] User does not has view deleted projects permissions");
	//Disable deleted projects Functionality.
	del.removeAttribute("href");
	del.removeAttribute("onclick");
	del.style.color  = "gray";
	if(delp != null)
	{
	  delp.style.visibility = "hidden";
	}
  }
 }

/*Control thread updates or creation*/
function updateControlThread(canEditThread)
{
	var selnt = document.getElementById("Create_new_Thread");
	if(selnt != null)
	{
		if (canEditThread == "false")
		{
			console.log("[DEBUG] User does not has create new thread permissions");
			
			// Disable create functionality
			selnt.removeAttribute("href");
			selnt.removeAttribute("onclick");
			selnt.style.color  = "gray";
		}
	}
}

//Disable the menu elements if there are no children
function UpdateMenu()
{
  var existing_proj = document.getElementById("open_edit_proj");
  var e_proj = document.getElementById("work_on_existing_project");
  var read_only_proj= document.getElementById("read_proj");
  var r_proj = document.getElementById("visit_a_project");
  var del_project = document.getElementById("del_proj");
  var d_proj = document.getElementById("deleted_projects");
  //alert(existing_proj.children.length);
  if(existing_proj == null || existing_proj.children.length == 0)
  {
    e_proj.removeAttribute("href");
    e_proj.removeAttribute("onclick");
    e_proj.style.color  = "gray";
     
  }
  //alert(read_only_proj);
  if(read_only_proj == null)
  {
    r_proj.removeAttribute("href");
    r_proj.removeAttribute("onclick");
    r_proj.style.color  = "gray";
     
  }
  //alert(del_project);
  if(del_project == null)
  {
    
    d_proj.removeAttribute("href");
    d_proj.removeAttribute("onclick");
    d_proj.style.color  = "gray";
  }
}
</script>

<script type="text/javascript" >
var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

function OnCreate()
{
	window.open ('New_CreateProject.jsp?database=<%=database%>'); 
}
function OnOpenProject(name)
{
	window.location='/ITM2/New_HomePage.jsp?database=<%=database%>&projectname='+name;
}

function OnCreateThread(){
	window.open('thread.jsp?database=<%=database%>&projectname=<%=strname%>');
}

function OnOpenThread(focus){
	window.open('threadLoad.jsp?database=<%=database%>&projectname=<%=strname%>&threadfocus='+focus);
}
function OnRefresh(){
	
	window.open('New_Refresh.jsp?database=<%=database%>');

}
// close layer when click-out
document.onclick = mclose;
</script>
</head>
<body  onload="UpdateMenu();updateDatabaseControl('<%= usertype%>');updateDeletedProjectsControl('<%= usertype%>');updateControls('<%= publishedPermission%>', '<%= currentKFdbPermission%>', '<%= groupPermission%>', '<%= ownPermission%>');updateControlThread('<%= canEditThread%>');"> <!--bgcolor="#E3F5F9"  -->
	<div id="sticky">
	<div id="header">
	<table border="0">
		<tr>
			<td rowspan="2"><img src="img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100" /></td>
			<td><img src="img/itmlogo840x70name.gif" alt="ITM LOGO" height="70" width="840" /></td>
		</tr>
		<tr>
			<td style="background-color:#01B0F1;">
			<ul id="sddm">
			<li><a href="#">Project</a>
			
			 <div class="m1">
			   <ul>
			  		<li><a onclick="OnCreate()"href="#" id ="Create_new_Project">New</a></li>
			  		<li id="existing_proj"><a href="" id="work_on_existing_project">Open/Edit</a> 
			              <% if(rs != null){
			            	  if(rs.isBeforeFirst()){
			              %>  
			              <div class ="exisitng_projects_div">
			                 <ul id="open_edit_proj">
			              <%while(rs.next()){%>
				               <li> <a onclick="OnOpenProject('<%=rs.getString(1)%>')" href="#" title='<%=rs.getString(1)%> <%=rs.getString("teacher")%>, <%=rs.getString("fromyear")%> - <%=rs.getString("toyear")%>)'><%=rs.getString(1)%></a></li>    
				          <% 
				          } %> 
				          </ul>
				        </div>
			  		    <% } }%>
			        </li>
			        <li id="visit_proj"><a href="" id="visit_a_project">Visit</a>
			  		     <%if(rs_read != null){
			  		    	 if(rs_read.isBeforeFirst()){
			  		     %>	
			  		     <div class ="visit_projects_div">
			           		<ul id="read_proj">
			  			  <%while(rs_read.next()){%>
				                <li> <a onclick="OnOpenProject('<%=rs_read.getString(1)%>')" href="#" title='<%=rs_read.getString(1)%> <%=rs_read.getString("teacher")%>, <%=rs_read.getString("fromyear")%> - <%=rs_read.getString("toyear")%>)'><%=rs_read.getString(1)%></a></li>
				         <% 
				         } %> 
				          </ul>
			             </div> 
			  		   <%  }}%>
			        </li>
			        <li id="deleted_proj"><a href=""  id="deleted_projects">Deleted</a>
			            <% if(rs_deleted != null){
			            	if(rs_deleted.isBeforeFirst()){
			            %>  
			            <div class ="deleted_projects_div" id ="delete_p">
			  			   <ul id="del_proj">
			  			<%while(rs_deleted.next()){%>
				             <li> <a onclick="OnOpenProject('<%=rs_deleted.getString(1)%>')" href="#" title='<%=rs_deleted.getString(1)%> <%=rs_deleted.getString("teacher")%>, <%=rs_deleted.getString("fromyear")%> - <%=rs_deleted.getString("toyear")%>)'><%=rs_deleted.getString(1)%></a></li> 
				          <% 
				         }%>
				         </ul>
				        </div>  
			  		<% } }%>
			        </li>
			  </ul>  
			 </div>
			</li>
			<%if(strname!= null){%>
			<li><a href="#">Thread</a>
		        <div class="m1">
		        <ul>
			        <% if(strname != null){%>
			         	<li><a onclick="OnCreateThread()" href="#" id="Create_new_Thread">New</a></li>
			        <% } %> 		
			  		<%if(r_thread[0] != null){%>
				        <%
				        int cnt = 0;
				        while(r_thread[cnt] != null){%>
				   
				       <li><a onclick="OnOpenThread('<%=r_thread[cnt]%>')"href="#"><%=r_thread[cnt]%></a></li>
				        <% 
				        cnt++;}
			        }%>      
			     </ul>
			    </div>
		      </li>
		    <li><a href="#">Map</a>
		      		<div class="m1" id="m3">
			      		<div id="m3threadlist">
			      		<form name="get_threads" id="get_threads">
			      		<%if(r_thread[0] != null){%>
			      		Select Threads to show<br>
			      		-------------<br>
					        <%
					        int cnt = 0;
					        while(r_thread[cnt] != null){%>
					        <div><input type="checkbox" name="threadcheck" value="<%=r_thread[cnt]%>"/><%=r_thread[cnt]%></div>
					        <% 
					        cnt++;}
				        }%>
			      		</form>
			      		</div>
			      		<div id="choosemapcontral">
			      		
			      		<a href="#" onclick="mapselectall()" style="float: left;width:98px;padding: 5px 0px;">Select All</a>
			      		<a href="#" onclick="mapreset()"style="float: right;width:98px;padding: 5px 0px;">Reset</a>
			      		</div>
		      		   
		      		<div>
		      		
		      		 <a href="#" onclick="MapLoad()">Show Map</a>
		      		
		      		</div>
		      	 </div> 		
		      </li>
			<% }else{%>
				<li class="active"><a href="#"><font color="gray">Thread</font></a></li>
				<li class="active"><a href="#"><font color="gray">Map</font></a></li>
			<% } %>
			    <li><a href="">Network</a>
			      <div class ="m1">
			          <ul> 
			    	     <li><a href="/ITM2/New_Refresh.jsp?database=<%=database%>&projectname=<%=strname%>&host=<%= host%>&dbase=<%= dbase %>" id ="update_db">Update Database</a></li>
			    	     <li><a href="/ITM2/PublishProject.jsp?database=<%=database%>" id ="publish_project">Publish Project</a></li>
			          </ul>
			       </div>
			     </li>
				<li><a href="/ITM2/index.jsp?logout=1&host=<%= host%>&dbase=<%= dbase %>">Logout</a>
<!-- 				<div class="m1"> -->
<!-- 			  		<a href="/ITM2/index.jsp?logout=1">Logout ITM</a> -->
<!-- 			  	</div> -->
			  	</li>
			  </ul>
			</td>
		</tr>
	</table><%if(strname != null){%>
		<div id="projinfo">
			<%
			int threadcnt = 0;
    		while(r_thread[threadcnt] != null){threadcnt++;} 
    		%>
	<p><font> You are working on Project:</font>
	<a href="infoproject.jsp?database=<%=database%>&projectname=<%=strname%>" target="_blank"><%=strname%></a> (Teacher: <%=teachername%>)&nbsp;&nbsp;This project has <%=threadcnt%> idea thread(s). Click "Thread" to <bold>add or view/edit</bold> threads.
	<%if (threadcnt>0){ %> Click "<bold>Map</bold>" to show threads.<%} %></p>
	<hr>
</div>
	
	
	<%} %>
			
</div>
</div>
<div id="wrapperu">
<div id="wrapper">
<%
if(strname==null){%>
<div style="visibility: visible;">
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>W</b>elcome to Idea Thread Mapper (ITM): A knowledge mapping tool to help student groups review, organize, and develop ideas during online discussions (e.g. in Knowledge Forum). Use the following functions  to reflect on your discussions:</p>
<p><b>Project</b>: Fill information about what you are studying (i.e. content area) and who you are;</p>
<p><b>Thread</b>:  Set a focus, find important ideas (i.e. notes), show them on a timeline, summarize progress and next steps;</p>
<p><b>Map</b>: Show your idea threads on a map, reflect on what  you have learned as a whole group and what you need to focus on next.</p>
<br><p>You can revisit and update your idea threads map over time as your online discussions continue. </p>
<br><p>Let ideas connect and grow for a better world!</p>
<hr>
<center><img src="img/nsf.gif" alt="nsf" height="42" width="42" /> The ITM tool is developed through a National Science Foundation Cyberlearning grant.<br>
PIs:  Dr. Jianwei Zhang and Dr. Mei-Hwa Chen at the University at Albany.
</center>
</div>
<%	
}
%>
<div id="raw_area"></div>
<div id="ctr">
		<ul id="flag">
		<li>&nbsp;&nbsp;&nbsp;&nbsp;Note :&nbsp;&nbsp;</li>
		<li id="showalltitle">&nbsp;|&nbsp;<a href="javascript:void(0)" onclick="showalltitle()";>Show Title</a>&nbsp;|&nbsp;</li>
		<li id="showallauthor"><a href="javascript:void(0)" onclick="showallauthor()";>Show Author</a>&nbsp;|&nbsp;</li>
		<li id="showalllink">
		<a href="javascript:void(0)" onclick="showbldlink()";>Show Build-on</a>&nbsp;|&nbsp;
		<a href="javascript:void(0)" onclick="showreflink()";>Show Reference</a>&nbsp;|&nbsp;
		</li>
		</ul>
		
		<ul id="zoom">
		<li>Zoom:&nbsp;&nbsp;</li>
		<li><a href="javascript:void(0)" onclick="bar(0)";>2 Day</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(1)";>1 Week</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(2)";>2 Weeks</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(3)";>1 Month</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(4)";>3 Month</a>&nbsp;|&nbsp; </li>
		<li><a href="javascript:void(0)" onclick="bar(5)";>All</a> </li>
		</ul>
</div>
<div id="time_area">
	<div id="time_left">Time:</div>
	<div id="time_right"></div>
</div>
<div id="bar_area">
	<div class="l_left" id="bar_left"></div>
	<div class="l_right" id="bar_right">
	<div id="bar_w_left" onclick="windowmoveleft()"></div>
	<div id="bar_b">
		<div id="pointer" onmousedown="ptdown(event)" onmouseup="ptup(event)"></div>
	</div>
	<div id="bar_w_right" onclick="windowmoveright()"></div>
	</div>
</div>

<div id="draw_area"></div>
</div>
</div>	
<%
	//release the database connection
	if(rs != null){
		   try{
			   rs.close();
		   }catch (SQLException e){
			   e.printStackTrace();
		   }
	}

	if(rs_deleted != null){
		   try{
			   rs_deleted.close();
		   }catch (SQLException e){
			   e.printStackTrace();
		   }
	}


	if(opdb != null){
		opdb.CloseCon();
	}
	
	if(opdb_thread != null){
		opdb_thread.CloseCon();
	}
	
	if(s != null){
		s.Close();
	}
%>

</body>
</html>
