<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ page session = "true" %>
<%@page import="code.AccessToProjects"%>
<%@page import="code.Connect"%>
<%@ page import="java.util.*,java.sql.*,code.*,database.*,java.text.SimpleDateFormat;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/************validate the user session*******************/
String user =" ";
String usertype =" ";
String dbase=" ";
String strdb =" ";
	session = request.getSession(false);
	if(session != null){
		if (session.getAttribute("username") == null){
			response.sendRedirect("/ITM2/index.jsp");
		}
		else
		{
		    user = (String)session.getAttribute("username");
			usertype = (String)session.getAttribute("usertype");
		}
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Publish an Inquiry Project to the ITM Network</title>
<link href="css/PublishProject.css" rel="stylesheet" type="text/css">
<style type="text/css">
.inButtonColor {
 background-image: url(images/button.jpg);
 width:197px;
 height:72px;}
.divinput{
margin: 0px 0px 0px 10px;
width: 300px;
float: left;
}
</style>

<%
String database = request.getParameter("database");
%>


<script type="text/javascript" >

function checkByParent(aId, aChecked) {
  var collection = document.getElementById(aId).getElementsByTagName('INPUT');
  for (var x=0; x<collection.length; x++) {
      if (collection[x].type.toUpperCase()=='CHECKBOX')
          collection[x].checked = aChecked;
  }
}

function check()
  {
  document.getElementById("check1").checked=true;
  }
function uncheck()
  {
  document.getElementById("check1").checked=false;
  }


function UpdateList(projectname,teacher,grade,fromyear,toyear)
{
	var p = document.getElementById("project");
	var pname = projectname +"( Teacher: "+teacher + " Grade: "+grade+" FromYear: "+fromyear+" ToYear: "+toyear+" )";
	//alert(pname);
	p.options.add(new Option(pname));
} 

//Project of javascript objects as threads with notecount ,authorcount,fromdate,todate
//Initialize a global javascript object

var projectThreadInfo = {};
var threadInfo ={};

function UpdateProjectThreadList(projectnm,thread_name,threadvals)
{
  if(!projectThreadInfo.hasOwnProperty(projectnm))
  {
    projectThreadInfo[projectnm] = {};
  }
  projectThreadInfo[projectnm][thread_name] = threadvals;
  
}

function SelectionChange(value)
{   
	
	var new_div = document.createElement("div");
	var data_div = document.getElementById("thread_info");
	new_div.style.height= 150;
	new_div.style.overflow ='auto';
  
	
	//var text = null;
	
	var p = document.getElementById("project");
	//Get the current project selected in drop down
	var str = p.value;
	//split to extract the project name
	var n = str.split("(",1);
	
	
	//var thread_str = null;
	
	//alert(n);
	
    for(var proj in projectThreadInfo)
      {
        if(proj == n)
        {
          var table = document.createElement('table');
          var tableth = document.createElement('thead');
    	  
          var tr1 = document.createElement('tr');
          
          var th1 = document.createElement('th');
          th1.setAttribute('scope', 'col');
          th1.appendChild(document.createTextNode('Select'));
          
          var th2 = document.createElement('th');
          th2.setAttribute('scope', 'col');
          th2.appendChild(document.createTextNode('Thread Name'));
          
          var th3 = document.createElement('th');
          th3.setAttribute('scope', 'col');
          th3.appendChild(document.createTextNode('# Notes'));
          
          var th4 = document.createElement('th');
          th4.setAttribute('scope', 'col');
          th4.appendChild(document.createTextNode('# Authors'));
          
          var th5 = document.createElement('th');
          th5.setAttribute('scope', 'col');
          th5.appendChild(document.createTextNode('From Date'));
          
          var th6 = document.createElement('th');
          th6.setAttribute('scope', 'col');
          th6.appendChild(document.createTextNode('To Date'));
          
          //var th7 = document.createElement('th');
          //th7.setAttribute('scope', 'col');
          //th7.appendChild(document.createTextNode('To'));
          
          tr1.appendChild(th1);
          tr1.appendChild(th2);
          tr1.appendChild(th3);
          tr1.appendChild(th4);
          tr1.appendChild(th5);
          tr1.appendChild(th6);
          //tr1.appendChild(th7);
          tableth.appendChild(tr1);
              
          table.setAttribute('id', 'hor-minimalist-a');
          
          table.appendChild(tableth);
          table.appendChild(document.createElement('tbody'));
          new_div.appendChild(table);
          populateTable(table, projectThreadInfo, n);
          /* for (var thread in projectThreadInfo[n])
           {
              //alert(thread +' | '+projectThreadInfo[n][thread].notecount+'notes | '+projectThreadInfo[n][thread].authorcount +'authors | from '+projectThreadInfo[n][thread].fromdate+' | to '+projectThreadInfo[n][thread].todate);
              var thread_str = thread.link("www.google.com") + ' | ' + projectThreadInfo[n][thread].notecount + 'notes | '+projectThreadInfo[n][thread].authorcount + ' authors | from '+projectThreadInfo[n][thread].fromdate+' | to '+projectThreadInfo[n][thread].todate;
              var span = document.createElement('span');
              span.innerHTML = thread_str;
              var cb = document.createElement('input');
              cb.type ="checkbox";
              new_div.appendChild(cb);
              new_div.appendChild(span);
              new_div.appendChild(document.createElement("br"));
          } */
        }
      }
    data_div.innerHTML = '';
    data_div.appendChild(new_div);
}

function populateTable(table, projectThreadInfo, n) {
  tbody = table.children[1];
  for (var thread in projectThreadInfo[n]) {
    
	  row = document.createElement("TR");
	  
	  cell0 = document.createElement("TD");
	  
	  var cb = document.createElement('input');
      cb.type ="checkbox";
	  cell0.appendChild(cb);
	  
	  cell1 = document.createElement("TD");
	  var span = document.createElement('span');
	  span.innerHTML = thread.link("");
	  
	  /// On click open the thread when it happens
	  span.setAttribute('onclick', 'openthread(\"'+n+'\",\"'+thread+'\");return false;');
	  cell1.appendChild(span);
	  
	  cell2 = document.createElement("TD");
	  cell2.appendChild(document.createTextNode(projectThreadInfo[n][thread].notecount));
	  
	  cell3 = document.createElement("TD");
	  cell3.appendChild(document.createTextNode(projectThreadInfo[n][thread].authorcount));
	  
	  //cell4 = document.createElement("TD");
	  //cell4.appendChild(document.createTextNode(projectThreadInfo[n][thread].notecount));
	  
	  cell4 = document.createElement("TD");
	  cell4.appendChild(document.createTextNode(projectThreadInfo[n][thread].fromdate));
	  
	  cell5 = document.createElement("TD");
	  cell5.appendChild(document.createTextNode(projectThreadInfo[n][thread].todate));
	  
	  row.appendChild(cell0);
	  row.appendChild(cell1);
	  row.appendChild(cell2);
	  row.appendChild(cell3);
	  row.appendChild(cell4);
	  row.appendChild(cell5);
	  //row.appendChild(cell6);
	  
	  tbody.appendChild(row);
  };
}

function openthread(projectname, threadname) 
{
  window.open('threadLoad.jsp?database=<%=database%>&projectname=' +projectname+'&threadfocus='+threadname);	
}

</script>

<LINK REL=StyleSheet HREF="css/stickyhead.css" TYPE="text/css"></LINK>
<LINK REL=StyleSheet HREF="css/PublishProject.css" TYPE="text/css"></LINK>
</head>	

<body>
<div id="sticky">
	<div id="header">
	<table border="0">
		<tbody><tr>
			<td><img src="img/itmlogo100x100.gif" alt="ITM LOGO" height="100" width="100"></td>
			<td style="background-color: #01B0F1;color:yellow;font-family:Arial, Helvetica, sans-serif;font-size:30px;" height="15" width="900">
			&nbsp;&nbsp;<center>Publish an Inquiry Project to the ITM Network</center>
			</td>
		</tr>
	</tbody></table>			
<%
String r_thread[] = new String[1000];
Operatedb opdb_thread = null;
sqls s = new sqls();
ResultSet rs_thread = null;
/*******************read current project's thread*************************************/
    

	

%>
<div id = "wrapper" style="overflow:auto">
<p>The ITM network helps students and teachers from around the world to share their knowledge building work through publishing idea threads that involve productive conversations and progress </p>
<table id="main_table">
  <tr>
    <td>1.Choose an inquiry project to share with other classrooms and schools
       <select name ="project" id ="project"  style = "width:500px" OnChange ="SelectionChange(value)" >
         <option selected="selected" style="width:500px "><b>Choose a Project</b></option>
      </select> 
     <%
        /*************read all projects from database****************/
        sqls sobj = new sqls();
        //Operatedb op = new Operatedb(sobj,database);
        ResultSet rs = null;
        ResultSet countnoteid = null;
        ResultSet countauthors = null;
        ResultSet noteid_from_date = null;
        ResultSet noteid_to_date = null;
        //String[] columnNames = new String[4];
        //columnNames[0] = "projectname";
        //columnNames[1] = "teacher";
        //columnNames[2] = "fromyear";
        //columnNames[3] = "toyear";
        Statement stmt = sobj.Connect(database);
        String strsql_project = "Select projectname,grade,teacher,fromyear,toyear from project,project_grade where project.idproject = project_grade.projectid";
        //rs = op.GetMultipleRecordsFromDB("project", columnNames,"");
        rs = stmt.executeQuery(strsql_project);
        
        
         //get existing threads
    	
    	String[] tablearray = new String[2];
    	opdb_thread = new Operatedb(s,database);
    	
    	tablearray[0]="project";tablearray[1]="project_thread";
    	
        
        while(rs.next())
        {
        	String projectname = rs.getString("projectname");
        	String teacher = rs.getString("teacher");
        	int fromyear = rs.getInt("fromyear");
        	int toyear = rs.getInt("toyear");
        	String grade = rs.getString("grade");
        	String strcon = "projectname='"+ projectname +"' and idproject=projectid group by threadfocus";
        	rs_thread = opdb_thread.MulGetRecordsFromDB(tablearray,"threadfocus",strcon,2);
        	int cr_thread = 0;
        	String rs_thread_nm = null;
        	String notecount = null;
        	String authorcount = null;
        	String fromdate = null;
        	String todate = null;
        	
        	while (rs_thread.next()){
        		
        		rs_thread_nm = rs_thread.getString(1);
        		
        		Statement stmt1 = sobj.Connect(database);
                String strsql_noteid ="select  count(distinct thread_note.noteid) from  thread_note inner join  note_table on  thread_note.noteid =  note_table.noteid inner join  author_note on  thread_note.noteid =  author_note.noteid inner join  author_table on  author_note.authorid =  author_table.authorid and  thread_note.threadfocus='"+rs_thread_nm +"'";
                countnoteid= stmt1.executeQuery(strsql_noteid);
                while(countnoteid.next())
                {
                	notecount = countnoteid.getString(1);
                	System.out.println("notecount"+notecount);
                }
                stmt1.close();
                if(countnoteid != null)
                {
                	try{
                		countnoteid.close();
                	}catch(SQLException e){
                		e.printStackTrace();
                	}
                }
                
                Statement stmt2 = sobj.Connect(database);
                String strsql_author ="select count(distinct authorid) from  thread_note inner join  author_note on  thread_note.noteid =  author_note.noteid and  thread_note.threadfocus='"+rs_thread_nm+"'";
                countauthors = stmt2.executeQuery(strsql_author);
                while(countauthors.next())
                {
                	authorcount = countauthors.getString(1);
                	System.out.println("authorcount"+authorcount);
                }
                stmt2.close();
                if(countauthors != null)
                {
                	try{
                		countauthors.close();
                	}catch(SQLException e){
                		e.printStackTrace();
                	}
                }
                
                String strarr1[] = new String[2];
                String from = null;
                Statement stmt3 = sobj.Connect(database);
                String strsql_fromdate ="select min(createtime) from  note_table,thread_note  where note_table.noteid = thread_note.noteid and  thread_note.threadfocus='"+rs_thread_nm+"'";
                noteid_from_date = stmt3.executeQuery(strsql_fromdate);
                while(noteid_from_date.next())
                {
                	from = noteid_from_date.getString(1);
                	if(from != null)
                	{
                		strarr1 = from.split(" ", 2);
                	}
                	else
                	{
                		strarr1[0] = null;
                	}
                	fromdate = strarr1[0];
                	System.out.println("fromdate"+fromdate);
                }
                stmt3.close();
                if(noteid_from_date != null)
                {
                	try{
                		noteid_from_date.close();
                	}catch(SQLException e){
                		e.printStackTrace();
                	}
                }
                
                String strarr2[] = new String[2];
                String to = null;
                Statement stmt4 = sobj.Connect(database);
                String strsql_todate ="select max(createtime) from note_table, thread_note where note_table.noteid = thread_note.noteid and  thread_note.threadfocus='"+rs_thread_nm+"'";
                noteid_to_date = stmt4.executeQuery(strsql_todate);
                while(noteid_to_date.next())
                {
                	to = noteid_to_date.getString(1);
                	if(to != null)
                	{
                		strarr2 = from.split(" ", 2);
                	}
                	else
                	{
                		strarr2[0] = null;
                	}
                	todate = strarr2[0];
                	//todate = noteid_to_date.getString(1);
                	System.out.println("todate"+todate);
                }
                stmt4.close();
                if(noteid_to_date != null)
                {
                	try{
                		noteid_to_date.close();
                	}catch(SQLException e){
                		e.printStackTrace();
                	}
                }
        	%>
        		<script type ="text/javascript">
        		  UpdateProjectThreadList('<%=projectname%>','<%= rs_thread_nm %>',{"notecount" :'<%= notecount%>',"authorcount":'<%= authorcount %>',"fromdate":'<%= fromdate%>',"todate":'<%= todate%>'});
        		</script>
        	<%}
        	if(rs_thread != null){
        		try{
        			rs_thread.close();
        		}catch(SQLException e){
        			e.printStackTrace();
        		}
        	}
        	
        	%>
        	<script type ="text/javascript">
	        	var projectname = "<%= projectname %>";
	        	var teacher = "<%= teacher %>";
	        	var fromyear = "<%= fromyear %>";
	        	var toyear = "<%= toyear %>";
	        	var grade = "<%= grade %>";
	        	UpdateList(projectname,teacher,grade,fromyear,toyear); 
        	</script>
     <% 
        }
     %>
     
    </td>
  </tr>
 <tr>
   <td>2. Choose idea threads that involve productive conversations and progress.
    <div>
      <input type="button" value="Select All" onclick="checkByParent('thread_info', true);">
      <input type="button" value="Deselect All" onclick="checkByParent('thread_info', false);">
    </div>
     <div class ="thread_info" id ="thread_info" style ="width:500px;height;100px;overflow:auto"> 
    </div>
   </td>
 </tr>
 <tr>
   <td>3. Check and protect private information.</td>
 </tr>
<tr>
 <td>
	<form>
	   <input type="checkbox" id="check1">Show all userid's.
	</form>
  It is your(person who publishes this project)responsibility to further manually check all the note content to make sure there is no sensitive information
 </td>
</tr>
<tr>
  <td>4. Share teacher reflection.<a href="TeacherReflection.jsp?database=<%=database%>"> Record/view teacher reflection here</a></td>
</tr>
<tr>
</table>
<br/>
<br/>
<div id="button" style ="padding-left:300px;float:left">
<ul>
<li><a href="" onclick="" id="cancel" >Cancel</a></li>
<li><a href="" onclick="" id="save" >Save</a></li>
<li><a href="" onclick="" id="publish" >Publish</a></li>
</ul>
</div>
</div>
</div>
</div>
</body>
</html>
